﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testLoadFont : MonoBehaviour {

    private Text text;
    private Font normalFont;
    private Font alterFont;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Start () {
        normalFont = text.font;
        alterFont = Resources.Load("fontTest") as Font;

        StartCoroutine(coroutine(2));
    }

    IEnumerator coroutine(float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            text.font = alterFont;
            yield return new WaitForSeconds(time);
            text.font = normalFont;
        }

    }
}
