﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(RectTransform))]
public class ObjectUIHandler : MonoBehaviour {

    public static Vector2[] DefaultPosition =
        {
        new Vector2(-50, 0),
        new Vector2(50, 0),
        new Vector2(0, 0),
        new Vector2(0, 0)
    };

    private void OnEnable()
    {
        GameMaster.OUI_Handler = this;
    }
    private void OnDisable()
    {
        if (GameMaster.OUI_Handler == this)
            GameMaster.OUI_Handler = null;
    }
}
