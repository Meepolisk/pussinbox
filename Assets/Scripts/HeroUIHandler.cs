﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(RectTransform))]
public class HeroUIHandler : MonoBehaviour {

    public static Vector2[] DefaultPosition =
        {
        new Vector2(0, 0),
        new Vector2(0, 0),
        new Vector2(0, 0),
        new Vector2(0, 0)
    };
    
    private void OnEnable()
    {
        GameMaster.HUI_Handler = this;
    }
    private void OnDisable()
    {
        if (GameMaster.HUI_Handler == this)
            GameMaster.HUI_Handler = null;
    }
}
