﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CameraSystem;

public class EgyptTest : MapScript
{
    public RFlag firstMovement;
    public FirstCapturePoint firstCapturePoint;
    public SecondCapturePoint secondCapturePoint;
    [Header("=======END GAME===========")]
    public Platform platformEnd;
    public RFlag endMovement;
    
    private void Start()
    {
        StartCoroutine(orderMoveFirst());
        firstCapturePoint.Setup(FinishCapA);
    }

    void FinishCapA()
    {
        secondCapturePoint.Setup(baseWinGame);
    }

    IEnumerator orderMoveFirst()
    {
        yield return new WaitForSeconds(2f);
        Player.number[1].hero.movement.auto.create(firstMovement.center);
    }

    protected override void WinGame()
    {
        base.WinGame();

        platformEnd.gameObject.SetActive(false);
        Player.number[1].hero.movement.auto.create(endMovement.center);
        //orderMoveEnd();
    }
    IEnumerator orderMoveEnd()
    {
        yield return new WaitForSeconds(1f);
        Player.number[1].hero.movement.auto.create(endMovement.center);
    }

    [Serializable]
    public class FirstCapturePoint
    {
        public List<Platform> platforms;
        public UnitGroupScript enemyCheck;
        public Egypt_CP_Construction capturePoint;
        public CameraZone zoneLock;
        public CameraZone zoneNext;

        Action finishAction;

        public void Setup(Action _finishAction)
        {
            enemyCheck.setup(activatePoint);
            finishAction = _finishAction;
        }
        void activatePoint()
        {
            capturePoint.Activate(lockZoneB, unlockZoneC);
        }
        void lockZoneB()
        {
            CameraZone.ChangeRegion(zoneLock);
            platforms[0].gameObject.SetActive(true);
            platforms[1].gameObject.SetActive(true);
        }
        void unlockZoneC()
        {
            CameraZone.ChangeRegion(zoneNext);
            platforms[0].gameObject.SetActive(true);
            platforms[1].gameObject.SetActive(false);
            finishAction();
        }
    }
    [Serializable]
    public class SecondCapturePoint
    {
        public List<Platform> platforms;
        public Egypt_CP_Construction capturePoint;
        public CameraZone zoneLock;

        public void Setup(Action _wingameAction)
        {
            capturePoint.Activate(lockZoneD, _wingameAction);
        }
        void lockZoneD()
        {
            CameraZone.ChangeRegion(zoneLock);
            platforms[0].gameObject.SetActive(true);
        }
    }

}
