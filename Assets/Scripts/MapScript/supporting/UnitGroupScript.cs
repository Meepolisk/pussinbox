﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnitGroupScript
{
    public List<Unit> units = new List<Unit>();
    private Action action;

    public void setup(Action _action)
    {
        action = _action;
        rEvent.Unit_Die += REvent_Unit_Die;
    }

    private void REvent_Unit_Die(Unit _unitA, Unit _unitB)
    {
        if (units.Contains(_unitA))
        {
            units.Remove(_unitA);
            if (units.Count <= 0)
                Trigger();
        }
    }
    private void Trigger()
    {
        action();
        rEvent.Unit_Die -= REvent_Unit_Die;
    }
}