﻿using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EvelatorScene : MapScript
{
    public float actionDelayTime = 3f;
    public Evelator evelator;
    public Gate GateB2;
    public Gate GateB3;

    private void Start()
    {
        StartCoroutine(movingElevator(1, moveEvelatorTo2));
    }

    IEnumerator movingElevator(int _index, Action _actionNext = null)
    {
		yield return new WaitForSeconds(actionDelayTime);
        evelator.MovingToIndex(_index,_actionNext);
    }
    void moveEvelatorTo2()
    {
        evelator.sideActive = true;
        GateB2.isOpen = true;
        //StartCoroutine(movingElevator(2, moveEvelatorTo3));
    }
    void moveEvelatorTo3()
    {
        StartCoroutine(movingElevator(3, moveEvelatorTo4));
    }
    void moveEvelatorTo4()
    {
        StartCoroutine(movingElevator(4));
    }
}
