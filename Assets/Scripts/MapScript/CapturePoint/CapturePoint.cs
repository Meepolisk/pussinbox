﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Animator))]
public abstract class CapturePoint : MonoBehaviour {
    protected enum Stage { Disabled = 0, Available = 1, InProcess = 2, Decay = 3, Captured = 4, Finish = 5}

    [System.Serializable]
    public class ObjectUIProfile
    {
        public short order = 0;
    }

    public ObjectUIProfile UI;
    private OUI_CapPoint uiRef;

    [ReadOnly]
    [SerializeField]
    protected Stage _stage = Stage.Disabled;
    public int stage
    {
        get
        {
            return (int)_stage;
        }
    }

    public float processTime = 3f;
    public float decayTime = 10f;
    [ReadOnly]
    [SerializeField]
    protected float _processTick = 0f;
    public float captureTime = 60f;
    [ReadOnly]
    [SerializeField]
    protected float _captureTick = 0f;
    private bool _isCaptured = false;
    
    private List<Unit> units = new List<Unit>();
    private Action startAction;
    private Action finishAction;

    protected RegionCaller regCaller;
    protected bool active = false;
    #region Animator
    private Animator animator;
    private bool flash
    {
        set
        {
            float speed = 1f;
            if (value)
                speed = 3f;
            animator.SetFloat("FlashSpeed", speed);
            uiRef.Flashing = value;
        }
    }
    public float processTick
    {
        protected get
        {
            return _processTick;
        }
        set
        {
            _processTick = value;
            uiRef.ProcessScale = (_processTick / processTime);
        }
    }
    public float captureTick
    {
        protected get
        {
            return _captureTick;
        }
        set
        {
            _captureTick = value;
            uiRef.CaptureTime = (int)(captureTime - _captureTick);
        }
    }
    public bool Enabled
    {
        set
        {
            animator.SetBool("Enabled", value);
            uiRef.Enabled = value;
        }
    }
    public bool Captured
    {
        protected get
        {
            return _isCaptured;
        }
        set
        {
            _isCaptured = value;
            animator.SetBool("Captured", value);
            uiRef.Captured = value;
        }
    }
    #endregion

    private void Awake () {
        animator = GetComponent<Animator>();
        regCaller = GetComponentInChildren<RegionCaller>();
        regCaller.Setup(unitEnter, unitLeave);
    }
    private void Start()
    {
        StartCoroutine(InstanceUI());
    }
    IEnumerator InstanceUI()
    {
        yield return new WaitForEndOfFrame();
        uiRef = (Instantiate(Resources.Load("UI/OUI/CapturePoint"), GameMaster.OUI_Handler.transform) as GameObject).GetComponent<OUI_CapPoint>();
        uiRef.Setup(this,UI.order);
        flash = true;
    }

    private void unitEnter(Unit _unit)
    {
        if ((_unit is Hero) && _unit.player != 0)
        {
            units.Add(_unit);
            if (units.Count > 0 && 
                (_stage == Stage.Available || _stage == Stage.Decay))
            {
                changeToProcess();
            }
        }
    }
    private void unitLeave(Unit _unit)
    {
        if (units.Contains(_unit))
        {
            units.Remove(_unit);
            if (units.Count == 0  &&
                (_stage == Stage.Captured || _stage == Stage.InProcess))
            {
                changeToDecay();
            }
        }
    }
    
    protected void LateUpdate () {
        if (_stage == Stage.Disabled || _stage == Stage.Available || _stage == Stage.Finish)
            return;

        if (Captured)
        {
            captureTick += Time.deltaTime;
            if (captureTick >= captureTime)
            {
                baseFinish();
            }
        }
        if (_stage == Stage.InProcess)
        {
            processTick += Time.deltaTime;
            if (processTick >= processTime)
            {
                changeToCaptured();
            }
        }
        else if (_stage == Stage.Decay)
        {
            processTick -= Time.deltaTime * (processTime / decayTime);
            if (processTick <= 0)
            {
                changeToAvailable();
            }
        }
        update();
    }
    private void baseStart()
    {
        active = true;

        start();

        if (startAction != null)
            startAction();
    }
    private void baseFinish()
    {
        active = false;
        _stage = Stage.Finish;
        Enabled = false;
        captureTick = captureTime;
        finish();
        if (finishAction != null)
            finishAction();
    }

    protected abstract void start();
    protected abstract void finish();
    protected virtual void update() { }

    void changeToAvailable()
    {
        _stage = Stage.Available;
        Captured = false;
        processTick = 0f;
        flash = false;
    }
    void changeToProcess()
    {
        _stage = Stage.InProcess;
        flash = true;
    }
    void changeToDecay()
    {
        _stage = Stage.Decay;
        flash = true;
    }
    void changeToCaptured()
    {
        if (captureTick == 0f) //chạy lần đầu
        {
            baseStart();
        }
        processTick = processTime;
        _stage = Stage.Captured;
        Captured = true;
        flash = false;
    }

    public void Activate(Action _startAction, Action _finishAction)
    {
        startAction = _startAction;
        finishAction = _finishAction;
        Enabled = true;
        regCaller.active = true;
        captureTick = 0f;
        changeToAvailable();
    }
}
