﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Animator))]
public class Egypt_CP_Construction : CapturePoint 
{
#region template
    //protected override void start()
    //{
    //    Debug.Log("start");
    //}
    //protected override void update()
    //{
    //    Debug.Log(captureScale);
    //}
    //protected override void finish()
    //{
    //    Debug.Log("finish");
    //}
#endregion


    [Header("========================")]
    public Unit JackalPrefabRef;

    private RFlag[] dropRopeLocations;
    protected override void start()
    {
        dropRopeLocations = GetComponentsInChildren<RFlag>();
        spawnCoroutine = StartCoroutine(spawnRope());
    }
    protected override void update()
    {
        //Debug.Log("Stage: " + stage.ToString()+ "[" + captureScale + "]");
        foreach (var group in specialCase)
        {
            if (group.UpdateCheck(captureTick))
                group.Trigger(transform.position);
        }
    }
    protected override void finish()
    {
        StopCoroutine(spawnCoroutine);
    }

    Coroutine spawnCoroutine;
    IEnumerator spawnRope()
    {
        float nextSpawnTime;
        float _case = UnityEngine.Random.Range(0f, 100f);
        if (_case < 40f)
        {
            generateSpawning(1); nextSpawnTime = 10f;
        }
        else if (_case < 80f)
        {
            generateSpawning(2); nextSpawnTime = 12f;
        }
        else
        {
            generateSpawning(3); nextSpawnTime = 15f;
        }

        if (active)
        {
            yield return new WaitForSeconds(nextSpawnTime);
            spawnCoroutine = StartCoroutine(spawnRope());
        }
    }

    private void generateSpawning (short number)
    {
        //HashSet<int> unspawnedIndex = new HashSet<int>();
        HashSet<int> spawnedIndex = new HashSet<int>();
        while (spawnedIndex.Count < number)
        {
            if (spawnedIndex.Count == dropRopeLocations.Length)
                break; //để chống lỗi

            int pick = UnityEngine.Random.Range(0, dropRopeLocations.Length);
            if (spawnedIndex.Contains(pick))
                continue;

            //spawn lính code:
            spawnOne(dropRopeLocations[pick].center);
            spawnedIndex.Add(pick);
        }
    }
    private void spawnOne(Vector2 _pos)
    {
        GameObject newGO = Instantiate(JackalPrefabRef.gameObject);
        Unit unit = newGO.GetComponent<Unit>();
        if (unit)
        {
            unit.center = _pos;
            if (unit.center.x > regCaller.center.x)
            {
                unit.modelControler.flipOnStart = true;
            }

            AI_Jackal jackAI = unit.GetComponentInChildren<AI_Jackal>();
            if (jackAI)
            {
                jackAI.info.stage = RAI.Stage.action;
                jackAI.ropeIn = true;
                //jackAI.orders.Order(new OrderMoveToPoint(regCaller.center));
                jackAI.sightInfo.maxRangeCanSee = 30f;
                jackAI.info.focusUnit = Player.number[1].hero;
                jackAI.info.searchTime = 10f;
            }
        }
    }

    #region special case
    [System.Serializable]
    public class SpecialCase
    {
        public float time;
        public List<Unit> units = new List<Unit>();
        [ReadOnly]
        public bool triggered = false;

        public bool UpdateCheck(float _time)
        {
            if (!triggered && _time >= time)
            {
                triggered = true;
                return true;
            }
            return false;
        }
        public void Trigger(Vector2 pos)
        {
            foreach (var unit in units)
            {
                unit.gameObject.SetActive(true);
                unit.movement.auto.create(pos);
            }
        }
    }
    public List<SpecialCase> specialCase;
    
#endregion
}
