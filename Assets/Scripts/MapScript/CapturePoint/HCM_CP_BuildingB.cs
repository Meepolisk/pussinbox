﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class HCM_CP_BuildingB : CapturePoint 
{
#region template
    //protected override void start()
    //{
    //    Debug.Log("start");
    //}
    //protected override void update()
    //{
    //    Debug.Log(captureScale);
    //}
    //protected override void finish()
    //{
    //    Debug.Log("finish");
    //}
#endregion


    [Header("========================")]
    public Unit TroopPrefabs;
    public float timeScale = 5;

    private RFlag[] spawnLocations;
    protected override void start()
    {
        spawnLocations = GetComponentsInChildren<RFlag>();
        spawnCoroutine = StartCoroutine(spawnCreep());
    }
    protected override void update()
    {
        foreach (var group in specialCase)
        {
            if (group.UpdateCheck(captureTick))
                group.Trigger(transform.position);
        }
    }
    protected override void finish()
    {
        StopCoroutine(spawnCoroutine);
    }

    Coroutine spawnCoroutine;
    IEnumerator spawnCreep()
    {
        if (Captured)
        {
            generateSpawning();
        }
        if (active)
        {
            yield return new WaitForSeconds(timeScale);
            spawnCoroutine = StartCoroutine(spawnCreep());
        }
    }

    private void generateSpawning ()
    {
        HashSet<int> spawnedIndex = new HashSet<int>();
        while (spawnedIndex.Count < 2)
        {
            if (spawnedIndex.Count == spawnLocations.Length)
                break; //để chống lỗi

            int pick = UnityEngine.Random.Range(0, spawnLocations.Length);
            if (spawnedIndex.Contains(pick))
                continue;

            //spawn lính code:
            spawnOne(spawnLocations[pick].center);
            spawnedIndex.Add(pick);
        }
    }
    private void spawnOne(Vector2 _pos)
    {
        GameObject newGO = Instantiate(TroopPrefabs.gameObject);
        Unit unit = newGO.GetComponent<Unit>();
        if (unit)
        {
            unit.center = _pos;
            if (unit.center.x > regCaller.center.x)
            {
                unit.modelControler.flipOnStart = true;
            }
            Vector2 targetPoint = new Vector2(transform.position.x, newGO.transform.position.y);
            unit.orders.Order(new OrderMoveToPoint(targetPoint));
            AI_Jackal rai = unit.GetComponentInChildren<AI_Jackal>();
            if (rai)
            {
                rai.ropeIn = true;
                rai.canSurprise = false;
            }
        }
    }

    #region special case
    [System.Serializable]
    public class SpecialCase
    {
        public float time;
        public List<Unit> units = new List<Unit>();
        [ReadOnly]
        public bool triggered = false;

        public bool UpdateCheck(float _time)
        {
            if (!triggered && _time >= time)
            {
                triggered = true;
                return true;
            }
            return false;
        }
        public void Trigger(Vector2 pos)
        {
            foreach (var unit in units)
            {
                unit.gameObject.SetActive(true);
                unit.movement.auto.create(pos);
            }
        }
    }
    public List<SpecialCase> specialCase;
    
#endregion
}
