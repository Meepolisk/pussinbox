﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(SystemObjectHandler))]
public class MapScript : MonoBehaviour {
    public AudioClip[] loseSound;
    public AudioClip[] winSound;

    private void Awake()
    {
        rEvent.Unit_Die += REvent_GremlinDie;
    }
    private void OnDisable()
    {
        rEvent.Unit_Die -= REvent_GremlinDie;
    }

    protected bool gameActive = true;

    private void REvent_GremlinDie(Unit _unitKilled, Unit _unitB)
    {
        if (_unitKilled.player != 0)
            StartCoroutine(CheckHero());
    }
    IEnumerator CheckHero()
    {
        yield return new WaitForSeconds(0.2f);
        if (Player.number[1].hero == null)
        {
            LoseGame();
        }
    }
    protected void baseLoseGame()
    {
        if (!gameActive) return;

        LoseGame();
        gameActive = false;
    }
    protected void baseWinGame()
    {
        if (!gameActive) return;

        WinGame();
        gameActive = false;
    }
    protected virtual void LoseGame()
    {
        MusicHandler.Stop();
        GameMaster.PlayRandomSound(loseSound);
        StartCoroutine(resetGame());
    }
    
    protected virtual void WinGame()
    {
        MusicHandler.Stop();
        GameMaster.PlayRandomSound(winSound);

        Hero[] heroes = FindObjectsOfType<Hero>();
        foreach( var hr in heroes)
        {
            hr.canControl = false;
        }

        StartCoroutine(resetGame());
    }

    IEnumerator resetGame()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
