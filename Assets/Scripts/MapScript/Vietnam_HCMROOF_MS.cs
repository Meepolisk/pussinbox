﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CameraSystem;

public class Vietnam_HCMROOF_MS : MapScript
{
    public RFlag firstMovement;
    public FirstCapturePoint firstCapturePoint;
    public SecondCapturePoint secondCapturePoint;
    [Header("=======END GAME===========")]
    public GameObject GateEndGameObject;
    public Platform platformEnd;
    public RFlag endMovement;

    public RegionCaller endGameRegion;
    private void unitEnter(Unit _unit)
    {
        if ((_unit is Hero) && _unit.player != 0)
        {
            baseWinGame();
        }
    }

    private void Start()
    {
        StartCoroutine(orderMoveFirst());
        firstCapturePoint.Setup(FinishCapA);
        endGameRegion.Setup(unitEnter, null);
        endGameRegion.active = true;
    }

    void FinishCapA()
    {
        secondCapturePoint.Setup(OpenGateToWinGame);
    }
    
    IEnumerator orderMoveFirst()
    {
        yield return new WaitForSeconds(1f);
        Player.number[1].hero.movement.auto.create(firstMovement.center);
    }

    void OpenGateToWinGame()
    {
        platformEnd.gameObject.SetActive(false);
        GateEndGameObject.GetComponent<Animator>().SetTrigger("Open");
        endGameRegion.active = true;
    }

    [Serializable]
    public class FirstCapturePoint
    {
        public UnitGroupScript enemyCheck;
        public CapturePoint capturePoint;
        public CameraZone zoneLock;
        public CameraZone zoneNext;

        Action finishAction;

        public void Setup(Action _finishAction)
        {
            enemyCheck.setup(activatePoint);
            finishAction = _finishAction;
        }
        void activatePoint()
        {
            capturePoint.Activate(lockZoneB, unlockZoneC);
        }
        void lockZoneB()
        {
            if (zoneLock != null)
                CameraZone.ChangeRegion(zoneLock);
        }
        void unlockZoneC()
        {
            if (zoneNext != null)
                CameraZone.ChangeRegion(zoneNext);
            finishAction();
            //Dĩa thắng AB - 30 cái, wave S110- 20 cái
        }
    }
    [Serializable]
    public class SecondCapturePoint
    {
        public CapturePoint capturePoint;
        public CameraZone zoneLock;

        public void Setup(Action _wingameAction)
        {
            capturePoint.Activate(lockZoneD, _wingameAction);
        }
        void lockZoneD()
        {
            CameraZone.ChangeRegion(zoneLock);
        }
    }

}
