﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDrone2_GunController : MonoBehaviour {

    public float targetValue = 0f;
    [ReadOnly]
    public float currentValue = 0f;
    [Tooltip("Leave 0 to insta-lock")]
    public float valuePerSec = 20f;
    public float min = -10f, max = 40f;
    public string animatorParameter = "GunRotation";

    private ModelHandler model;

    private void Start()
    {
        RObject rObject = GetComponentInParent<RObject>();
        model = rObject.model;

        currentValue = targetValue;
        rotating();
    }

    // Update is called once per frame
    void Update () {
        if (targetValue == currentValue) return;
        rotating();

	}
    void rotating()
    {
        float _RealTargetValue = Mathf.Clamp(targetValue, min, max);

        //if (degreePerSec <= 0f)
        //{
        //    currentValue = targetValue;
        //}


        if (currentValue > _RealTargetValue)
        {
            currentValue = Mathf.Clamp(currentValue - (valuePerSec * Time.deltaTime), _RealTargetValue, currentValue);
        }
        else
        {
            currentValue = Mathf.Clamp(currentValue + (valuePerSec * Time.deltaTime), currentValue, _RealTargetValue);
        }
        float result = (currentValue - min) / (max - min);
        model.animation.SetFloat(animatorParameter, result);
    }
}
