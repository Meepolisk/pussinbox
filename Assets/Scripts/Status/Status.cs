﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Status : MonoBehaviour {


    public SpecialEffect seProfile;

    public string statusName = "unamed status";
    public float _duration = 0f;
    public float duration
    {
        set
        {
            _duration = Mathf.Clamp(value, 0f, float.MaxValue);
        }
        get
        {
            return _duration;
        }
    }
    protected float remainingTime = 0f;

    protected Unit unit;
    
    [ReadOnly]
    public SpecialEffect specialEffect;
    
    private void Awake()
    {
        this.gameObject.name = "[" + this.GetType().Name + "] " + this.statusName;
        unit = GetComponentInParent<Unit>();
        if (duration > 0f)
            remainingTime = _duration;
    }
    private void OnEnable()
    {
        Debug.Log(this.gameObject.name + " status enable");
        takeEffect();

        //enable SpecialEffect
        if (seProfile == null) return;
        //specialEffect = SpecialEffect.CreateOnUnit(seProfile.prefabs, unit, seProfile.attachmentPoint, seProfile.Zangel);
    }
    protected virtual void takeEffect()
    {

    }
    private void Update()
    {
        if (duration > 0f)
        {
            remainingTime -= Time.deltaTime;
            if (remainingTime <= 0f)
                destroy();
        }
    }
    public void destroy()
    {
        Destroy(this.gameObject);
    }

    private void OnDisable()
    {
        Debug.Log(this.gameObject.name + " status disable");
        specialEffect.Destroy();
        wearOff();
    }
    protected virtual void wearOff()
    {
    }

    #region static things
    public static void Destroy(Status status)
    {
        status.destroy();
    }
    
    public static Status _lastCreatedStatus = null;

    public static void AddStatus(Unit _unit, Status _status)
    {
        Transform goTrans = findStatusContainer(_unit);

        GameObject ObjectStatus = Instantiate(_status.gameObject, Vector2.zero, Quaternion.Euler(new Vector3(0f, 0f, 0f)), goTrans);
        ObjectStatus.transform.localPosition = Vector2.zero;
        _lastCreatedStatus = _status;//.GetComponent<Status>();
    }
    

    const string statusContainerName = "Status";
    static Transform findStatusContainer(Unit unit)
    {
        try
        {
            Transform goTrans = unit.transform.Find(statusContainerName).transform;
            return goTrans;
        }
        catch
        {
            return (unit.gameObject.createEmptyChild(statusContainerName).transform);
        }
        //if (goTrans != null)
        //    return goTrans;
        //return (unit.gameObject.createEmptyChild(statusContainerName).transform);
    }
    #endregion
}
