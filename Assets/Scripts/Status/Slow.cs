﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slow : Status {

    [Range(0.1f, 3f)]
    public float _movementSpeedScale = 0.6f;
    //[Range(0.1f, 3f)]
    //float _attackSpeedScaleBonus = 0.6f;
    private const string BonusCode = "StatusSlow";

    public float movementSpeedScale
    {
        set
        {
            _movementSpeedScale = Mathf.Clamp(value, 0.1f, 3f);
        }
        get
        {
            return _movementSpeedScale;
        }
    }


    protected override void takeEffect()
    {
        unit.movement.movementSpeed.addMultis(_movementSpeedScale,BonusCode);
    }
    protected override void wearOff()
    {
        unit.movement.movementSpeed.removeMultis(BonusCode);
    }
}
