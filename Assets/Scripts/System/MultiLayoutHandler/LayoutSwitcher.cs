﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using System.Linq;
using UnityObject = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor;
#endif
namespace DiroxUtils
{
    public enum TransformLayoutChangeType
    {
        NOPE, LOCAL //,LOSSY
    }

    [ExecuteInEditMode]
    public class LayoutSwitcher : MonoBehaviour
    {
        public enum Layout
        {
            LANDSCAPE,
            PORTRAIT
        }
#if UNITY_EDITOR
        private static bool _defaultLayout = false;

        private void Restore()
        {
            Switch();
        }

        private void OnEnable()
        {
            Restore();
            if (Application.isPlaying)
            {
                Debug.Log("OnEnable Play", gameObject);
            }
            else
            {
                //CheckToAddList();

                //Debug.Log("OnEnable UNITY", gameObject);
                //reinit currentLayer
                if (IsLandScape == _defaultLayout)
                    return;

                Debug.Log("LAYOUTSWITCHER RE-INITILAZED", gameObject);
                IsLandScape = (currentLayout == Layout.LANDSCAPE) ? true : false;
            }
        }

        private void Update()
        {
            //Debug.Log(_defaultLayout);
            if (Application.isPlaying)
                return;

            StoreValue();
        }
        public void StoreValue()
        {
            //if (componentRefHandler == null || componentRefHandler.parent == null)
            //{
            //    Debug.Log("Refresh the ComponentRef");
            //    componentRefHandler = new ComponentRefHandler(this);
            //    componentRefHandler.Refresh();
            //}
            componentRefHandler.Store();
            if (transformCopier != null && transform.hasChanged)
            {
                transformCopier.Store(transform);
            }
        }

        public Layout CurrentLayout
        {
            get
            {
                return (_isLandScape ? Layout.LANDSCAPE : Layout.PORTRAIT);
            }
            set
            {
                switch (value)
                {
                    case Layout.LANDSCAPE:
                        IsLandScape = true;
                        currentLayout = Layout.LANDSCAPE;
                        break;
                    case Layout.PORTRAIT:
                        IsLandScape = false;
                        currentLayout = Layout.PORTRAIT;
                        break;
                }
            }
        }
#endif
        [SerializeField]
        public Layout currentLayout = Layout.PORTRAIT;

        [SerializeField]
        internal ComponentRefHandler componentRefHandler;

        [SerializeField]
        internal TransformCopier transformCopier = null;

        #region depth level check
        private int _depthLevel = 0;
        public int DepthLevel
        {
            get
            {
                return _depthLevel;
            }
        }
        public void GetParentIndex()
        {
            _depthLevel = 0;
            Transform checkingTransform = transform.parent;
            while (checkingTransform != null)
            {
                _depthLevel++;
                checkingTransform = checkingTransform.parent;
            }
        }
        #endregion

        private void ApplyCurrentLayout()
        {
            currentLayout = IsLandScape ? Layout.LANDSCAPE : Layout.PORTRAIT;
        }

        public void Reset()
        {
            Debug.Log("Reset LayoutSwitcher", gameObject);
            //CheckToAddList();

            componentRefHandler = new ComponentRefHandler(this);
            transformCopier = new TransformCopier(transform);

            ApplyCurrentLayout();
        }

        public void Switch()
        {
            ApplyCurrentLayout();

            transformCopier.Switch(transform);
            componentRefHandler.Apply();

            SwitcherAddOn[] switcherAddOns = GetComponents<SwitcherAddOn>();
            foreach (var item in switcherAddOns)
            {
                item.Switch(_isLandScape);
            }
        }

        #region static stuff
        private static bool _isLandScape = false;

        public static bool IsLandScape
        {
            get
            {
                return _isLandScape;
            }
            set
            {
                if (_isLandScape == value)
                    return;

                _isLandScape = value;
                if (_isLandScape)
                {
                    Screen.orientation = ScreenOrientation.Landscape;
                    //Debug.Log("Layout switched to landscape");
                }
                else
                {
                    Screen.orientation = ScreenOrientation.Portrait;
                    //Debug.Log("Layout switched to portrait");
                }
                StartEvent_OnSwitch(_isLandScape);
#if UNITY_EDITOR
                //store the default value
                if (!Application.isPlaying)
                {
                    _defaultLayout = _isLandScape;
                    //Debug.Log("=======================================Default set to " + _defaultLayout);
                }
#endif
                Activate();
            }
        }

        private static void Activate()
        {
            LayoutSwitcher[] unSorted = GameObject.FindObjectsOfType<LayoutSwitcher>();
            List<LayoutSwitcher> list = new List<LayoutSwitcher>();
            foreach (var switcher in unSorted)
            {
                switcher.GetParentIndex();
                list.Add(switcher);
            }
            list.Sort((x, y) => x.DepthLevel.CompareTo(y.DepthLevel));

            foreach (var switcher in list)
            {
                switcher.Switch();
                //Debug.Log("SWITCHHHHHH " + switcher.DepthLevel, switcher.gameObject);
            }
        }

        #region event
        public static Action<bool> onLayoutSwitch;

        private static void StartEvent_OnSwitch(bool _isLandscape)
        {
            if (onLayoutSwitch != null)
            {
                onLayoutSwitch(_isLandscape);
            }
        }
        #endregion

        ////this function will run when project finish recompile
        //[UnityEditor.Callbacks.DidReloadScripts]
        //private static void OnScriptsReloaded()
        //{
        //    Debug.Log("Refreshing all LayoutSwitcher");
        //    LayoutSwitcher[] allSwitcher = Resources.FindObjectsOfTypeAll<LayoutSwitcher>() as LayoutSwitcher[];

        //    foreach (var switcher in allSwitcher)
        //    {
        //        switcher.CheckToAddList();
        //    }
        //}

#if UNITY_EDITOR
        [MenuItem("Dirox/Switch Layout/Landscape %&L")]
#endif
        public static void SwitchToLandscape()
        {
            IsLandScape = true;
        }

#if UNITY_EDITOR
        [MenuItem("Dirox/Switch Layout/Portrait %&K")]
#endif
        public static void SwitchToLPortrait()
        {
            IsLandScape = false;
        }
        #endregion
    }

    #region EDITOR
#if UNITY_EDITOR
    [CustomEditor(typeof(LayoutSwitcher))]
    public class LayerSwitcherBaseCustomInspector : Editor
    {
        private LayoutSwitcher main;

        public void OnEnable()
        {
            main = target as LayoutSwitcher;

            if (Application.isPlaying)
                return;

            main.currentLayout = main.CurrentLayout;
        }

        private void OnSceneGUI()
        {
            if (Application.isPlaying)
                return;

            RefreshComponentRef();
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            main.CurrentLayout = main.currentLayout;

            if (Application.isPlaying)
                return;
            RefreshComponentRef();
            main.StoreValue();
        }

        private void RefreshComponentRef()
        {
            main.componentRefHandler.Refresh();
        }
    }
#endif
    #endregion

    #region class SwitcherAddOn

    [RequireComponent(typeof(LayoutSwitcher))]
    public abstract class SwitcherAddOn : MonoBehaviour
    {
        public abstract void Switch(bool _isLandScape);
    }

    public abstract class SwitcherAddOn<T> : SwitcherAddOn where T : Component
    {
        [SerializeField]
        protected T component;

        internal void AutoRefreshComponent()
        {
            component = GetComponent<T>();
        }
        internal void Reset()
        {
            AutoRefreshComponent();
        }
    }

    //#if UNITY_EDITOR
    //    [CustomEditor(typeof(SwitcherAddOn))]
    //    public class LayerSwitcherBaseCustomInspector : Editor
    //    {
    //        private LayoutSwitcher main;

    //        public void OnEnable()
    //        {
    //            main = target as LayoutSwitcher;

    //            if (Application.isPlaying)
    //                return;

    //            main.currentLayout = main.CurrentLayout;
    //        }

    //        private void OnSceneGUI()
    //        {
    //            if (Application.isPlaying)
    //                return;

    //            RefreshComponentRef();
    //        }

    //        public override void OnInspectorGUI()
    //        {
    //            base.OnInspectorGUI();
    //            main.CurrentLayout = main.currentLayout;

    //            if (Application.isPlaying)
    //                return;
    //            RefreshComponentRef();
    //        }

    //        private void RefreshComponentRef()
    //        {
    //            main.componentRefHandler.Refresh();
    //        }
    //    }
    //#endif
    #endregion

    #region Class Transform Copier
    [System.Serializable]
    internal class TransformCopier
    {
        [System.Serializable]
        private class TransformData
        {
            public Vector3 storedPos;
            public Vector3 storedRotation;
            public Vector3 storedScale;

            public TransformData(Transform transform)
            {
                Get(transform);
            }

            public void Get(Transform transform)
            {
                storedPos = transform.localPosition;
                storedRotation = transform.localEulerAngles;
                storedScale = transform.localScale;

                //storedPos = transform.position;
                //storedRotation = transform.eulerAngles;
                //storedScale = transform.lossyScale;
            }
            public void Set(Transform transform)
            {
                transform.localPosition = storedPos;
                transform.localEulerAngles = storedRotation;
                transform.localScale = storedScale;

                //transform.position = storedPos;
                //transform.eulerAngles = storedRotation;
                //SetGlobalScale(transform, storedScale);
            }
            private static void SetGlobalScale(Transform transform, Vector3 value)
            {
                transform.localScale = Vector3.one;
                transform.localScale = new Vector3(value.x / transform.lossyScale.x, value.y / transform.lossyScale.y, value.z / transform.lossyScale.z);
            }
        }

        public TransformLayoutChangeType ChangeType = TransformLayoutChangeType.LOCAL;
        public bool IgnoreChildren = false;

        [ReadOnly]
        [SerializeField]
        private TransformData _landScapeData;

        [ReadOnly]
        [SerializeField]
        private TransformData _portraitData;

        public TransformCopier(Transform transform)
        {
            _landScapeData = new TransformData(transform);
            _portraitData = new TransformData(transform);
        }
        //~TransformCopier()
        //{
        //    Debug.Log("TransformCopier destructor");
        //}

        public void Store(Transform transform)
        {
            if (ChangeType == TransformLayoutChangeType.NOPE)
                return;

            if (LayoutSwitcher.IsLandScape)
            {
                //Debug.Log("Apply landscape", transform.gameObject);
                _landScapeData.Get(transform);
            }
            else
            {
                //Debug.Log("Apply portrait", transform.gameObject);
                _portraitData.Get(transform);
            }
        }

        public void Switch(Transform transform)
        {
            if (ChangeType == TransformLayoutChangeType.NOPE)
                return;

            List<Transform> allChild = null;
            if (IgnoreChildren && transform.childCount > 0)
            {
                allChild = new List<Transform>();
                Transform childCheck = transform.GetChild(0);
                while (transform.childCount > 0)
                {
                    childCheck = transform.GetChild(0);
                    allChild.Add(childCheck);
                    childCheck.SetParent(null);
                }
            }

            if (LayoutSwitcher.IsLandScape)
            {
                _landScapeData.Set(transform);
            }
            else
            {
                _portraitData.Set(transform);
            }

            if (allChild != null)
            {
                foreach (var item in allChild)
                {
                    item.SetParent(transform);
                }
            }
        }
    }
    #endregion


    #region class ComponentRef
    public abstract class StoredValue
    {
        [SerializeField]
        private bool isActive;
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                if (isActive == value)
                    return;

                isActive = value;
            }
        }

        public abstract object Data
        {
            get; set;
        }

        public static StoredValue Create (FieldInfo field,Component component)
        {
            Type type = field.FieldType;

            if (type.GetType() == typeof(int))
                return new StoredValue<int>(field, component);
            else if (type.GetType() == typeof(float))
                return new StoredValue<float>(field, component);
            else if (type.GetType() == typeof(string))
                return new StoredValue<string>(field, component);
            else if (type.GetType() == typeof(bool))
                return new StoredValue<bool>(field, component);
            else if (type.GetType() == typeof(Vector2))
                return new StoredValue<Vector2>(field, component);
            else if (type.GetType() == typeof(Vector3))
                return new StoredValue<Vector3>(field, component);
            else if (type.GetType() == typeof(Vector4))
                return new StoredValue<Vector4>(field, component);
            else if (type.GetType() == typeof(Quaternion))
                return new StoredValue<Quaternion>(field, component);
            else if (type.GetType() == typeof(Matrix4x4))
                return new StoredValue<Matrix4x4>(field, component);
            else if (type.GetType() == typeof(Color))
                return new StoredValue<Color>(field, component);
            else if (type.GetType() == typeof(Rect))
                return new StoredValue<Rect>(field, component);
            else if (type.GetType() == typeof(LayerMask))
                return new StoredValue<LayerMask>(field, component);
            

            else
                return null;
        }

#if UNITY_EDITOR
        public string Name;
#endif
    }
    [System.Serializable]
    public class StoredValue<T> : StoredValue
    {
        [SerializeField]
        public Type ActualType { private set; get; }

        public T valueLandscape;
        public T valuePortrait;

        public override object Data
        {
            set
            {
                if (LayoutSwitcher.IsLandScape)
                {
                    valueLandscape = (T)value;
                }
                else
                {
                    valuePortrait = (T)value;
                }
            }
            get
            {
                if (LayoutSwitcher.IsLandScape)
                {
                    return valueLandscape;
                }
                else
                {
                    return valuePortrait;
                }
            }
        }
        public StoredValue(FieldInfo _fieldInfo, Component component, bool _isActive = false)
        {
            IsActive = _isActive;
            ActualType = _fieldInfo.FieldType;

            valueLandscape = (T)_fieldInfo.GetValue(component);
            valuePortrait = valueLandscape;

#if UNITY_EDITOR
            Name = _fieldInfo.Name;
#endif
        }

        //public void Store()
        //{
        //    if (LayoutSwitcher.IsLandScape)
        //    {
        //        valueLandscape = fieldInfo.GetValue(component);
        //        Debug.LogWarning("Stored Landscape :" + valueLandscape);
        //    }
        //    else
        //    {
        //        valuePortrait = fieldInfo.GetValue(component);
        //        Debug.LogWarning("Stored Portrait :" + valuePortrait);
        //    }
        //}

        //public void Apply()
        //{
        //    if (LayoutSwitcher.IsLandScape)
        //    {
        //        fieldInfo.SetValue(component, valueLandscape);
        //        Debug.LogWarning("Apply Landscape :" + valueLandscape);
        //    }
        //    else
        //    {
        //        fieldInfo.SetValue(component, valuePortrait);
        //        Debug.LogWarning("Apply Portrait :" + valuePortrait);
        //    }
        //}
    }

    [System.Serializable]
    internal class ComponentRef
    {
        [SerializeField]
        private bool isActive = false;
        internal bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                if (isActive == value) return;

                isActive = value;
                if (isActive)
                {
                    Init();
                }
            }
        }
        public Component component;

        //public Dictionary<FieldInfo, FieldInfoData> fieldsDict = new Dictionary<FieldInfo, FieldInfoData>();
        public List<StoredValue> fieldsList;// = new List<FieldInfoData>();

        //public string Name = "Component";

        public ComponentRef(Component _component, bool _isActive = false)
        {
            Debug.Log("Construction of " + _component.GetType().Name);

            component = _component;
            IsActive = _isActive;
            //Name = _component.GetType().Name;
        }

        public void Init()
        {
            InitField();
            InitProperty();
        }
        private void InitField()
        {
            Debug.Log("Init for " + component.GetType().Name);

            if (fieldsList != null)
            {
                //Debug.LogWarning("Init fields for " + component.GetType().Name);
                return;
            }
            //fieldsDict = new Dictionary<FieldInfo, FieldInfoData>();
            fieldsList = new List<StoredValue>();
            //fieldsDict.Clear();

            FieldInfo[] _inputFields = component.GetType().GetFields();
            //foreach (var field in _inputFields)
            for (int i = 0; i < _inputFields.Length; i++)
            {
                //fieldsList.Add(field, new FieldInfoData(field, component));
                fieldsList.Add(StoredValue.Create(_inputFields[i], component));
                
            }
        }
        private void InitProperty()
        {

        }

        public void Store()
        {
            //foreach (var item in fieldsList)
            //{
            //    if (item.IsActive)
            //        item.Store();
            //}

            Debug.Log("Store");

            FieldInfo[] _fields = component.GetType().GetFields();
            int len = _fields.Length;
            for (int i = 0; i < len; i++)
            {
                if (fieldsList[i].IsActive)
                {
                    object value = _fields[i].GetValue(component);
                    fieldsList[i].Data = value;
                }
            }
        }

        public void Apply()
        {
            if (component == null || !isActive)
            {
                return;
            }

            if (fieldsList == null || fieldsList.Count == 0)
                return;

            Debug.Log("SwitchField");
            //foreach (var item in fieldsList)
            //{
            //    if (item.IsActive)
            //        item.Apply();
            //}

            FieldInfo[] _fields = component.GetType().GetFields();
            int len = _fields.Length;
            for (int i = 0; i <= len; i++)
            {
                if (fieldsList[i].IsActive)
                {
                    _fields[i].SetValue(component, fieldsList[i].Data);
                }
            }
        }
    }

    [System.Serializable]
    public class ComponentRefHandler
    {
        public Component parent;

        [SerializeField]
        internal List<ComponentRef> list;

        //public HashSet<ComponentRef> list = new HashSet<ComponentRef>();

        public ComponentRefHandler(Component _parent)
        {
            Debug.Log("ComponentRefHandler Constructor: " + _parent.GetType().Name);
            list = new List<ComponentRef>();
            parent = _parent;
        }
        //~ComponentRefHandler()
        //{
        //    Debug.Log("ComponentRefHandler Destructor: " + parent.GetType().Name);
        //}
        public void Store()
        {
            if (list == null || list.Count == 0)
                return;

            foreach (var item in list)
            {
                if (item.IsActive)
                    item.Store();
            }
        }

        public void Apply()
        {
            if (list == null || list.Count == 0)
                return;
            foreach (var item in list)
            {
                if (item.IsActive)
                    item.Apply();
            }
        }

#if UNITY_EDITOR
        public void Refresh(UnityEngine.Object _object)
        {
            try
            {
                parent = _object as Component;
                Refresh();
            }
            catch (Exception ex)
            {
                Debug.LogError("OH NOOOOOOOOOOOOOOOO: " + ex.ToString());
            }
        }
        public void Refresh()
        {
            if (parent == null)
                return;

            //checking change
            int oldCount = list.Count;

            Component[] allComponent = parent.GetComponents<Component>();
            List<Component> _components = new List<Component>();
            foreach (var comp in allComponent)
            {
                if (comp.GetType() == typeof(Transform) || comp.GetType() == parent.GetType() || comp is SwitcherAddOn)
                    continue;

                _components.Add(comp);
            }

            List<ComponentRef> removeList = new List<ComponentRef>();
            //check in stuff
            foreach (var item in list)
            {
                if (item.component == null)
                {
                    removeList.Add(item);
                    continue;
                }

                if (_components.Contains(item.component))
                {
                    _components.Remove(item.component);
                }
            }
            //remove
            foreach (var item in removeList)
            {
                list.Remove(item);
                Debug.Log("Refresh Remove " + item.GetType().FullName);
            }

            //finally add the result
            if (_components.Count > 0)
            {
                foreach (var comp in _components)
                {
                    Debug.Log("Refresh Add " + comp.GetType().FullName);
                    list.Add(new ComponentRef(comp));
                }
            }

            if (oldCount != list.Count)
                Debug.Log("OH ONOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");

            if (!Application.isPlaying)
                RefreshRow();
        }

        public int row = 0;
        public int additionalSpace = 0;
        public void RefreshRow()
        {
            //refresh row
            row = 0;
            additionalSpace = 0;
            if (list == null)
                return;
            foreach (var item in list)
            {
                if (item.fieldsList == null)
                {
                    Debug.Log("FieldDict " + item.GetType().Name + " == null. Re-init...");
                    item.Init();
                }
                row++;
                if (item.IsActive)
                {
                    row += item.fieldsList.Count;
                    additionalSpace++;
                }
            }
        }
#endif
    }
    #region Editor
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ComponentRefHandler))]
    public class ComponentRefHandlerInspector : PropertyDrawer
    {
        private const float boolCellWidth = 20;
        private const float kButtonWidth = 18f;
        private const float componentHeight = 17f;
        private const float additionalSpaceHeight = 3f;
        private const float typeCeelWidth = 30f;

        private ComponentRefHandler handler;
        private UnityEngine.Object target;

        //private bool _Foldout;

        private static GUIStyle redStyle, grayStyle;
        [UnityEditor.Callbacks.DidReloadScripts]
        private static void ScriptReloaded()
        {
            redStyle = new GUIStyle();
            redStyle.normal.textColor = Color.red;
            grayStyle = new GUIStyle();
            grayStyle.normal.textColor = Color.gray;
            grayStyle.clipping = TextClipping.Clip;
        }

        private void CheckInitialize(SerializedProperty property, GUIContent label)
        {
            target = property.serializedObject.targetObject;
            if (handler == null)
            {
                handler = fieldInfo.GetValue(target) as ComponentRefHandler;

                //_Foldout = EditorPrefs.GetBool(label.text);
            }

            //Debug.Log("Refresh from Inspector");
            handler.Refresh(target);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            CheckInitialize(property, label);
            //if (_Foldout)
            //    return (handler.list.Count + 1) * memberHeight;
            //return memberHeight;
            var result = (handler.row * componentHeight) + (handler.additionalSpace * additionalSpaceHeight);
            if (result <= componentHeight * 1.5f)
                return componentHeight * 1.5f;
            return result;
        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            CheckInitialize(property, label);

            //foldout check
            //Rect foldoutRect = new Rect(position.x, position.y, position.width, position.height);
            //foldoutRect.width -= 2 * kButtonWidth;
            //EditorGUI.BeginChangeCheck();
            //_Foldout = EditorGUI.Foldout(foldoutRect, _Foldout, label, true);
            //if (EditorGUI.EndChangeCheck())
            //    EditorPrefs.SetBool(label.text, _Foldout);

            //if (!_Foldout)
            //    return;



            //try
            //{
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            //position.y += componentHeight;
            //EditorGUI.BeginProperty(position, label, property);
            // Don't make child fields be indented
            //var indent = EditorGUI.indentLevel;
            //EditorGUI.indentLevel = 0;


            Color defaultColor = GUI.color;
            if (handler.list.Count == 0)
            {
                Rect rect = new Rect(position.x, position.y, position.width, position.height);
                GUI.color = Color.red;
                GUI.Label(rect, " Couldnt find any other component!");
                GUI.color = defaultColor;
            }
            foreach (ComponentRef componentRef in handler.list)
            {
                //Calculate rects & draw field
                Rect comp_activeRect = new Rect(position.x, position.y, boolCellWidth, boolCellWidth);
                bool value_isActive = componentRef.IsActive;
                //change check
                EditorGUI.BeginChangeCheck();
                value_isActive = EditorGUI.Toggle(comp_activeRect, value_isActive);
                if (EditorGUI.EndChangeCheck())
                {
                    componentRef.IsActive = value_isActive;
                    return;
                }

                Rect comp_nameRect = new Rect(position.x + boolCellWidth, position.y, position.width - boolCellWidth, position.height);
                GUI.Label(comp_nameRect, componentRef.component.GetType().Name);

                if (componentRef != null && componentRef.IsActive)
                {
                    //Drawing Field
                    foreach (var field in componentRef.fieldsList)
                    {
                        var key = field;
                        position.y += componentHeight;

                        bool fieldIsActive = field.IsActive;
                        Rect field_activeRect = new Rect(position.x + boolCellWidth, position.y, boolCellWidth, boolCellWidth);

                        EditorGUI.BeginChangeCheck();
                        fieldIsActive = EditorGUI.Toggle(field_activeRect, fieldIsActive);
                        if (EditorGUI.EndChangeCheck())
                        {
                            field.IsActive = fieldIsActive;
                            return;
                        }
                        //EditorGUI.Toggle(field_activeRect, fieldIsActive);

                        string fieldName = field.Name;
                        float itemWidth = (position.width - boolCellWidth) / 4f;
                        Rect field_nameRect = new Rect(position.x + boolCellWidth * 2, position.y, itemWidth, position.height);
                        GUI.Label(field_nameRect, fieldName);

                        ////Type fieldType = field.Value.GetType();
                        //Type fieldType = field.ActualType;
                        //string fieldTypeName = fieldType.FullName;
                        //GUI.color = Color.gray;
                        //Rect field_typeRect = new Rect(position.x + boolCellWidth * 2 + itemWidth, position.y, itemWidth, position.height);
                        //GUI.Label(field_typeRect, "(" + fieldTypeName + ")", grayStyle);
                        //GUI.color = defaultColor;

                        //if (!fieldIsActive)
                        //    continue;

                        //var valueLandscape = field.valueLandscape;
                        //Rect field_valueRect_LandScape = new Rect(position.x + boolCellWidth * 2 + itemWidth * 2, position.y, itemWidth, componentHeight);
                        //GUI.enabled = false;
                        //valueLandscape = DoField(field_valueRect_LandScape, fieldType, valueLandscape);
                        //GUI.enabled = true;

                        //var valuePortrait = field.valuePortrait;
                        //Rect field_valueRect_Portrait = new Rect(position.x + boolCellWidth * 2 + itemWidth * 3, position.y, itemWidth, componentHeight);
                        //GUI.enabled = false;
                        //valuePortrait = DoField(field_valueRect_Portrait, fieldType, valuePortrait);
                        //GUI.enabled = true;

                        //var value = field.Key.GetValue(componentRef.component);
                        //Rect field_valueRect = new Rect(position.x + boolCellWidth * 2 + itemWidth * 2, position.y, itemWidth, componentHeight);
                        //GUI.enabled = false;
                        //value = DoField(field_valueRect, fieldType, value);
                        //GUI.enabled = true;
                    }

                    position.y += additionalSpaceHeight;
                }
                position.y += componentHeight;
            }

            // Set indent back to what it was
            //EditorGUI.indentLevel = indent;

            //EditorGUI.EndProperty();
            //}
            //catch
            //{
            //    Debug.LogWarning(handler.GetType().Name + " bugged! Refreshing...");
            //    handler.Refresh(target);
            //}
        }

        private static readonly Dictionary<Type, Func<Rect, object, object>> _Fields =
            new Dictionary<Type, Func<Rect, object, object>>()
            {
            { typeof(int), (rect, value) => EditorGUI.IntField(rect, (int)value) },
            { typeof(float), (rect, value) => EditorGUI.FloatField(rect, (float)value) },
            { typeof(string), (rect, value) => EditorGUI.TextField(rect, (string)value) },
            { typeof(bool), (rect, value) => EditorGUI.Toggle(rect, (bool)value) },
            { typeof(Vector2), (rect, value) => EditorGUI.Vector2Field(rect, GUIContent.none, (Vector2)value) },
            { typeof(Vector3), (rect, value) => EditorGUI.Vector3Field(rect, GUIContent.none, (Vector3)value) },
            { typeof(Bounds), (rect, value) => EditorGUI.BoundsField(rect, (Bounds)value) },
            { typeof(Rect), (rect, value) => EditorGUI.RectField(rect, (Rect)value) }
            };

        private static T DoField<T>(Rect rect, Type type, T value)
        {
            Func<Rect, object, object> field;
            if (_Fields.TryGetValue(type, out field))
                return (T)field(rect, value);

            if (type.IsEnum)
                return (T)(object)EditorGUI.EnumPopup(rect, (Enum)(object)value);

            if (typeof(UnityObject).IsAssignableFrom(type))
                return (T)(object)EditorGUI.ObjectField(rect, (UnityObject)(object)value, type, true);

            GUI.Label(rect, "Unsupported value", redStyle);
            return value;
        }
    }
#endif
    #endregion
    #endregion


    public static partial class ExtensionMethods
    {
        private static readonly List<Type> serializableTypeList = new List<Type>
        {
            typeof(int),
            typeof(float),
            typeof(bool),
            typeof(double),
            typeof(string)
        };

        public static bool CanBeSerialized(this FieldInfo fieldInfo)
        {
            Type type = fieldInfo.FieldType;
            return type.CanBeSerialized();
        }

        public static bool CanBeSerialized(this Type type)
        {
            if (serializableTypeList.Contains(type))
                return true;
            if (type.IsEnum)
                return true;
            if (typeof(UnityObject).IsAssignableFrom(type))
                return true;

            return false;
        }
    }
}

