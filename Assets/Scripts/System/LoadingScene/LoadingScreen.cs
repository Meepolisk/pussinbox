﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class LoadingScreen : MonoBehaviour {

    public string loadSceneName;

    Animator animator;
    Text text;

    public void Setup(string _sceneName)
    {
        loadSceneName = _sceneName;
    }
    private void Awake()
    {
        animator = GetComponent<Animator>();
        text = GetComponentInChildren<Text>();
    }
    private void Start()
    {
        if (!string.IsNullOrEmpty(loadSceneName))
        {
            StartCoroutine(LoadAsyncMap());
        }
    }

    IEnumerator LoadAsyncMap()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(loadSceneName);
        operation.allowSceneActivation = false;
        while (!operation.isDone)
        {
            float process = Mathf.Clamp01(operation.progress / 0.9f);
            AnimatorUpdate(process);
            if (process >= 1f && Input.anyKeyDown)
                operation.allowSceneActivation = true;
            yield return null;
        }
    }

    void AnimatorUpdate(float _scale)
    {
        animator.SetFloat("loadingScale",_scale);
        text.text = ((int)(_scale * 100f)).ToString() + "%";
    }
}
