﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//#if UNITY_EDITOR
//using UnityEditor;
//#endif

//[ExecuteInEditMode]
//public class RColliderHandler : MonoBehaviour
//#if UNITY_EDITOR
//    , hideTransform, lockTransform
//#endif 
//{
//    [ReadOnly]
//    public bool editMode = false;
//    [HideInInspector]
//    public bool createGround = false;
//    [HideInInspector]
//    public Vector3 vec1;
//    [HideInInspector]
//    public Vector3 vec2;

//    public Vector3 getCenter
//    {
//        get
//        {
//            return ((vec2 + vec1) / 2);
//        }
//    }
//    public Vector3 getSize
//    {
//        get
//        {
//            return new Vector3(Mathf.Abs(vec1.x - vec2.x), Mathf.Abs(vec1.y - vec2.y));
//        }
//    }

//    public void Reset()
//    {
//        vec1 = Vector3.zero;
//        vec2 = Vector3.zero;
//        createGround = false;
//        editMode = false;

//    }
//    private string getType()
//    {
//        if (createGround)
//            return "Vertical";
//        else
//            return "Horizontal";
//    }
//    public void CreateCollider()
//    {
//        GameObject newCol = new GameObject();
//        string str = getType();
//        //newCol.tag = str;
//        newCol.tag = "collider";
//        newCol.name = str;
//        newCol.transform.parent = gameObject.transform;
//        newCol.AddComponent<BoxCollider2D>();
//        newCol.transform.position = getCenter;
//        newCol.transform.localScale = getSize;
//        newCol.layer = LayerMask.NameToLayer("GroundCollider");
//        SpriteRenderer sprdr = newCol.AddComponent<SpriteRenderer>();
//        //Platform grCld = 
//        newCol.AddComponent<Platform>();
//        sprdr.sprite = RPlatformer.LoadSystemTexture("blank");
//        switch (getType())
//        {
//            case "Vertical":
//                sprdr.color = new Color(1, 0, 0, 0.2f);
//                //grCld.directionType = Platform.DirectionType.Vertical;
//                break;
//            case "Horizontal":
//                sprdr.color = new Color(0, 1, 1, 0.2f);
//                //grCld.directionType = Platform.DirectionType.Horizontal;
//                break;
//        }
//        sprdr.sortingLayerName = "DevView";

//        //reset trạng thái
//        Reset();
//    }
//}

//#if UNITY_EDITOR
//[CustomEditor(typeof(RColliderHandler))]
//public class RColliderEditor : Editor
//{
    
//    void OnSceneGUI()
//    {
//        RColliderHandler t = target as RColliderHandler;

//        #region See Collider
//        BoxCollider2D[] collider2Ds = t.GetComponentsInChildren<BoxCollider2D>();
//        foreach (var col in collider2Ds)
//        {
//            GameObject go = col.gameObject;
//            if (go.tag == "colliderGround")
//            {
//                Handles.color = Color.red;
//                Handles.DrawWireCube(col.transform.position, col.transform.localScale);
//            }
//        }
//        #endregion

//        //Draw button
//        Handles.BeginGUI();
//        GUILayout.BeginArea(new Rect(5, 5, 90, 50));
//        if (GUILayout.Button("+Ground"))
//        {
//            t.createGround = true;
//            t.editMode = true;
//        }
//        GUILayout.EndArea();
//        GUILayout.BeginArea(new Rect(5, 23, 90, 50));
//        if (GUILayout.Button("+Side"))
//        {
//            t.createGround = false;
//            t.editMode = true;
//        }
//        GUILayout.EndArea();
//        Handles.EndGUI();



//        #region getMouse
//        if (t.editMode == true)
//            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
//        else
//            return;

//        Vector3 mousePosition = Event.current.mousePosition;
//        mousePosition.y = SceneView.currentDrawingSceneView.camera.pixelHeight - mousePosition.y;
//        mousePosition = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(mousePosition);
//        //mousePosition.y = -mousePosition.y;
//        mousePosition.z = 0f;

//        Event e = Event.current;
//        //Check the event type and make sure it's left click.
//        if (e.type == EventType.MouseDown && e.button == 0)
//        {
//            t.vec1 = mousePosition;
//            e.Use();  //Eat the event so it doesn't propagate through the editor.
//        }
//        else if (e.type == EventType.MouseUp && e.button == 0)
//        {
//            t.vec2 = mousePosition;
//            t.CreateCollider();
//            e.Use();  //Eat the event so it doesn't propagate through the editor.
//        }
//        else if (e.type == EventType.MouseUp && e.button == 1)
//        {
//            Debug.Log("Cancel");
//            t.Reset();
//            e.Use();  //Eat the event so it doesn't propagate through the editor.
//        }

//        #endregion
//    }
//}
//#endif
