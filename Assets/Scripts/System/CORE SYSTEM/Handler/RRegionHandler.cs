﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//#if UNITY_EDITOR
//using UnityEditor;
//#endif

//[ExecuteInEditMode]
//public class RRegionHandler : MonoBehaviour
//#if UNITY_EDITOR
//    ,hideTransform, lockTransform
//#endif 
//{
//    [ReadOnly]
//    public bool editMode = false;
//    [HideInInspector]
//    public Vector3 vec1;
//    [HideInInspector]
//    public Vector3 vec2;

//    public Vector3 getCenter
//    {
//        get
//        {
//            return ((vec2 + vec1) / 2);
//        }
//    }
//    public Vector3 getSize
//    {
//        get
//        {
//            return new Vector3(Mathf.Abs(vec1.x - vec2.x), Mathf.Abs(vec1.y - vec2.y));
//        }
//    }

//    public void Reset()
//    {
//        vec1 = Vector3.zero;
//        vec2 = Vector3.zero;
//        editMode = false;

//    }
//    public void CreateCollider()
//    {
//        GameObject newCol = new GameObject();
//        newCol.tag = "Region";
//        newCol.name = "Region";
//        newCol.layer = LayerMask.NameToLayer("Region");
//        newCol.transform.parent = gameObject.transform;
//        newCol.transform.position = getCenter;
//        newCol.transform.localScale = getSize;

//        BoxCollider2D box = newCol.AddComponent<BoxCollider2D>();
//        box.isTrigger = true;

//        SpriteRenderer sprdr = newCol.AddComponent<SpriteRenderer>();
//        sprdr.sprite = RPlatformer.LoadBlankTexture();
//        sprdr.color = new Color(0, 1, 0, 0.4f);
//        sprdr.sortingLayerName = "DevView";
//        newCol.AddComponent<RegionEvent>();

//        //reset trạng thái
//        Reset();
//    }
//}

//#if UNITY_EDITOR
//[CustomEditor(typeof(RRegionHandler))]
//public class RRegionHandlerEditor : Editor
//{

//    void OnSceneGUI()
//    {
//        RRegionHandler t = target as RRegionHandler;
        
//        #region getMouse
//        //Draw button
//        Handles.BeginGUI();
//        GUILayout.BeginArea(new Rect(5, 5, 90, 50));
//        if (GUILayout.Button("+Region"))
//        {
//            t.editMode = true;
//        }
//        GUILayout.EndArea();
//        Handles.EndGUI();



//        if (t.editMode == true)
//            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
//        else
//            return;

//        Vector3 mousePosition = Event.current.mousePosition;
//        mousePosition.y = SceneView.currentDrawingSceneView.camera.pixelHeight - mousePosition.y;
//        mousePosition = SceneView.currentDrawingSceneView.camera.ScreenToWorldPoint(mousePosition);
//        //mousePosition.y = -mousePosition.y;
//        mousePosition.z = 0f;

//        Event e = Event.current;
//        //Check the event type and make sure it's left click.
//        if (e.type == EventType.MouseDown && e.button == 0)
//        {
//            t.vec1 = mousePosition;
//            e.Use();  //Eat the event so it doesn't propagate through the editor.
//        }
//        else if (e.type == EventType.MouseUp && e.button == 0)
//        {
//            t.vec2 = mousePosition;
//            t.CreateCollider();
//            e.Use();  //Eat the event so it doesn't propagate through the editor.
//        }
//        else if (e.type == EventType.MouseUp && e.button == 1)
//        {
//            Debug.Log("Cancel");
//            t.Reset();
//            e.Use();  //Eat the event so it doesn't propagate through the editor.
//        }

//        #endregion
//    }
//}
//#endif
