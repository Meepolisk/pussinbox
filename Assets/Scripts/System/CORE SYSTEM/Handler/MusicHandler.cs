﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class MusicHandler : MonoBehaviour
#if UNITY_EDITOR
    ,hideTransform, lockTransform
#endif  
{
    public ThemeMusic themeMusic;

    private static MusicHandler thisMusicHandler;
    private static AudioSource audioSource;
    private Coroutine loopCoroutine;

    private void Awake()
    {
        if (thisMusicHandler != null)
        {
            Destroy(thisMusicHandler.gameObject);
        }
        thisMusicHandler = this;
        audioSource = GetComponent<AudioSource>();
        audioSource.spatialBlend = 0f;
    }

    public void Start()
    {
        audioSource.clip = themeMusic.intro;
        audioSource.Play();
        float time = themeMusic.intro.length;
        loopCoroutine = StartCoroutine(replaceLoop(time));
    }
    IEnumerator replaceLoop (float time)
    {
        yield return new WaitForSeconds(time);
        audioSource.clip = themeMusic.loop;
        audioSource.loop = true;
        audioSource.Play();
    }
    public static void Play()
    {
        audioSource.Play();
    }
    public static void Stop()
    {
        audioSource.Stop();
        thisMusicHandler.StopCoroutine(thisMusicHandler.loopCoroutine);
    }
}
