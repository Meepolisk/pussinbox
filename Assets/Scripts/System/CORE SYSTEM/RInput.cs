﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RInput
{
    private static AbilityButton upKey = new AbilityButton(RButton.Up, RButtonType.press);
    private static AbilityButton downKey = new AbilityButton(RButton.Down, RButtonType.press);
    private static AbilityButton leftKey = new AbilityButton(RButton.Left, RButtonType.press);
    private static AbilityButton rightKey = new AbilityButton(RButton.Right, RButtonType.press);

    public static float GetAxisX
    {
        get
        {
            if (leftKey.isTrigger)
                return -1;
            else if (rightKey.isTrigger)
                return 1;
            return Input.GetAxis("Horizontal");
        }
    }
    public static float GetAxisY
    {
        get
        {
            if (upKey.isTrigger)
                return 1;
            else if (downKey.isTrigger)
                return -1;
            return Input.GetAxis("Vertical");
        }
    }
}
