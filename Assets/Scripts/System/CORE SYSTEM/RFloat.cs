﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class RFloat
{
    [ReadOnlyWhenAnimation]
    [ReadOnlyWhenPlaying]
    [SerializeField]
    private float _base;
    public float baseValue
    {
        set
        {
            _base = value;
            update();
        }
        get
        {
            return _base;
        }
    }

    private Dictionary<string,float> _adds;
    private Dictionary<string, float> _multis;
    private float? _min = null;
    private float? _max = null;

    [ReadOnly]
    public float current = 0f;

    public RFloat (float v_base, float? v_min = null, float? v_max = null)
    {
        _base = v_base;
        _adds = new Dictionary<string, float>();
        _multis = new Dictionary<string, float>();
        _min = v_min; _max = v_max;
        update();
    }
    public void addBonus (float v_value, string v_code)
    {
        if (_adds.ContainsKey(v_code))
            _adds[v_code] = v_value;
        else
            _adds.Add(v_code, v_value);
        update();
    }
    public void removeBonus (string v_code)
    {
        _adds.Remove(v_code);
        update();
    }

    public void addMultis(float v_value, string v_code)
    {
        if (_multis.ContainsKey(v_code))
            _multis[v_code] = v_value;
        else
            _multis.Add(v_code, v_value);
        update();
    }
    public void removeMultis(string v_code)
    {
        _multis.Remove(v_code);
        update();
    }

    public void update()
    {
        float f = _base;
        foreach (var add in _adds)
        {
            f += add.Value;
        }
        foreach (var multi in _multis)
        {
            f *= multi.Value;
        }
        if (_min != null && f < _min) f = (float)_min;
        if (_max != null && f > _max) f = (float)_max;
        current = f;
    }
}
#region Editor
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(RFloat))]
public class RFloatEditor : PropertyDrawer
{

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var rect1 = new Rect(position.x, position.y, position.width, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
        if (Application.isPlaying)
        {
            EditorGUI.PropertyField(rect1, property.FindPropertyRelative("current"), GUIContent.none);
        }
        else
        {
            EditorGUI.PropertyField(rect1, property.FindPropertyRelative("_base"), GUIContent.none);
        }

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
#endregion