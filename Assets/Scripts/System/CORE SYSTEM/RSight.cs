﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class RSight : MonoBehaviour
{
    [ReadOnly]
    public List<Unit> units;

    protected RAI aiRef;
    public bool containUnit(Unit _unit)
    {
        if (units.Contains(_unit))
            return true;
        return false;
    }
    public Unit[] getUnitInside()
    {
        if (units.Count == 0) return null;
        return units.ToArray();
    }

    public void setup(RAI _ai)
    {
        aiRef = _ai;
        rEvent.Unit_Die += REvent_Unit_Die;
    }
    private void OnDisable()
    {
        rEvent.Unit_Die -= REvent_Unit_Die;
    }

    private void REvent_Unit_Die(Unit _unitA, Unit _unitB)
    {
        if (units.Contains(_unitA))
            units.Remove(_unitA);
    }

    protected virtual void Start()
    {
        units = new List<Unit>();
    }

    protected void OnTriggerEnter2D(Collider2D col)
    {
        RObjectCollider unitCollider = col.GetComponent<RObjectCollider>();
        if (unitCollider)
        {
            Unit enteredUnit = unitCollider.model.rObject as Unit;
            if (!PlayerGroup.isAlly(enteredUnit.player, aiRef.unitRef.player)
                && !units.Contains(enteredUnit))
            {
                units.Add(enteredUnit);
            }
        }
    }

    protected void OnTriggerExit2D(Collider2D col)
    {
        RObjectCollider unitCollider = col.GetComponent<RObjectCollider>();
        if (unitCollider)
        {
            Unit leaveUnit = unitCollider.model.rObject as Unit;
            if (units.Contains(leaveUnit))
            {
                units.Remove(leaveUnit);
                aiRef.UnitLeaveBoxSight(leaveUnit);
            }
        }
    }

    #region static
    public static bool canSee(Vector2 pos, Unit otherUnit, float _maxDistance, bool _throughWall = false)
    {
        if (otherUnit.isDead) return false;

        Vector2 otherCenterPos = otherUnit.center;

        // tạo raycast
        RaycastHit2D[] hits = Physics2D.RaycastAll(pos, (otherCenterPos - pos), _maxDistance);
        //if (raycast.collider == (Collider2D) otherBox) //Raycast thành công
        foreach (var hit in hits)
        {
            if (!_throughWall && hit.transform.gameObject.GetComponent<Platform>() != null) //Raycast vào tường
            {
                if (hit.transform.gameObject.GetComponent<Platform>().blockMissile)
                {
                    Debug.DrawLine(pos, otherCenterPos, Color.red, Time.deltaTime);
                    return false;
                }
            }
            if (hit.transform.gameObject.GetComponentInParent<Unit>() == otherUnit) //Raycast thành công
            {
                Debug.DrawLine(pos, otherCenterPos, Color.green, Time.deltaTime);
                return true;
            }
        }
        return false;
    }
    void Update()
    {
        RayCheckUnitInSight();
    }
    protected void RayCheckUnitInSight()
    {
        if (aiRef.info.stage == RAI.Stage.action) return; //nếu là action thì không check
        if (units.Count == 0) return; //không có unit trong box thì khỏi check

        foreach (Unit _unit in units)
        {
            if (RSight.canSee(aiRef.startingRaycastPoint, _unit, aiRef.sightInfo.maxRangeCanSee, aiRef.sightInfo.canSeeThroughWall))
            {
                aiRef.GetNewFocusedUnit(_unit);
                break;
            }
        }
    }
    #endregion
}
