﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
public class RCamera : MonoBehaviour
{
    [SerializeField]
    protected new Camera camera;

    protected List<Unit> mainObjects = new List<Unit>();

    
    protected Vector2 AbsPosition
    {
        get
        {
            Vector2 result = Vector2.zero;
            if (mainObjects.Count < 1)
                return result;
            foreach (var tar in mainObjects)
            {
                result += tar.rObjectCollider.center;
            }
            result /= mainObjects.Count;
            return result;
        }
    }

    protected virtual void Awake()
    {
        GetObject();
    }
    protected virtual void Start()
    {
        rEvent.Unit_Controlable += REvent_Controlable;
        rEvent.Unit_Uncontrolable += REvent_Uncontrolable;
    }
    private void OnDisable()
    {
        rEvent.Unit_Controlable -= REvent_Controlable;
        rEvent.Unit_Uncontrolable -= REvent_Uncontrolable;
    }

    private void REvent_Controlable(Unit _unit)
    {
        GetObject();
    }
    private void REvent_Uncontrolable(Unit _unit)
    {
        if (mainObjects.Contains(_unit))
        {
            GetObject();
        }
    }
    private void GetObject()
    {
        mainObjects.Clear();
        PlayerController[] objects = Component.FindObjectsOfType<PlayerController>();
        foreach (var obj in objects)
        {
            if (obj.enabled == true)
                mainObjects.Add(obj.GetComponent<Unit>());
        }
    }
}
