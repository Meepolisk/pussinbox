﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraSystem;

[RequireComponent(typeof(BoxCollider2D))]
public class ZoneChanger : MonoBehaviour
{
    public CameraZone to;

    private new BoxCollider2D collider;

    private void Awake()
    {
        collider = GetComponent<BoxCollider2D>();
        collider.isTrigger = true;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        PlayerController playerController = col.GetComponentInParent<PlayerController>();
        if (playerController != null && playerController.unitRef.player != 0)
        {
            CameraZone.ChangeRegion(to);
        }
    }
}
