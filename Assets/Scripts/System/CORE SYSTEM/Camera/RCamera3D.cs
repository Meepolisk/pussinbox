﻿using UnityEngine;
using System.Collections;

public class RCamera3D : RCamera
{
    public float heightAbove = 5f;

    private void LateUpdate()
    {
        follow();
    }

    void follow()
    {
        Vector2 resultPos = AbsPosition;
        gameObject.transform.position = new Vector3(resultPos.x, resultPos.y + heightAbove, gameObject.transform.position.z);
    }
}
