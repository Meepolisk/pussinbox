﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace CameraSystem
{
    [RequireComponent(typeof(FlareLayer))]
    public class RCamera2D : RCamera
    {
        public enum Mode { none, follow };
        public Mode mode { private set; get; } = Mode.none;

        public RObject target;
        public float minSize;
        public float maxSize;

        private RSO_System SOIV;
        private float size
        {
            set => camera.orthographicSize = value;
            get => camera.orthographicSize;
        }

        protected override void Awake()
        {
            base.Awake();
            //lấy Init
            CameraZone.activeCamera = this;
            SOIV = Resources.Load("ScriptableObject/RSO_System") as RSO_System;
            minSize = SOIV.camera.minSize;
            maxSize = SOIV.camera.maxSize;
        }
        private void LateUpdate()
        {
            Follow();
        }

        public class Rect
        {
            public float top, btm, left, right;
        }
        private void EditRect(Rect _rect, Vector2 _target, float _offset)
        {
            if (_target.y + _offset > CameraZone.activeRegion.yMax)
                _rect.top = CameraZone.activeRegion.yMax;
            else
                _rect.top = _target.y + _offset;

            if ((_target.y - _offset) < CameraZone.activeRegion.yMin)
                _rect.btm = CameraZone.activeRegion.yMin;
            else
                _rect.btm = _target.y - _offset;

            if (_target.x + _offset > CameraZone.activeRegion.xMax)
                _rect.right = CameraZone.activeRegion.xMax;
            else
                _rect.right = _target.x + _offset;

            if (_target.x - _offset < CameraZone.activeRegion.xMin)
                _rect.left = CameraZone.activeRegion.xMin;
            else
                _rect.left = _target.x - _offset;

            DrawCube(_rect.top, _rect.btm, _rect.left, _rect.right, Color.cyan);
        }
        private void Follow()
        {
            if (CameraZone.activeRegion == null) return;

            if (mainObjects.Count <= 0) return;

            Rect mainRect = new Rect();
            foreach (var mainObject in mainObjects)
            {
                EditRect(mainRect, mainObject.center, minSize / 2f);
            }

            if (target != null)
            {
                float minYSize = minSize * 2f;
                float maxYSize = maxSize * 2f;
                float ratio = (float)Screen.width / (float)Screen.height;
                float minXSize = minYSize * ratio;
                float maxXSize = maxYSize * ratio;

                Rect subRect = new Rect();
                EditRect(subRect, target.center, 3f);

                if (subRect.top > mainRect.top)
                    mainRect.top = Mathf.Clamp(subRect.top, mainRect.top, mainRect.btm + maxYSize);
                if (subRect.btm < mainRect.btm)
                    mainRect.btm = Mathf.Clamp(subRect.btm, mainRect.top - maxYSize, mainRect.btm);

                if (subRect.right > mainRect.right)
                    mainRect.right = Mathf.Clamp(subRect.right, mainRect.right, mainRect.left + maxXSize);
                if (subRect.left < mainRect.left)
                    mainRect.left = Mathf.Clamp(subRect.left, mainRect.right - maxXSize, mainRect.left);
            }
            DrawCube(mainRect.top, mainRect.btm, mainRect.left, mainRect.right, Color.yellow);

            adjustCamera(mainRect);
        }
        private void DrawCube(float _top, float _btm, float _left, float _right, Color _color)
        {
            Vector2 _a = new Vector2(_left, _top);
            Vector2 _b = new Vector2(_right, _top);
            Vector2 _c = new Vector2(_right, _btm);
            Vector2 _d = new Vector2(_left, _btm);
            Debug.DrawLine(_a, _b, _color);
            Debug.DrawLine(_b, _c, _color);
            Debug.DrawLine(_c, _d, _color);
            Debug.DrawLine(_d, _a, _color);
        }
        void adjustCamera(Rect _rect)
        {
            Vector2 firstCenter = new Vector2((_rect.left + _rect.right) / 2, (_rect.top + _rect.btm) / 2);
            RDebug.DrawPoint(firstCenter, 2f, Color.red, Time.deltaTime);

            float ratio = (float)Screen.width / (float)Screen.height;
            float height = (Mathf.Abs(_rect.top - _rect.btm) / 2f);
            float weight = (Mathf.Abs(_rect.left - _rect.right) / 2f) / ratio;
            float _size = Mathf.Clamp(Mathf.Max(height, weight), minSize, maxSize);


            float BTM = (firstCenter.y - _size);
            if (BTM < CameraZone.activeRegion.yMin)
                firstCenter.y += (CameraZone.activeRegion.yMin - BTM);
            else
            {
                float TOP = firstCenter.y + _size;
                if (TOP > CameraZone.activeRegion.yMax)
                {
                    firstCenter.y -= (TOP - CameraZone.activeRegion.yMax);
                }
            }
            float LEFT = firstCenter.x - (_size * ratio);
            if (LEFT < CameraZone.activeRegion.xMin)
                firstCenter.x += (CameraZone.activeRegion.xMin - LEFT);
            else
            {
                float RIGHT = firstCenter.x + (_size * ratio);
                if (RIGHT > CameraZone.activeRegion.xMax)
                {
                    firstCenter.x -= (RIGHT - CameraZone.activeRegion.xMax);
                }
            }

            transform.position = firstCenter;
            size = _size;
        }

        const float minPanTime = 0.2f;
        public void panCamera(Vector2 newPos, float time = 1f)
        {

        }
    }
}
