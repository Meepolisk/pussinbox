﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CameraSystem
{
    public class CameraZone : MonoBehaviour
    {
        #region static
        public static CameraZone activeRegion;
        public static RCamera2D activeCamera;
        public static void ChangeRegion(CameraZone newRegion = null)
        {
            if (newRegion != null) // không phải gọi lần đầu
            {
                if (activeRegion != null && activeRegion.isActive == true)
                {
                    activeRegion.isActive = false;
                }
                newRegion.isActive = true;
                activeRegion = newRegion;
            }
        }
        #endregion
        [SerializeField]
        [ReadOnlyWhenPlaying]
        private bool _isActive = false;
        public bool isActive
        {
            get => _isActive;
            set
            {
                Platform[] fps = GetComponentsInChildren<Platform>();
                foreach (var p in fps)
                {
                    p.enabled = value;
                }
                _isActive = value;
            }
        }

        public Vector2 center
            => transform.position;
        public Vector2 scale
            => transform.lossyScale;

        private void Awake()
        {
            if (isActive)
                CameraZone.activeRegion = this;
        }


        [ReadOnlyWhenPlaying]
        public int yMax;
        [ReadOnlyWhenPlaying]
        public int yMin;
        [ReadOnlyWhenPlaying]
        public int xMin;
        [ReadOnlyWhenPlaying]
        public int xMax;

        public List<CameraZoneSpecialCase> zoneCases = new List<CameraZoneSpecialCase>();

#if UNITY_EDITOR
        public void RefreshInspector()
        {
            //Chỉnh position, apply vào biến
            Vector2 pos;
            pos.x = (xMin + xMax) / 2f;
            pos.y = (yMax + yMin) / 2f;
            gameObject.transform.position = pos;

            //Chỉnh độ rộng của locker

            Vector2 scale;
            scale.x = Mathf.Abs(xMin - xMax);
            scale.y = Mathf.Abs(yMax - yMin);
            gameObject.transform.SetGlobalScale(scale);

            //active
            isActive = _isActive;
        }
        public void RefreshGUI()
        {
            Vector2 pos = gameObject.transform.position;
            Vector2 scale = gameObject.transform.lossyScale;
            yMax = (int)(pos.y + (scale.y / 2f));
            yMin = yMax - (int)scale.y;
            xMax = (int)(pos.x + (scale.x / 2f));
            xMin = xMax - (int)scale.x;
            RefreshInspector();
        }

        #region EDITOR
        [Header("Debug")]
        [SerializeField]
        private Color color = new Color(1f, 1f, 1f, 0.2f);
        private void OnDrawGizmos()
        {
            Gizmos.color = color;
            Vector2 size = new Vector2(xMax - xMin, yMax - yMin);
            Gizmos.DrawCube(center, size);
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(CameraZone))]
        public class CustomInspectorCameraZone : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                CameraZone p = (CameraZone)target;

                p.RefreshInspector();
            }
            void OnSceneGUI()
            {
                CameraZone p = (CameraZone)target;

                p.RefreshGUI();

                Vector2[] allPoints =
                {
                new Vector2 (p.xMin, p.yMax),
                new Vector2 (p.xMax, p.yMax),
                new Vector2 (p.xMax, p.yMin),
                new Vector2 (p.xMin, p.yMin)
            };

                Handles.color = new Color(1, Mathf.Cos(Time.unscaledTime * 4), 1, 1);
                Handles.DrawLine(allPoints[0], allPoints[1]);
                Handles.DrawLine(allPoints[1], allPoints[2]);
                Handles.DrawLine(allPoints[2], allPoints[3]);
                Handles.DrawLine(allPoints[3], allPoints[0]);
            }
        }
#endif

        #endregion
#endif
    }
    [System.Serializable]
    public class CameraZoneSpecialCase
    {
        public float x;
        public float yMin;
        public float yMax;
    }
}