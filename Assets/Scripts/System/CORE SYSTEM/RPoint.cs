﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class RPoint
{
    [SerializeField]
    private float _current;
    [SerializeField]
    private float _max;
    public float current
    {
        get
        {
            return Mathf.Clamp(_current, 0f, _max);
        }
        set
        {
            _current = Mathf.Clamp(value, 0f, _max);
        }
    }
    public float max
    {
        get
        {
            if (_max < 0)
                return 0f;
            return _max;
        }
        set
        {
            if (value < 0)
            {
                _max = 0; return;
            }
            _max = value;
            //RefreshIconAmount();
        }
    }
    
    public RPoint (float vmax)
    {
        _max = vmax;
    }
    public RPoint(float vcur, float vmax)
    {
        _current = vcur;
        _max = vmax;
    }

    public RPoint clone()
    {
        RPoint newOne = new RPoint(_current,_max);
        //newOne._current = _current;
        //newOne._max = _max;
        return newOne;
    }
    public float remainingScale
    {
        get
        {
            try
            {
                return current / max;
            }
            catch
            {
                return 0f;
            }
        }
    }
}
#region Editor
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(RPoint))]
public class RPointInspector : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var rect1 = new Rect(position.x, position.y, (position.width / 2) - 5, position.height);
        var rect2 = new Rect(position.x + (position.width / 2) - 5, position.y, 10, position.height);
        var rect3 = new Rect(position.x + (position.width / 2) + 5, position.y, (position.width / 2) - 5, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("_current"), GUIContent.none);
        GUI.Label(rect2, "/");
        EditorGUI.PropertyField(rect3, property.FindPropertyRelative("_max"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
#endregion