﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public enum RButton {
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3,
    Item = 4,
    Attack = 5,
    Jump = 6,
    Special = 7,
    Ability = 8,
    Shift = 9,
    Start = 10,
    Cancel = 11
}
public enum RButtonType
{
    down = 1, up = 2, press = 3
}
//public enum RButtonStatus
//{
//    none = 0, down = 1, up = 2, press = 3
//}
[System.Serializable]
public class RKey
{
    public static int RKeyCodeLength = System.Enum.GetValues(typeof(RButton)).Length;
    

    public string name;
    public RKeyCode key1;
    public RKeyCode key2;
    //public RButtonStatus status = RButtonStatus.none;

    #region Constructor
    public RKey(string _name, RKeyCode _key1)
    {
        name = _name;
        key1 = _key1;
    }
    public RKey(string _name, RKeyCode _key1, RKeyCode _key2)
    {
        name = _name;
        key1 = _key1;
        key2 = _key2;
    }
    #endregion

    public bool getKeyDown()
    {
        if (Input.GetKeyDown(key1.toKeyCode()) || Input.GetKeyDown(key2.toKeyCode()))
        {
            //Debug.Log("1");
            return true;
        }
        return false;
    }
    public bool getKeyUp()
    {
        if (Input.GetKeyUp(key1.toKeyCode()) || Input.GetKeyUp(key2.toKeyCode()))
        {
            //Debug.Log("2");
            return true;
        }
        return false;
    }
    public bool getKey()
    {
        if (Input.GetKey(key1.toKeyCode()) || Input.GetKey(key2.toKeyCode()))
        {
            return true;
        }
        return false;
    }

    public RKey clone()
    {
        return new RKey(this.name ,this.key1, this.key2);
    }
}
[System.Serializable]
public static class RInputHandler{
    public static RKey[] rKeys = new RKey[RKey.RKeyCodeLength];

    public static bool getKeyDown(RButton rKeyButton)
    {
        return rKeys[(int)rKeyButton].getKeyDown();
    }
    public static bool getKeyUp(RButton rKeyButton)
    {
        return rKeys[(int)rKeyButton].getKeyUp();
    }
    public static bool getKey(RButton rKeyButton)
    {
        return rKeys[(int)rKeyButton].getKey();
    }

    public static void Apply(RInputProfile profiles)
    {
        for(int i = 0; i < RKey.RKeyCodeLength; i++)
        {
            rKeys[i] = profiles.rKeyProfiles[i].clone();
        }
        //Debug.Log("RInput applied");
    }
}
[System.Serializable]
public class RInputProfile
{
    public RKey[] rKeyProfiles = new RKey[RKey.RKeyCodeLength];

    public RInputProfile()
    {
        rKeyProfiles[(int)RButton.Cancel] = new RKey("Cancel", RKeyCode.Escape, RKeyCode.JoystickButton8);
        rKeyProfiles[(int)RButton.Start] = new RKey("Start", RKeyCode.Return, RKeyCode.JoystickButton9);
    }
    public void LoadDefault()
    {
        rKeyProfiles[(int)RButton.Up] = new RKey("Up", RKeyCode.UpArrow);
        rKeyProfiles[(int)RButton.Down] = new RKey("Down", RKeyCode.DownArrow);
        rKeyProfiles[(int)RButton.Left] = new RKey("Left", RKeyCode.LeftArrow);
        rKeyProfiles[(int)RButton.Right] = new RKey("Right", RKeyCode.RightArrow);
        rKeyProfiles[(int)RButton.Item] = new RKey("Item", RKeyCode.Space, RKeyCode.JoystickButton4);
        rKeyProfiles[(int)RButton.Attack] = new RKey("Attack", RKeyCode.C, RKeyCode.JoystickButton3);
        rKeyProfiles[(int)RButton.Jump] = new RKey("Jump", RKeyCode.X, RKeyCode.JoystickButton2);
        rKeyProfiles[(int)RButton.Special] = new RKey("Special", RKeyCode.V, RKeyCode.JoystickButton0);
        rKeyProfiles[(int)RButton.Ability] = new RKey("Ability", RKeyCode.F,RKeyCode.JoystickButton1);
        rKeyProfiles[(int)RButton.Shift] = new RKey("Shift", RKeyCode.Z, RKeyCode.JoystickButton5);
        //Debug.Log("Control data loaded: default");
    }
    public void LoadCurrent()
    {
        for (int i = 0; i < RKey.RKeyCodeLength; i++)
        {
            rKeyProfiles[i] = RInputHandler.rKeys[i].clone();
        }
        //Debug.Log("Control data loaded: default");
    }
}