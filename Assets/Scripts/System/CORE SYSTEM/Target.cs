﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class Target
{
    public bool enemy = true;
    public bool ally = false;
    public bool wall = true;
}
#region Editor
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(Target))]
public class TargetDrawer : PropertyDrawer
{

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var rect1 = new Rect(position.x, position.y, (position.width / 2) - 5, position.height);
        var rect2 = new Rect(position.x + (position.width / 2) - 5, position.y, 10, position.height);
        var rect3 = new Rect(position.x + (position.width / 2) + 5, position.y, (position.width / 2) - 5, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("_current"), GUIContent.none);
        GUI.Label(rect2, "/");
        EditorGUI.PropertyField(rect3, property.FindPropertyRelative("_max"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
#endregion