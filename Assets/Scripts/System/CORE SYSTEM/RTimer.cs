﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class RTimer : MonoBehaviour
{

    #region Attribute & Constructor
    public string _name = "unamed";
    public float time;
    public bool isPaused = true;
    public bool oneShootOnly = false;

    public float timerMax;
    public Action actionExpire = null;
    public Action actionUpdate = null;
    public bool rewing;
    public string timerName
    {
        set { _name = value; }
    }

    public static GameObject handler;

    #endregion
    #region public method
    ///<summary>
    ///Create a 1-shoot-only Rtimer, auto destroy on expire
    ///</summary>
    public static RTimer Add1TimeTimer(string timerName, float second, Action _actionExpire, Action _actionUpdate = null)
    {
        RTimer newTimer = handler.AddComponent<RTimer>();
        newTimer.setData(timerName, second, false, _actionExpire, _actionUpdate);
        newTimer.oneShootOnly = true;
        newTimer.start();
        return newTimer;
    }
    /// <summary>
    /// Create timer, attach to a GameObject. Attach to TimerHandler if GameObject is null.
    /// </summary>
    /// <param name="go">the GameObject that Timer will attach to. Leave NULL will attach to TimerHandler</param>
    /// <returns></returns>
    public static RTimer Add(GameObject go = null)
    {
        if (go == null)
            go = handler;
        RTimer newTimer = go.AddComponent<RTimer>();
        return newTimer;
    }

    public void setData(string timerName, float _second, bool _canRewing, Action _actionExpire, Action _actionUpdate = null)
    {
        _name = timerName;
        setData(_second, _canRewing, _actionExpire, _actionUpdate);
    }
    public void setData(float _second, bool _canRewing, Action _actionExpire, Action _actionUpdate = null)
    {
        timerMax = _second;
        rewing = _canRewing;
        actionExpire = _actionExpire;
        actionUpdate = _actionUpdate;
    }
    public void pause()
    {
        isPaused = true;
    }
    public void start()
    {
        time = 0f;
        isPaused = false;
    }
    public void stop()
    {
        isPaused = true;
        time = 0f;
    }
    public float getRemainTime
    {
        get
        {
            float f = timerMax - time;
            return f;
        }
    }
    public void addTime(float addSec)
    {
        time -= addSec;
    }
    #endregion
    void Awake()
    {
        time = 0f;
        //if (debugHIDE)
        //    this.hideFlags = HideFlags.HideInInspector;
    }
    void destroyOneShotTimer()
    {
        if (oneShootOnly)
        {
            Destroy(this);
        }
    }
    void Update()
    {
        if (isPaused) return;
        if (actionUpdate != null)
        {
            actionUpdate.Invoke();
        }
        time += Time.deltaTime;
        if (time >= timerMax)
        {
            time = 0f;
            actionExpire.Invoke();
            if (!rewing)
            {
                isPaused = true;
                destroyOneShotTimer();
            }
        }
    }
    public static RTimer[] GetAllTimerInObject(GameObject gobj)
    {
        return gobj.GetComponentsInChildren<RTimer>();
    }
}
