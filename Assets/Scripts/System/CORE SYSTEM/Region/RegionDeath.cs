﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class RegionDeath : Region
{
    protected override void ObjectEnterRegion(RObject _rObject)
    {
        Unit triggered = _rObject.GetComponent<Unit>();
        if (triggered == null) return;

        triggered.damage(triggered, triggered.hitPoint.max * 2f);
    }
}
