﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(BoxCollider2D))]
public class RegionCaller : Region
{
    enum Type { None, OnEnter, OnLeave }

    [SerializeField]
    private Type oneTimeShoot = Type.None;

    protected rDelegate_Unit actionEnter;
    protected rDelegate_Unit actionLeave;

    public void Setup (rDelegate_Unit _actionEnter, rDelegate_Unit _actionLeave = null)
    {
        actionEnter = _actionEnter;
        actionLeave = _actionLeave;
    }

    protected override void ObjectEnterRegion(RObject _rObject)
    {
        Unit triggered = _rObject.GetComponent<Unit>();
        if (triggered == null) return;

        if (actionEnter != null)
            actionEnter(triggered);

        if (oneTimeShoot == Type.OnEnter)
            active = false;
    }
    protected override void ObjectLeaveRegion(RObject _rObject)
    {
        Unit triggered = _rObject.GetComponent<Unit>();
        if (triggered == null) return;

        if (actionLeave != null)
            actionLeave(triggered);

        if (oneTimeShoot == Type.OnLeave)
            active = false;
    }
}
