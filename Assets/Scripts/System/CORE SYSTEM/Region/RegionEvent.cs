﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class RegionEvent : Region
{
    List<Unit> containingUnits;
    
    public bool containUnit(Unit _unit)
    {
        if (containingUnits.Contains(_unit))
            return true;
        return false;
    }
    public Unit[] getUnitInside()
    {
        if (containingUnits.Count == 0) return null;
        return containingUnits.ToArray();
    }

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        containingUnits = new List<Unit>();
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    protected override void ObjectEnterRegion (RObject _rObject)
    {
        Unit triggered = _rObject.GetComponent<Unit>();
        if (triggered == null) return;

        if (!containingUnits.Contains(triggered))
            containingUnits.Add(triggered);
        rEvent.Start.Unit_EnterRegion(this, triggered);
    }

    protected override void ObjectLeaveRegion (RObject _rObject)
    {
        Unit triggered = _rObject.GetComponent<Unit>();
        if (triggered == null) return;

        if (containingUnits.Contains(triggered))
            containingUnits.Remove(triggered);
        rEvent.Start.Unit_LeaveRegion(this, triggered);
    }
}
