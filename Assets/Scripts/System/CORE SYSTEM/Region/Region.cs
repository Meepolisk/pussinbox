﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Region : MonoBehaviour
{
    [SerializeField]
    [ReadOnlyWhenPlaying]
    private bool _active = true;
    public bool active
    {
        get
        {
            return _active;
        }
        set
        {
            box.enabled = value;
            _active = value;
        }
    }

    protected BoxCollider2D box;

    public Vector2 center
    {
        get
        {
            return gameObject.transform.position;
        }
    }
    protected virtual void Awake()
    {
        box = GetComponent<BoxCollider2D>();
        box.isTrigger = true;

        active = _active;
    }
    protected bool baseCondition(RObject _rObject)
    {
        if (_rObject == null)
            return false;
        return (condition(_rObject));
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        RObject _rObject = col.GetComponentInParent<RObject>();
        if (baseCondition(_rObject))
        {
            ObjectEnterRegion(_rObject);
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        RObject _rObject = col.GetComponentInParent<RObject>();
        if (baseCondition(_rObject))
        {
            ObjectLeaveRegion(_rObject);
        }
    }

    protected virtual void ObjectEnterRegion(RObject _rObject) { }
    protected virtual void ObjectLeaveRegion(RObject _rObject) { }
    protected virtual bool condition(RObject _rObject) { return true; }
}
