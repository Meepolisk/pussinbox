﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HUIAddOn : MonoBehaviour {

    protected HUI mainRef;
    protected Animator animator;

    protected virtual void Awake()
    {
        mainRef = GetComponentInParent<HUI>();
        animator = GetComponent<Animator>();
    }
    protected virtual void Start()
    {
        mainRef.GetAddOns(this);
    }
    public abstract void Setup(HUI _hui);
}
