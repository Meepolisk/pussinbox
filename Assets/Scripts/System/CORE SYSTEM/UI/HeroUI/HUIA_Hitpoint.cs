﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUIA_Hitpoint : HUIAddOn {

    [ReadOnly]
    public HitPoint hitpoint;
    
    public override void Setup(HUI _hui)
    {
        hitpoint = mainRef.heroRef.hitPoint;
    }

    // Update is called once per frame
    void Update () {
        animator.SetFloat("HPScale", hitpoint.scale);
    }
}
