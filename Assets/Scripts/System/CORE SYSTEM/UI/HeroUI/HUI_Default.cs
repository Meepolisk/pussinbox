﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HUI_Default : HUI
{
    public GameObject healthIcon;

    private RawImage heroPortrait;
    private List<GameObject> healthIcons = new List<GameObject>();
    private GameObject HealthBar;
    private float energyWidth = 214f;
    private RectTransform energyBarRect;
    public float oldEnergy = 0f;
    
    protected override void Awake()
    {
        base.Awake();
        heroPortrait = transform.Find("HeroPortrait").gameObject.GetComponent<RawImage>();
        HealthBar = transform.Find("HealthBar").gameObject;
    }
    public override void Setup(PlayerController _controller)
    {
        base.Setup(_controller);

        heroPortrait.texture = _controller.heroicProfile.heroImage;
        heroPortrait.color = new Color(1f, 1f, 1f, 1f);
        reinstanceHP(heroRef.GetComponent<Unit>().hitPoint);
        getEnergy();

        //reset animation
        this.gameObject.GetComponent<Animator>().Rebind();

        //instance đống UI system mới
        UI_Profile[] uiPs = gameObject.GetComponentsInChildren<UI_Profile>();
        foreach (var ui in uiPs)
        {
            ui.unitRef = heroRef;
        }
    }
    #region HitPoint
    private void getEnergy()
    {
        energyWidth = transform.Find("UltimateBar").gameObject.GetComponent<RectTransform>().rect.width;
        energyBarRect = gameObject.findChild("energyBar").GetComponent<RectTransform>();

    }
    private void setHP()
    {
        HitPoint newHP = heroRef.GetComponent<Unit>().hitPoint;
        reinstanceHP(newHP);
        refreshHP(newHP);
    }
    void refreshHP(HitPoint newHP)
    {
        //int len = healthIcons.Count;
        //vẽ health
        int tmp_i = 0; //lưu tạm thời vị trí hp icon đang tính
        int tmp_len = newHP.healthIcon; //lưu tạm thời vị trí hp icon sẽ tới

        for (int i = tmp_i; i < tmp_len; i++)
        {
            float alpha = (newHP.health.current / HitPoint.lifePerIcon) - i;
            healthIcons[i].GetComponent<RawImage>().color
                = new Color(1f, 1f, 1f, (Mathf.Clamp01(alpha)));
        }
        // vẽ armor
        int lastCheck = newHP.healthIcon;   
        tmp_i = tmp_len; //lưu tạm thời vị trí hp icon đang tính
        tmp_len = tmp_len + newHP.armorIcon; //lưu tạm thời vị trí hp icon sẽ tới
        if (newHP.armorIcon > 0)
        {
            for (int i = tmp_i; i < tmp_len; i++)
            {
                float alpha = (newHP.armor.current / HitPoint.lifePerIcon) + (newHP.bonusArmor / HitPoint.lifePerIcon) - i + lastCheck;
                healthIcons[i].GetComponent<RawImage>().color
                    = new Color(1f, 0.66f, 0f, (Mathf.Clamp01(alpha)));
            }
        }
        // vẽ shield
        lastCheck = newHP.healthIcon + newHP.armorIcon;
        tmp_i = tmp_len; //lưu tạm thời vị trí hp icon đang tính
        tmp_len = tmp_len + newHP.shieldIcon; //lưu tạm thời vị trí hp icon sẽ tới
        if (newHP.shieldIcon > 0)
        {
            for (int i = tmp_i; i < tmp_len; i++)
            {
                float alpha = (newHP.shield.current / HitPoint.lifePerIcon) - i + lastCheck;
                healthIcons[i].GetComponent<RawImage>().color
                    = new Color(0f, 1f, 1f, (Mathf.Clamp01(alpha)));
            }
        }
        if (tmp_len < healthIcons.Count)
        {
            for (int i = tmp_len; i < healthIcons.Count; i++)
            {
                healthIcons[i].GetComponent<RawImage>().color
                    = new Color(0f, 0f, 0f, 0f);
            }
        }
        //Debug.Log("refresh HP");
    }
    void reinstanceHP(HitPoint newHP)
    {
        int iMax = newHP.iconAmountMax;
        if (iMax <= 1) iMax = 1;
        //xóa bọn cũ
        foreach (var go in healthIcons)
        {
            Destroy(go);
        }
        healthIcons.Clear();
        /*h = chiều dài thanh chứa = iMax*s
        i = số stt nút, iMax = số nút max
        w = độ rộng nút = 4h / 3i;
        s = khoảng cách tâm 2 nút = 3w/4
        a = khoảnh cách nút đầu tiên phải lùi = 1/4w
        m = khoảng cách rìa 2 nút */
        float h = HealthBar.GetComponent<RectTransform>().rect.width;
        float w = (4f * h) / (3f * iMax);
        float s = (3f * w) / 4f;
        float a = w / 4f;
        float m = 3f;
        for (int i = 0; i < iMax; i++)
        {
            GameObject newone = GameObject.Instantiate(healthIcon);
            newone.transform.SetParent(HealthBar.transform);
            float X = ((float)i * s) + a;
            RectTransform rectra = newone.GetComponent<RectTransform>();
            rectra.anchoredPosition = new Vector2(X, 0f);
            rectra.sizeDelta = new Vector2((w - m), HealthBar.GetComponent<RectTransform>().rect.height);
            rectra.localScale = new Vector2(1f, 1f);
            newone.name = "HealthIcon[" + i + "]";
            healthIcons.Add(newone);

        }
        //Debug.Log("Reinstance HP");
    }
    Texture2D detectHeroIcon(Texture2D sourceTexture, Texture2D destinationTexture)
    {
        Texture2D newtexture = new Texture2D(sourceTexture.width, sourceTexture.height, TextureFormat.ARGB32, false);

        Color[] sourcePixels = sourceTexture.GetPixels(0);
        Color[] destinationPixels = destinationTexture.GetPixels(0);

        //for (int i = 0; i < destinationPixels.Length; ++i)
        //    destinationPixels[i].a = 0.0f;

        int xEnd = newtexture.width;
        int yEnd = newtexture.height;
        for (int y = 0; y < yEnd; ++y)
        {
            for (int x = 0; x < xEnd; ++x)
            {
                int index = (y * xEnd + x);
                if (sourcePixels[index].a > 0.1f)
                    destinationPixels[index].a = (1f - sourcePixels[index].a);
            }
        }

        newtexture.SetPixels(destinationPixels, 0);
        newtexture.Apply();
        return newtexture;
    }
    #endregion
    void refreshEnergy()
    {
        float oldCurrent = heroRef.energy.current;
        if (oldCurrent != oldEnergy)
        {
            float newX = -energyWidth * (1 - heroRef.energy.remainingScale);
            energyBarRect.anchoredPosition = new Vector2(newX, 0f);
            oldEnergy = oldCurrent;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
#if UNITY_EDITOR
        if (heroRef == null) return;
#endif
        setHP();
        refreshEnergy();
    }
}

