﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUIA_Ability : HUIAddOn {
    
    public string GameObjectName;
    [ReadOnly]
    public Ability abiRef;

    private Text text;

    protected override void Awake()
    {
        base.Awake();
        text = GetComponentInChildren<Text>();
    }

    public override void Setup(HUI _hui)
    {
        abiRef = GetAbi();
        if (abiRef == null)
            this.gameObject.SetActive(false);
    }
    Ability GetAbi()
    {
        foreach (Ability abi in mainRef.heroRef.abilities)
        {
            if(abi.gameObject.name == GameObjectName)
                return abi;
        }
        return null;
    }

    // Update is called once per frame
    void Update () {
        if (abiRef != null)
        {
            Refresh();
        }
    }
    protected virtual void Refresh()
    {
        //animator.SetBool("isAvailable", abiRef.isAvailable);
        if (abiRef.cooldown != null)
        {
            animator.SetFloat("CooldownScale", abiRef.cooldown.UIscale);
            animator.SetInteger("Stack", abiRef.cooldown.stack.currentStack);

            //string
            float remaningTime = abiRef.cooldown.UIremaining;
            string result = "";
            if (remaningTime > 2)
            {
                result = ((int)remaningTime).ToString();
            }
            else
            {
                if (remaningTime == 0)
                    result = "";
                else
                {
                    result = remaningTime.ToString("f1");
                }
            }
            text.text = result;
        }
        if (abiRef.energy != null)
        {
            animator.SetBool("isAvailable", abiRef.energy.abiRefCastCondition());
        }
    }
}
