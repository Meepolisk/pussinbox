﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUIA_Energy : HUIAddOn {

    [ReadOnly]
    public RPoint energy;

    public override void Setup(HUI _hui)
    {
        energy = mainRef.heroRef.energy;
    }

    // Update is called once per frame
    void Update () {
        animator.SetFloat("MPScale", energy.remainingScale);
    }
}
