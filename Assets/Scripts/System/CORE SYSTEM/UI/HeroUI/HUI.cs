﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HUI : MonoBehaviour
{
    protected List<HUIAddOn> HUIAs = new List<HUIAddOn>();

    protected Animator animator;
    protected Unit _heroRef;
    public Unit heroRef
    {
        get
        {
            return _heroRef;
        }
        protected set
        {
            _heroRef = value;
        }
    }

    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
    }
    protected virtual void Start()
    {
        //setup HUIAs
        foreach (var HUIA in GetComponentsInChildren<HUIAddOn>())
        {
            GetAddOns(HUIA);
        }
    }
    public virtual void Setup(PlayerController _controller)
    {
        heroRef = _controller.unitRef;
        if (heroRef == null) return;
        transform.SetParent(GameMaster.HUI_Handler.transform);
        GetComponent<RectTransform>().anchoredPosition = HeroUIHandler.DefaultPosition[heroRef.player - 1];
    }
    public void GetAddOns(HUIAddOn _addOn)
    {
        if (!HUIAs.Contains(_addOn))
        {
            HUIAs.Add(_addOn);
            _addOn.Setup(this);
        }
    }
}