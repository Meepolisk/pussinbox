﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class OUI_CapPoint : MonoBehaviour {

    public CapturePoint capPointRef;
    Animator animator;
    Text text;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        text = GetComponentInChildren<Text>();
    }
    public void Setup(CapturePoint _cappoint, short order)
    {
        capPointRef = _cappoint;
        RectTransform rect = GetComponent<RectTransform>();
        rect.anchoredPosition = ObjectUIHandler.DefaultPosition[order];
    }

    public int CaptureTime
    {
        set
        {
            text.text = value.ToString();
        }
    }
    public float ProcessScale
    {
        set
        {
            animator.SetFloat("ProcessScale", value);
        }
    }
    public bool Enabled
    {
        set
        {
            animator.SetBool("Enabled", value);
        }
    }
    public bool Captured
    {
        set
        {
            animator.SetBool("Captured", value);
        }
    }
    public bool Flashing
    {
        set
        {
            animator.SetBool("Flashing", value);
        }
    }
}
