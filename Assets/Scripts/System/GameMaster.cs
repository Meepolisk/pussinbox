﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-5000)]
public class GameMaster : MonoBehaviour
#if UNITY_EDITOR
    , hideTransform, lockTransform
#endif 
{
    #region SystemObjectRef
    public static HeroUIHandler HUI_Handler;
    public static ObjectUIHandler OUI_Handler;
#endregion

    public static bool isLoadingScene = true;
    [System.Serializable]
    public class DebugModeClass
    {
        public bool Region = false;
        public bool CameraZone = false;
        public bool Platform = false;
        public bool RGizmos = false;
    }
    public static bool DebugMode
    {
        get
        {
            return gameMaster.debugMode.Region || gameMaster.debugMode.CameraZone || gameMaster.debugMode.Platform || gameMaster.debugMode.RGizmos;
        }
    }
    public DebugModeClass debugMode;

    private static GameMaster gameMaster;
	private void Awake () {

        if (gameMaster == null)
        {
            DontDestroyOnLoad(gameObject);
            gameMaster = this;
        }
        else
        {
            Destroy(gameObject);
        }
        CullingMaskCamera();

        //setup thing
        //Physics2D.IsTouching()
    }
    IEnumerator WaitGameCheck()
    {
        isLoadingScene = true;
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(3);
        isLoadingScene = false;
        Time.timeScale = 1;
    }

    void CullingMaskCamera()
    {
        if (!debugMode.Region)
            Camera.main.cullingMask &= ~(1 << LayerMask.NameToLayer("Region"));
        if (!debugMode.CameraZone)
            Camera.main.cullingMask &= ~(1 << LayerMask.NameToLayer("CameraZone"));
        if (!debugMode.Platform)
            Camera.main.cullingMask &= ~(1 << LayerMask.NameToLayer("PlatformCollider"));
        if (!debugMode.RGizmos)
            Camera.main.cullingMask &= ~(1 << LayerMask.NameToLayer("RGizmos"));
    }
    
    public static void PlaySound(AudioClip _sound, float _vol = 1f)
    {
        GameObject newGO = Camera.main.gameObject.createEmptyChild(_sound.name);
        AudioSource AS = newGO.AddComponent<AudioSource>();
        AS.clip = _sound;
        AS.spatialBlend = 0f; //2D sound
        AS.Play();
        Destroy(newGO, AS.clip.length + 0.05f);
    }

    public static void PlayRandomSound(AudioClip[] _sounds, float _vol = 1)
    {
        if (_sounds != null && _sounds.Length > 0)
        {
            AudioClip audio = _sounds[Random.Range(0, _sounds.Length)];
            PlaySound(audio, _vol);
        }
    }
}
