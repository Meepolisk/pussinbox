﻿using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif
namespace RPlatform
{
    [System.Serializable]
    public class RPlatformLine
    {
        [HideInInspector]
        [SerializeField]
        private Transform transform;

        [SerializeField]
        private Vector2 start;
        public Vector2 Start
        {
            get
            {
                return start;
            }
            internal set
            {
                if (start == value)
                    return;

                start = value;
                //RefreshPolygonPoints();
            }
        }
        [SerializeField]
        private Vector2 end;
        public Vector2 End
        {
            get
            {
                return end;
            }
            internal set
            {
                if (end == value)
                    return;

                end = value;
                //RefreshPolygonPoints();
            }
        }
        [SerializeField]
        private bool dropable = false;
        public bool Dropable
        {
            get
            {
                return dropable;
            }
        }
        [SerializeField]
        private bool blockMissile = true;
        public bool BlockMissile
        {
            get
            {
                return blockMissile;
            }
        }
        public float Angle
        {
            get
            {
                float result = (start - end).Angle2D();
                return result;
            }
        }
        public float Length
        {
            get
            {
                return Vector2.Distance(start, end);
            }
        }
        const float platformWeight = 0.5f;
        private Vector2[] _polygonPoints = new Vector2[4];
        /// <summary>
        /// A collection of 4 vector2, which [0] is Start, [1] is End
        /// </summary>
        public Vector2[] PolygonPoint
        {
            get
            {
                return _polygonPoints;
            }
        }
        private void RefreshPolygonPoints()
        {
            _polygonPoints[0] = Start.Local2World(transform);
            _polygonPoints[1] = End.Local2World(transform);

            Vector2 vec = (_polygonPoints[1] - _polygonPoints[0]);
            if (Vector2.Distance(_polygonPoints[0], _polygonPoints[1]) > platformWeight * 2f)
            {
                Vector2 tmpVec = -vec.normalized * platformWeight;
                _polygonPoints[2] = _polygonPoints[1] + tmpVec + new Vector2(-tmpVec.y, tmpVec.x);
                tmpVec = vec.normalized * platformWeight;
                _polygonPoints[3] = _polygonPoints[0] + tmpVec + new Vector2(tmpVec.y, -tmpVec.x);
            }
            else
            {
                Vector2 tmpVec = _polygonPoints[0] + (vec / 2f);
                //RDebug.DrawPoint(tmpVec, 0.5f, Color.red, Time.deltaTime);
                tmpVec = tmpVec + new Vector2(vec.y, -vec.x).normalized * platformWeight;
                _polygonPoints[2] = _polygonPoints[3] = tmpVec;
            }
        }
#if UNITY_EDITOR
        
        public void InverseDirection()
        {
            Vector2 tmp = end;
            end = start;
            start = tmp;
            RefreshPolygonPoints();
        }
        [HideInInspector]
        public RPlatformPoint startPoint;
        [HideInInspector]
        public RPlatformPoint endPoint;
        public Color color
        {
            get
            {
                return Color.red;
            }
        }

        internal RPlatformLine(Transform _transform, RPlatformPoint _startPoint, RPlatformPoint _endPoint)
        {
            transform = _transform;
            startPoint = _startPoint;
            endPoint = _endPoint;
            

            RefreshPolygonPoints();
        }
        internal RPlatformLine(Transform _transform, Vector2 _start, Vector2 _end)
        {
            transform = _transform;
            start = _start;
            end = _end;

            RefreshPolygonPoints();
        }

        //const int toleranceIndex = 2;
        public void RefreshInfo(Vector2? _start = null, Vector2? _end = null)
        {
            Start = (_start != null) ? (Vector2)_start : startPoint.pos;
            End = (_end != null) ? (Vector2)_end : endPoint.pos;

            RefreshPolygonPoints();
        }
        
        //~RPlatformLine()
        //{
        //    if (startPoint.connectedPoints.Contains(endPoint))
        //        startPoint.connectedPoints.Remove(endPoint);

        //    if (endPoint.connectedPoints.Contains(startPoint))
        //        endPoint.connectedPoints.Remove(startPoint);
        //}
#endif
    }
}