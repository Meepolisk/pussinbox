﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
namespace RPlatform
{
    public partial class PlatformHandler : MonoBehaviour
    {
        internal class PointEditHelper : EditHelper<PlatformHandler>
        {
            internal PointEditHelper(PlatformHandler _handler) : base(_handler)
            {
                //base.EditHelper(_handler);
                SetupPointsOnLines();
                CreateRestorePoint();
            }
            ~PointEditHelper()
            {
                Debug.Log("PointEditHelper of destroyed");
            }

            private List<RPlatformPoint> selectedPoints;
            internal List<RPlatformPoint> SelectedPoints
            {
                get
                {
                    return selectedPoints;
                }
            }
            private List<RPlatformPoint> allPoints;
            internal List<RPlatformPoint> AllPoints
            {
                get
                {
                    return allPoints;
                }
            }

            //protected override void EnterEditMode()
            //{
            //    Debug.Log("PointEditHelper: EnterEditMode", handler.gameObject);
            //    SetupPointsOnLines();
            //}
            //protected override void LeaveEditMode()
            //{
            //    Debug.Log("PointEditHelper: LeaveEditMode", handler.gameObject);
            //    allPoints = null;
            //    selectedPoints = null;
            //}
            const float acceptabletolerance = 0.01f;
            private bool IsNearbyVector(Vector2 _vecA, Vector2 _vecB, float _acceptableDelta)
            {
                if (Vector2.Distance(_vecA, _vecB) <= _acceptableDelta)
                    return true;
                return false;
            }
            private RPlatformPoint GetNearby(List<RPlatformPoint> _listPoint, Vector2 _vec)
            {
                foreach (var pickedItem in _listPoint)
                {
                    if (IsNearbyVector(pickedItem.pos, _vec, acceptabletolerance))
                    {
                        return pickedItem;
                    }
                }
                return null;
            }

            private void SetupPointsOnLines()
            {
                Debug.Log("SetupPoints", handler.gameObject);
                selectedPoints = new List<RPlatformPoint>();
                allPoints = new List<RPlatformPoint>();
                List<RPlatformPoint> listPoints = new List<RPlatformPoint>();
                foreach (var item in handler.Lines)
                {
                    Vector2 vStart = item.Start;
                    RPlatformPoint pStart = GetNearby(listPoints, vStart);
                    if (pStart == null)
                    {
                        //Debug.Log("pStart true");
                        pStart = new RPlatformPoint(vStart);
                        listPoints.Add(pStart);
                    }
                    item.startPoint = pStart;

                    Vector2 vEnd = item.End;
                    RPlatformPoint pEnd = GetNearby(listPoints, vEnd);
                    if (pEnd == null)
                    {
                        //Debug.Log("pEnd true");
                        pEnd = new RPlatformPoint(vEnd);
                        listPoints.Add(pEnd);
                    }
                    item.endPoint = pEnd;
                }
                //Refresh PointList

                allPoints.Clear();

                foreach (var item in handler.Lines)
                {
                    if (!allPoints.Contains(item.startPoint))
                    {
                        allPoints.Add(item.startPoint);
                    }
                    if (!allPoints.Contains(item.endPoint))
                    {
                        allPoints.Add(item.endPoint);
                    }
                }
            }

            public void AddLine(RPlatformPoint _start, RPlatformPoint _end)
            {
                handler.Lines.Add(new RPlatformLine(handler.transform, _start, _end));
            }

            private void UpdateLinesInfo()
            {
                foreach (var item in handler.Lines)
                {
                    item.RefreshInfo();
                }
            }

            public void AddPoint(Vector2 _pos)
            {
                RPlatformPoint newPoint = new RPlatformPoint(_pos.World2Local(handler.transform));
                allPoints.Add(newPoint);
                if (SomePointIsSelected)
                {
                    foreach (var item in SelectedPoints)
                    {
                        Debug.Log("PointEditHelper: Append Point");
                        AddLine(newPoint, item);
                    }
                }
                else
                {
                    Debug.Log("PointEditHelper: Add Point");
                }
                Select(newPoint);
            }

            public void LineUpSelected()
            {
                handler.Lines.Add(new RPlatformLine(handler.transform, SelectedPoints[0], SelectedPoints[1]));
            }
            public void RemoveSelected()
            {
                List<RPlatformLine> lineToRemove = new List<RPlatformLine>();
                foreach (var item in SelectedPoints)
                {
                    allPoints.Remove(item);
                    foreach (var line in handler.Lines)
                    {
                        if (line.endPoint == item || line.startPoint == item)
                            lineToRemove.Add(line);
                    }
                }
                SelectedPoints.Clear();
                foreach (var item in lineToRemove)
                {
                    handler.Lines.Remove(item);
                }
            }
            internal enum NextClickAction
            {
                NONE, ADD, INSERT
            }

            #region hotkey
            private bool pressedCtrl = false;
            public bool Pressed_Ctrl
            {
                get
                {
                    return pressedCtrl;
                }
            }
            private bool pressedAlt = false;
            public bool Pressed_Alt
            {
                get
                {
                    return pressedAlt;
                }
            }
            const KeyCode ctrlHotKey = KeyCode.LeftControl;
            const KeyCode altHotKey = KeyCode.LeftAlt;
            const KeyCode escHotKey = KeyCode.Escape;
            void UpdateHotkeyState(Event e)
            {
                switch (e.keyCode)
                {
                    case (ctrlHotKey):
                        {
                            if (e.type == EventType.KeyDown)
                            {
                                pressedCtrl = true;
                            }
                            else if (e.type == EventType.KeyUp)
                            {
                                pressedCtrl = false;
                            }
                            break;
                        }
                    case (altHotKey):
                        {
                            if (e.type == EventType.KeyDown)
                            {
                                pressedAlt = true;
                            }
                            else if (e.type == EventType.KeyUp)
                            {
                                pressedAlt = false;
                                //refresh fakePos
                                foreach (var item in SelectedPoints)
                                {
                                    item.fakePos = item.pos;
                                }
                            }
                        }
                        break;
                    case (escHotKey):
                        {
                            if (e.type == EventType.KeyUp)
                            {
                                DoEscButton();
                            }
                        }
                        break;
                }
            }
            void DoEscButton()
            {
                if (nextClickAction != NextClickAction.NONE)
                {
                    nextClickAction = NextClickAction.NONE;
                    Debug.Log("NextClickAction Cancled");
                }
                else if (SelectedPoints.Count > 0)
                {
                    SelectedPoints.Clear();
                    dirty = true;
                }
            }
            #endregion

            private NextClickAction nextClickAction = NextClickAction.NONE;
            public bool IsWaitingClick
            {
                get
                {
                    return (nextClickAction != NextClickAction.NONE);
                }
            }

            #region button Function
            public bool CanBeAdd
            {
                get
                {
                    if (nextClickAction == NextClickAction.ADD)
                        return false;
                    return true;
                }
            }
            public bool CanBeDelete
            {
                get
                {
                    if (!SomePointIsSelected)
                        return false;
                    return true;
                }
            }
            public bool CanBeLineUp
            {
                get
                {
                    if (SelectedPoints.Count != 2)
                        return false;
                    foreach (var item in handler.Lines)
                    {
                        if (SelectedPoints.Contains(item.startPoint) && SelectedPoints.Contains(item.endPoint))
                            return false;
                    }
                    return true;
                }
            }

            public void PrepareToAdd()
            {
                nextClickAction = NextClickAction.ADD;
            }
            #endregion

            #region OPEN/CLOSE
            List<RPlatformLine> backupLines;
            private void CreateRestorePoint()
            {
                backupLines = new List<RPlatformLine>();
                foreach (var item in handler.Lines)
                {
                    backupLines.Add(new RPlatformLine(handler.transform, item.Start, item.End));
                }
            }

            public void Restore()
            {
                if (backupLines == null)
                    return;

                handler.Lines.Clear();
                foreach (var item in backupLines)
                {
                    handler.Lines.Add(item);
                }
            }
            #endregion

            public void RefreshState()
            {
                Event e = Event.current;
                UpdateHotkeyState(e);
                //Debug.Log(pressedCtrl);
            }

            public void Refresh()
            {
                UpdateLinesInfo();
                if (!SomePointIsSelected)
                    selectedMovingPoint = null;
            }

            public bool SomePointIsSelected
            {
                get
                {
                    return (selectedPoints.Count > 0);
                }
            }
            public bool PointIsSelected(RPlatformPoint _point)
            {
                if (selectedPoints.Contains(_point))
                    return true;
                return false;
            }

            public int Index(RPlatformPoint _point)
            {
                return (allPoints.IndexOf(_point));
            }

            public void Select(RPlatformPoint _point, bool forceCtrl = false)
            {
                if (!allPoints.Contains(_point))
                    return;

                if (Pressed_Ctrl || forceCtrl)
                {
                    if (selectedPoints.Contains(_point))
                    {
                        SelectedPoints_Remove(_point);
                    }
                    else
                    {
                        SelectedPoints_Add(_point);
                    }
                }
                else
                {
                    selectedPoints.Clear();
                    SelectedPoints_Add(_point);
                }
            }
            public void Select(List<RPlatformPoint> _points, bool forceCtrl)
            {
                if (!forceCtrl)
                    selectedPoints.Clear();

                foreach (var item in _points)
                {
                    if (!selectedPoints.Contains(item))
                        SelectedPoints_Add(item);
                }

                Dirty = true;
            }
            public bool Dirty = false;

            private void SelectedPoints_Add(RPlatformPoint _point)
            {
                Debug.Log("Add Point to selected points");
                ApplyColor_Point(_point);
                selectedPoints.Add(_point);
                RefreshSelectedList();
            }
            private void SelectedPoints_Remove(RPlatformPoint _point)
            {
                Debug.Log("Remove Point from selected points");
                selectedPoints.Remove(_point);
                RefreshSelectedList();
            }

            private void ApplyColor_Point(RPlatformPoint _point)
            {
                if (selectedPoints.Count == 0)
                    _point.ApplyColor(0.3f);
                else if (selectedPoints.Count == 1)
                {
                    float color = (selectedPoints[0].HueColor + 0.5f);
                    if (color > 1)
                        color = color - 1;
                    _point.ApplyColor(color);
                }
                else
                {
                    float result = 0f;
                    List<float> hues = new List<float>();
                    foreach (var item in selectedPoints)
                    {
                        hues.Add(item.HueColor);
                    }
                    hues.Sort();

                    float longest = 0f;
                    for (int i = 0; i < hues.Count - 1; i++)
                    {
                        float tmp = (hues[i + 1] - hues[i]);
                        if (tmp > longest)
                        {
                            longest = tmp;
                            result = hues[i] + tmp / 2f;
                        }
                    }
                    //for last and first
                    float a = hues[selectedPoints.Count - 1];
                    float b = hues[0] + 1f;
                    float tmp2 = (b - a);
                    if (tmp2 > longest)
                    {
                        longest = tmp2;
                        result = (a + tmp2 / 2f);
                    }

                    if (result > 1)
                        result = result - 1;

                    _point.ApplyColor(result);
                }
            }

            private void RefreshSelectedList()
            {
                if (selectedPoints.Count == 0)
                    return;

            }
            Vector2 alternatePos;
            public void MovingSelectedPoint(RPlatformPoint _mainPoint)
            {
                Vector2 firstPos = new Vector2(_mainPoint.fakePos.x, _mainPoint.fakePos.y);
                Vector2 target = Handles.PositionHandle(_mainPoint.fakePos, Quaternion.identity);
                _mainPoint.fakePos = target;
                if (Pressed_Alt)
                    _mainPoint.pos = GetClosedScaledPost(_mainPoint.fakePos, GetConnectedPoint(_mainPoint));
                else
                    _mainPoint.pos = target;
                Vector2 deltaPos = _mainPoint.pos - firstPos;

                if (SelectedPoints.Count > 1 && (deltaPos.x != 0 || deltaPos.y != 0))
                {
                    foreach (var item in SelectedPoints)
                    {
                        if (item != _mainPoint)
                        {
                            Vector2 targetPos = item.pos + deltaPos;
                            item.pos = targetPos;
                            item.fakePos = targetPos;
                        }
                    }
                }

            }
            private RPlatformPoint selectedMovingPoint;
            public RPlatformPoint SelectedMovingPoint
            {
                get
                {
                    return selectedMovingPoint;
                }
            }
            private void UpdateHandlePosition()
            {
                if (nextClickAction != NextClickAction.NONE)
                    return;

                Vector2 mousePos = MousePosition.World2Local(handler.transform);
                selectedMovingPoint = SelectedPoints[0];
                if (selectedPoints.Count > 1)
                {
                    float closestDistance = Vector2.Distance(selectedMovingPoint.pos, mousePos);
                    for (int i = 1; i < SelectedPoints.Count; i++)
                    {
                        float distance = Vector2.Distance(SelectedPoints[i].pos, mousePos);
                        if (distance < closestDistance)
                        {
                            closestDistance = distance;
                            selectedMovingPoint = SelectedPoints[i];
                        }
                    }
                }
            }
            public void ResetHandlePosition()
            {
                selectedMovingPoint = null;
            }
            public void MovingSelectedPoint()
            {
                //Handles.DrawLine(mousePos, _mainPoint.pos);

                Vector2 firstPos = new Vector2(selectedMovingPoint.pos.x, selectedMovingPoint.pos.y);
                Vector2 target = Handles.PositionHandle(selectedMovingPoint.fakePos, Quaternion.identity);
                selectedMovingPoint.fakePos = target;
                if (Pressed_Alt)
                    selectedMovingPoint.pos = GetClosedScaledPost(selectedMovingPoint.fakePos, GetConnectedPoint(selectedMovingPoint));
                else
                    selectedMovingPoint.pos = target;
                Vector2 deltaPos = selectedMovingPoint.pos - firstPos;

                if (SelectedPoints.Count > 1 && (deltaPos.x != 0 || deltaPos.y != 0))
                {
                    foreach (var item in SelectedPoints)
                    {
                        if (item != selectedMovingPoint)
                        {
                            Vector2 targetPos = item.pos + deltaPos;
                            item.pos = targetPos;
                            item.fakePos = targetPos;
                        }
                    }
                }
            }
            Vector2 GetClosedScaledPost(Vector2 _targetPos, List<RPlatformPoint> _comparePoint)
            {
                if (_comparePoint == null || _comparePoint.Count == 0)
                    return _targetPos;

                float shortedRange = 100f;
                Vector3 selectedPos = _targetPos;
                foreach (var item in GetScaledPos(_targetPos, _comparePoint))
                {
                    float range = Vector3.Distance(_targetPos, item);
                    if (range < shortedRange)
                    {
                        selectedPos = item;
                        shortedRange = range;
                    }
                }
                return selectedPos;
            }

            List<Vector2> GetScaledPos(Vector2 _targetPos, List<RPlatformPoint> _comparePoint)
            {
                List<Vector2> scaledVecs = new List<Vector2>();
                foreach (var item in _comparePoint)
                {
                    scaledVecs.Add(new Vector2(item.pos.x, _targetPos.y));
                    scaledVecs.Add(new Vector2(_targetPos.x, item.pos.y));
                }
                return scaledVecs;
            }

            public List<RPlatformPoint> GetConnectedPoint(RPlatformPoint _point)
            {
                //Debug.Log("GetConnectedPos", handler.gameObject);
                List<RPlatformPoint> result = new List<RPlatformPoint>();
                foreach (var item in handler.Lines)
                {
                    //if (isLegal(_point, item.startPoint))
                    //    result.Add(item.endPoint);
                    //if (isLegal(_point, item.endPoint))
                    //    result.Add(item.startPoint);
                    if (item.startPoint == _point && !SelectedPoints.Contains(item.endPoint))
                    {
                        result.Add(item.endPoint);
                    }
                    else if (item.endPoint == _point && !SelectedPoints.Contains(item.startPoint))
                    {
                        result.Add(item.startPoint);
                    }
                }
                return result;
            }
            private bool isLegal(RPlatformPoint _pointA, RPlatformPoint _pointB)
            {
                if (SelectedPoints.Contains(_pointB))
                    return false;
                if (_pointA != _pointB)
                    return false;
                return true;
            }

            #region Editor Event / Mouse Input
            public void OnMouseDown()
            {
                switch (nextClickAction)
                {
                    case (NextClickAction.NONE):
                        {
                            mouseDownPos = MousePosition;
                            break;
                        }
                    case (NextClickAction.ADD):
                        {
                            toAddPoint = true;
                            break;
                        }
                }
            }

            public void OnMouseDrag()
            {
            }
            public void OnMouseUp()
            {
                switch (nextClickAction)
                {
                    case (NextClickAction.ADD):
                        {
                            AddPoint(MousePosition);
                            break;
                        }
                    case (NextClickAction.NONE):
                        {
                            if (mouseDownPos != null)
                            {
                                Vector2 a = ((Vector2)mouseDownPos);
                                Vector2 b = MousePosition;
                                float top = (a.y > b.y) ? a.y : b.y;
                                float btm = (a.y < b.y) ? a.y : b.y;
                                float left = (a.x < b.x) ? a.x : b.x;
                                float right = (a.x > b.x) ? a.x : b.x;
                                Select(GetPointInRect(top, btm, left, right), Pressed_Ctrl);
                            }
                            break;
                        }
                }

                if (mouseDownPos != null)
                    mouseDownPos = null;
                if (toAddPoint)
                    toAddPoint = false;

                nextClickAction = NextClickAction.NONE;
            }
            public void OnMouseUpdate()
            {
                if (SomePointIsSelected && Tools.current == Tool.Move)
                {
                    UpdateHandlePosition();
                }
            }

            public void Update(float _deltaTime)
            {
            }
            public override void OnGUIUpdate()
            {
                switch (nextClickAction)
                {
                    case (NextClickAction.NONE):
                        {
                            if (mouseDownPos != null)
                            {
                                Handles.matrix = Matrix4x4.identity;

                                Vector2 a = (Vector2)mouseDownPos;
                                Vector2 c = MousePosition;

                                Vector2 center = new Vector2((a.x < c.x) ? a.x : c.x, (a.y < c.y) ? a.y : c.y);
                                Vector2 size = new Vector2(Mathf.Abs(a.x - c.x), Mathf.Abs(a.y - c.y));
                                Rect rect = new Rect(center, size);
                                Handles.DrawSolidRectangleWithOutline(rect, new Color(0.1f, 0.1f, 1, 0.10f), Color.white);
                            }
                            break;
                        }
                    case (NextClickAction.ADD):
                        {
                            if (toAddPoint)
                            {
                                DrawTargetLine();
                            }
                            break;
                        }
                }
            }
            #endregion
            private void DrawTargetLine()
            {
                Handles.matrix = handler.transform.localToWorldMatrix;
                Vector2 targetPos = MousePosition.World2Local(handler.transform);
                Debug.Log(targetPos);
                //draw Line
                if (SomePointIsSelected)
                {
                    if (Pressed_Alt)
                    {
                        targetPos = GetClosedScaledPost(targetPos, SelectedPoints);
                    }
                    float colorValue = 0.7f + (0.3f * REditorUtils.EditorBlinkParam);
                    Handles.color = new Color(0.3f, 1, 0.3f, colorValue);
                    foreach (var item in selectedPoints)
                    {
                        Handles.DrawLine(item.pos, targetPos);
                    }
                }

                //Handles.DrawLine(new Vector2(1,2), new Vector2 (2,2));
                //RDebug.DrawPoint(targetPos, 30f, Color.red, 0.2f);
                float pickSize = (HandleUtility.GetHandleSize(targetPos) * pointSize) * (0.3f + (0.7f * REditorUtils.EditorBlinkParam));
                Handles.DotHandleCap(GUIUtility.GetControlID(FocusType.Passive), targetPos, Quaternion.identity, pickSize, EventType.Repaint);
                //Debug.Log("asdasdsada");
            }

            private List<RPlatformPoint> GetPointInRect(float top, float btm, float left, float right)
            {
                List<RPlatformPoint> result = new List<RPlatformPoint>();
                foreach (var item in AllPoints)
                {
                    Vector2 pos = item.pos.Local2World(handler.transform);
                    if (pos.x > left && pos.x < right && pos.y < top && pos.y > btm)
                    {
                        result.Add(item);
                    }
                }
                return result;
            }
            private Vector2? mouseDownPos;
            private bool toAddPoint = false;
            
        }
    }
}

#endif