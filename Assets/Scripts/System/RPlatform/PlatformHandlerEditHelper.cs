﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
namespace RPlatform
{
    public partial class PlatformHandler : MonoBehaviour
    {
        public abstract class EditHelper<T> where T : Component
        {
            protected T handler;

            protected bool dirty = false;

            protected EditHelper(T _handler)
            {
                handler = _handler;
            }

            //protected abstract void EnterEditMode();

            //protected abstract void LeaveEditMode();

            protected Vector2 MousePosition
            {
                get
                {
                    return HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;
                }
            }

            public abstract void OnGUIUpdate();
        }
    }

    public static class REditorUtils
    {
        public static bool Hidden
        {
            get
            {
                return (bool)typeof(Tools).GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
            }
            set
            {
                typeof(Tools).GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, value);
            }
        }

        public static float EditorBlinkParam
        {
            get
            {
                return Mathf.Sin(Time.unscaledTime * 8f);
            }
        }
    }
}

#endif