﻿#if UNITY_EDITOR
using UnityEngine;
using System;
using UnityEditor;
namespace RPlatform
{
    public partial class PlatformHandler : MonoBehaviour
    {
        public const float pointSize = 0.08f;

        [CustomEditor(typeof(PlatformHandler))]
        public class PlatformHandlerEditor : Editor
        {
            const float lineSize = 0.05f;
            const float pointGUIButtonOffset = 30f;
            const float pointGUIButtonWidth = 50f;
            const float pointGUIVectorWidth = 30f;

            private PlatformHandler handler;
            private PointEditHelper pointHelper
            {
                get
                {
                    return handler.pointHelper;
                }
            }
            private LineEditHelper lineHelper
            {
                get
                {
                    return handler.lineHelper;
                }
            }
            private bool isActive
            {
                get
                {
                    return (handler.IsEditMode);
                }
                //set
                //{
                //    handler.IsEditMode_LINE = value;
                //}
            }

            private void OnEnable()
            {
                EditorApplication.update += Update;
                handler = target as PlatformHandler;
            }

            private void OnDisable()
            {
                EditorApplication.update -= Update;
                REditorUtils.Hidden = false;
                LEAVE_EDIT_POINT(true);
            }
            double lastTime;
            private void Update()
            {
                float deltaTime = (float)(EditorApplication.timeSinceStartup - lastTime);
                //Debug.Log(deltaTime);
                if (pointHelper != null)
                {
                    pointHelper.Update(deltaTime);
                }

                lastTime = EditorApplication.timeSinceStartup;
            }

            #region Edit Save region
            private void ENTER_EDIT_POINT()
            {
                handler.IsEditMode_POINT = true;
            }
            private void ENTER_EDIT_LINE()
            {
                handler.IsEditMode_LINE = true;
            }


            private void LEAVE_EDIT(bool _save)
            {
                if (handler.IsEditMode_LINE)
                {
                    LEAVE_EDIT_LINE();
                }
                else
                {
                    LEAVE_EDIT_POINT(_save);
                }
            }
            private void LEAVE_EDIT_POINT(bool _save)
            {
                if (!_save)
                {
                    pointHelper.Restore();
                }
                handler.IsEditMode_POINT = false;
            }
            private void LEAVE_EDIT_LINE()
            {
                //if (!_save)
                //{
                //    lineHelper.Restore();
                //}
                handler.IsEditMode_LINE = false;
            }

            private void DrawEditSaveCancelButtons()
            {
                EditorGUILayout.BeginHorizontal();
                if (!isActive)
                {
                    if (GUILayout.Button("EDIT POINTS"))
                    {
                        ENTER_EDIT_POINT();
                        return;
                    }
                    if (GUILayout.Button("EDIT LINES"))
                    {
                        ENTER_EDIT_LINE();
                        return;
                    }
                }
                else
                {
                    GUI.backgroundColor = Color.green;
                    if (GUILayout.Button("SAVE"))
                    {
                        LEAVE_EDIT(true);
                        return;
                    }
                    GUI.backgroundColor = Color.red;
                    if (GUILayout.Button("CANCEL"))
                    {
                        LEAVE_EDIT(false);
                        return;
                    }
                    GUI.backgroundColor = Color.white;
                }
                EditorGUILayout.EndHorizontal();
            }
            #endregion
            #region Custom Point Manager
            private void DrawCustomInspector_Infomation()
            {
                EditorGUILayout.BeginVertical();
                GUILayout.Label("Selected Points: ");
                if (!pointHelper.SomePointIsSelected)
                {
                    //GUILayout.Label("Nothing Is Selected");
                    GUILayout.Box("Nothing Is Selected");
                }
                else
                {
                    foreach (var item in pointHelper.SelectedPoints)
                    {
                        var rect = EditorGUILayout.BeginHorizontal();
                        GUILayout.Label("");
                        GUI.backgroundColor = Color.HSVToRGB(item.HueColor,1,0.95f);
                        GUIStyle tmpStyle = new GUIStyle(GUI.skin.button);
                        tmpStyle.normal.textColor = GUI.backgroundColor.InvertColorByHue(item.HueColor);
                        float x = pointGUIButtonOffset;
                        if (GUI.Button(new Rect(x, rect.y, pointGUIButtonWidth, rect.height), pointHelper.Index(item).ToString(),tmpStyle))
                        {
                            pointHelper.Select(item,true);
                            return;
                        }
                        //GUI.contentColor = Color.black;
                        GUI.backgroundColor = Color.white;
                        GUI.enabled = false;
                        x += pointGUIButtonWidth;
                        EditorGUI.FloatField(new Rect(x, rect.y, pointGUIVectorWidth, rect.height), item.pos.x);
                        x += pointGUIVectorWidth;
                        EditorGUI.FloatField(new Rect(x, rect.y, pointGUIVectorWidth, rect.height), item.pos.y);
                        GUI.enabled = true;
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.EndVertical();
            }
            private void DrawCustomInspector_PointButton()
            {
                EditorGUILayout.BeginHorizontal();
                GUI.enabled = (pointHelper.CanBeAdd);
                if (pointHelper.SomePointIsSelected)
                {
                    if (GUILayout.Button("Append Point"))
                    {
                        pointHelper.PrepareToAdd();
                    }
                }
                else
                {
                    if (GUILayout.Button("Add Point"))
                    {
                        pointHelper.PrepareToAdd();
                    }
                }

                GUI.enabled = (pointHelper.CanBeDelete);
                if (GUILayout.Button("Delete"))
                {
                    pointHelper.RemoveSelected();
                }
                GUI.enabled = (pointHelper.CanBeLineUp);
                if (GUILayout.Button("Line Up"))
                {
                    pointHelper.LineUpSelected();
                }
                EditorGUILayout.EndHorizontal();
                GUI.enabled = true;
            }
            #endregion
            public override void OnInspectorGUI()
            {
                DrawEditSaveCancelButtons();

                if (!isActive)
                {
                    DrawDefaultInspector();
                    return;
                }
                if (handler.IsEditMode_POINT)
                {
                    pointHelper.RefreshState();
                    DrawCustomInspector_Infomation();
                    DrawCustomInspector_PointButton();
                }
                if (handler.IsEditMode_LINE)
                {

                }
            }

            void OnSceneGUI()
            {
                if (!isActive || handler == null)
                    return;
                
                Handles.matrix = handler.transform.localToWorldMatrix;
                if (handler.IsEditMode_POINT)
                {
                    DrawEditPoint();
                }
                else
                {
                    DrawEditLine();
                }
                //Debug.Log(handler.Lines[0].Angle);
            }

            #region point draw
            private void DrawEditPoint()
            {
                REditorUtils.Hidden = pointHelper.SomePointIsSelected;
                Event e = Event.current;

                pointHelper.Refresh();
                pointHelper.RefreshState();
                //bool selected = false;
                foreach (var point in pointHelper.AllPoints)
                {
                    lastCheckedPoint = point;
                    //hsize = ;
                    float pickSize = HandleUtility.GetHandleSize(point.pos) * pointSize;
                    if (Handles.Button(point.pos, Quaternion.identity, pickSize, pickSize, DrawPoint))
                    {
                        pointHelper.Select(point);
                    }
                }

                if (pointHelper.SelectedMovingPoint != null && Tools.current == Tool.Move)
                {
                    pointHelper.MovingSelectedPoint();
                }

                if (e.isMouse)
                {
                    if (e.button == 0)
                    {
                        switch (e.type)
                        {
                            case EventType.MouseDown:
                                Editor_MouseDown();
                                break;
                            case EventType.MouseDrag:
                                Editor_MouseDrag();
                                break;
                            case EventType.MouseUp:
                                Editor_MouseUp();
                                break;
                        }
                    }
                    Editor_MouseUpdate();
                }

                if (pointHelper.SomePointIsSelected || pointHelper.Dirty)
                {
                    Repaint();
                    SceneView.RepaintAll();
                    GUIUtility.keyboardControl = 0;
                    pointHelper.Dirty = false;
                }

                DisableSelection(e);
                pointHelper.OnGUIUpdate();
            }

            RPlatformPoint lastCheckedPoint = null;
            void DrawPoint(int controlID, Vector3 position, Quaternion rotation, float size, EventType eventType)
            {
                float colorValue = 0.95f + (0.05f * REditorUtils.EditorBlinkParam);
                bool isSelected = pointHelper.PointIsSelected(lastCheckedPoint);
                if (isSelected)
                {
                    Handles.color = Color.HSVToRGB(lastCheckedPoint.HueColor,1f, colorValue);
                }
                else
                {
                    Handles.color = new Color(colorValue, colorValue, colorValue, 1);
                }
                Handles.DotHandleCap(controlID, position, Quaternion.identity, size, eventType);
                string label = "";
                if (pointHelper.Pressed_Ctrl)
                {
                    if (isSelected)
                    {
                        label = "-";
                    }
                    else
                    {
                        label = "+";
                    }
                }
                else
                {
                    label = pointHelper.Index(lastCheckedPoint).ToString();
                }
                Handles.Label(position, label);
                lastCheckedPoint = null;
            }
            #endregion

            #region draw line
            private void DrawEditLine()
            {
                REditorUtils.Hidden = lineHelper.SomeLineIsSelected;
                Event e = Event.current;

                //lineHelper.Refresh();
                //lineHelper.RefreshState();
                ////bool selected = false;
                //foreach (var point in lineHelper.AllPoints)
                //{
                //    lastCheckedPoint = point;
                //    //hsize = ;
                //    float pickSize = HandleUtility.GetHandleSize(point.pos) * pointSize;
                //    if (Handles.Button(point.pos, Quaternion.identity, pickSize, pickSize, DrawPoint))
                //    {
                //        lineHelper.Select(point);
                //    }
                //}

                //if (lineHelper.SelectedMovingPoint != null && Tools.current == Tool.Move)
                //{
                //    lineHelper.MovingSelectedPoint();
                //}

                //if (e.isMouse)
                //{
                //    if (e.button == 0)
                //    {
                //        switch (e.type)
                //        {
                //            case EventType.MouseDown:
                //                Editor_MouseDown();
                //                break;
                //            case EventType.MouseDrag:
                //                Editor_MouseDrag();
                //                break;
                //            case EventType.MouseUp:
                //                Editor_MouseUp();
                //                break;
                //        }
                //    }
                //    Editor_MouseUpdate();
                //}

                //if (lineHelper.SomePointIsSelected || lineHelper.Dirty)
                //{
                //    Repaint();
                //    SceneView.RepaintAll();
                //    GUIUtility.keyboardControl = 0;
                //    lineHelper.Dirty = false;
                //}
                
                DisableSelection(e);
                lineHelper.OnGUIUpdate();
            }
            #endregion

            #region mouse call
            private Vector2 GetMousePos()
            {
                return HandleUtility.GUIPointToWorldRay(Event.current.mousePosition).origin;
            }

            private void Editor_MouseDown()
            {
                pointHelper.OnMouseDown();
                //Debug.Log("Editor: MouseDown");
            }
            private void Editor_MouseDrag()
            {
                pointHelper.OnMouseDrag();
                //Debug.Log("Editor: MouseDrag");
            }
            private void Editor_MouseUp()
            {
                pointHelper.OnMouseUp();
                //Debug.Log("Editor: MouseUp");
                if (GUI.changed)
                {
                    EditorUtility.SetDirty(handler);
                    Debug.Log("Repainted");
                    Repaint();

                    Undo.RecordObject(handler, "Platform Handler: point changed");
                    Debug.Log("Editor: MouseUp - GUI Changed, set dirty");
                }
            }
            private void Editor_MouseUpdate()
            {
                pointHelper.OnMouseUpdate();
                //Debug.Log("Editor: MouseUpdate");
            }
            #endregion
            
            private void DisableSelection(Event e)
            {
                // disable selection: get controlid to avoid messing with other tools
                int controlId = GUIUtility.GetControlID(FocusType.Passive);
                if (e.isMouse && e.button == 0 && e.type == EventType.MouseDown)
                {
                    GUIUtility.hotControl = controlId;
                    Event.current.Use();
                }
            }
        }
    }
}

#endif