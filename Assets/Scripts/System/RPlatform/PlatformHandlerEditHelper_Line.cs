﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
namespace RPlatform
{
    public partial class PlatformHandler : MonoBehaviour
    {
        internal class LineEditHelper : EditHelper<PlatformHandler>
        {
            internal LineEditHelper(PlatformHandler _handler) : base(_handler)
            {
                CreateRestorePoint();
            }
            
            private List<RPlatformPoint> selectedPoints;
            internal List<RPlatformPoint> SelectedPoints
            {
                get
                {
                    return selectedPoints;
                }
            }

            List<RPlatformLine> backupLines;
            private void CreateRestorePoint()
            {
                backupLines = new List<RPlatformLine>();
                foreach (var item in handler.Lines)
                {
                    backupLines.Add(new RPlatformLine(handler.transform, item.Start, item.End));
                }
            }
            //protected override void EnterEditMode()
            //{
            //}

            //protected override void LeaveEditMode()
            //{
            //}
            public bool SomeLineIsSelected
            {
                get
                {
                    return (true);
                }
            }
            public override void OnGUIUpdate()
            {

            }
        }
    }
}
#endif