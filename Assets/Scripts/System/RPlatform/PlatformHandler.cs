﻿using System.Collections.Generic;
using UnityEngine;

namespace RPlatform
{
    public partial class PlatformHandler : MonoBehaviour
    {
        public List<RPlatformLine> Lines = new List<RPlatformLine>();


#if UNITY_EDITOR

        internal PointEditHelper pointHelper = null;
        internal LineEditHelper lineHelper = null;
        public bool IsEditMode_POINT
        {
            get
            {
                return (pointHelper != null);
            }
            set
            {
                if (IsEditMode_POINT == value)
                    return;

                if (value)
                {
                    if (IsEditMode_LINE == true)
                        IsEditMode_LINE = false;
                    pointHelper = new PointEditHelper(this);
                }
                else
                    pointHelper = null;
            }
        }
        public bool IsEditMode_LINE
        {
            get
            {
                return (lineHelper != null);
            }
            set
            {
                if (IsEditMode_LINE == value)
                    return;

                if (value)
                {
                    if (IsEditMode_POINT == true)
                        IsEditMode_POINT = false;
                    lineHelper = new LineEditHelper(this);
                }
                else
                    lineHelper = null;
            }
        }
        public bool IsEditMode
        {
            get
            {
                return (pointHelper != null || lineHelper != null);
            }
            //set
            //{
            //    if (IsEditMode == value)
            //        return;

            //    if (value)
            //        EnterEditMode();
            //    else
            //        LeaveEditMode();
            //}
        }
        private void LeaveEditMode ()
        {
            if (IsEditMode_LINE)
                IsEditMode_LINE = false;
            if (IsEditMode_POINT)
                IsEditMode_POINT = false;
        }
        //private void EnterEditMode()
        //{
        //}
        //private void LeaveEditMode()
        //{
        //}

        private void OnDrawGizmos()
        {
            DrawGizmos(false);
        }

        private void OnDrawGizmosSelected()
        {
            DrawGizmos(true);
        }

        private Color polygonColor = new Color(0f, 1f, 0f, 0.5f);
        private void DrawGizmos(bool _selected = false)
        {
            foreach (var item in Lines)
            {
                Vector2[] polygonPoints = item.PolygonPoint;

                Gizmos.color = Color.red; //item.color;
                Gizmos.DrawLine(polygonPoints[0], polygonPoints[1]);

                if (_selected && !IsEditMode_POINT)
                {
                    Gizmos.color = polygonColor;
                    Gizmos.DrawLine(polygonPoints[2], polygonPoints[3]);
                    Gizmos.DrawLine(polygonPoints[0], polygonPoints[3]);
                    Gizmos.DrawLine(polygonPoints[1], polygonPoints[2]);
                }
            }
        }
        private void Reset()
        {
            if (IsEditMode)
            {
                LeaveEditMode();
            }

            Vector2 _vec1 = new Vector2(0, 0);
            Vector2 _vec2 = new Vector2(0, 1);
            Vector2 _vec3 = new Vector2(1, 1);
            
            Lines.Add(new RPlatformLine(transform, _vec1, _vec2));
            Lines.Add(new RPlatformLine(transform, _vec2, _vec3));

        }
#endif
    }

    public static partial class ExtensionMethods
    {
        public static Vector2 Local2World(this Vector2 vec, Transform _transform)
        {
            return new Vector2(
                (vec.x * _transform.lossyScale.x) + _transform.position.x,
                (vec.y * _transform.lossyScale.y) + _transform.position.y);
        }
        public static Vector2 World2Local(this Vector2 vec, Transform _transform)
        {
            return new Vector2(
                (vec.x / _transform.lossyScale.x) - _transform.position.x,
                (vec.y / _transform.lossyScale.y) - _transform.position.y);
        }
        public static Color InvertColorByHue(this Color _color, float _hue)
        {
            return Color.HSVToRGB((_hue + 0.5f) % 1f, 1f, 1f);
        }
        public static float Angle2D(this Vector2 _a, Vector2 _b)
        {
            float result = Vector2.Angle(_b - _a, Vector2.right);
            if (_b.y < _a.y)
                result *= -1f;
            return result;
        }
        public static float Angle2D(this Vector2 _vec)
        {
            float result = Vector2.Angle(_vec, Vector2.right);
            if (_vec.y < 0f)
                result *= -1f;
            return result;
        }
    }

}