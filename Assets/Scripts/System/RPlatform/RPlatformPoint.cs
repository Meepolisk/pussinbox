﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;

namespace RPlatform
{
    [System.Serializable]
    public class RPlatformPoint
    {
        public Vector2 pos;
        [HideInInspector]
        public Vector2 fakePos;
        public float HueColor = 0f;

        public RPlatformPoint(Vector2 _pos)
        {
            pos = _pos;
            fakePos = _pos;
        }

        public void ApplyColor (float _hue)
        {
            HueColor = _hue;
            //Debug.Log("Color = "+ _hue);
        }
    }
#endif
}