﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dirox.Localization
{
    [DefaultExecutionOrder(-100)]
    public class LocalizationHandler : SingletonBase<LocalizationHandler>
    {
        [SerializeField]
        private LocalizedDataManager scriptableObjectRef;

        public static LocalizedDataManager Source
            => Instance.scriptableObjectRef;
    }
}
