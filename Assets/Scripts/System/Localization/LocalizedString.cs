﻿//using UnityEngine;
//#if UNITY_EDITOR
//using UnityEditor;
//#endif

//namespace Dirox.Localization
//{
//    [System.Serializable]
//    public class LocalizedString
//    {
//        [SerializeField]
//        private LocalizedDataManager chachacha;

//    }

//#if UNITY_EDITOR
//    [CustomPropertyDrawer(typeof(LocalizedString))]
//    public class RPointInspector : PropertyDrawer
//    {

//        // Draw the property inside the given rect
//        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//        {
//            EditorGUI.BeginProperty(position, label, property);
//            // Draw label
//            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

//            // Don't make child fields be indented
//            var indent = EditorGUI.indentLevel;
//            EditorGUI.indentLevel = 0;

//            // Calculate rects
//            var rect1 = new Rect(position.x, position.y, (position.width / 2) - 5, position.height);
//            var rect2 = new Rect(position.x + (position.width / 2) - 5, position.y, 10, position.height);
//            var rect3 = new Rect(position.x + (position.width / 2) + 5, position.y, (position.width / 2) - 5, position.height);

//            EditorGUI.PropertyField(rect1, property.FindPropertyRelative("_current"), GUIContent.none);
//            GUI.Label(rect2, "/");
//            EditorGUI.PropertyField(rect3, property.FindPropertyRelative("_max"), GUIContent.none);

//            // Set indent back to what it was
//            EditorGUI.indentLevel = indent;

//            EditorGUI.EndProperty();
//        }
//    }
//#endif
//}