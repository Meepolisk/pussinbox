﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityObject = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace Dirox.Localization
{
    [CreateAssetMenu(menuName = "Localized Data Manager")]
    public class LocalizedDataManager : ScriptableObject
    {
        private int _defaultLanguageIndex = 0;
        public int defaultLanguageIndex
        {
            get
            {
                return _defaultLanguageIndex;
            }
            private set
            {
                if (_defaultLanguageIndex == value)
                    return;

                _defaultLanguageIndex = value;
                DefaultLanguageChangeNoti();
            }
        }
        public string defaultLanguageID
        {
            get
            {
                return languageID[defaultLanguageIndex];
            }
            set
            {
                if (languageID.Contains(value))
                {
                    defaultLanguageIndex = languageID.IndexOf(value);
                    SaveDefaultLanguage();
                    return;
                }
                Debug.LogError("Couldnt found language id, change to " + languageID[0]);
                defaultLanguageIndex = 0;
            }
        }

        [SerializeField]
        internal List<string> languageID;

        [SerializeField]
        internal StringDictionary stringLD = new StringDictionary();
        [SerializeField]
        internal TextureDictionary textureLD = new TextureDictionary();

        private void DeserializeDictionaries()
        {
            stringLD.SaveAndDeserialize();
            textureLD.SaveAndDeserialize();
        }

        private void DefaultLanguageChangeNoti()
        {
            if (onChangeDefaultLanguage != null)
            {
                onChangeDefaultLanguage();
            }
        }
        private const string defaultLanguageCode = "DefaultLocalizedLanguage";
        private void SaveDefaultLanguage()
        {
            PlayerPrefs.SetString(defaultLanguageCode, defaultLanguageID);
            Debug.Log("Default Language saved: " + defaultLanguageID);
        }
        private void LoadDefaultLanguage()
        {
            if (!PlayerPrefs.HasKey(defaultLanguageCode))
            {
                SaveDefaultLanguage();
                return;
            }
            defaultLanguageID = PlayerPrefs.GetString(defaultLanguageCode);
        }

        #region UnityCall

        private void Reset()
        {
            Debug.Log("LocalizedDataManager init");
            stringLD.Setup(this);
            textureLD.Setup(this);

            languageID = new List<string>();
            languageID.Add("English");
            languageID.Add("Japanese");

            stringLD.Reset(languageID, new List<string> { "stringA", "stringB", "stringC" });
            textureLD.Reset(languageID, new List<string> { "textureA", "textureB" });
        }

        private void OnEnable()
        {
            Debug.Log("LocalizedDataManager awaking...");
            LoadDefaultLanguage();
        }

        #endregion

        #region public call
        #region event
        public event Action onChangeDefaultLanguage;
        #endregion

        public LocalizationDictionary<T> GetLocalizationDictionary<T>()
        {
            LocalizationDictionary<T> output;
            if (typeof(T) == typeof(string))
            {
                output = stringLD as LocalizationDictionary<T>;
            }
            else
            {
                output = textureLD as LocalizationDictionary<T>;
            }
            if (output.isSerialized == false)
            {
                output.SerializeToDictionary();
            }
            return output;
        }

        public void AddLanguage(string _language)
        {

        }
        public void RemoveLanguage(string _language)
        {

        }

        #region DataExtract
        private string SafeLanguageID(string _languageKey)
        {
            if (_languageKey == string.Empty)
                return defaultLanguageID;
            return _languageKey;
        }
        public T GetData<T>(string _languageKey, string _idKey)
        {
            var localizedDict = GetLocalizationDictionary<T>();
            try
            {
                return localizedDict.dict[SafeLanguageID(_languageKey)][_idKey];
            }
            catch
            {
                return localizedDict.DefaultData;
            }
        }
        public void SetData<T>(string _languageKey, string _idKey, T _newValue)
        {
            var localizedDict = GetLocalizationDictionary<T>();

            localizedDict.dict[SafeLanguageID(_languageKey)][_idKey] = _newValue;
        }
        public List<string> GetIDList<T>()
        {
            var localizedDict = GetLocalizationDictionary<T>();
            return localizedDict.keyIDs;
        }
        #endregion
        #endregion

        [System.Serializable]
        public class StringDictionary : LocalizationDictionary<String>
        {
            internal override string DefaultData
            {
                get
                {
                    return string.Empty;
                }
            }
        }
        [System.Serializable]
        public class TextureDictionary : LocalizationDictionary<Texture>
        {
            internal override Texture DefaultData
            {
                get
                {
                    return null;
                }
            }
        }

        [System.Serializable]
        public abstract class LocalizationDictionary<T>
        {
            [SerializeField]
            protected LocalizedDataManager handler;

            public List<string> keyIDs;
            public List<T> dataSet;

            public Dictionary<string, Dictionary<string, T>> dict;

            internal bool isSerialized
            {
                get
                {
                    return (dict != null);
                }
            }
            internal Type dataType
            {
                get
                {
                    return typeof(T);
                }
            }

            public void Setup(LocalizedDataManager _handler)
            {
                handler = _handler;
            }

            internal abstract T DefaultData { get; }
            public void Reset(List<string> _keyLanguageDefault, List<string> _keyIDDefault)
            {
                keyIDs = _keyIDDefault;
                dataSet = new List<T>();
                foreach (var itemLanguage in _keyLanguageDefault)
                {
                    foreach (var item in keyIDs)
                    {
                        dataSet.Add(DefaultData);
                    }
                }
                CheckToSerialize();
            }

            internal void SerializeToDictionary()
            {
                Debug.Log("SerializeToDictionary " + this.GetType().Name + "...");
                dict = new Dictionary<string, Dictionary<string, T>>();
                int dataSetIndex = 0;
                for (int i = 0; i < handler.languageID.Count; i++)
                {
                    var curDict = new Dictionary<string, T>();
                    dict.Add(handler.languageID[i], curDict);
                    for (int j = 0; j < keyIDs.Count; j++)
                    {
                        curDict.Add(keyIDs[j], dataSet[dataSetIndex]);
                        dataSetIndex++;
                    }
                }
            }
            internal void CheckToSerialize()
            {
                if (!isSerialized)
                    SerializeToDictionary();
            }
            public void SaveAndDeserialize()
            {
                CheckToSerialize();
                Debug.Log("Deserialize and Save Data for " + this.GetType().Name + "...");

                dataSet = new List<T>();
                for (int i = 0; i < handler.languageID.Count; i++)
                {
                    string languageID = handler.languageID[i];
                    for (int j = 0; j < keyIDs.Count; j++)
                    {
                        string keyID = this.keyIDs[j];
                        dataSet.Add(dict[languageID][keyID]);
                    }
                }
            }
            #region public call

            internal void AddNewID(string _key)
            {
                CheckToSerialize();

                Debug.Log("Add " + _key);
                keyIDs.Add(_key);
                foreach (var keyDict in dict)
                {
                    keyDict.Value.Add(_key, DefaultData);
                }
            }
            internal void RemoveOldID(string _key)
            {
                CheckToSerialize();

                if (!isSerialized)
                    return;

                foreach (var keyDict in dict)
                {
                    keyDict.Value.Remove(_key);
                }
                keyIDs.Remove(_key);
            }
            internal void ChangeID(string _oldID, string _newID)
            {
                if (_oldID == _newID)
                    return;

                Debug.Log("Change " + _oldID + " to " + _newID);
                foreach (var langName in handler.languageID)
                {
                    //T temp = dict[langName][_oldID];
                    dict[langName].Add(_newID, dict[langName][_oldID]);
                    dict[langName].Remove(_oldID);
                }
                keyIDs.Add(_newID);
                keyIDs.Remove(_oldID);
            }

            internal T GetData(string _language, string _key)
            {
                try
                {
                    return dict[handler.SafeLanguageID(_language)][_key];
                }
                catch
                {
                    return DefaultData;
                }
            }
            #endregion
        }


#if UNITY_EDITOR
        [CustomEditor(typeof(LocalizedDataManager))]
        public class LocalizationEditor : UnityObjectEditor<LocalizedDataManager>
        {

            protected override void OnEnable()
            {
                base.OnEnable();
                editHelper = new EditHelper<string>(this);
            }
            protected void OnDisable()
            {
                handler.DeserializeDictionaries();
            }

            public override void OnInspectorGUI()
            {
                //DrawDefaultInspector();

                //Draw Header
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Localized Data", EditorStyles.centeredGreyMiniLabel);
                EditorGUILayout.EndHorizontal();

                DrawDataTypeSelector();
                DrawDetailDictionary();

                //EditorUtility.SetDirty(handler);
            }
            private int _currentDataTypeIndex = 0;
            private int currentDataTypeIndex
            {
                get
                {
                    return _currentDataTypeIndex;
                }
                set
                {
                    if (_currentDataTypeIndex == value)
                        return;
                    _currentDataTypeIndex = value;

                    editHelper = null;
                    switch (_currentDataTypeIndex)
                    {
                        case 0:
                            editHelper = new EditHelper<string>(this);
                            break;
                        case 1:
                            editHelper = new EditHelper<Texture>(this);
                            break;
                    }
                }
            }
            private void DrawDataTypeSelector()
            {
                EditorGUILayout.BeginHorizontal();

                bool isString = (currentDataTypeIndex == 0);
                isString = GUILayout.Toggle(isString, "String", EditorStyles.miniButtonLeft);
                if (isString)
                {
                    currentDataTypeIndex = 0;
                }

                bool isTexture = (currentDataTypeIndex == 1);
                isTexture = GUILayout.Toggle(isTexture, "Texture", EditorStyles.miniButtonRight);
                if (isTexture)
                {
                    currentDataTypeIndex = 1;
                }
                EditorGUILayout.EndHorizontal();
            }
            #region Draw ReorderableList
            private EditHelper editHelper;
            private bool isEditMode
            {
                get
                {
                    return (editHelper != null);
                }
            }

            public abstract class EditHelper
            {
                protected LocalizationEditor handler;
                protected List<string> idList;
                protected string filter = "";
                public ReorderableList reorderList;

                //private struct ReorderableData
                //{
                //    string idList;
                //    uint
                //}

                public abstract void RefreshFilter(string _filter);
                public abstract void DrawItemValue(string _language);
                internal abstract void DrawingStuff();
            }
            public class EditHelper<T> : EditHelper
            {
                private LocalizationDictionary<T> localizeDictionaryRef;

                public EditHelper(LocalizationEditor _handler)
                {
                    _currentSelectedLanguageIndex = 0;
                    handler = _handler;
                    localizeDictionaryRef = handler.handler.GetLocalizationDictionary<T>();
                    idList = new List<string>();
                    RefreshFilter(filterText);
                    reorderList = new ReorderableList(idList, typeof(T), false, false, false, false);
                    reorderList.drawElementCallback = (rect, index, isActive, isFocused) =>
                    {
                        rect.y += 2;
                        string keyID = idList[index];
                        Dictionary<string, T> currentDictionary = localizeDictionaryRef.dict[SelectedLanguage];

                        float dataHeight = EditorGUIUtility.singleLineHeight;
                        Rect keyRect = new Rect(rect.x, rect.y, rect.width * 0.4f, EditorGUIUtility.singleLineHeight);
                        Rect valueRect = new Rect(rect.x + rect.width * 0.4f + 5f, rect.y, rect.width * 0.6f - 5f, dataHeight);


                        EditorGUI.LabelField(keyRect, keyID);
                        if (isActive)
                        {
                            currentDictionary[keyID] = DoField(valueRect, typeof(T), currentDictionary[keyID]);
                        }
                        else
                        {
                            GUI.enabled = false;
                            DoField<T>(valueRect, typeof(T), currentDictionary[keyID]);
                            GUI.enabled = true;
                        }
                    };
                    reorderList.onSelectCallback = (list) =>
                    {
                        Edit_Select(idList[list.index]);
                    };
                }
                internal void AddRecord()
                {
                    reorderList.index = -1;
                    Edit_Select();
                }
                internal void RemoveRecord()
                {
                    string removedKey = idList[reorderList.index];
                    idList.RemoveAt(reorderList.index);
                    localizeDictionaryRef.RemoveOldID(removedKey);
                }

                public override void RefreshFilter(string _filter)
                {
                    Debug.Log("Apply refresh " + _filter);

                    filter = _filter;
                    idList.Clear();
                    if (string.IsNullOrEmpty(filter))
                    {
                        idList.AddRange(localizeDictionaryRef.keyIDs);
                    }
                    else
                    {
                        foreach (var itemID in localizeDictionaryRef.keyIDs)
                        {
                            if (itemID.Contains(filter))
                            {
                                idList.Add(itemID);
                            }
                        }
                    }
                    if (reorderList != null)
                        reorderList.index = -1;
                    Edit_Select();
                }
                private bool addMode
                {
                    get
                    {
                        return string.IsNullOrEmpty(selectedID);
                    }
                    set
                    {
                        selectedID = "";
                    }
                }
                private string selectedID = "";
                private string editingID = "";
                private Dictionary<string, T> editingValue;
                public override void DrawItemValue(string _languageID)
                {
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.LabelField(_languageID, EditorStyles.centeredGreyMiniLabel);
                    DoFieldGUILayout<T>(localizeDictionaryRef.GetData(_languageID, selectedID), GUILayout.Height(100f));
                    EditorGUILayout.EndVertical();
                }

                Vector2 scrollPos;
                internal override void DrawingStuff()
                {
                    DrawSelectRegion();
                    DrawEditRegion();
                }
                internal void DrawSelectRegion()
                {
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    DrawLanguageSelector();
                    DrawSearchFilter();
                    scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(300f));
                    reorderList.DoLayoutList();
                    EditorGUILayout.EndScrollView();
                    DrawAddRemoveButton();
                    EditorGUILayout.EndVertical();
                }

                private int _currentSelectedLanguageIndex { get; set; }
                private string SelectedLanguage
                {
                    get
                    {
                        if (handler == null)
                            return "";
                        return handler.handler.languageID[_currentSelectedLanguageIndex];
                    }
                }
                private void DrawLanguageSelector()
                {
                    if (_currentSelectedLanguageIndex < 0)
                        _currentSelectedLanguageIndex = 0;

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Language preview: ");
                    _currentSelectedLanguageIndex = EditorGUILayout.Popup(_currentSelectedLanguageIndex, handler.handler.languageID.ToArray());
                    if (GUILayout.Button("Edit"))
                    {
                        EditorUtility.DisplayDialog("Sorry!", "This feature is under construction", "ok", "okay!");
                    }
                    EditorGUILayout.EndHorizontal();
                }
                string filterText = "";
                private void DrawSearchFilter()
                {
                    EditorGUILayout.BeginHorizontal();
                    filterText = EditorGUILayout.TextField(filterText);
                    if (GUILayout.Button("Filter"))
                    {
                        RefreshFilter(filterText);
                    }
                    EditorGUILayout.EndHorizontal();
                }
                internal void DrawAddRemoveButton()
                {
                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button("Add"))
                    {
                        AddRecord();
                    }
                    GUI.enabled = (reorderList.index > -1);
                    if (GUILayout.Button("Remove"))
                    {
                        RemoveRecord();
                    }
                    GUI.enabled = true;
                    EditorGUILayout.EndHorizontal();
                }
                internal void DrawEditRegion()
                {
                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(addMode ? "Add new record" : "Id Key (" + selectedID + ")");
                    editingID = EditorGUILayout.TextField(editingID);
                    EditorGUILayout.EndHorizontal();

                    foreach (var languageItem in handler.handler.languageID)
                    {
                        DrawItemValue(languageItem);
                    }

                    EditorGUILayout.BeginHorizontal();

                    if (GUILayout.Button(addMode ? "Add" : "Save"))
                    {
                        Edit_Save();
                    }
                    if (GUILayout.Button("Cancel"))
                    {
                        Edit_Cancel();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();
                }

                void Edit_Select(string _idKey = "")
                {
                    addMode = string.IsNullOrEmpty(_idKey);
                    selectedID = _idKey;
                    //Debug.Log(addMode ? "add new key" : "selected: " + _idKey);

                    Edit_Refresh();
                }
                void Edit_Save()
                {
                    string errorTitle = "";
                    string errorDecr = "";
                    string newName = "";
                    if (checkIDError(ref errorTitle, ref errorDecr, ref newName))
                    {
                        if (EditorUtility.DisplayDialog(errorTitle, errorDecr, "OK", "Cancel") == false)
                        {
                            return;
                        }
                    }

                    if (addMode)
                    {
                        localizeDictionaryRef.AddNewID(newName);
                    }
                    else
                    {
                        localizeDictionaryRef.ChangeID(selectedID, newName);
                        idList.Remove(selectedID);
                    }
                    idList.Add(newName);
                }
                bool checkIDError(ref string _title, ref string _description, ref string _newName)
                {
                    if (string.IsNullOrEmpty(editingID))
                    {
                        _title = "Invalid ID";
                        _newName = UniqueID("newRecord");
                        _description = "New ID key can not be blank. Save as \"" + _newName + "\"?";
                        return true;
                    }
                    if (localizeDictionaryRef.keyIDs.Contains(editingID) == true)
                    {
                        if (addMode || (!addMode && editingID != selectedID))
                        {
                            _title = "Invalid ID";
                            _newName = UniqueID(editingID);
                            _description = "Already contain ID key \"" + editingID + "\". Save as \"" + _newName + "\"?";
                            return true;
                        }
                    }
                    _newName = editingID;
                    return false;
                }

                void Edit_Cancel()
                {
                    Edit_Refresh();
                }
                void Edit_Refresh()
                {
                    editingID = selectedID;
                    if (addMode)
                    {

                    }
                    else
                    {

                    }
                }
                private string UniqueID(string _id)
                {
                    if (idList.Contains(_id) == false)
                    {
                        return _id;
                    }

                    ushort count = 1;
                    while (true)
                    {
                        string result = _id + count.ToString();
                        if (idList.Contains(result) == false)
                            return result;
                        count++;
                    }
                }
            }

            private void DrawDetailDictionary()
            {
                if (editHelper == null)
                    return;

                editHelper.DrawingStuff();
            }
            #region EditRegion


            #endregion

            private static readonly Dictionary<Type, Func<object, GUILayoutOption[], object>> _FieldsGuiLayout =
                new Dictionary<Type, Func<object, GUILayoutOption[], object>>()
            {
                    { typeof(int), (value, option) => EditorGUILayout.IntField((int)value, option) },
                    { typeof(float), (value, option) => EditorGUILayout.FloatField((float)value, option) },
                    { typeof(string), (value, option) => EditorGUILayout.TextField((string)value, option) },
                    { typeof(bool), (value, option) => EditorGUILayout.Toggle((bool)value, option) },
                    { typeof(Vector2), (value, option) => EditorGUILayout.Vector2Field(GUIContent.none, (Vector2)value, option) },
                    { typeof(Vector3), (value, option) => EditorGUILayout.Vector3Field(GUIContent.none, (Vector3)value, option) },
                    { typeof(Bounds), (value, option) => EditorGUILayout.BoundsField((Bounds)value, option) },
                    { typeof(Rect), (value, option) => EditorGUILayout.RectField((Rect)value, option) }
            };

            private static T DoFieldGUILayout<T>(T _value, params GUILayoutOption[] _options)
            {
                Type type = typeof(T);
                Func<object, GUILayoutOption[], object> field;
                if (_FieldsGuiLayout.TryGetValue(type, out field))
                    return (T)field(_value, _options);

                if (type.IsEnum)
                    return (T)(object)EditorGUILayout.EnumPopup((Enum)(object)_value, _options);

                if (typeof(UnityObject).IsAssignableFrom(type))
                    return (T)(object)EditorGUILayout.ObjectField((UnityObject)(object)_value, type, true, _options);

                EditorGUILayout.LabelField("Unsupported value", redStyle);
                return _value;
            }

            private static readonly Dictionary<Type, Func<Rect, object, object>> _Fields =
            new Dictionary<Type, Func<Rect, object, object>>()
            {
            { typeof(int), (rect, value) => EditorGUI.IntField(rect, (int)value) },
            { typeof(float), (rect, value) => EditorGUI.FloatField(rect, (float)value) },
            { typeof(string), (rect, value) => EditorGUI.TextArea(rect, (string)value) },
            { typeof(bool), (rect, value) => EditorGUI.Toggle(rect, (bool)value) },
            { typeof(Vector2), (rect, value) => EditorGUI.Vector2Field(rect, GUIContent.none, (Vector2)value) },
            { typeof(Vector3), (rect, value) => EditorGUI.Vector3Field(rect, GUIContent.none, (Vector3)value) },
            { typeof(Bounds), (rect, value) => EditorGUI.BoundsField(rect, (Bounds)value) },
            { typeof(Rect), (rect, value) => EditorGUI.RectField(rect, (Rect)value) }
            };

            private static T DoField<T>(Rect rect, Type type, T value)
            {
                Func<Rect, object, object> field;
                if (_Fields.TryGetValue(type, out field))
                    return (T)field(rect, value);

                if (type.IsEnum)
                    return (T)(object)EditorGUI.EnumPopup(rect, (Enum)(object)value);

                if (typeof(UnityObject).IsAssignableFrom(type))
                    return (T)(object)EditorGUI.ObjectField(rect, (UnityObject)(object)value, type, true);

                GUI.Label(rect, "Unsupported value", redStyle);
                return value;
            }

            private static GUIStyle redStyle;

            [UnityEditor.Callbacks.DidReloadScripts]
            private static void ScriptReloaded()
            {
                redStyle = new GUIStyle();
                redStyle.normal.textColor = Color.red;
            }
            #endregion
        }
#endif

    }

#if UNITY_EDITOR
    public class UnityObjectEditor<T> : Editor where T : UnityEngine.Object
    {
        private T _handler;
        public T handler
        {
            protected set
            {
                _handler = value;
            }
            get
            {
                return _handler;
            }
        }

        protected virtual void OnEnable()
        {
            _handler = (T)target;
        }
    }
#endif
}