﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Dirox.Localization
{
    [System.Serializable]
    public abstract class LocalizedData<T>
    {
        [SerializeField]
        private LocalizedDataManager source;
        public LocalizedDataManager Source
        {
            get
            {
                return source;
            }
        }

        [SerializeField]
        private string idKey;

        public T Value
        {
            get
            {
                try
                {
                    return source.GetData<T>("", idKey);
                }
                catch
                {
                    return DefaultValue;
                }
            }
        }

        public LocalizedData(LocalizedDataManager _source, string _idkey)
        {
            source = _source;
            idKey = _idkey;
        }


        protected abstract T DefaultValue { get; }
    }

    [System.Serializable]
    public class LocalizedString : LocalizedData<string>
    {
        public LocalizedString(LocalizedDataManager _source, string _idkey) : base(_source, _idkey) { }

        protected override string DefaultValue
        {
            get
            {
                return "<ErrorString>";
            }
        }
    }
}