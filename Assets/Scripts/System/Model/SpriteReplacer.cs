﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

//INDEVELOP

[System.Serializable]
public class SpritesAlternate : SerializableDictionary<string, Sprite> { }

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(SpritesAlternate))]
public class SpritesAlternateDrawer : DictionaryDrawer<string, Sprite> { }
#endif

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteReplacer : MonoBehaviour
{
    public SpritesAlternate spritesAlternate;


    [HideInInspector]
    new public SpriteRenderer renderer;
    [HideInInspector]
    public SpritesHandler spriteHandler;

    private void OnEnable()
    {
        renderer = GetComponent<SpriteRenderer>();
        spriteHandler = GetComponentInParent<SpritesHandler>();
        if (spriteHandler != null)
        {
            spriteHandler.replacer.Add(this);
        }
    }
    private void OnDisable()
    {
        if (spriteHandler != null)
        {
            spriteHandler.replacer.Remove(this);
        }
    }
    
    public void Replace( )
    {

    }
}

[System.Serializable]
public class AlternateSprite
{
    public string name;
    public Sprite sprite;
}