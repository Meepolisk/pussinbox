﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteHelper : MonoBehaviour
{
    #region variable
    [HideInInspector]
    new public SpriteRenderer renderer;
    [HideInInspector]
    public SpritesHandler spriteHandler;


    #region overlay Opacity
    private bool _overlayOpacity = false;
    private float _baseOpacity = 1;
    public void OverlayOpacity(float _newAlpha)
    {
        Color currentColor = renderer.color;
        if (_newAlpha >= 1) //thì trả về baseOpacity
        {
            renderer.color = new Color (currentColor.r, currentColor.g, currentColor.b, _baseOpacity);
            _overlayOpacity = false;
        }
        else
        {
            if (_overlayOpacity == false) //nếu lần đầu overlay opacity thì lưu màu
            {
                _baseOpacity = renderer.color.a;
                _overlayOpacity = true;
            }
            renderer.color = new Color (currentColor.r, currentColor.g, currentColor.b, _baseOpacity * _newAlpha);
        }
    }
    public bool Hide
    {
        set
        {
            if (value == true)
            {
                renderer.enabled = false;
            }
            else
            {
                renderer.enabled = true;
            }
        }
    }
    #endregion
    [HideInInspector]
    public Material defaultMaterial;
    [HideInInspector]
    public Shader solidShader;
    public bool whiteSprite
    {
        set
        {
            if (value == true)
            {
                renderer.material.shader = solidShader;
                renderer.color = Color.white;
            }
            else
            {
                renderer.material = defaultMaterial;
                renderer.color = Color.white;
            }
        }
        
    }
    #endregion

    public void Setup(SpritesHandler _spriteHandler)
    {
        if (_spriteHandler == null)
            return;
        spriteHandler = _spriteHandler;
        renderer = GetComponent<SpriteRenderer>();
        //defaultMaterial = renderer.material;
        defaultMaterial  = new Material(Shader.Find("Sprites/Default"));
        solidShader = Shader.Find("GUI/Text Shader");

        spriteHandler.helpers.Add(this);
    }
}
