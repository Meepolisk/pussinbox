﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class GlobalPosition : MonoBehaviour
{
    [ReadOnly]
    public Vector2 globalPosition;
    [ReadOnly]
    public float globalZ;
    // Update is called once per frame
    void Update()
    {
        globalPosition = gameObject.transform.position;
        globalZ = gameObject.transform.eulerAngles.z;
    }
}
#endif