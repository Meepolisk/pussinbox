﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerGroup
{
    public static short[] teamA = { 0 };
    public static short[] teamB = { 1, 2, 3, 4 };
    
    static short findGroup(short playerNumber)
    {
        if (System.Array.IndexOf<short>(teamA, playerNumber) > -1)
            return 0;
        if (System.Array.IndexOf<short>(teamB, playerNumber) > -1)
            return 1;
        return 3;
    }

    public static bool isAlly (short playerNumberA, short playerNumberB)
    {
        if (findGroup(playerNumberA) == findGroup(playerNumberB))
        {
            //Debug.Log("isAlly detected");
            return true;
        }
        //Debug.Log("isEnemy detected");
        return false;
    }
    public static bool isAlly(Unit UplayerNumberA, Unit UplayerNumberB)
    {
        short playNumA = UplayerNumberA.player;
        short playNumB = UplayerNumberB.player;
        //bool b = isAlly(playNumA, playNumB);
        //if (b)
        //    Debug.Log(UplayerNumberA.gameObject.name + " ALLY " + UplayerNumberB.gameObject.name);
        //else
        //    Debug.Log(UplayerNumberA.gameObject.name + " ALLY " + UplayerNumberB.gameObject.name);
        //        return b;
        return isAlly(playNumA, playNumB);
    }

    
}
