﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RFlag : MonoBehaviour
{
    public bool independent = false;
    public Vector2 center
    {
        get
        {
            return transform.position;
        }
    }
    
    public static void Create()
    {
        GameObject newGO = Instantiate(RPlatformer.LoadMovementFlag());
        newGO.AddComponent<RFlag>();
        SpriteRenderer sprite = newGO.GetComponent<SpriteRenderer>();
        sprite.color = Color.blue;
        sprite.hideFlags = HideFlags.HideInInspector;
        newGO.SetActive(true);
    }
    private void Awake()
    {
        if (independent)
        transform.parent = null;
    }
}
