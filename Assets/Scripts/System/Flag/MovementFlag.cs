﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementFlag : RFlag
{
    [ReadOnly]
    public RObject follow;
    [ReadOnly]
    public Vector2 offset;
    [ReadOnly]
    public ROrder orderRef;
    public bool canDestroy = true;

    public void Destroy()
    {
        Destroy(gameObject);
    }
    private void OnDestroy()
    {
        if (orderRef != null)
        {
            orderRef.baseFinish();
        }
    }
    private static GameObject createOrChange(Unit _owner)
    {
        MovementFlag _flag = _owner.movement.auto.target;
        if (_flag != null)
        {
            return _flag.gameObject;
        }
        return Instantiate(RPlatformer.LoadMovementFlag());
    }

    //dạng target
    public static MovementFlag Create(Unit _owner, Vector2 _pos)
    {
        GameObject newGO = createOrChange(_owner);
        newGO.transform.position = _pos;
        MovementFlag newFlag = newGO.AddOrGetComponent<MovementFlag>();
        newFlag.offset = Vector2.zero;
        newFlag.follow = null;
        newFlag.canDestroy = true;
        newGO.SetActive(true);
        return newFlag;
    }
    //dạng follow
    public static MovementFlag Create(Unit _owner, RObject _follow, Vector2? _offset = null)
    {
        GameObject newGO = createOrChange(_owner);
        MovementFlag newFlag = newGO.AddOrGetComponent<MovementFlag>();
        newFlag.follow = _follow;
        if (_offset != null)
            newFlag.offset = (Vector2)_offset;
        else
            newFlag.offset = Vector2.zero;
        newGO.transform.position = newFlag.follow.center + newFlag.offset;
        newFlag.canDestroy = false;
        newGO.SetActive(true);
        return newFlag;
    }

    private void Update()
    {
        if (follow != null)
        {
            transform.position = follow.center + offset;
        }
    }
}
