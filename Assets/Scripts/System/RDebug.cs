﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RDebug {
    
	public static void DrawCircle(Vector2 pos, float AoE, Color color, float time)
    {
        for (int i = 1; i < 37; i++)
        {
            Vector2 vec1 = (pos - RPlatformer.newVector(i*10, AoE));
            Vector2 vec2 = (pos - RPlatformer.newVector((i - 1) * 10, AoE));
            Debug.DrawLine(vec1, vec2, color, time);
        }
    }
    public static Vector2 newVector(float angle, float range)
    {
        float radian = angle * Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian)) * range;
    }
    public static void DrawPoint(Vector2 pos, float scale, Color color, float time)
    {
        Vector2 vec1a = new Vector2(pos.x + scale, pos.y + scale);
        Vector2 vec1b = new Vector2(pos.x - scale, pos.y - scale);
        Vector2 vec2a = new Vector2(pos.x - scale, pos.y + scale);
        Vector2 vec2b = new Vector2(pos.x + scale, pos.y - scale);
        Debug.DrawLine(vec1a, vec1b, color, time);
        Debug.DrawLine(vec2a, vec2b, color, time);
    }
}
