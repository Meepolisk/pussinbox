﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class SpecialEffect : RObject
{
    public float decayTime = 5f;
    public float timedLife = 0f;

    protected override void Start()
    {
        base.Start();
        //mọi Special Effect đều bỏ đi Physic
        if (rObjectCollider != null)
            Destroy(rObjectCollider.gameObject);
        if (rigid != null)
            Destroy(rigid);
    }
    public void Destroy()
    {
        model.animation.SetTrigger("Die");
        Destroy(gameObject, decayTime);
    }

    #region static things
    public static SpecialEffect _lastCreatedSpecialEffect = null;
    
    public static SpecialEffect Create(ModelProfile _profile, Transform _transform, Vector2? offset = null)
    {
        _lastCreatedSpecialEffect = GenerateSpecialEffect(_profile);
        _lastCreatedSpecialEffect.transform.parent = _transform;
        if (offset != null)
            _lastCreatedSpecialEffect.transform.localPosition = (Vector2)offset;
        else
            _lastCreatedSpecialEffect.transform.localPosition = Vector2.zero;
        return _lastCreatedSpecialEffect;
    }
    public static SpecialEffect Create (ModelProfile _profile, Vector2 position)
    {
        _lastCreatedSpecialEffect = GenerateSpecialEffect(_profile);
        _lastCreatedSpecialEffect.transform.position = position;
        return _lastCreatedSpecialEffect;
    }
    private static SpecialEffect GenerateSpecialEffect (ModelProfile _profile)
    {
        GameObject go = new GameObject();
        go.name = "[FX] " + _profile.model.gameObject.name;

        SpecialEffect newFX = go.AddComponent<SpecialEffect>();
        newFX.GenerateModel(_profile);

        return newFX;
    }

    /// <summary>
    /// detroy last created special effect
    /// </summary>
    public static void DestroyLastCreatedSpecialEffect()
    {
        _lastCreatedSpecialEffect.Destroy();
    }

    static SpecialEffect getScript(GameObject go)
    {
        SpecialEffect se = go.GetComponent<SpecialEffect>();
        if (se != null)
            return se;
        return go.AddComponent<SpecialEffect>();
    }   
    #endregion
    
}