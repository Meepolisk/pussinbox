﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif


/// <summary>
/// To use this class, must call function .getData(UnitCaster, TriggerAction)
/// </summary>
[System.Serializable]
public class MissileProfile
{
    [System.Serializable]
    public class Advance
    {
        [System.Serializable]
        public class ContactEffect
        {
            public ModelProfile hitUnit;
            public ModelProfile hitPlatform;
        }
        public Rmissile customMissileBehaviorScript = null;
        public ContactEffect contactEffect;
        public bool triggerOnExpire = true;
        public MissileTarget detectTarget;
        public int detectTime = 1;
    }
    public static Rmissile lastCreatedMissile = null;

    public ModelProfile missileModel;
    [Range(0.1f, 10f)]
    public float timedLife = 2f;
    [Range(0f, 60f)]
    public float speed = 20f;
    public Advance advance;

    [HideInInspector]
    public rDelegate_Vec_GO exploAction;
    [HideInInspector]
    public Unit owner;
    /// <summary>
    /// Must call this function before use Missile.Create
    /// </summary>
    /// <param name="unit">the Unit Script attached to this Game Object</param>
    /// <param name="functionExplo">the function that have a parameter is Rmissile</param>
    public void setData(Unit unit, rDelegate_Vec_GO functionExplo)
    {
        owner = unit;
        exploAction = functionExplo;
    }
    /// <summary>
    /// To use this, make sure you have set up the MissileProfile by using MissileProfile.setData
    /// </summary>
    /// <param name="Zangle">Vector2 of custom position</param>
    /// <param name="Zangle">Angle of missile, 0 is vector (1,0) and 90 is vector(0,1)</param>
    /// <param name="_data">pass the MissileProfile data, make sure to setup it first</param>
    public static void Create(Vector2 pos, float Zangle, MissileProfile _data)
    {
        if (_data.owner == null)
        {
            Debug.LogError("Missile chưa được định danh");
            return;
        }
        GameObject emptyGameObject = new GameObject();
        emptyGameObject.transform.position = pos;

        //init từng loại Missile
        Rmissile theMissile = null;
        if (_data.advance.customMissileBehaviorScript != null)
        {
            //theMissile = emptyGameObject.AddComponent(typeof(_data.customMissileBehaviorScript));
            theMissile = _data.advance.customMissileBehaviorScript.CloneToGameObject<Rmissile>(emptyGameObject);
            theMissile.enabled = true;
        }
        else 
        {
            theMissile = emptyGameObject.AddComponent<Rmissile_normal>();
        }

        emptyGameObject.name = _data.missileModel.model.gameObject.name;
        GameObject bulletInstance = GameObject.Instantiate(_data.missileModel.model.gameObject, emptyGameObject.transform, false);
        bulletInstance.transform.localPosition = Vector2.zero;
        bulletInstance.GetComponent<ModelHandler>().size = new Vector2(_data.missileModel.size, _data.missileModel.size);
        //set góc xoay
        float newZangle = Zangle.toAbs360();
        if (!(newZangle < 90 || newZangle >= 270))
        {
            newZangle -= 180f;
            theMissile.model.sprites.Flip();
        }
        emptyGameObject.transform.eulerAngles = new Vector3(0, 0, newZangle);
        theMissile.Zdirection = Zangle;
        theMissile.Setup(_data);
        lastCreatedMissile = theMissile;
    }
}

//#region Editor MissileProfile
//#if UNITY_EDITOR
//[CustomPropertyDrawer(typeof(MissileProfile))]
//public class CI_MissileProfile : PropertyDrawer
//{
//    bool expand = false;

//    // Draw the property inside the given rect
//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        var target = property.serializedObject.targetObject;
//        //MissileProfile _missileProfile = fieldInfo.GetValue(target) as MissileProfile;

//        EditorGUI.BeginProperty(position, label, property);
//        // Draw label
//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        
//        // Calculate rects
//        var rect1 = new Rect(position.x, position.y, 20, position.height);
//        var rect2 = new Rect(position.x + 20, position.y, 100, position.height);
//        var rect3 = new Rect(position.x + 120, position.y, position.width - (120), position.height);

//        // Draw fields - passs GUIContent.none to each so they are drawn without labels
//        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
//        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("active"), GUIContent.none);
//        if (isActive)
//        {
//            GUI.Label(rect2, "Speed Scale: ");
//            EditorGUI.PropertyField(rect3, property.FindPropertyRelative("speedScale"), GUIContent.none);
//        }

//        // Set indent back to what it was
//        //EditorGUI.indentLevel = indent;

//        EditorGUI.EndProperty();
//    }
//}
//#endif
//#endregion