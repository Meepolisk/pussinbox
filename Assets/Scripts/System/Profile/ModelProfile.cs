﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class ModelProfile
{
    public ModelHandler model;
    public float size = 1f;

    public ModelHandler InstanceOnRObject(RObject _parentObject)
    {
        GameObject instance = GameObject.Instantiate(model.gameObject, _parentObject.transform, false);
        instance.transform.localPosition = Vector2.zero;
        ModelHandler modelH = instance.GetComponent<ModelHandler>();
        if (size > 0f)
            modelH.size = new Vector2(size, size);
        return modelH;
    }
    public static bool operator ==(ModelProfile thisMH, ModelProfile otherMH)
    {
        if (object.ReferenceEquals(thisMH, null))
        {
            return object.ReferenceEquals(thisMH.model, null);
        }

        return thisMH.model.Equals(otherMH.model);
    }
    public static bool operator !=(ModelProfile thisMH, ModelProfile otherMH)
    {
        if (!object.ReferenceEquals(thisMH, null))
        {
            return !object.ReferenceEquals(thisMH.model, null);
        }

        return !thisMH.model.Equals(otherMH.model);
    }
}

#region Editor
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ModelProfile))]
public class ModelProfileDrawer : PropertyDrawer
{

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var rect1 = new Rect(position.x, position.y, position.width - 60, position.height);
        var rect2 = new Rect(position.x + position.width - 60, position.y, 30, position.height);
        var rect3 = new Rect(position.x + position.width - 30, position.y, 30, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("model"), GUIContent.none);
        GUI.Label(rect2, "Size");
        EditorGUI.PropertyField(rect3, property.FindPropertyRelative("size"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
#endregion