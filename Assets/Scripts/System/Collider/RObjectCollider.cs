﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalConfig;
using System;

[RequireComponent(typeof(Collider2D))]
public class RObjectCollider : RObjectAddOn
#if UNITY_EDITOR
    , editorHierachyChange, hideTransform, lockTransform
#endif 
{
    #region SemiRObject
    private short _semiLayer = 0;
    public bool SemiLayer
    {
        get
        {
            if (_semiLayer == 0)
                return false;
            return true;
        }
        set
        {
            if(gameObject.layer == (int)RLayerMask.RObject || gameObject.layer == (int)RLayerMask.RObjectSemi)
            {
                if (value == true)
                    _semiLayer++;
                else _semiLayer--;

                if (_semiLayer > 0)
                {
                    gameObject.layer = (int)RLayerMask.RObjectSemi;
                }
                else
                {
                    gameObject.layer = (int)RLayerMask.RObject;
                }
            }
        }
    }
#endregion
    
    public ModelHandler model;
    [ReadOnly]
    public new Collider2D collider;
    [HideInInspector]
    public Rigidbody2D rigidRef;

    public Vector2 center
    {
        get
        {
            if (collider.offset.x != 0 || collider.offset.y != 0)
            {
                float x = ((transform.lossyScale.x * collider.offset.x) + transform.position.x);
                float y = ((transform.lossyScale.y * collider.offset.y) + transform.position.y);
                return new Vector2(x, y);
            }
            else
            {
                return transform.position;
            }
        }
    }

    protected override void Awake()
    {
        base.Awake();
        rObjectRef.rObjectCollider = this;
        //tìm Rigid
        rigidRef = rObjectRef.GetComponent<Rigidbody2D>();
        if (rigidRef == null)
        {
            Rigidbody2D thisRigid = gameObject.GetComponent<Rigidbody2D>();
            if (thisRigid != null)
            {
                rigidRef = thisRigid.Clone(rObjectRef.gameObject);
                Destroy(thisRigid);
            }
            else
            {
                rigidRef = rObjectRef.gameObject.AddComponent<Rigidbody2D>();
            }
        }
        rObjectRef.rigid = rigidRef;
        rObjectRef.rigid.isKinematic = true; //tạm để vậy để unit không fall

        if (collider == null)
            collider = GetComponent<Collider2D>();

        if (model == null)
        {
            model = transform.parent.GetComponent<ModelHandler>();
            model.rCollider = this;
            
        }
        
        if (!collider.isTrigger)
        {
            checkContact = true;
            contacts = new ContactManager(this);
        }
    }
    protected override void Start()
    {
        base.Start();

    }

    public override void FinishSetup()
    {
        base.FinishSetup();
        rObjectRef.rigid.isKinematic = false; //tạm để vậy để unit không fall
    }

    #region velocity
    private Vector2 _endFrameVelocity;
    private Coroutine endFrameVelocity = null;
    public Vector2 velocity
    {
        set
        {
            _endFrameVelocity = value;
            if (endFrameVelocity == null)
            {
                endFrameVelocity = StartCoroutine(SetEndFrameVelocity());
            }
        }
    }
    IEnumerator SetEndFrameVelocity()
    {
        yield return new WaitForFixedUpdate();
        //Debug.Log("[" + Time.time + "]" + model.rObject.gameObject.name + ": set Velocity to " + _endFrameVelocity);
        rigidRef.velocity = _endFrameVelocity;
        endFrameVelocity = null;
        yield return null;
    }
    #endregion

    #region force
    private Vector2 _endFrameForce;
    private Coroutine endFrameForce = null;
    public void AddForce(Vector2 _force)
    {
        _endFrameForce = _force;
        if (endFrameForce == null)
        {
            endFrameForce = StartCoroutine(SetEndFrameForce());
        }
    }
    IEnumerator SetEndFrameForce()
    {
        yield return new WaitForFixedUpdate();
        //Debug.Log("[" + Time.time + "]" + model.rObject.gameObject.name + ": AddForce " + _endFrameForce);
        rigidRef.AddForce(_endFrameForce);
        endFrameForce = null;
        yield return null;
    }
    #endregion
    
    [System.Serializable]
    public class ContactManager
    {
        private RObjectCollider objectColliderRef;

        public class Contacts
        {
            private ContactManager manager;
            private float minCheck;
            private float maxCheck;
            public Contacts(float _min, float _max, ContactManager _manager)
            {
                minCheck = _min; maxCheck = _max; manager = _manager;
            }

            public List<Platform> getPlatform
            {
                get
                {
                    List<Platform> platforms = new List<Platform>();
                    foreach (var contact in manager.list)
                    {
                        if (contact.platform.angle.isInRange(minCheck, maxCheck)
                            && Physics2D.IsTouching(contact.platform.collider,manager.objectColliderRef.collider))
                            //&& PointIsOnLeftOfVector(manager.objectColliderRef.center, contact.platform.pointA, contact.platform.pointB))
                            platforms.Add(contact.platform);
                    }
                    return platforms;
                }
            }
            private bool PointIsOnLeftOfVector(Vector2 M, Vector2 pA, Vector2 pB)
            {
                //check điểm M nằm bên trái hay bên phải đường thẳng từ AB
                // nếu = 0 là trùng trên đường, < 0 là bên trái và > 0 là bên phải
                return (Mathf.Sign((pA.x - pB.x) * (M.y - pB.y) - (pA.y - pB.y) * (M.x - pB.x)) < 0);
            }
            public bool isContacted(out List<Platform> _platform)
            {
                _platform = getPlatform;
                if (_platform.Count > 0)
                {
                    return true;
                }
                return false;
            }
            public bool isContacted()
            {
                if (getPlatform.Count > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public ContactManager(RObjectCollider _objectColliderRef)
        {
            objectColliderRef = _objectColliderRef;
            down = new Contacts(MovementConfig.WalkAngleDirection - MovementConfig.WalkAngleScale,
                MovementConfig.WalkAngleDirection + MovementConfig.WalkAngleScale, this);
            right = new Contacts(120f, 180f, this);
            left = new Contacts(0f, 60f, this);
        }

        public Contacts down;
        public Contacts left;
        public Contacts right;

        [ReadOnly]
        public List<RContact> list = new List<RContact>();
        internal void Add(Collision2D _collision)
        {
            RContact checkingContact;
            if (RContact.isPlatformContact(_collision, out checkingContact))
            {
                _finishAdd(checkingContact);
            }
        }
        private void _finishAdd(RContact rContact)
        {
            list.Add(rContact);
            objectColliderRef.rObjectRef.OnContactPlatform(rContact.platform, rContact.points.ToArray());
        }
        internal void RefreshOrAdd(Collision2D _collision)
        {
            RContact checkingContact;
            if (RContact.isPlatformContact(_collision, out checkingContact))
            {
                RContact selectContact = null;
                foreach (RContact loopCheckContact in list)
                {
                    if (loopCheckContact.platform == checkingContact.platform)
                    {
                        selectContact = loopCheckContact;
                        break;
                    }
                }
                if (selectContact != null)
                    selectContact.Replace(_collision);
                else
                    _finishAdd(checkingContact);
            }
            
        }
        internal void Remove(Collision2D _collision)
        {
            RContact checkingContact;
            if (RContact.isPlatformContact(_collision, out checkingContact))
            {
                RContact selectContact = null;
                foreach (var loopCheckContact in list)
                {
                    if (loopCheckContact.platform == checkingContact.platform)
                    {
                        selectContact = loopCheckContact;
                        break;
                    }
                }
                if (selectContact != null)
                {
                    list.Remove(selectContact);
                }
            }
        }
    }

    private bool checkContact = false;
    [ReadOnly]
    public ContactManager contacts;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (checkContact)
            contacts.Add(collision);
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (checkContact)
            contacts.RefreshOrAdd(collision);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (checkContact)
            contacts.Remove(collision);
    }
    #region EDITOR_SETUP

#if UNITY_EDITOR
    private void Reset()
    {
        if (Application.isPlaying) return;

        setup();
        //Debug.Log("editor Reset");
    }
    private void setup()
    {
        if (Application.isPlaying) return;

        RObject _rObjectBase = GetComponentInParent<RObject>();
        if (_rObjectBase)
        {
            collider = GetComponent<Collider2D>();
            try
            {
                model = transform.parent.GetComponent<ModelHandler>();
                model.rCollider = this;
            }
            catch
            {
                gameObject.SetActive(false);
                Debug.LogError("Phải có ModelHandler nằm trên 1 cấp", gameObject);
                return;
            }
        }
        else
        {
            collider = null;
            model = null;
        }

        
    }
    public void editorHierachyChange()
    {
        //Debug.Log("editor hierachy Change");
        setup();
    }

#endif
    #endregion
}
