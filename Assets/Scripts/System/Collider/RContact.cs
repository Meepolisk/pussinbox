﻿using UnityEngine;
using System.Collections.Generic;



[System.Serializable]
public class RContact
{
    public Platform platform;
    public List<Vector2> points = new List<Vector2>();

    public RContact (Collision2D _collision, Platform _platform)
    {
        platform = _platform;
        ConvertPoint(_collision.contacts);
    }
    public void Replace(Collision2D _collision)
    {
        ConvertPoint(_collision.contacts);
        //Debug.Log("Replace");
    }
    private void ConvertPoint(ContactPoint2D[] _contactPoints)
    {
        points.Clear();
        foreach (var contact in _contactPoints)
        {
            points.Add(new Vector2(contact.point.x, contact.point.y));
            //Debug.Log(contact.collider.name + " hit " + contact.otherCollider.name);
            RDebug.DrawPoint(contact.point, Time.fixedDeltaTime, Color.white, Time.deltaTime);
        }
    }
    
    public static bool isPlatformContact(Collision2D _collision, out RContact _checkedContact)
    {
        Platform platform = _collision.collider.GetComponent<Platform>();
        if (platform)
        {
            _checkedContact = new RContact(_collision, platform);
            return true;
        }
        _checkedContact = null;
        return false;
    }
}
