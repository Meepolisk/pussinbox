﻿//using UnityEngine;
//using System.Collections;
//using System;

////#if UNITY_EDITOR
////using UnityEditor;
////#endif

//[RequireComponent(typeof(SpriteRenderer), typeof(PlatformEffector2D), typeof(BoxCollider2D))]
//public class Platform : MonoBehaviour, hideTransform
//{
//#if UNITY_EDITOR

//    protected float recRed = 1f, recGreen = 0, recBlue = 0, recAlpha = 1;
    
//    public virtual void Refresh()
//    {
//        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
//        effector = gameObject.GetComponent<PlatformEffector2D>();
//        box = gameObject.GetComponent<BoxCollider2D>();
//        //gameObject.GetComponent<Material>().hideFlags = HideFlags.HideInInspector;
//        spriteRenderer.hideFlags = HideFlags.HideInInspector;
//        effector.hideFlags = HideFlags.HideInInspector;
//        box.hideFlags = HideFlags.HideInInspector;
//        spriteRenderer.color = new Color(recRed, recGreen, recBlue, recAlpha);

//        //spriteRenderer.hideFlags = HideFlags.None;
//        //effector.hideFlags = HideFlags.None;
//        box.hideFlags = HideFlags.None;

//    }
//    public virtual void RefreshUI()
//    {
//        _angle = (gameObject.transform.eulerAngles.z + 90f).to360();
//        _center = gameObject.transform.position;

//        //hiệu chỉnh "Lengh" dựa theo LossyScale nếu LossyScale khác 1
//        if ((transform.localScale.x / transform.lossyScale.x) != 1)
//        {
//            length = _length * transform.lossyScale.x;
//        }
//        //local scale X, và Y luôn phải có LossyScale = 1 để hình ảnh dễ nhìn
//        transform.localScale = new Vector3(transform.localScale.x / transform.lossyScale.x, transform.localScale.y / transform.lossyScale.y, 1f);
//    }
//    public virtual void RefreshInspector()
//    {
//        gameObject.layer = LayerMask.NameToLayer("PlatformCollider");
//        surfaceArc = _surfaceArc;
//        blockMissile = _blockMissile;
//        dropable = _dropable;
//        physic = _physic;
//        angle = _angle;
//        length = _length;
//        center = _center;
//    }

//    [SerializeField]
//    [ReadOnlyWhenPlaying]
//    protected Vector2 _center;
//    public Vector2 center
//    {
//        set
//        {
//            gameObject.transform.position = value;
//            _center = value;
//        }
//    }
//    [SerializeField]
//    [ReadOnlyWhenPlaying]
//    protected float _length = 5;
//    public float length
//    {
//        set
//        {
//            float x = (value < 1f) ? 1f : value;
//            Vector2 result = new Vector2(x, 1f);
//            box.size = result;
//            spriteRenderer.size = result;
//            _length = x;
//        }
//        get
//        {
//            return _length;
//        }
//    }
//#endif


//    [SerializeField]
//    [ReadOnlyWhenPlaying]
//    [Range(0, 360)]
//    protected float _surfaceArc = 180;
//    public float surfaceArc
//    {
//        set
//        {
//            effector.surfaceArc = value;
//            _surfaceArc = value;
//        }
//    }

//    [Range(0, 360)]
//    [SerializeField]
//    //[ReadOnlyWhenPlaying]
//    protected float _angle;
//    public float angle
//    {
//        set
//        {
//            gameObject.transform.eulerAngles = new Vector3(0f, 0f, value - 90f);
//            _angle = (gameObject.transform.eulerAngles.z + 90f).to360();
//        }
//        get
//        {
//            return _angle;
//        }
//    }

//    [SerializeField]
//    protected bool _blockMissile;
//    public bool blockMissile
//    {
//        set
//        {
//            if (value == true)
//            {
//                recAlpha = 1f;
//                effector.colliderMask |= (1 << LayerMask.NameToLayer("Missile"));
//            }
//            else
//            {
//                recAlpha = 0.4f;
//                effector.colliderMask &= ~(1 << LayerMask.NameToLayer("Missile"));
//            }
//            _blockMissile = value;
//        }
//        get
//        {
//            return _blockMissile;
//        }
//    }
//    [SerializeField]
//    protected bool _dropable;
//    public bool dropable
//    {
//        set
//        {
//#if UNITY_EDITOR
//            if (value == true)
//            {
//                recGreen = 1;
//            }
//            else
//            {
//                recGreen = 0;
//            }
//#endif
//            _dropable = value;
//        }
//        get
//        {
//            return _dropable;
//        }
//    }
//#if UNITY_EDITOR
//    [ReadOnlyWhenPlaying]
//    [SerializeField]
//    protected PhysicsMaterial2D _physic;
//#endif
//    public PhysicsMaterial2D physic
//    {
//#if UNITY_EDITOR
//        set
//        {
//            box.sharedMaterial = value;
//        }
//#endif
//        get
//        {
//            return box.sharedMaterial;
//        }
//    }

//    [ReadOnly]
//    [SerializeField]
//    protected SpriteRenderer spriteRenderer;
//    [ReadOnly]
//    [SerializeField]
//    protected PlatformEffector2D effector;
//    [ReadOnly]
//    [SerializeField]
//    protected BoxCollider2D box;

//}

//#region EDITOR
//#if UNITY_EDITOR
//[CustomEditor(typeof(Platform))]
//public class CustomInspectorPlatform : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        base.OnInspectorGUI();
//        Platform p = (Platform)target;

//        p.Refresh();
//        p.RefreshInspector();

//    }
//    void OnSceneGUI()
//    {
//        Platform p = (Platform)target;

//        p.Refresh();
//        p.RefreshUI();
//    }
//}
//#endif

//#endregion