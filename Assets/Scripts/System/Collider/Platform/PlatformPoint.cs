﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlatformPoint
{
    public Vector2 point = Vector3.zero;
}
