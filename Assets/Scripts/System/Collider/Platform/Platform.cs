﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(SpriteRenderer), typeof(PlatformEffector2D), typeof(BoxCollider2D))]
public class Platform : MonoBehaviour
#if UNITY_EDITOR
    , editorUpdate, editorHierachyChange, hideSprite
#endif 
{

#region unity editor
#if UNITY_EDITOR
    [SerializeField]
    [HideInInspector]
    private float recRed = 1f, recGreen = 0, recBlue = 0, recAlpha = 1;

    public void EditorUpdate()
    {
        if (this.enabled == false)
        {
            recAlpha = (Mathf.Cos(Time.unscaledTime * 12) / 4f);
        }
        Refresh();
        RefreshUI();
    }
    
    public virtual void Refresh()
    {
        gameObject.GetComponent<SpriteRenderer>().color = new Color(recRed, recGreen, recBlue, recAlpha);
    }
    public virtual void RefreshUI()
    {
        _angle = (gameObject.transform.eulerAngles.z + 90f).toAbs360();
        //_center = gameObject.transform.position;

        //hiệu chỉnh "Lengh" dựa theo LossyScale nếu LossyScale khác 1
        if ((transform.localScale.x / transform.lossyScale.x) != 1)
        {
            length = _length * transform.lossyScale.x;
        }
        //local scale X, và Y luôn phải có LossyScale = 1 để hình ảnh dễ nhìn
        transform.SetGlobalScale(Vector3.one);

        reTransform();
    }
    public virtual void RefreshInspector()
    {
        gameObject.layer = LayerMask.NameToLayer("PlatformCollider");
        surfaceArc = _surfaceArc;
        blockMissile = _blockMissile;
        dropable = _dropable;
        physic = _physic;
        angle = _angle;
        length = _length;
        pointA = _pointA;
        pointB = _pointB;

        //enable
        GetComponent<BoxCollider2D>().enabled = this.enabled;
    }
    private void Reset()
    {
        Setup();
    }
    public void editorHierachyChange()
    {
        Setup();
    }
    private void Setup()
    {
        //spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        effector = gameObject.GetComponent<PlatformEffector2D>();
        collider = gameObject.GetComponent<BoxCollider2D>();

        collider.offset = new Vector2(0, -0.5f);

        //spriteRenderer.hideFlags = HideFlags.HideInInspector;
        effector.hideFlags = HideFlags.HideInInspector;
        collider.hideFlags = HideFlags.HideInInspector;
    }
#if UNITY_EDITOR
    [SerializeField]
    [ReadOnlyWhenPlaying]
    protected Vector2 _pointA;
    [SerializeField]
    [ReadOnlyWhenPlaying]
    protected Vector2 _pointB;

#endif
    
    [SerializeField]
    [ReadOnlyWhenPlaying]
    protected float _length = 5;

    private void reTransform()
    {
        _pointA = pointA;
        _pointB = pointB;
    }
    private void Update()
    {
        //reTransform();
    }
#endif
    #endregion

    private void OnEnable()
    {
        GetComponent<BoxCollider2D>().enabled = true;
    }
    private void OnDisable()
    {
        GetComponent<BoxCollider2D>().enabled = false;
    }

    public Vector2 pointA
    {
        get
        {
            return (Vector2)transform.position + RPlatformer.Angle2vector2(angle + 90f, length / 2f);
        }
        set
        {
#if UNITY_EDITOR
            _pointA = value;
#endif
            transform.position = (pointB + value) / 2f;
        }
    }
    public Vector2 pointB
    {
        get
        {
            return (Vector2)transform.position + RPlatformer.Angle2vector2(angle - 90f, length / 2f);
        }
        set
        {
#if UNITY_EDITOR
            _pointB = value;
#endif
            Vector2 pA = pointA;
            transform.position = (pA + value) / 2f;
        }
    }
    public float length
    {
        set
        {
            float x = value;
            Vector2 result = new Vector2(Mathf.Abs(x), 1f);
            collider.size = result;
            spriteRenderer.size = result;
#if UNITY_EDITOR
            _length = x;
#endif
        }
        get
        {
            return collider.size.x;
        }
    }

    [SerializeField]
    [ReadOnlyWhenPlaying]
    [Range(0, 360)]
    protected float _surfaceArc = 180;
    public float surfaceArc
    {
        set
        {
            effector.surfaceArc = value;
            _surfaceArc = value;
        }
    }

    [Range(0, 360)]
    [SerializeField]
    //[ReadOnlyWhenPlaying]
    protected float _angle;
    public float angle
    {
        set
        {
            gameObject.transform.eulerAngles = new Vector3(0f, 0f, value - 90f);
            _angle = (gameObject.transform.eulerAngles.z + 90f).toAbs360();
        }
        get
        {
            return _angle;
        }
    }

    [SerializeField]
    protected bool _blockMissile;
    public bool blockMissile
    {
        set
        {
#if UNITY_EDITOR
            if (value == true)
            {
                recAlpha = 1f;
            }
            else
            {
                recAlpha = 0.4f;
            }
#endif
            _blockMissile = value;
        }
        get
        {
            return _blockMissile;
        }
    }
    [SerializeField]
    protected bool _dropable;
    public bool dropable
    {
        set
        {
#if UNITY_EDITOR
            if (value == true)
            {
                recGreen = 1;
            }
            else
            {
                recGreen = 0;
            }
#endif
            _dropable = value;
        }
        get
        {
            return _dropable;
        }
    }

    [ReadOnlyWhenPlaying]
    [SerializeField]
    protected PhysicsMaterial2D _physic;

    [ReadOnly]
    [SerializeField]
    protected SpriteRenderer spriteRenderer;

    [ReadOnly]
    [SerializeField]
    protected PlatformEffector2D effector;


    public PhysicsMaterial2D physic
    {
#if UNITY_EDITOR
        set
        {
            collider.sharedMaterial = value;
        }
#endif
        get
        {
            return collider.sharedMaterial;
        }
    }

    [ReadOnly]
    public new BoxCollider2D collider;

    private void Awake()
    {
        gameObject.layer = 9; //PlatformCollider
        effector.colliderMask = 0;
        if (blockMissile)
        {
            effector.colliderMask |= (1 << 12); //missle
        }
        if (!dropable)
        {
            effector.colliderMask |= (1 << 13); //rObjectSemi
        }
        effector.colliderMask |= (1 << 10); //fraps
        effector.colliderMask |= (1 << 8); //rObject
    }


}

#region EDITOR
#if UNITY_EDITOR
[CustomEditor(typeof(Platform))]
public class CustomInspectorPlatform : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Platform p = (Platform)target;
        
        p.RefreshInspector();
    }
    void OnSceneGUI()
    {
        Platform p = (Platform)target;

        #region See Box
        //Vector2 pointA = t.getScale();
        //Vector2 pointB = t.getCenter(scale);
        if (p.enabled == true)
        {
            Handles.color = new Color(Mathf.Sin(Time.unscaledTime * 4), Mathf.Cos(Time.unscaledTime * 4), 1, 1);
            Handles.DrawLine(p.pointA, p.pointB);
        }
        #endregion
    }
}
#endif

#endregion