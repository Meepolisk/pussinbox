﻿namespace GlobalConfig
{
    public static class MovementConfig
    {
        public static float WalkAngleDirection = 90f;
        public static float WalkAngleScale = 30f;

        public static float UpPlatformDirection = 270f;
        public static float UpPlatformScale = 30f;
    }
}

public enum RLayerMask
{
    RObject = 8,
    RObjectSemi = 13,
    PlatformCollider = 9,
    Fraps = 10,
    Region = 11,
    Missile = 12,
    CameraZone = 14,
}
