﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RSO_System", menuName = "Scriptable/RSO_System", order = 1)]
public class RSO_System : ScriptableObject {

    [System.Serializable]
    public class Camera
    {
        public float mainTargetMaxRange = 2f;

        public float minSize = 10;
        public float maxSize = 16;

        public float maxRangeToAdjust = 2f;
        public float lerpSmooth = 0.3f;
    }
    public Camera camera;
}
