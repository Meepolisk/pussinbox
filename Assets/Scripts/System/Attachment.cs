﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[DisallowMultipleComponent]
[RequireComponent(typeof(SpriteRenderer))]
public class Attachment : MonoBehaviour
#if UNITY_EDITOR
    ,editorUpdate, editorHierachyChange, hideSprite
#endif 

{
#if UNITY_EDITOR
    [SerializeField]
    [ReadOnly]
    /// <summary>
    /// Không dùng trong runtime
    /// </summary>
    private Vector2 _globalPosition;

    [SerializeField]
    [ReadOnly]
    /// <summary>
    /// Không dùng trong runtime
    /// </summary>
    private float _globalZ;
    public void updateTransform()
    {
        _globalPosition = center;
        _globalZ = zAngle;
    }

    private void Update()
    {
        updateTransform();
    }

    [HideInInspector]
    public new SpriteRenderer renderer;
    
    private void Reset()
    {
        setup();
    }
    private void setup()
    {
        renderer = GetComponent<SpriteRenderer>();
        if (renderer)
        {
            renderer.sprite = RPlatformer.LoadSystemTexture("rGizmosAttachment");
            renderer.sortingLayerID = SortingLayer.NameToID("DevView");
            renderer.color = Color.red;
        }

        try
        {
            RObject _object = GetComponentInParent<RObject>();
            sprites = _object.GetComponentInChildren<SpritesHandler>();
        }
        catch
        {
            gameObject.SetActive(false);
            Debug.LogError("Attachments cần nằm trong một SpritesHandler hoặc trong một SpritesPart có kết nối với ModelHandler", gameObject);
        }


    }
    public void editorHierachyChange()
    {
        setup();
    }
    public void EditorUpdate()
    {
        updateTransform();
    }
    public void RefreshInspector()
    {
        if (Application.isPlaying) return;

        updateName();
    }
    public void updateName()
    {
        if (!gameObject.activeSelf) return; //không active thì k check

        string _name = gameObject.name;
        if (string.IsNullOrEmpty(_name) || (_name.Length == 1 && _name[0] == '#'))
        {
            gameObject.SetActive(false);
            Debug.LogError("Attachment cần phải có tên, không tính dấu # ban đầu", gameObject);
            return;
        }
        else if (_name[0] != '#')
        {
            gameObject.name = '#' + _name;
        }
    }

    [Header("=====DEBUG REF========")]
#endif

    [ReadOnly]
    public SpritesHandler sprites;

    public Vector2 center
    {
        get
        {
            return transform.position;
        }
    }
    public float zAngle
    {
        get
        {

            float result = transform.eulerAngles.z;
            if (transform.lossyScale.x < 0)
                result += 180f;
            //if (sprites != null && sprites.isFlipped)
            //    return result + 180f;
            return result;
        }
    }


    private void Awake()
    {
        sprites.attachments.Add(gameObject.name.Substring(1), this);
        gameObject.layer = LayerMask.NameToLayer("RGizmos");
    }
}

#region EDITOR
#if UNITY_EDITOR
[CustomEditor(typeof(Attachment), true)]
public class CI_Attachment : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Attachment t = (Attachment)target;
        
        t.RefreshInspector();
    }
}
#endif
#endregion