﻿//#if UNITY_EDITOR
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;

//[ExecuteInEditMode]
//public class EditorMaster : MonoBehaviour
//{
//    [Range(0f, 2f)]
//    public float timeScale = 1f;


//    private bool isEditorModel = true;
//    public static bool isEditModelMode
//    {
//        get
//        {
//            if (!editorMaster)
//                return false;
//            return editorMaster.isEditorModel;
//        }
//    }
//    private static EditorMaster editorMaster;

//    void OnEnable()
//    {
//        EditorApplication.hierarchyChanged += HierarchyChanged;
//        editorMaster = this;
//    }

//    void OnDisable()
//    {
//        EditorApplication.hierarchyChanged -= HierarchyChanged;
//    }


//    private void Update()
//    {
//        retimeScale();
//        updateEditor();
//    }
//    void updateEditor()
//    {
//        if (Application.isPlaying) return;

//        Transform[] all = GameObject.FindObjectsOfType<Transform>();
//        foreach (var tran in all)
//        {
//            if (tran.GetComponent<hideTransform>() != null)
//            {
//                tran.hideFlags = HideFlags.HideInInspector;
//                if (tran.GetComponent<lockTransform>() != null)
//                {
//                    tran.localPosition = Vector3.zero;
//                    tran.eulerAngles = Vector3.zero;
//                }
//            }
//            else
//                tran.hideFlags = HideFlags.None;

//            Renderer renderer = tran.GetComponent<Renderer>();
//            if (renderer)
//            {
//                if (tran.GetComponent<hideSprite>() != null)
//                {
//                    renderer.hideFlags = HideFlags.HideInInspector;
//                    //renderer.sharedMaterial.hideFlags = HideFlags.HideInInspector;
//                }
//                else
//                {
//                    renderer.hideFlags = HideFlags.None;
//                    //renderer.sharedMaterial.hideFlags = HideFlags.None;
//                }
//            }

//            editorUpdate eU = tran.GetComponent<editorUpdate>();
//            if (eU != null)
//            {
//                eU.EditorUpdate();
//            }
//        }
//    }
//    private void HierarchyChanged()
//    {
//        if (Application.isPlaying) return;

//        Transform[] all = GameObject.FindObjectsOfType<Transform>();
//        foreach (var tran in all)
//        {
//            editorHierachyChange eH = tran.gameObject.GetComponent<editorHierachyChange>();
//            if (eH != null)
//            {
//                eH.editorHierachyChange();
//            }
//        }
//    }

//    void retimeScale()
//    {
//        if (!GameMaster.isLoadingScene)
//            Time.timeScale = timeScale;
//    }
//}


//#endif