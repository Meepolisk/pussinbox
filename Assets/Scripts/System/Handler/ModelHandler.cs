﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ModelHandler : ModelHelper
#if UNITY_EDITOR
    , editorHierachyChange, editorUpdate, hideTransform
#endif 
{
    public bool flipWhenTurn = true;
    
    [ReadOnly]
    public SpritesHandler sprites;
    [ReadOnly]
    public RObjectCollider rCollider;
    
    public new AnimationHelper animation = new AnimationHelper();

    protected override void Awake()
    {
        base.Awake();

        if (rObject.model == null)
        {
            rObject.model = this;
        }

        animation.setup(this);
        animation.InitList(animator);
    }
    public Vector2 size
    {
        get
        {
            return (Vector2)sprites.transform.localScale;
        }
        set
        {
            transform.localScale = new Vector3(value.x, value.y, 1f);
        }
    }

    #region public

    #region HIDE
    private bool _hide = false;
    public bool Hide
    {
        set
        {
            _hide = value;
            sprites.Hide = _hide;
            rCollider.gameObject.SetActive(!_hide);
        }
        get
        {
            return _hide;
        }
    }

    #endregion
    public void Flip(bool _forceFlip = false)
    {
        if (!_forceFlip && !flipWhenTurn) return;

        sprites.Flip();
        //Sight for AI
        RSight[] allrSight = rObject.GetComponentsInChildren<RSight>();
        foreach (var sight in allrSight)
        {
            //Debug.Log(sight.gameObject.name);
            sight.transform.localPosition = new Vector2(-sight.transform.localPosition.x, sight.transform.localPosition.y);
            sight.transform.localEulerAngles = new Vector3(sight.transform.localEulerAngles.x + 180f, sight.transform.localEulerAngles.y,
                sight.transform.localEulerAngles.z);
        }
    }
    public void sendTriggerChild(string _name)
    {
        animation.SetTrigger(_name, false);
    }
    public void sendBoolChild(string _name, int _value)
    {
        animation.SetBool(_name, (_value != 0)? true : false, false);
    }
    public void sendFloatChild(string _name, float _float)
    {
    }

    #endregion

    #region EDITOR_SETUP
#if UNITY_EDITOR
    private void Reset()
    {
        setup();
    }
    private void setup()
    {
        try
        {
            RObject _rObjectBase = transform.parent.GetComponent<RObject>();
            rObject = _rObjectBase;
            rObject.model = this;
        }
        catch
        {
            rObject = null;
            sprites = null;
            rCollider = null;
        }
    }

        public void editorHierachyChange()
        {
            setup();
        }

    public void EditorUpdate()
    {
        HideAndRetransform();
    }

    public void HideAndRetransform()
    {
        if (transform.parent != null && rObject != null)
        {
            transform.localPosition = Vector3.zero;
        }
        transform.eulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;
    }
#endif
    #endregion
}