﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class PartRef : MonoBehaviour
#if UNITY_EDITOR
#endif 
{
    #region var
    public TransformRef partRef;

    #region UnityEditor
#if UNITY_EDITOR
    [System.Serializable]
    public class PartInfo
    {
        [ReadOnly]
        public Vector3 refPosition;
        [ReadOnly]
        public Vector3 refRotation;
        [ReadOnly]
        public Vector3 refScale;
    }

    public PartInfo info = new PartInfo();
#endif
    #endregion
    
    #endregion
    
}
