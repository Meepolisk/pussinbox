﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class UnitPart : ModelHelper
#if UNITY_EDITOR
    , editorHierachyChange
#endif 
{
    #region var
    [ReadOnly]
    public ModelHandler mainModel;
    
    [HideInInspector]
    public List<SpriteHelper> helpers = new List<SpriteHelper>();
    
    #endregion

    protected override void Awake()
    {
        base.Awake();
    }
    protected void Start()
    {
        InstanceHelpers();
    }
    private void OnEnable()
    {
        mainModel.animation.InitList(animator);
    }
    private void OnDisable()
    {
        mainModel.animation.RemoveList(animator);
    }
    private void InstanceHelpers()
    {
        SpriteRenderer[] sprds = GetComponentsInChildren<SpriteRenderer>();
        foreach (var spr in sprds)
        {
            SpriteHelper newSH;
            newSH = spr.GetComponent<SpriteHelper>();
            if (newSH.isNull())
                newSH = spr.gameObject.AddComponent<SpriteHelper>();
            newSH.Setup(mainModel.sprites);
            helpers.Add(newSH);
        }
    }

    #region UnityEditor
#if UNITY_EDITOR
    private void Reset()
    {
        setup();
    }
    public void editorHierachyChange()
    {
        setup();
    }
    private void setup()
    {
        rObject = transform.parent.GetComponent<RObject>();
        if (rObject)
        {
            try
            {
                mainModel = transform.parent.GetComponentInChildren<ModelHandler>();

            }
            catch
            {
                gameObject.SetActive(false);
                Debug.LogError("UnitPart phải đặt cùng cấp parent-child với một ModelHandler khác", gameObject);
            }
        }
        else
        {
            Debug.LogWarning("ModelHandler cần một component RObject ở parent", gameObject);
        }  
    }
#endif
    #endregion
}
