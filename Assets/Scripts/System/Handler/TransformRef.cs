﻿using UnityEngine;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[DisallowMultipleComponent]
public class TransformRef : MonoBehaviour
#if UNITY_EDITOR
, editorUpdate, editorHierachyChange
#endif 
{
    #region UnityEditor
#if UNITY_EDITOR
    [System.Serializable]
    public class PartInfo
    {
        [ReadOnly]
        public Vector3 globalPosition;
        [ReadOnly]
        public Vector3 globalRotation;
        [ReadOnly]
        public Vector3 globalScale;
    }

    public PartInfo info = new PartInfo();
#endif
    #endregion
    public Transform partRef;
    public bool mimicPosition = true;
    public bool mimicRotate = true;
    public bool globalRotate = false;
    public bool mimicScale = true;
    protected void Update()
    {
        if (transform.hasChanged)
            FollowRef();
    }

    public void FollowRef()
    {
        if (partRef != null)
        {
            if (mimicPosition)
                partRef.transform.position = transform.position;
            if (mimicRotate)
            {
                if (globalRotate)
                {
                    float zAngle = transform.localEulerAngles.z;
                    if (transform.lossyScale.x < 0)// là flip
                        zAngle *= -1f;
                    partRef.transform.eulerAngles = new Vector3( 0f,0f, zAngle);
                }
                else
                    partRef.transform.eulerAngles = transform.eulerAngles;

            }
            if (mimicScale)
                partRef.transform.SetGlobalScale(transform.lossyScale);

#if UNITY_EDITOR
            info.globalPosition = transform.position;  
            info.globalRotation = transform.eulerAngles;
            info.globalScale = transform.localScale;
#endif
        }
    }

    #region EDITOR_SETUP
#if UNITY_EDITOR

    private void Reset()
    {
        setup();
    }
    private void setup()
    {
        gameObject.layer = LayerMask.NameToLayer("RGizmos");

        SpritesHandler spritesHandler = GetComponentInParent<SpritesHandler>();
        if (!spritesHandler)
        {
            gameObject.SetActive(false);
            Debug.LogError("TransformRef cần nằm trong một SpritesHandler hoặc SpritesPart", gameObject);
        }
    }
    public void editorHierachyChange()
    {
        setup();
    }
    public void EditorUpdate()
    {
        FollowRef();
    }
    public void RefreshInspector()
    {
        updateName();
    }
    public void updateName()
    {
        if (!gameObject.activeSelf) return; //không active thì k check

        string _name = gameObject.name;
        if (string.IsNullOrEmpty(_name) || (_name.Length == 1 && _name[0] == '@'))
        {
            gameObject.SetActive(false);
            Debug.LogError("PartRef cần phải có tên, không tính dấu @ ban đầu", gameObject);
            return;
        }
        else if (_name[0] != '@')
        {
            gameObject.name = '@' + _name;
        }
    }
#endif
    #endregion
}

#region EDITOR
#if UNITY_EDITOR
[CustomEditor(typeof(TransformRef), true)]
public class CI_PartRef : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        TransformRef t = (TransformRef)target;

        #region refresh name
        t.RefreshInspector();
        #endregion
    }
}
#endif
#endregion