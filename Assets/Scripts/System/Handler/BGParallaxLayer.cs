﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class BGParallaxLayer : MonoBehaviour
{
    [SerializeField]
    public float offX = 0f, offY = 0f;

    private void Awake()
    {
        transform.hideFlags = HideFlags.DontSave | HideFlags.NotEditable | HideFlags.HideInInspector;
    }

    private void OnDestroy()
    {
        transform.hideFlags = HideFlags.None;
    }
}