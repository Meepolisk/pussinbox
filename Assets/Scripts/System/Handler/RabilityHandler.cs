﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
[System.Serializable]
public class RabilitySlotManager
{
    [ReadOnly]
    public Ability abi;
    public short _slot = 0;
    public short slot
    {
        get { return _slot; }
        set
        {
            Mathf.Clamp(value, 0, 4);
        }
    }
}
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(RabilitySlotManager))]
public class RabilityProfileDrawer : PropertyDrawer
{

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var abiRect = new Rect(position.x, position.y, position.width - 10, position.height);
        var slotRect = new Rect(position.x + position.width - 20, position.y, 20, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(abiRect, property.FindPropertyRelative("abi"), GUIContent.none);
        EditorGUI.PropertyField(slotRect, property.FindPropertyRelative("_slot"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
public class RabilityHandler : MonoBehaviour
{

    public RabilitySlotManager[] ability;
}