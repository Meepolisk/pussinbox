﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class AnimationHelper
{
    //old system
    //public HashSet<string> APHS = new HashSet<string>();
    //ModelHandler handler;
    //Animator animator;
    //public AnimationHelper(ModelHandler _handler)
    //{
    //    handler = _handler;
    //    animator = handler.GetComponent<Animator>();
    //    handler.rObject.RegNewAction("BreakAnimator", Stop);
    //    initList();
    //}

    //public void Stop()
    //{
    //    //bat vat ly
    //    handler.sprites.physicCollidersEnable = true;

    //    animator.enabled = false;
    //}
    //public bool GetBool(string str)
    //{
    //    if (hasAnimation(str))
    //        return animator.GetBool(str);
    //    return false;
    //}

    //public void SetTrigger(string str)
    //{
    //    if (!condition(str)) return;
    //    animator.SetTrigger(str);
    //}
    //public void ResetTrigger(string str)
    //{
    //    if (!condition(str)) return;
    //    animator.ResetTrigger(str);
    //}
    //public void SetBool(string str, bool b)
    //{
    //    if (!condition(str)) return;
    //    animator.SetBool(str, b);
    //}
    //public void SetFloat(string str, float f)
    //{
    //    if (!condition(str)) return;
    //    animator.SetFloat(str, f);
    //}
    //public void SetInt(string str, int i)
    //{
    //    if (!condition(str)) return;
    //    animator.SetInteger(str, i);
    //}

    //private bool condition(string str)
    //{
    //    if (hasAnimation(str))
    //        return true;
    //    return false;
    //}
    //private void initList()
    //{
    //    if (animator == null) return;
    //    foreach (AnimatorControllerParameter param in animator.parameters)
    //    {
    //        APHS.Add(param.name);
    //    }
    //}
    //public bool hasAnimation(string str)
    //{
    //    if (APHS.Contains(str))
    //        return true;
    //    return false;
    //}
    //public void CheckAndPlay(string _animationString)
    //{
    //    foreach (AnimatorControllerParameter param in animator.parameters)
    //    {
    //        if (param.name == _animationString)
    //        {
    //            if (param.type == AnimatorControllerParameterType.Bool)
    //                animator.SetBool(_animationString, true);
    //            else if (param.type == AnimatorControllerParameterType.Trigger)
    //                animator.SetTrigger(_animationString);
    //            return;
    //        }
    //    }
    //}
    
    public Dictionary<string,List<Animator>> APHS = new Dictionary<string, List<Animator>>();
    List<Animator> animators = new List<Animator>();
    Animator mainAnimattor;
    ModelHandler handler;

    public void setup(ModelHandler _handler)
    {
        handler = _handler;
        mainAnimattor = _handler.GetComponent<Animator>();
        handler.rObject.RegNewAction("BreakAnimator", Stop);
    }

    public void Stop()
    {
        //bat vat ly
        handler.sprites.physicCollidersEnable = true;

        foreach (var anim in animators)
            anim.enabled = false;
    }
    //public bool GetBool(string str)
    //{
    //    if (hasAnimation(str))
    //        return APHS[str]. .GetBool(str);
    //    return false;
    //}

#if UNITY_EDITOR
    bool debugCanPass(Animator anim)
    {
        ModelHelper mh = anim.GetComponent<ModelHelper>();
        if (mh)
        {
            if (mh.DebugMode)
                return false;
            return true;
        }
        return false;
    }
#endif
    public void SetTrigger(string str, bool includeMain = true)
    {
        if (!condition(str)) return;

        foreach (var anim in APHS[str])
        {
            if (anim == mainAnimattor && !includeMain)
                continue;
#if UNITY_EDITOR
            if (debugCanPass(anim))
#endif
                anim.SetTrigger(str);
        }
    }
    public void ResetTrigger(string str, bool includeMain = true)
    {
        if (!condition(str)) return;

        foreach (var anim in APHS[str])
        {
            if (anim == mainAnimattor && !includeMain)
                continue;
#if UNITY_EDITOR
            if (debugCanPass(anim))
#endif
                anim.ResetTrigger(str);
        }
    }
    public void SetBool(string str, bool b, bool includeMain = true)
    {
        if (!condition(str)) return;

        foreach (var anim in APHS[str])
        {
            if (anim == mainAnimattor && !includeMain)
                continue;
#if UNITY_EDITOR
            if (debugCanPass(anim))
#endif
                anim.SetBool(str, b);
        }
    }
    public void SetFloat(string str, float f, bool includeMain = true)
    {
        if (!condition(str)) return;

        foreach (var anim in APHS[str])
        {
            if (anim == mainAnimattor && !includeMain)
                continue;
#if UNITY_EDITOR
            if (debugCanPass(anim))
#endif
                anim.SetFloat(str, f);
        }
    }
    public void SetInt(string str, int i, bool includeMain = true)
    {
        if (!condition(str)) return;

        foreach (var anim in APHS[str])
        {
            if (anim == mainAnimattor && !includeMain)
                continue;
#if UNITY_EDITOR
            if (debugCanPass(anim))
#endif
                anim.SetInteger(str, i);
        }
    }
    
    private bool condition(string str)
    {
        if (APHS.ContainsKey(str))
            return true;
        return false;
    }


    public void InitList(Animator _animator)
    {
        if (_animator == null) return;

        animators.Add(_animator);
        foreach (AnimatorControllerParameter param in _animator.parameters)
        {
            if (!APHS.ContainsKey(param.name))
            {
                APHS.Add(param.name,new List<Animator>());
            }
            APHS[param.name].Add(_animator);
        }
    }
    public void RemoveList(Animator _animator)
    {
        if (_animator == null) return;

        animators.Remove(_animator);
        foreach (AnimatorControllerParameter param in _animator.parameters)
        {
            APHS[param.name].Remove(_animator);
            if (APHS[param.name].Count == 0)
            {
                APHS.Remove(param.name);
            }
        }
    }
    //public void CheckAndPlay(string _animationString)
    //{
    //    foreach (AnimatorControllerParameter param in animator.parameters)
    //    {
    //        if (param.name == _animationString)
    //        {
    //            if (param.type == AnimatorControllerParameterType.Bool)
    //                animator.SetBool(_animationString, true);
    //            else if (param.type == AnimatorControllerParameterType.Trigger)
    //                animator.SetTrigger(_animationString);
    //            return;
    //        }
    //    }
    //}
}