﻿using UnityEngine;
using System.Collections.Generic;
using System;

[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public abstract class ModelHelper : MonoBehaviour
#if UNITY_EDITOR
    ,hideTransform
#endif 
{
#if UNITY_EDITOR
    public bool DebugMode = false;
#endif

    [System.Serializable]
    public class AudioAddOn
    {
        public AudioClip[] Awake;
    }

    [Header("=============")]
    [ReadOnly]
    public RObject rObject;
    public new AudioAddOn audio;

    protected Animator animator;

    protected virtual void Awake()
    {
        if (rObject == null)
        {
            rObject = transform.parent.GetComponent<RObject>();
        }

        animator = GetComponent<Animator>();

        //sound
        if (audio != null && audio.Awake.Length > 0)
            GameMaster.PlayRandomSound(audio.Awake);
    }


    #region animation
    
    public void SendMess(string message)
    {
        rObject.AnimationMess(message);
    }
    #endregion
}