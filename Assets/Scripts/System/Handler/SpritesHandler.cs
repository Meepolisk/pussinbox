﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class SpritesHandler : MonoBehaviour
#if UNITY_EDITOR
    ,hideTransform, editorHierachyChange, editorUpdate
#endif 
{
    [ReadOnly]
    public ModelHandler model;

    const short customDepthPlus = 20;
    /// <summary>
    /// Chinh order layer cua cac object con: spriteRenderer, spritesMask
    /// </summary>
    public int customDepth
    {
        set
        {
            if (value == 0) return;
            int result = value * (int)customDepthPlus;
            if (helpers.Count > 0)
            {
                foreach (var helper in helpers)
                {
                    helper.renderer.sortingOrder += result;
                }
                SpriteMask[] masks = GetComponentsInChildren<SpriteMask>();
                foreach (var mask in masks)
                {
                    mask.frontSortingOrder += result;
                    mask.backSortingOrder += result;
                }

            }
        }
    }
    [ReadOnlyWhenPlaying]
    public bool isRightFacing = true;
    [ReadOnly]
    public bool isFlipped = false;

    #region override Color
    public void OverlayOpacity(float _newAlpha)
    {
        foreach (var hp in helpers)
        {
            hp.OverlayOpacity(_newAlpha);
        }
    }
    #endregion

    private bool _whitenSprite = false;
    public bool whitenSprite
    {
        get
        {
            return _whitenSprite;
        }
        set
        {
            _whitenSprite = value;
            foreach (var hp in helpers)
            {
                hp.whiteSprite = _whitenSprite;
            }
        }
    }
    private GameObject _frapHandler;
    public GameObject frapHandler
    {
        get
        {
            if (_frapHandler == null)
                _frapHandler = gameObject.createEmptyChild("FrapHandler");
            return _frapHandler;
        }
    }
    [HideInInspector]
    public List <SpriteHelper> helpers = new List<SpriteHelper>();
    [HideInInspector]
    public List<SpriteReplacer> replacer = new List<SpriteReplacer>();
    [HideInInspector]
    public Dictionary<string, Attachment> attachments = new Dictionary<string, Attachment>();

    private List<Collider2D> _physicColliders = new List<Collider2D>();
    public bool physicCollidersEnable
    {
        set
        {
            if (_physicColliders.Count > 0)
            {
                foreach (var pC in _physicColliders)
                {
                    pC.enabled = value;
                }
            }
        }
    }
    
    public Vector2 GetAttachmentCenter(string _name)
    {
        try
        {
            return attachments[_name].center;
        }
        catch
        {
            return model.rObject.center;
        }
    }
    

    private void Awake()
    {
        if (model == null)
        {
            model = gameObject.GetComponentInParent<ModelHandler>();
            model.sprites = this;
        }
        
        InstanceHelpers();

        setPhysicCollider();
        physicCollidersEnable = false;
    }
    private void setPhysicCollider()
    {
        Collider2D[] cols = GetComponentsInChildren<Collider2D>();
        foreach (var pC in cols)
        {
            if (pC.GetComponent<Rigidbody2D>() != null)
            {
                _physicColliders.Add(pC);
            }
        }
    }

    private void InstanceHelpers()
    {
        SpriteRenderer[] sprds = GetComponentsInChildren<SpriteRenderer>();
        foreach (var spr in sprds)
        {
            SpriteHelper newSH;
            newSH = spr.GetComponent<SpriteHelper>();
            if (newSH.isNull())
                newSH = spr.gameObject.AddComponent<SpriteHelper>();
            newSH.Setup(this);
        }
    }

    #region SpritingCode
    private RTimer whitenSpriteTimer = null;
    const float minimunWhiteTime = 0.1f;
    public void StartWhitenSprite(float _time = 0.1f)
    {
        float _t = (_time < minimunWhiteTime) ? minimunWhiteTime : _time;
        whitenSpriteTimer = RTimer.Add1TimeTimer("[WhiteTimer]" + model.rObject.gameObject.name, _t, WhitenTimerExpire);
        whitenSprite = true;
    }
    private void WhitenTimerUpdate()
    {
        whitenSprite = !whitenSprite;
    }
    private void WhitenTimerExpire()
    {
        whitenSprite = false;
        whitenSpriteTimer = null;
    }

    RTimer fadingTimer = null;
    public void StartFading(float _timeToFade)
    {
        if (_timeToFade > 0f)
            fadingTimer = RTimer.Add1TimeTimer("[FadingTimer]" + gameObject.name, _timeToFade, FadingTimerExpire, FadingTimerUpdate);
        else
        {
            FadingTimerExpire();
        }
    }
    private void FadingTimerUpdate()
    {
        if (fadingTimer == null) return;
        float time = fadingTimer.time, timeMax = fadingTimer.timerMax;
        OverlayOpacity(1f - (time / timeMax));
    }
    private void FadingTimerExpire()
    {
        fadingTimer = null;
        OverlayOpacity(1f);
        Destroy(model.rObject.gameObject);
    }
    float fadeTime = 2f;
    public void WaitThenFade(float waitTime, float _fadeTime = 1.5f)
    {
        fadeTime = _fadeTime;
        if (waitTime > 0f)
        {
            RTimer.Add1TimeTimer("[DecayTimer]" + gameObject.name, waitTime, DecayTimerExpire);
        }
        else
        {
            DecayTimerExpire();
        }
    }
    private void DecayTimerExpire()
    {
        StartFading(fadeTime);
    }
    private float offsetX
    {
        get
        {
            if (model.rObject.rObjectCollider != null)
                return (model.rObject.rObjectCollider.center.x - transform.position.x);
            else
                return 0f;
        }
    }
    #endregion
    #region public call
    [ReadOnly][SerializeField]
    private bool _hide = false;
    public bool Hide
    {
        set
        {
            _hide = value;
            foreach (var hp in helpers)
            {
                hp.Hide = _hide;
            }
        }
        get
        {
            return _hide;
        }
    }
    public void Flip(bool checkTurnAnimation = true)
    {
        isRightFacing = !isRightFacing;
        
        //đổi chiều scaling
        Vector3 LS = transform.localScale;
        LS.x *= -1;
        transform.localScale = LS;
        //position
        if (isFlipped == true)
        {
            transform.localPosition = Vector3.zero;
        }
        else
        {
            Vector3 LP = transform.position;
            LP.x += (offsetX*2f);
            transform.position = LP;
#if UNITY_EDITOR
            editorPos = transform.position;
#endif
        }
        isFlipped = !isFlipped;

#if UNITY_EDITOR
        //if (!Application.isEditor) // nếu editor thì chắc chắn là flip từ util;
#endif
        if (checkTurnAnimation)
            model.animation.SetTrigger("Turn");
    }
    #endregion

    #region EDITOR_SETUP
#if UNITY_EDITOR
    private Vector2 editorPos = Vector2.zero;
    private void Reset()
    {
        Debug.Log("comp reset");
        setup();
    }
    private void setup()
    {
        RObject _rObjectBase = transform.parent.GetComponent<RObject>();
        if (_rObjectBase)
        {
            model = gameObject.GetComponentInParent<ModelHandler>();
            model.sprites = this;
        }
        else
        {
            model = null;
        }
    }

        public void editorHierachyChange()
        {
            setup();
        }

    public void EditorUpdate()
    {
        HideAndRetransform();
    }
    public void HideAndRetransform()
    {
        if (!Application.isPlaying)
        {
            transform.localPosition = editorPos;
        }
        transform.eulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;
    }
#endif
    #endregion
}
