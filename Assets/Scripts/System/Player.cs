﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Player
{
    public static Dictionary<short, rPlayer> number = new Dictionary<short, rPlayer>();
    public static void ReinstancePlayer()
    {
        number.Clear();
        number.Add(0, new rPlayer());
        number.Add(1, new rPlayer());
        number.Add(2, new rPlayer());
        number.Add(3, new rPlayer());
        number.Add(4, new rPlayer());
    }

    public class rPlayer
    {
        private Pilot _hero = null;
        public Pilot hero
        {
            get
            {
                return _hero;
            }
            set
            {
                //Debug.Log("set HERO = " + value);
                _hero = value;
            }
        }
        public List<Unit> units = new List<Unit>();
        public rPlayer()
        {
            units.Clear();
        }

        public void RegNewUnit(Unit _unit)
        {
            if (!units.Contains(_unit))
            {
                units.Add(_unit);
            }
        }
        public void UnRegUnit(Unit _unit)
        {
            if (units.Contains(_unit))
            {
                units.Remove(_unit);
            }
        }
    }
}


