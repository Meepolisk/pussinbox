﻿using System.Collections.Generic;
using UnityEngine;

public static partial class ExtensionMethods
{
    public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
    {
        transform.localScale = Vector3.one;
        try
        {
            transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, 1f);
        }
        catch
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }

    }
    public static T AddOrGetComponent<T>(this GameObject _gameObject) where T : Component
    {
        Component _comp;
        _comp = _gameObject.GetComponent<T>();
        if (_comp != null)
            return _comp as T;
        return _gameObject.AddComponent<T>();
    }
    public static Component CloneToGameObject(this Component original, GameObject destination)
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        // Copied fields can be restricted with BindingFlags
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy;
    }
    public static T CloneToGameObject<T>(this T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy as T;
    }
    public static Rigidbody2D Clone(this Rigidbody2D original, GameObject destination)
    {
        Rigidbody2D copy = destination.AddComponent<Rigidbody2D>();
        #region copy value
        copy.angularDrag = original.angularDrag;
        copy.angularVelocity = original.angularVelocity;
        copy.bodyType = original.bodyType;
        copy.centerOfMass = original.centerOfMass;
        copy.collisionDetectionMode = original.collisionDetectionMode;
        copy.isKinematic = original.isKinematic;
        if (copy.isKinematic != true) // nếu không có cái này thì sẽ hiện warning
            copy.constraints = original.constraints;
        copy.drag = original.drag;
        copy.freezeRotation = original.freezeRotation;
        copy.gravityScale = original.gravityScale;
        copy.inertia = original.inertia;
        copy.interpolation = original.interpolation;
        copy.mass = original.mass;
        copy.sharedMaterial = original.sharedMaterial;
        copy.simulated = original.simulated;
        copy.sleepMode = original.sleepMode;
        copy.useAutoMass = original.useAutoMass;
        copy.useFullKinematicContacts = original.useFullKinematicContacts;
        #endregion
        return copy;
    }

    public static float normalize(this float f)
    {
        return Mathf.Clamp(f, -1f, 1f);
    }
    public static float revertZangle(this float f)
    {
        f = f.toAbs360();
        if (f == 90 || f == -90) return f;
        return (180f - f);
    }
    /// <summary>
    /// Kết quả luôn ra số độ từ (-180, 180]
    /// </summary>
    /// <param name="f"></param>
    /// <returns></returns>
    public static float to180(this float f)
    {
        if (f <= -180f) return (f + 360f).to180();
        if (f > 180f) return (f - 360f).to180();
        return f;
    }
    /// <summary>
    /// Kết quả luôn ra số độ từ [0, 360)
    /// </summary>
    /// <param name="f"></param>
    /// <returns></returns>
    public static float toAbs360(this float f)
    {
        if (f < 0f) return (f + 360f).toAbs360();
        if (f >= 360f) return (f - 360f).toAbs360();
        return f;
    }
    //public Component _lastCheckedComponent
    //public static bool hasComponent<T>(this GameObject go, out Component _component) where T : Component
    //{
    //    _component = null;
    //    if (go.isNull())
    //        return false;
    //    if ((_component = go.GetComponent<T>()) != null)
    //    return true;
    //}
    public static bool isNull(this object obj)
    {
        return ReferenceEquals(obj, null) || obj == null || obj.Equals(null);
    }
    public static bool isCollider(this GameObject go)
    {
        if (go.GetComponent<Platform>() != null)
            return true;
        return false;
    }
    public static bool isInRange(this float var, float min, float max)
    {
        if (var >= min && var <= max)
            return true;
        return false;
    }
    public static bool isInRangeOffset(this float var, float value, float offset)
    {
        if (var >= (value - offset) && var <= (value + offset))
            return true;
        return false;
    }
    public static bool isInRangeOffset(this Vector2 pos, Vector2 target, float offset)
    {
        if (Vector2.Distance(pos, target) <= offset)
            return true;
        return false;
    }
    public static GameObject findChild(this GameObject gameObject, string name, bool findDisabled = true)
    {
        //Transform[] transforms = (Transform[]) gameObject.GetComponentsInChildren(typeof(Transform), true);
        Transform[] transforms = gameObject.GetComponentsInChildren<Transform>(findDisabled);
        foreach (Transform tran in transforms)
        {
            if (tran.gameObject.name == name)
            {
                return tran.gameObject;
            }
        }
        Debug.LogWarning("Can find child object named " + name + " in Object: " + gameObject.name);
        return null;
    }
    public static GameObject[] findChildsWithTag(this GameObject gameObject, string tagName, bool findDisabled = false)
    {
        List<GameObject> gos = new List<GameObject>();
        Transform[] allChilds = gameObject.GetComponentsInChildren<Transform>();
        foreach (Transform tran in allChilds)
        {
            GameObject t_go = tran.gameObject;
            //Debug.Log(t_go.name);
            //if (t_go.tag == tagName && !(findDisabled == false && t_go.activeInHierarchy == true))
            if (t_go.tag == tagName)
            {
                gos.Add(t_go);
            }
        }
        //debug
        if (gos.Count == 0)
        {
            Debug.Log("không tìm thấy child gameobject có tag " + tagName);
            return null;
        }
        return gos.ToArray();
    }
    public static GameObject createEmptyChild(this GameObject parent, string name)
    {
        GameObject child = new GameObject();
        child.name = name;
        child.transform.position = parent.transform.position;
        child.transform.SetParent(parent.transform);
        return child;
    }
    public static GameObject createChild(this GameObject parent, GameObject prefab)
    {
        GameObject child = GameObject.Instantiate(prefab);
        child.transform.position = parent.transform.position;
        child.transform.SetParent(parent.transform);
        return child;
    }
    public static Color changeAlpha(this Color oldColor, float alpha)
    {
        return new Color(oldColor.r, oldColor.g, oldColor.b, alpha);
    }
    public static Color changeColor(this Color oldColor, Color newColor)
    {
        return new Color(newColor.r, newColor.g, newColor.b, oldColor.a);
    }
    public static KeyCode toKeyCode(this RKeyCode _rkeycode)
    {
        return (KeyCode)(int)_rkeycode;
    }
    #region Vector2
    public static Vector2 fromPoint2Point(this Vector2 vec, Vector2 source, Vector2 target)
    {
        return vec;
    }
    public static Vector2 withOffset(this Vector2 point, Vector2 targetPos, float rangeOffset)
    {
        Vector2 vec = point + ((targetPos - point).normalized * rangeOffset);
        return vec;
    }
    public static Vector2 forceResistGravity(this Rigidbody2D rigid)
    {
        return (Vector2.up * -Physics2D.gravity.y * rigid.gravityScale * rigid.mass);
    }
    #endregion
}
