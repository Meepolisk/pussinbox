﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : RObject {
    #region var
    [Header("============Unit===========")]
    public short player = 0;
    public HitPoint hitPoint = new HitPoint();
    public RPoint energy = new RPoint(0,100);
    public float energyRegen = 1f;
    public bool isDead = false;
    [ReadOnly]
    public short pauseTick = 0;
    public bool canControl
    {
        get
        {
            if (pauseTick <= 0)
                return true;
            return false;
        }
        set
        {
            if (value == false)
            {
                pauseTick++;
            }
            else
                pauseTick--;
        }
    }
    public float decayTime = 3f;
    
    [ReadOnly]
    public Movement movement;
    [HideInInspector]
    public ROrderHandler orders;
    #endregion

    #region private stuff
    protected override void Awake ()
    {
        base.Awake();
        orders = gameObject.AddComponent<ROrderHandler>();
        hitPoint.setup(this);
    }
    protected virtual void OnEnable()
    {
        if (!isDead)
            Player.number[player].RegNewUnit(this);
        rEvent.Start.Unit_Enabled(this);
    }
    
    protected virtual void OnDisable()
    {
        rEvent.Start.Unit_Disabled(this);
    }
    protected override IEnumerator setup()
    {
        model.sprites.gameObject.SetActive(false);
        yield return null;
        model.sprites.gameObject.SetActive(true);
        FinishSetup();
    }


    protected virtual void Die (Unit _killer)
    {
        if (isDead) return;

        gameObject.GetComponent<Rigidbody2D>().velocity =  Vector2.zero;
        //gameObject.GetComponent<Rigidbody2D>().gravityScale = 0f;
        model.animation.SetTrigger("Die");
        model.animation.SetBool("isDead",true);
        isDead = true;

        //remove all timer
        RTimer[] timers = RTimer.GetAllTimerInObject(gameObject);
        foreach (RTimer timer in timers)
            timer.stop();
        
        //testHang
        Model_ExploOnDead meoD = gameObject.GetComponentInChildren<Model_ExploOnDead>();
        if (meoD != null)
        {
            meoD.ExploOnDead();
        }

        //Remove Unit khỏi game
        if (decayTime != 0)
            model.sprites.WaitThenFade(decayTime);
        
        foreach (var addOn in GetComponentsInChildren<UnitAddOn>())
        {
            addOn.Unit_Die();
        }
        rEvent.Start.Unit_Die(this, _killer);
        //Un reg player list
        Player.number[player].UnRegUnit(this);
    }

    protected virtual void Update()
    {
        regenEnergy();
        regenShield();
        hitPoint.stun.update();
    }
    private void regenEnergy()
    {
        energy.current += energyRegen * Time.deltaTime;
    }
    private void regenShield()
    {
        if (hitPoint.shieldRegenTimer >= HitPoint.shieldRegenTimerMax)
        {
            hitPoint.shield.current += HitPoint.shieldRegenPerSec * Time.fixedDeltaTime;
        }
        else
        {
            hitPoint.shieldRegenTimer += Time.deltaTime;
        }
    }
    #endregion
    #region public stuff

    /// <summary>
    /// Deal dmg theo DAMAGE
    /// </summary>
    /// <param name="target"></param>
    /// <param name="dmg"></param>
    public virtual void damage(Unit target, Damage dmg)
    {
        if (target.hitPoint == null || target.hitPoint.invulnerable)
            return;

        float finalDmg = dmg.amount;
        if (PlayerGroup.isAlly(this, target))
            finalDmg *= dmg.allyScale;
        else
            finalDmg *= dmg.enemyScale;
        damage(target, finalDmg);
    }
    /// <summary>
    /// Deal dmg theo PURE DMG
    /// </summary>
    /// <param name="target"></param>
    /// <param name="dmg"></param>
    public void damage(Unit target, float dmgAmount)
    {
        if (target.hitPoint == null || target.hitPoint.invulnerable)
            return;

        if (isDead) return;
        if (dmgAmount <= 0f) return;
        target.BeingHit(dmgAmount);
        DealDamage(dmgAmount);

        if (target.hitPoint.health.current <= 0)
        {
            //Debug.Log(name + " killed " + target.name);
            target.Die(this);
        }
        else
        {
            target.model.sprites.StartWhitenSprite(0.1f);
            if (target.hitPoint.stun.willStun == true)
            {
                target.model.animation.SetTrigger("Hit");
                target.StopAllActiveAbility();
                target.hitPoint.stun.willStun = false;
            }
        }
    }
    protected virtual void DealDamage(float finalDmg)
    {

    }
    protected virtual void BeingHit(float finalDmg)
    {
        hitPoint.damaged(finalDmg);
    }

    public void copyPlayerStat(Unit oldUnit)
    {
        this.player = oldUnit.player;
        //movement
        if (model.sprites.isRightFacing != oldUnit.model.sprites.isRightFacing)
        {
            movement.actionFlip();
        }
    }

    private bool _hide = false;
    private bool _baseRigidKine = false;
    public bool Hide
    {
        set
        {
            if (value == true && !_hide)
            {
                model.Hide = true;
                this.enabled = false;
                _baseRigidKine = rigid.isKinematic;
                rigid.velocity = Vector2.zero;
                rigid.isKinematic = true;
            }
            else if (value == false && _hide)
            {
                model.Hide = false;
                this.enabled = true;
                rigid.isKinematic = _baseRigidKine;
            }
            _hide = value;
        }
        
    }

    public void playSoundRandom(AudioClip[] _audios)
    {
        if (_audios.Length <= 0) return;
        int i = Random.Range(0, _audios.Length);
        AudioSource.PlayClipAtPoint(_audios[i], transform.position);
    }
    #endregion

    #region physic frome ObjectCollider


#endregion

    #region static method
    public static bool GameObjectIsUnit(GameObject go)
    {
        if (go.GetComponent<Unit>() != null)
            return true;
        return false;
    }

    public static Unit[] GetUnitsInRange(Vector2 pos, float AoE)
    {
        Collider2D[] affects = Physics2D.OverlapCircleAll(pos, AoE);
        List<Unit> group = new List<Unit>();
        foreach (Collider2D _targetCol in affects)
        {
            if (_targetCol.GetComponent<RObjectCollider>() != null)
            {
                Unit _targetUnit = _targetCol.GetComponentInParent<Unit>();
                if (_targetUnit != null && !group.Contains(_targetUnit))
                {
                    group.Add(_targetUnit);
                }
            }
        }
        if (group.Count > 0)
            return group.ToArray();
        else
            return null;
    }
    #endregion

    #region Ability
    public Dictionary<string, Ability> abilityGroupActive = new Dictionary<string, Ability>();
    
    [HideInInspector]
    public List<Ability> abilities = new List<Ability>();
    [HideInInspector]
    public List<AbilityButton> button = new List<AbilityButton>();
    
    public void AddGroup(string key)
    {
        if (!abilityGroupActive.ContainsKey(key))
            abilityGroupActive.Add(key, null);
    }
    public bool CheckCurrentGroupAbi(Ability _abi)
    {
        string _groupName = _abi.group.name;
        if (string.IsNullOrEmpty(_groupName)) //default thì pass luôn
            return false;

        Ability _activeAbi = abilityGroupActive[_groupName];
        if (_activeAbi == null) //nếu k có active Abi trong group này thì
            return false;
        if (_activeAbi.group.priority <= _abi.group.priority) //nếu thằng đang active, có priority nhỏ hơn hoặc thấp hơn thằng check thì next
            return false;
        return true;
    }
    public void ReplaceCurrentGroupAbi(Ability _abi)
    {
        if (string.IsNullOrEmpty(_abi.group.name)) return; //k làm gì cả nếu là abi group default

        if (abilityGroupActive[_abi.group.name] != null) //nếu group này có active abi
        {
            abilityGroupActive[_abi.group.name].baseAbilityFinished();
        }
        abilityGroupActive[_abi.group.name] = _abi;
    }
    public void RemoveCurrentGroupAbi(Ability _abi)
    {
        if (string.IsNullOrEmpty(_abi.group.name)) return; //k làm gì cả nếu là abi group default

        if (abilityGroupActive[_abi.group.name] == _abi)
            abilityGroupActive[_abi.group.name] = null;
    }
    public void StopAllActiveAbility()
    {
        foreach (var abi in abilities)
        {
            if (abi.isActive)
                abi.baseAbilityFinished();
        }
    }
    #endregion
}

//#region EDITOR
//#if UNITY_EDITOR
//[CustomEditor(typeof(Unit))]
//public class CustomInspectorUnit : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        base.OnInspectorGUI();
//        Unit unit = (Unit)target;

//        #region adjust Var to get / set
//        unit.hitPoint.health.current = unit.hitPoint.health._current;
//        unit.hitPoint.shield.current = unit.hitPoint.shield._current;
//        unit.hitPoint.armor.current = unit.hitPoint.armor._current;
//        #endregion
//    }
//}
//#endif
//#endregion
