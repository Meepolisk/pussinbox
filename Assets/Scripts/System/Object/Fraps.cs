﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frap : RObject {
    public bool autoDestruct = true;
    public float lifeTime = 2f;
    private float lifeTimeTick = 0f;
    private bool lifeTimeEnable = false;
    public REditorFloat decayTime = new REditorFloat(1.5f,3f);
    public float fadeTime = 0.5f;
    //public Vector2 forceApply = Vector2.zero;
    
    protected override void Start()
    {
        base.Start();
        if (autoDestruct)
            lifeTimeEnable = true;

        //apllyForce
        //rigid.AddForce(forceApply);
    }
    protected override void FinishSetup()
    {
        base.FinishSetup();
        rObjectCollider.gameObject.layer = (int)RLayerMask.Fraps;
        rObjectCollider.collider.isTrigger = false;
    }

    private void Update()
    {
        if (lifeTimeEnable)
        {
            lifeTimeTick += Time.deltaTime;
            if (lifeTimeTick>=lifeTime)
                Destruct();
        }
    }
    private void Destruct()
    {
        lifeTimeEnable = false;
        model.sprites.WaitThenFade(decayTime.value, fadeTime);
    }

    public static Frap _lastCreatedFrap;
    public static Frap Create (FrapProfile _data, Vector2 _pos, float _zAngle)
    {
        GameObject emptyGameObject = RPlatformer.CreateEmptyGameObject(_pos, _zAngle);
        emptyGameObject.name = _data.model.model.gameObject.name;
        _lastCreatedFrap = emptyGameObject.AddComponent<Frap>();
        _data.model.InstanceOnRObject(_lastCreatedFrap);

        _lastCreatedFrap.lifeTime = _data.lifeTime.value;
        //_lastCreatedFrap.decayTime = _data.decayTime.value;
        //_lastCreatedFrap.fadeTime = _data.fadeTime.value;
        return _lastCreatedFrap;
        //_lastCreatedFrap.forceApply = _data.forceApply;
    }

    public static Frap CreateFromSprite(Sprite _sprite, float _lifeTime, GameObject _originalGO)
    {
        Vector2 _pos = _originalGO.transform.position;
        //float _zAngle = _originalGO.transform.eulerAngles.z;
        GameObject emptyGameObject = new GameObject();
        emptyGameObject.name = "[StaticFrap]" + _originalGO.name;
        _lastCreatedFrap = emptyGameObject.AddComponent<Frap>();
        _lastCreatedFrap.lifeTime = _lifeTime;
        emptyGameObject.AddComponent<Rigidbody2D>();
        GameObject _modelGO = emptyGameObject.createEmptyChild("Model");
        _modelGO.AddComponent<ModelHandler>();
        //sprite
        GameObject _spriteGO = _modelGO.createEmptyChild("Sprites");
        _spriteGO.AddComponent<SpritesHandler>();
        SpriteRenderer _oldRenderer = _originalGO.GetComponent<SpriteRenderer>();
        SpriteRenderer _renderer = _spriteGO.AddComponent<SpriteRenderer>();
        _renderer.sprite = _sprite;
        _renderer.sortingLayerID = _oldRenderer.sortingLayerID;
        _renderer.sortingOrder = _oldRenderer.sortingOrder;
        _spriteGO.AddComponent<PolygonCollider2D>();
        _spriteGO.AddComponent<RObjectCollider>();
        //collider
        //GameObject _colliderGO = _modelGO.createEmptyChild("RObjectCollider");
        //chỉnh vị trí sau cùng
        emptyGameObject.transform.position = _originalGO.transform.position;
        emptyGameObject.transform.localScale = _originalGO.transform.lossyScale;
        return _lastCreatedFrap;
    }
    T CopyComponent<T>(T original, GameObject destination) where T : Component
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy as T;
    }
}

[System.Serializable]
public class FrapProfile
{
    public ModelProfile model;
    public REditorFloat lifeTime = new REditorFloat(2f);

    public Frap Create (Vector2 _pos, float _zAngle)
    {
        return Frap.Create(this, _pos, _zAngle);
    }
}