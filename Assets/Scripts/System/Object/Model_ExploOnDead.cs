﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(ModelHandler))]
public class Model_ExploOnDead : MonoBehaviour
{
    public List<FrapManager> aditionalFraps = new List<FrapManager>();
    public Vector2 offset = new Vector2(1f, 1f);
    public string ReplaceSpriteName = "explo";
    [Header("Explosion Effect")]
    public ModelProfile specialEffect;
    private ModelHandler modelHandler;

    
    private void Awake()
    {
        modelHandler = GetComponent<ModelHandler>();
    }

    public void ExploOnDead()
    {
        Vector2 unitPos = modelHandler.rObject.rObjectCollider.center;
        //tạo effect
        SpecialEffect.Create(specialEffect, unitPos);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();

        //tạo Fraps:
        GameObject frapHandler = modelHandler.sprites.frapHandler;
        List<GameObject> listFrapRigid = new List<GameObject>();

        #region chuyển các part có Component SpriteReplacer
        if (modelHandler.sprites.replacer.Count > 0)
        {
            Transform[] allparts = modelHandler.sprites.GetComponentsInChildren<Transform>();
            {
                foreach (var part in allparts)
                {
                    SpriteReplacer replacer = part.GetComponent<SpriteReplacer>();
                    if (replacer != null)
                    {
                        Frap newFrap = Frap.CreateFromSprite(replacer.spritesAlternate["explo"], Random.Range(4f, 6f), replacer.gameObject);
                        listFrapRigid.Add(newFrap.gameObject);
                    }
                    part.gameObject.SetActive(false);
                }
            }
            //foreach (SpriteReplacer replacer in modelHandler.sprites.replacer)
            //{
                
            //    Frap newFrap = Frap.CreateFromSprite(replacer.spritesAlternate["explo"], Random.Range(4f, 6f), replacer.gameObject);

            //    replacer.gameObject.SetActive(false);

            //    listFrapRigid.Add(newFrap.gameObject);
            //}
        }
        #endregion

        #region tạo sparepart mới
        if (aditionalFraps.Count > 0) //chỉ tạo nếu có sparePart mới
        {
            foreach (var frP in aditionalFraps)
            {
                for (int i = 0; i < frP.count.value; i++)
                {
                    Vector2 pos = unitPos + new Vector2(Random.Range(-offset.x, offset.x), Random.Range(-offset.y, offset.y));
                    Frap newFrap = Frap.Create(frP.frap, pos, Random.Range(0f, 360f));
                    listFrapRigid.Add(newFrap.gameObject);
                }
            }
        }
        #endregion
        #region Tạo lực
        if (listFrapRigid.Count > 0)
        {
            Vector2 centerPos = modelHandler.sprites.transform.position;
            foreach (var frap in listFrapRigid)
            {
                //frap.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 1500f);
                
                Vector2 force = (Vector2)frap.transform.position - centerPos;
                frap.GetComponent<Rigidbody2D>().AddRelativeForce(force.normalized * Random.Range(500f, 1000f));
            }
        }
        #endregion
        //anim.enabled = false;
    }

}
 