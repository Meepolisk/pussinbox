﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent(typeof(Unit))]
public class PlayerController : UnitAddOn
{
    #region CacBien
    [System.Serializable]
    public class HeroProfile
    {
        public Texture heroImage;
        public HUI profile;
    }

    //bool holdDirection = false;
    public HeroProfile heroicProfile;
    
    private HUI UIref = null;

    #endregion
    
    protected void OnEnable()
    {
        if (UIref != null)
            UIref.gameObject.SetActive(true);
        rEvent.Start.Unit_Controlable(unitRef);
    }
    public override void FinishSetup()
    {
        base.FinishSetup();
        InstanceUI();
    }
    private void OnDisable()
    {
        if (UIref != null)
            UIref.gameObject.SetActive(false);
        rEvent.Start.Unit_Uncontrolable(unitRef);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        DestroyHeroUI();
    }
    public void InstanceUI()
    {
        if (!finishSetup) return; //chưa setup thì k instance
        if (UIref != null) return; //có rồi thì k instance nữa

        if (unitRef.player < 1 || unitRef.player > 4) return;
        if (heroicProfile.profile == null)
        {
            UIref = (Instantiate(Resources.Load("UI/HUI/HUI_Default")) as GameObject).GetComponent<HUI>();
        }
        else
        {
            UIref = (Instantiate(heroicProfile.profile.gameObject) as GameObject).GetComponent<HUI>();
        }
        UIref.Setup(this);
        //StartCoroutine(InstanceNextFrame());
    }
    private void DestroyHeroUI()
    {
        if (unitRef.player < 1 || unitRef.player > 4) return;
        if (UIref != null)
        {
            Destroy(UIref.gameObject);
        }
    }

    private void Update()
	{
        if (!finishSetup) return;

        //dieu khien
        if (unitRef.canControl && !unitRef.isDead)
        {
            //chỗ này sau này phân thành điều khiển nhiều người local coop
            DieuKhienDiChuyen();
            DieuKhienKiNang();
        }
    }

    void DieuKhienDiChuyen()
    {
        if (unitRef.movement.auto.isActive)
            return;
        float x = RInput.GetAxisX;// ("Horizontal");
        float y = RInput.GetAxisY;// ("Vertical");
        unitRef.movement.setInputDirection(x,y);
    }
    void DieuKhienKiNang()
    {
        if (unitRef.movement.auto.isActive)
            return;

        if (unitRef.button.Count <= 0) return;

        foreach (var button in unitRef.button)
        {
            button.trigger();
            //if (button.isTrigger())
            //{
            //    abi._hotkeyState = HotkeyState.press;
            //}
            //else
            //{
            //    abi._hotkeyState = HotkeyState.unpress;
            //}
        }
    }
}
