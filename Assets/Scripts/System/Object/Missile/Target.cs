﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class MissileTarget
{
    public bool enemy = true;
    public bool ally = false;
    public bool wall = true;
}

#region Editor
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(MissileTarget))]
public class MissileTargetDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        float tickSize = 15f;
        float textSize = position.width * (1f / 3f);
        var rect1a = new Rect(position.x, position.y, tickSize, position.height);
        var rect1b = new Rect(position.x + tickSize, position.y, textSize - tickSize, position.height);
        var rect2a = new Rect(position.x + textSize, position.y, tickSize, position.height);
        var rect2b = new Rect(position.x + textSize + tickSize, position.y, textSize - tickSize, position.height);
        var rect3a = new Rect(position.x + textSize * 2f, position.y, tickSize, position.height);
        var rect3b = new Rect(position.x + textSize * 2f + tickSize, position.y, textSize*2 - tickSize, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
        EditorGUI.PropertyField(rect1a, property.FindPropertyRelative("enemy"), GUIContent.none);
        GUI.Label(rect1b, "enemy");
        EditorGUI.PropertyField(rect2a, property.FindPropertyRelative("ally"), GUIContent.none);
        GUI.Label(rect2b, "ally");
        EditorGUI.PropertyField(rect3a, property.FindPropertyRelative("wall"), GUIContent.none);
        GUI.Label(rect3b, "wall");


        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
#endregion