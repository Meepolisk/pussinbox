﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rmissile_bounce : Rmissile
{
    //protected override void TurnOffMissile()
    //{
    //    Collider2D clder2D = rObjectCollider.collider;
    //    clder2D.isTrigger = false;
    //    rigid.gravityScale = 1f;
    //    rObjectCollider.gameObject.layer = LayerMask.NameToLayer("Fraps");

    //    if (true) // nếu hit unit phải giả lực force, vì Fraps không tác dụng với Collider của rObject
    //    {
    //        rigid.velocity *= (Random.Range(-1.3f, -0.7f) * clder2D.sharedMaterial.bounciness);
    //        detectTime = 0;
    //    }
    //    base.TurnOffMissile();
    //}

    protected override void CONTACT_PLATFORM(Platform _platform)
    {
        base.CONTACT_PLATFORM(_platform);
        if (!_isDead) return;

        Collider2D clder2D = rObjectCollider.collider;
        rigid.velocity *= (Random.Range(0.5f, 5f) * clder2D.sharedMaterial.bounciness);
    }
    protected override void CONTACT_UNIT(Unit _unit)
    {
        base.CONTACT_UNIT(_unit);
        if (!_isDead) return;

        Collider2D clder2D = rObjectCollider.collider;
        rigid.velocity *= (Random.Range(-2f, -0.5f) * clder2D.sharedMaterial.bounciness);
        detectTime = 0;
    }
    protected override void TurnOffMissile()
    {
        Collider2D clder2D = rObjectCollider.collider;
        clder2D.isTrigger = false;
        rigid.gravityScale = 1f;
        rObjectCollider.gameObject.layer = (int)RLayerMask.Fraps;
        base.TurnOffMissile();
    }
}


