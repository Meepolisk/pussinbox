﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Rmissile : RObject
{

    protected int detectTime = 1;
    protected Unit owner;
    [ReadOnly]
    public MissileTarget detectTarget;
    [ReadOnly]
    public ModelProfile unitHitEffect;
    [ReadOnly]
    public ModelProfile platformHitEffect;
    protected bool triggerOnExpire = false;
    protected rDelegate_Vec_GO exploAction;
    [ReadOnly]
    public float Zdirection;
    protected float speed = 10f;
    protected List<GameObject> objectTargets;

    protected bool _isDead = false;
    protected float time = 1f;

    private bool finishSetup = false;
    protected override void FinishSetup()
    {
        base.FinishSetup();
        rObjectCollider.gameObject.layer = (int)RLayerMask.Missile;
        rigid.velocity = RPlatformer.newVector(Zdirection, speed);
        StartCoroutine(expireCorotine());
        finishSetup = true;
    }
    IEnumerator expireCorotine()
    {
        yield return new WaitForSeconds(time);
        EXPIRE();
    }
    protected override void Awake()
    {
        base.Awake();
        gameObject.layer = (int)RLayerMask.Missile;
    }
    protected virtual void OnDisable()
    {
        StopAllCoroutines();
    }
    public virtual void Setup(MissileProfile data)
    {
        detectTime = data.advance.detectTime;
        owner = data.owner;
        detectTarget = data.advance.detectTarget;
        triggerOnExpire = data.advance.triggerOnExpire;
        exploAction = data.exploAction;
        speed = data.speed;
        time = data.timedLife;
        unitHitEffect = data.advance.contactEffect.hitUnit;
        platformHitEffect = data.advance.contactEffect.hitPlatform;

         objectTargets = new List<GameObject>();
        //this.gameObject.GetComponent<Rigidbody2D>().velocity = RPlatformer.newVector(Zdirection, data.speed);
        
    }
    
    protected virtual void CONTACT_UNIT(Unit _unit)
    {
        if (_unit.isDead)
            return;
        if (_unit.hitPoint == null)
            return;
        else 
        {
            if (_unit.hitPoint.invulnerable)
                return;
        }
        if (detectTime <= 0)
            return;
        if (PlayerGroup.isAlly(owner, _unit))
        {
            if (!detectTarget.ally)
                return;
        }
        else
        {
            if (!detectTarget.enemy)
                return;
        }
        if (detectTime > 0)
        {
            detectTime--;
            TRIGGER(_unit.gameObject);
            if (unitHitEffect.model != null)
                createFX_Unit();
        }
    }
    protected virtual void createFX_Unit()
    {
        Vector2 pos = this.center;
        SpecialEffect.Create(unitHitEffect, pos);
        SpecialEffect._lastCreatedSpecialEffect.zAngle = 0f;
        if (this.model.sprites.isFlipped)
            SpecialEffect._lastCreatedSpecialEffect.model.sprites.Flip();
        SpecialEffect.DestroyLastCreatedSpecialEffect();
    }
    const float maxRaycastRannge = 5f;
    protected virtual void CONTACT_PLATFORM (Platform _platform)
    {
        if (!detectTarget.wall) return;

        detectTime = 0;
        if (platformHitEffect != null)
        {
            //tìm ra vị trí contact
            Vector2 contactPoint = center;
            // tạo raycast
            RaycastHit2D[] hits = Physics2D.RaycastAll(center, RPlatformer.Angle2vector2(zAngle), maxRaycastRannge);
            foreach (var hit in hits)
            {
                if (hit.collider == _platform.collider)
                {
                    contactPoint = hit.point;
                    break;
                }
            }
            
            Vector2 pos = contactPoint;
            SpecialEffect.Create(platformHitEffect, pos);
            SpecialEffect._lastCreatedSpecialEffect.zAngle = _platform.angle - 180f;
            SpecialEffect.DestroyLastCreatedSpecialEffect();
        }
        TRIGGER(_platform.gameObject);
    }
    protected virtual void EXPIRE()
    {
        if (_isDead) return;
        if (triggerOnExpire)
        {
            //data.triggerOnExpire = false;
            detectTime = 0;
            TRIGGER(null);
        }
    }
    protected virtual void TRIGGER(GameObject _go)
    {
        exploAction.Invoke(gameObject.transform.position, _go);
        model.animation.SetTrigger("Die");
        if (detectTime <= 0)
        {
            TurnOffMissile();
        }
    }
    protected virtual void TurnOffMissile()
    {
        //model.Animation.SetTrigger("Expire");
        _isDead = true;
        float waitTime = Random.Range(GraphicSetting.MissileDecayTime * 0.8f, GraphicSetting.MissileDecayTime * 1.2f);
        model.sprites.WaitThenFade(waitTime,0.5f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject thisCol = col.gameObject;
        if (!objectTargets.Contains(thisCol))
        {
            objectTargets.Add(thisCol);
            detectThings(thisCol);
        }
    }
    private void detectThings(GameObject col)
    {
        #region Các điều kiện luôn sai và return
        if (_isDead)
            return;
        if (!finishSetup) //tạm thời thế này, chứ không optimize
            return;
        #endregion

        Platform rPF = col.GetComponent<Platform>();
        if (rPF)
        {
            CONTACT_PLATFORM(rPF);
        }
        RObjectCollider rOC = col.GetComponent<RObjectCollider>();
        if (rOC && rOC.model.rObject is Unit)
        {
            Unit _unit = rOC.model.rObject as Unit;
            if (_unit != owner)
            {
                CONTACT_UNIT(_unit);
            }
        }
    }

}


