﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class HitPoint
{
    [ReadOnly]
    [SerializeField]
    private short _invulnerable = 0;
    public bool invulnerable
    {
        get
        {
            return (_invulnerable>0)? true : false;
        }
        set
        {
            if (value == true)
                _invulnerable++;
            else
                _invulnerable--;
            //Debug.Break();
        }
    }
    private void setupInvulnerable()
    {
        unitRef.RegNewAction("Hit", stopInvulnerableFromAnimator);
    }
    private void stopInvulnerableFromAnimator()
    {
        invulnerable = false;
    }

    //Health
    public RPoint health = new RPoint(200f);
    public int healthIconMax
    {
        get { return Mathf.CeilToInt(health.max / lifePerIcon); }
    }
    public int healthIcon
    {
        get { return Mathf.CeilToInt(health.current / lifePerIcon); }
    }

    //Armor
    public RPoint armor = new RPoint(0f);
    public float _bonusArmor = 0f;
    public float bonusArmor
    {
        get
        {
            return Mathf.Clamp(_bonusArmor, 0, 200f);
        }
        set
        {
            _bonusArmor = Mathf.Clamp(value, 0, 200f);
        }
    }
    public int armorIconMax
    {
        get { return Mathf.CeilToInt((armor.max + _bonusArmor) / lifePerIcon); }
    }
    public int armorIcon
    {
        get { return Mathf.CeilToInt((armor.current + _bonusArmor) / lifePerIcon); }
    }
    public static float armorDamageReductionPercent = 0.5f; //50%
    private static float armorDamageReductionMax = 5f; //max = 5 point

    //Shield
    public RPoint shield = new RPoint(0f);
    public int shieldIconMax
    {
        get { return Mathf.CeilToInt(shield.max / lifePerIcon); }
    }
    public int shieldIcon
    {
        get { return Mathf.CeilToInt(shield.current / lifePerIcon); }
    }
    public static float shieldRegenPerSec = 30f;
    public float shieldRegenTimer = 3f;
    public static float shieldRegenTimerMax = 3f;
    
    //other
    public const float lifePerIcon = 25f;
    [SerializeField][ReadOnly]
    private int _iconAmountMax;
    public int iconAmountMax
    {
        get
        {
            RefreshIconAmount();
            return _iconAmountMax;
        }
        set { _iconAmountMax = value; }
    }
    public StunInfo stun;
    [System.Serializable]
    public class StunInfo
    {
        public float rawAmount = 50f;
        public float recoverTime = 0f;
        public float _currentStack = 0f;

        //private HitPoint hpRef;

        //public void awake(HitPoint _hpRef)
        //{
        //    hpRef = _hpRef;
        //}
        bool isEnabled
        {
            get
            {
                if (recoverTime <= 0 || rawAmount <= 0)
                    return false;
                return true;
            }
        }
        private bool _isActive = false;

        public void takeDamge(float _amount)
        {
            if (!isEnabled) return;
            if (_amount >= rawAmount)
                trigger();
            else
                add(_amount);
        }
        public void update()
        {
            Recover();
        }
        void Recover()
        {
            if (!_isActive) return;

            _currentStack -= (rawAmount / recoverTime) * Time.deltaTime;
            if (_currentStack <= 0)
                turnOff();
        }
        void add(float _bonusAmount)
        {
            _isActive = true;
            _currentStack += _bonusAmount;
            if (_currentStack >= rawAmount)
            {
                trigger();
            }
        }
        void turnOff()
        {
            _isActive = false;
            _currentStack = 0f;
        }
        void trigger()
        {
            turnOff();
            //gọi gì đó ở đây
            willStun = true;
        }

        [HideInInspector]
        public bool willStun = false;

    }
    private Unit unitRef;
    public void setup(Unit _unitRef)
    {
        unitRef = _unitRef;
        setupInvulnerable();

        health.current = health.current;
        shield.current = shield.current;
        armor.current = armor.current;
        RefreshIconAmount();
    }

    private void RefreshIconAmount()
    {
        _iconAmountMax = healthIconMax + shieldIconMax + armorIconMax;
    }
    public void damaged(float dmgAmount)
    {
        if (dmgAmount <= 0f || health.current <= 0f) return;
        shieldRegenTimer = 0f;
        float tmpDMG = dmgAmount;
        stun.takeDamge(dmgAmount);
        if (shield.current > 0f)
        {
            if (shield.current >= dmgAmount)
            {
                shield.current -= tmpDMG;
                //Debug.Log("Damaged " + tmpDMG.ToString("n2") + " shield");
                return;
            }
            else
            {
                tmpDMG -= shield.current;
                //Debug.Log("Damaged " + shield.current.ToString("n2") + " shield");
                shield.current = 0f;
            }
        }
        if (_bonusArmor > 0)
        {
            tmpDMG = ArmorToRealDmg(tmpDMG);
            if (_bonusArmor >= tmpDMG)
            {
                _bonusArmor -= tmpDMG;
                return;
            }
            else
            {
                tmpDMG -= _bonusArmor;
                _bonusArmor = 0f;
            }
        }
        if (armor.current > 0)
        {
            tmpDMG = ArmorToRealDmg(tmpDMG);
            if (armor.current >= tmpDMG)
            {
                armor.current -= tmpDMG;
                //Debug.Log("Damaged " + tmpDMG.ToString("n2") + " armor");
                return;
            }
            else
            {
                tmpDMG -= armor.current;
                //Debug.Log("Damaged " + armor.current.ToString("n2") + " armor");
                armor.current = 0f;
            }
        }
        if (health.current >= tmpDMG)
        {
            health.current -= tmpDMG;
            //Debug.Log("Damaged " + tmpDMG.ToString("n2") + " health");
            return;
        }
        else
        {
            //tmpDMG -= health.current;
            health.current = 0f;
            //Debug.Log("Damaged " + health.current.ToString("n2") + " health");
        }
        return;
    }
    public void healed(float healAmount)
    {
        if (healAmount <= 0f) return;
        float tmpHeal = healAmount;
        health.current += tmpHeal;
        tmpHeal -= (health.max - health.current);
        armor.current += tmpHeal;
        tmpHeal -= (armor.max - armor.current);
        shield.current += tmpHeal;
        tmpHeal -= (shield.max - shield.current);
        //Debug.Log("Healed " + (healAmount-tmpHeal).ToString("n2") + " health");
    }
    private float ArmorToRealDmg(float dmg)
    {
        return (dmg * armorDamageReductionPercent) > armorDamageReductionMax ?
            (dmg - armorDamageReductionMax)
            : (dmg - (dmg * armorDamageReductionPercent));
    }
    public float scale
    {
        get
        {
            try
            {
                //Debug.Log("HP scale = " + (remaining / max));
                return (remaining / max);
            }
            catch
            {
                return 0f;
            }
        }
    }
    public float percent
    {
        get
        {
            return scale * 100f;
        }
    }
    public float remaining
    {
        get { return (health.current + armor.current + bonusArmor + shield.current); }
    }
    public float max
    {
        get { return (health.max + armor.max + bonusArmor + shield.max); }
    }
    //public HitPoint clone()
    //{
    //    HitPoint newOne = new HitPoint();
    //    newOne.health = health.clone();
    //    newOne.armor = armor.clone();
    //    newOne.shield = shield.clone();
    //    newOne.iconAmountMax = iconAmountMax;
    //    return newOne;
    //}
    public bool isEqual(HitPoint hp)
    {
        if (iconAmountMax != hp.iconAmountMax) return false;
        if (health.current != hp.health.current) return false;
        if (armor.current != hp.armor.current) return false;
        if (bonusArmor != hp.bonusArmor) return false;
        if (shield.current != hp.shield.current) return false;
        return true;
    }
}
