﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Unit))]
public abstract class Movement : UnitAddOn
{
    [Tooltip("0 là dùng được, 1+ là disable")]
    //[ReadOnly]
    [SerializeField]
    private short enableCount = 0;
    public bool enable
    {
        get
        {
            if (enableCount > 0)
                return false;
            return true;
        }
        set
        {
            if (value == true)
                enableCount--;
            else
                enableCount++;
        }
    }

    //[HideInInspector]
    public AutoMovement auto;
    public RFloat movementSpeed = new RFloat(8f);
    public InputDelay inputDelay;
    private bool _forceMove = false;
    public bool forceMove
    {
        set
        {
            if (value == true)
            {
                if (_input == Vector2.zero)
                {
                    if (unitRef.model.sprites.isRightFacing)
                        _input = Vector2.right;
                    else
                        _input = Vector2.left;
                }
            }

            _forceMove = value;
        }
        protected get
        {
            return _forceMove;
        }
    }
    [HideInInspector]
    public bool holdDirection = false;

    #region input
    [ReadOnly]
    public Vector2 _overrideForce;
    [SerializeField]
    [ReadOnly]
    private Vector2 _currentMoveForce;
    public Vector2 currentMoveForce
    {
        get
        {
            return _currentMoveForce;
        }
    }
    protected Vector2 currentMoveForceGetAndSet
    {
        get
        {
            //if (inputDelay == null)
            //    return Vector2.zero;

            //tính x và y để cho ra kết quả vector. x, y bị tác động bởi scale;
            float x = forceCalc(Input.x * inputDelay.xScale, _currentMoveForce.x);
            float y = forceCalc(Input.y * inputDelay.yScale, _currentMoveForce.y);
            _currentMoveForce = new Vector2(x, y);
            if (_overrideForce != Vector2.zero)
            {
                Vector2 tmp = _overrideForce;
                _overrideForce = Vector2.zero;
                return tmp;
            }
            return _currentMoveForce;
        }
    }
    private float forceCalc(float input, float _currentMoveForce)
    {
        float result = 0f;
        float currentInputSpeed = input * movementSpeed.current;
        if (inputDelay.amount <= 0f)
        {
            return currentInputSpeed;
        }
        float maxSpeed = getSign(input) * movementSpeed.current;
        //nếu input đưa vào cùng chiều (hoặc input = 0) và force hiện tại giá trị lớn hơn
        if (Mathf.Abs(currentInputSpeed) < Mathf.Abs(_currentMoveForce) &&
            ((Mathf.Sign(currentInputSpeed) == Mathf.Sign(_currentMoveForce)) || (input == 0f)))
        {
            if (_currentMoveForce == 0f)
                return 0f;
            float min = (_currentMoveForce > 0f) ? 0f : -Mathf.Abs(maxSpeed);
            float max = (_currentMoveForce > 0f) ? Mathf.Abs(maxSpeed) : 0f;
            float heSoNguoc = (_currentMoveForce < 0f) ? -1f : 1f;
            float Q = _currentMoveForce - (heSoNguoc * movementSpeed.current * (Time.fixedDeltaTime / inputDelay.amount));
            result = Mathf.Clamp(Q, min, max);
        }
        else
        {
            result = Mathf.Clamp(_currentMoveForce + (maxSpeed * (Time.fixedDeltaTime / inputDelay.amount)), -Mathf.Abs(currentInputSpeed), Mathf.Abs(currentInputSpeed));
        }
        return result;
    }
    private float getSign(float x)
    {
        if (x == 0f)
            return 1f;
        return (x / Mathf.Abs(x));
    }
    [SerializeField]
    [ReadOnly]
    private Vector2 _input;
    public Vector2 Input
    {
        get
        {
            if (!unitRef.canControl || !enable)
                return Vector2.zero;
            return _input;
        }
        set
        {
            if (!(_forceMove && value == Vector2.zero))
                _input = value;
        }
    }
    #endregion
    #region old input
    //    [ReadOnly]
    //    public float _currentMoveForceX = 0f;
    //    protected float currentMoveForceX
    //    {
    //        get
    //        {
    //            float currentInputSpeed = inputX * movementSpeed.current;
    //            if (inputDelay <= 0f)
    //            {
    //                return currentInputSpeed;
    //            }
    //            float maxSpeed = getSign(inputX) * movementSpeed.current;
    //            if (Mathf.Abs(currentInputSpeed) < Mathf.Abs(_currentMoveForceX) && Mathf.Sign(currentInputSpeed) == Mathf.Sign(_currentMoveForceX)) //nếu input đưa vào cùng chiều và force hiện tại giá trị nhỏ hơn
    //            {
    //                if (_currentMoveForceX == 0f)
    //                    return 0f;
    //                float tmp_cur = _currentMoveForceX;
    //                float min = (tmp_cur > 0f) ? 0f : -Mathf.Abs(maxSpeed);
    //                float max = (tmp_cur > 0f) ? Mathf.Abs(maxSpeed) : 0f;
    //                float Q = _currentMoveForceX - (-Mathf.Sign(tmp_cur) * movementSpeed.current * (Time.fixedDeltaTime / inputDelay));
    //                _currentMoveForceX = Mathf.Clamp(Q, min, max);
    //            }
    //            else
    //            {
    //                _currentMoveForceX = Mathf.Clamp(_currentMoveForceX + (maxSpeed * (Time.fixedDeltaTime / inputDelay)), -Mathf.Abs(currentInputSpeed), Mathf.Abs(currentInputSpeed));
    //            }
    //            Debug.Log("----->" + _currentMoveForceX);
    //            return _currentMoveForceX;
    //        }
    //    }
    //    [ReadOnly]
    //    public float _currentMoveForceY = 0f;
    //    protected float currentMoveForceY
    //    {
    //        get
    //        {
    //            float currentInputSpeed = inputY * movementSpeed.current;
    //            if (inputDelay <= 0f)
    //            {
    //                return currentInputSpeed;
    //            }
    //            float maxSpeed = getSign(inputY) * movementSpeed.current;
    //            if (Mathf.Abs(currentInputSpeed) < Mathf.Abs(_currentMoveForceY) && Mathf.Sign(currentInputSpeed) == Mathf.Sign(_currentMoveForceY)) //trả về 0 từ từ
    //            {
    //                if (_currentMoveForceY == 0f)
    //                    return 0f;
    //                float tmp_cur = _currentMoveForceY;
    //                float heSoNguoc = (tmp_cur < 0f) ? -1f : 1f;
    //                _currentMoveForceY = Mathf.Clamp(
    //                    _currentMoveForceY - (heSoNguoc * movementSpeed.current * (Time.fixedDeltaTime / inputDelay)), //phép toán
    //                    (tmp_cur > 0f) ? 0f : -maxSpeed, //min
    //                    (tmp_cur > 0f) ? maxSpeed : 0f); //max
    //            }
    //            else
    //                _currentMoveForceY = Mathf.Clamp(_currentMoveForceY + (maxSpeed * (Time.fixedDeltaTime / inputDelay)), -Mathf.Abs(currentInputSpeed), Mathf.Abs(currentInputSpeed));
    //            return _currentMoveForceY;
    //        }
    //    }

    //    private float getSign(float x)
    //    {
    //        if (x == 0f)
    //            return 1f;
    //        return (x / Mathf.Abs(x));
    //    }
    //    [ReadOnly]
    //    public float _inputX = 0f;
    //    public float inputX
    //    {
    //        get
    //        {
    //            if (!unit.canControl)
    //                return 0f;
    //            return _inputX;
    //        }
    //        set
    //        {
    //            _inputX = value;
    //        }
    //    }
    //    [ReadOnly]
    //    public float _inputY = 0f;
    //    public float inputY
    //    {
    //        get
    //        {
    //            if (!unit.canControl)
    //                return 0f;
    //            return _inputY;
    //        }
    //        set
    //        {
    //            _inputY = value;
    //        }
    //    }
    #endregion

    //protected GameObject detectorHandler;

    [ReadOnly]
    public bool isKnockedBack = false;
    [ReadOnly]
    public float knockbackTimer = 0f;

    protected override void Awake()
    {
        base.Awake();
        if (inputDelay == null)
            inputDelay = new InputDelay();
        auto = new AutoMovement(this);
    }
    protected virtual void OnEnable()
    {
        unitRef.movement = this;
    }
    protected override void Start()
    {
        base.Start();
    }
    public override void FinishSetup()
    {
        base.FinishSetup();
        movementSpeed.update();
    }
    protected virtual void OnDisable()
    {
        if (unitRef.movement == this)
            unitRef.movement = null;
    }

    protected void ResetKnockBack()
    {
        if (knockbackTimer <= 0f)
            isKnockedBack = false;
        else
            knockbackTimer -= Time.deltaTime;
    }


    #region movement method
    public void copyFrom(Movement oldMovement)
    {
        movementSpeed.baseValue = oldMovement.movementSpeed.baseValue;
        inputDelay.amount = oldMovement.inputDelay.amount;
        //Input = oldMovement.Input;
    }

    /// <summary>
    /// Unit này nhận vào input, giá trị tự động chuyển về -1:1
    /// </summary>
    /// <param name="x">x = input horizontal</param>
    /// <param name="y">y = input vertical</param>
    /// <param name="willFlip"> input kiểu này có khiến quay mặt đi không?</param>
    //public void setInputDirection(float x, float y, bool willFlip = true)
    public void setInputDirection(float x, float y, bool willFlip = true, bool overrideForce = false)
    {
        if (willFlip && enable)
        {
            // điều chỉnh flip
            if (x > 0 && !unitRef.model.sprites.isRightFacing)
                unitRef.movement.actionFlip();
            else if (x < 0 && unitRef.model.sprites.isRightFacing)
                unitRef.movement.actionFlip();
        }

        //truyền input vào RControler
        if (overrideForce)
            _overrideForce = new Vector2(x, y);
        Input = new Vector2(x.normalize(), y.normalize());
    }
    /// <summary>
    /// khi gọi hàm này, unit có input delay sẽ đạt max tốc độ
    /// </summary>
    public void setMaxSpeed()
    {
        float tmpID = inputDelay.amount;
        inputDelay.amount = 0f;
        Vector2 force = currentMoveForceGetAndSet;
        inputDelay.amount = tmpID;
    }

    public void actionFlip()
    {
        if (holdDirection) return;
        unitRef.model.Flip();
    }
    public void ActionForceStop()
    {
        _input = Vector2.zero;
        _currentMoveForce = Vector2.zero;
        unitRef.rigid.velocity = Vector2.zero;
    }

    public void actionFaceToUnit(Unit _otherUnit, bool _invert = false)
    {
        actionFaceToPoint(_otherUnit.center, _invert);
    }
    public void actionFaceToPoint(Vector2 _otherPos, bool _invert = false)
    {
        bool willFlip = false;
        if ((unitRef.model.sprites.isRightFacing && _otherPos.x < unitRef.center.x)
               || (!unitRef.model.sprites.isRightFacing && _otherPos.x > unitRef.center.x))
            willFlip = true;
        if (_invert)
            willFlip = !willFlip;
        if (willFlip)
            actionFlip();
    }

    #endregion

    #region Detector
    //private void CreateDetector()
    //{
    //    BoxCollider2D box = transform.Find("Sprites").gameObject.GetComponent<BoxCollider2D>();
    //    //BoxCollider2D box = gameObject.findChild("Sprites").GetComponent<BoxCollider2D>();
    //    if (box == null) { Debug.LogWarning("[CreateDetector failed] Cant find any BoxCollider2D", gameObject); return; }

    //    //detectorHandler = gameObject.createEmptyChild("_Detectors");
    //    detectorHandler = transform.Find("Sprites").gameObject.createEmptyChild("_Detectors");

    //    #region create and analyze size
    //    const float verticalHeight = 0.3f;
    //    GameObject upDetector = detectorHandler.createEmptyChild("upDetector");
    //    BoxCollider2D upBox = upDetector.AddComponent<BoxCollider2D>();
    //    upBox.size = new Vector2(box.size.x, verticalHeight);
    //    upBox.offset = new Vector2(box.offset.x, box.offset.y + (box.size.y / 2f) + verticalHeight/2f);
    //    upBox.isTrigger = true;
    //    RContactDetector dt_up = upDetector.AddComponent<RContactDetector>();
    //    dt_up.Init(this, RContactDetector.Direction.up);

    //    GameObject downDetector = detectorHandler.createEmptyChild("downDetector");
    //    BoxCollider2D downBox = downDetector.AddComponent<BoxCollider2D>();
    //    downBox.size = new Vector2(box.size.x, verticalHeight);
    //    downBox.offset = new Vector2(box.offset.x, box.offset.y - (box.size.y / 2f) - verticalHeight/2f);
    //    downBox.isTrigger = true;
    //    RContactDetector dt_down = downDetector.AddComponent<RContactDetector>();
    //    dt_down.Init(this, RContactDetector.Direction.down);

    //    GameObject leftDetector = detectorHandler.createEmptyChild("leftDetector");
    //    BoxCollider2D leftBox = leftDetector.AddComponent<BoxCollider2D>();
    //    leftBox.size = new Vector2(movementSpeed.current * Time.fixedDeltaTime, box.size.y * 0.95f);
    //    leftBox.offset = new Vector2(box.offset.x - (box.size.x / 2f) - (leftBox.size.x / 2f), box.offset.y);
    //    leftBox.isTrigger = true;
    //    RContactDetector dt_left = leftDetector.AddComponent<RContactDetector>();
    //    dt_left.Init(this, RContactDetector.Direction.left);

    //    GameObject rightDetector = detectorHandler.createEmptyChild("rightDetector");
    //    BoxCollider2D rightBox = rightDetector.AddComponent<BoxCollider2D>();
    //    rightBox.size = new Vector2(movementSpeed.current * Time.fixedDeltaTime, box.size.y * 0.95f);
    //    rightBox.offset = new Vector2(box.offset.x + (box.size.x / 2f) + (leftBox.size.x / 2f), box.offset.y);
    //    rightBox.isTrigger = true;
    //    RContactDetector dt_right = rightDetector.AddComponent<RContactDetector>();
    //    dt_right.Init(this,RContactDetector.Direction.right);
    //}
    #endregion
}
public static partial class ExtensionMethods
{
    public static void AddKnockBack(this Movement _movement, Vector2 force, float _time = 0.5f)
    {
        if (_movement != null)
        {
            _movement.isKnockedBack = true;
            _movement.knockbackTimer = _time;
            _movement.unitRef.rigid.AddForce(force);
        }
    }
}
