﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Unit))]
[System.Serializable]
public class Movement_Fly : Movement
{
    private float lastGravityScale = 1f;
    float SpeedX
    {
        set
        {
            if (value != 0f)
                unitRef.model.animation.SetBool("MovingX", true);
            else
                unitRef.model.animation.SetBool("MovingX", false);
            unitRef.model.animation.SetFloat("DirectX", value);
            unitRef.model.animation.SetFloat("SpeedX", Mathf.Abs(value));
        }
    }
    float SpeedY
    {
        set
        {
            if (value != 0f)
                unitRef.model.animation.SetBool("MovingY", true);
            else
                unitRef.model.animation.SetBool("MovingY", false);
            unitRef.model.animation.SetFloat("DirectY", value);
            unitRef.model.animation.SetFloat("SpeedY", Mathf.Abs(value));
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        lastGravityScale = unitRef.rigid.gravityScale;
        unitRef.rigid.gravityScale = 0f;
    }
    protected override void OnDisable()
    {
        base.OnEnable();
        unitRef.rigid.gravityScale = lastGravityScale;
    }

    private void FixedUpdate()
    {
        MoveUnit();
    }

    private bool Condition()
    {
        if (unitRef.isDead)
            return false;
        return true;
    }
    private void MoveUnit()
    {

        //setup variable
        Vector2 _currentMoveForceTmp = currentMoveForceGetAndSet;
        if (Condition())
        {
            #region cũ
            //float movementForceX = currentMoveForceX;
            //float newVeloX;
            //float veloX = rigid.velocity.x;
            ////nếu là knockback thì chỉ cho tác động nhỏ đến lực di chuyển, không thì vẫn tác động bth;
            //if (isKnockedBack)
            //{
            //    newVeloX = veloX + (movementForceX * Time.fixedDeltaTime);
            //}
            //else
            //{
            //    newVeloX = movementForceX;
            //}
            ////Giới hạn lực:
            //newVeloX = Mathf.Clamp(newVeloX, veloX < movementForceX ? veloX : movementForceX, veloX > movementForceX ? veloX : movementForceX);
            //float movementForceY = currentMoveForceY;
            //float newVeloY;
            //float veloY = rigid.velocity.y;
            ////nếu là knockback thì chỉ cho tác động nhỏ đến lực di chuyển, không thì vẫn tác động bth;
            //if (isKnockedBack)
            //{
            //    newVeloY = veloY + (movementForceY * Time.fixedDeltaTime);
            //}
            //else
            //{
            //    newVeloY = movementForceY;
            //}
            ////Giới hạn lực:
            //newVeloY = Mathf.Clamp(newVeloY, veloY < movementForceY ? veloY : movementForceY, veloY > movementForceY ? veloY : movementForceY);
            #endregion
            Vector2 movementForce = new Vector2(forceCalc(_currentMoveForceTmp.x,unitRef.rigid.velocity.x), forceCalc(_currentMoveForceTmp.y, unitRef.rigid.velocity.y));
            unitRef.rigid.velocity = movementForce;
        }
        
        SpeedX = ((Input.x) * movementSpeed.current) / movementSpeed.baseValue;
        SpeedY = ((Input.y) * movementSpeed.current) / movementSpeed.baseValue;
    }
    private void Update()
    {
        autoInput();
    }
    private void autoInput()
    {
        if (!auto.isActive) return;

        //condition
        Vector2 pos = unitRef.center;
        Vector2 target = (Vector2)auto.target.center;
        if (pos.isInRangeOffset(target, auto.offset) && auto.target.canDestroy)
        {
            auto.deactivate();
        }
        else
        {
            float inputX = (pos.x < target.x) ? 1f : -1f;
            float inputY = (pos.y < target.y) ? 1f : -1f;
            setInputDirection(inputX, inputY);
        }
    }
    private float forceCalc (float f, float velo)
    {
        float result;
        //nếu là knockback thì chỉ cho tác động nhỏ đến lực di chuyển, không thì vẫn tác động bth;
        if (isKnockedBack)
        {
            result = velo + (f * Time.fixedDeltaTime);
        }
        else
        {
            result = f;
        }
        //Giới hạn lực:
        return Mathf.Clamp(result, velo < f ? velo : f, velo > f ? velo : f);
    }
}
