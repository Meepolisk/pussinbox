﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class AutoMovement
{
    [ReadOnly]
    [SerializeField]
    private MovementFlag _target;
    public MovementFlag target
    {
        set
        {
            if (value != null)
            {
                _offset = (movementRef.inputDelay.amount*2f) + 0.25f;
            }
            _target = value;
        }
        get
        {
            return _target;
        }
    }

    [ReadOnly]
    private float _offset = 1f;
    public float offset
    {
        get
        {
            return _offset;
        }
    }

    public bool isActive
    {
        get
        {
            if (_target == null)
                return false;
            return true;
        }
    }
    public void deactivate()
    {
        if (_target == null) return;
        movementRef.setInputDirection(0f, 0f);
        _target.Destroy();
    }
    private Movement movementRef;

    public AutoMovement (Movement _movementRef)
    {
        movementRef = _movementRef;
    }
    public void create (Vector2 _pos, ROrder _order = null)
    {
        target = MovementFlag.Create(movementRef.unitRef,_pos);
        if (_order != null)
        {
            _target.orderRef = _order;
        }
    }
    public void create(RObject _follow, ROrder _order = null)
    {
        create(_follow, Vector2.zero, _order);
    }
    public void create(RObject _follow, Vector2 _offset, ROrder _order = null)
    {
        target = MovementFlag.Create(movementRef.unitRef, _follow, _offset);
        if (_order != null)
        {
            _target.orderRef = _order;
        }
    }
    public void moveLeft()
    {
        create(movementRef.unitRef, Vector2.left);
    }
    public void moveRight()
    {
        create(movementRef.unitRef, Vector2.right);
    }
}

[System.Serializable]
public class InputDelay
{
    public float amount = 0f;
    public float xScale = 1f;
    public float yScale = 1f;
}
#region Editor InputDelay
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(InputDelay))]
public class InputDelayInspector : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var rect1 = new Rect(position.x, position.y, (position.width - 160), position.height);
        var rect2 = new Rect(position.x + (position.width - 160), position.y, 45, position.height);
        var rect3 = new Rect(position.x + (position.width - 115), position.y, 35, position.height);
        var rect4 = new Rect(position.x + (position.width - 80), position.y, 45, position.height);
        var rect5 = new Rect(position.x + (position.width - 35), position.y, 35, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("amount"), GUIContent.none);
        GUI.Label(rect2, "xScale");
        EditorGUI.PropertyField(rect3, property.FindPropertyRelative("xScale"), GUIContent.none);
        GUI.Label(rect4, "yScale");
        EditorGUI.PropertyField(rect5, property.FindPropertyRelative("yScale"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
#endregion


//[System.Serializable]
//public class CrouchMovement
//{
//    public const string code = "BasicCrouch";

//    public bool active = false;
//    public float speedScale = 0.5f;
//    [HideInInspector]
//    public bool trigger = false;
//}


//#region Editor CrouchMovement
//#if UNITY_EDITOR
//[CustomPropertyDrawer(typeof(CrouchMovement))]
//public class CrouchMovementInspector : PropertyDrawer
//{
//    // Draw the property inside the given rect
//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        var target = property.serializedObject.targetObject;
//        CrouchMovement _cM = fieldInfo.GetValue(target) as CrouchMovement;
//        bool isActive = _cM.active;
        
//        EditorGUI.BeginProperty(position, label, property);
//        // Draw label
//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

//        // Don't make child fields be indented
//        //var indent = EditorGUI.indentLevel;
//        //EditorGUI.indentLevel = 0;

//        // Calculate rects
//        var rect1 = new Rect(position.x, position.y, 20, position.height);
//        var rect2 = new Rect(position.x + 20, position.y, 100, position.height);
//        var rect3 = new Rect(position.x + 120, position.y, position.width - (120), position.height);

//        // Draw fields - passs GUIContent.none to each so they are drawn without labels
//        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
//        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("active"), GUIContent.none);
//        if (isActive)
//        {
//            GUI.Label(rect2, "Speed Scale: ");
//            EditorGUI.PropertyField(rect3, property.FindPropertyRelative("speedScale"), GUIContent.none);
//        }

//        // Set indent back to what it was
//        //EditorGUI.indentLevel = indent;

//        EditorGUI.EndProperty();
//    }
//}
//#endif
//#endregion