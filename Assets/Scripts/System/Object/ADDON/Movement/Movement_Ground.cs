﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Unit))]
public class Movement_Ground : Movement
{
    #region animation call
    float Speed
    {
        set
        {
            if (value > 0f)
                unitRef.model.animation.SetBool("Moving", true);
            else
                unitRef.model.animation.SetBool("Moving", false);
            unitRef.model.animation.SetFloat("Speed", value);
        }
    }
    void inDrag(float newVeloX)
    {
        bool inDrag;
        if (newVeloX == 0 || Input.x == 0)
            inDrag = false;
        else
        {
            inDrag = ((Mathf.Sign(Input.x) * Mathf.Sign(newVeloX)) < 0);
        }
        unitRef.model.animation.SetBool("inDrag", inDrag);
    }
    float FallSpeed
    {
        set
        {
            unitRef.model.animation.SetFloat("FallSpeed", value);
        }
    }
    private bool _grounded = false;
    public bool Grounded
    {
        private set
        {
            unitRef.model.animation.SetBool("Grounded", value);
            _grounded = value;
        }
        get
        {
            return _grounded;
        }
    }
    #endregion

    [Header("===========GROUND MOVEMENT ONLY============")]
    public float knockBackResist = 4f;

    private void FixedUpdate()
    {
        MoveUnit();
    }

    private bool ConditionMoving()
    {
        if (unitRef.isDead)
            return false;
        return true;
    }

    private void MoveUnit()
    {
        //setup variable
        float movementForceX = currentMoveForceGetAndSet.x;
        float newVeloX = 0f;

        if (ConditionMoving())
        {
            float veloX = unitRef.rigid.velocity.x;
            //nếu là knockback thì chỉ cho tác động nhỏ đến lực di chuyển, không thì vẫn tác động bth;
            //if (isKnockedBack)
            if (isKnockedBack || !Grounded)
            {
                newVeloX = veloX + ((movementForceX * Time.fixedDeltaTime) * knockBackResist);
            }
            else
            {
                newVeloX = movementForceX;
            }

            //Giới hạn lực:
            float min = veloX < movementForceX ? veloX : movementForceX;
            float max = veloX > movementForceX ? veloX : movementForceX;
            newVeloX = Mathf.Clamp(newVeloX, min, max);


            //set new velocity
            if (newVeloX != 0f)
            {
                unitRef.rigid.velocity = new Vector2(newVeloX, unitRef.rigid.velocity.y);
            }
        }

        //animation di chuyển
        inDrag( newVeloX);
        Speed = (Mathf.Abs(Input.x) * movementSpeed.current) / movementSpeed.baseValue;

    }
    protected virtual void Update()
    {
        if (!finishSetup) return;

        checkGrounded();
        ResetKnockBack();
        autoInput();
    }
    const float offset = 0.5f;
    private void autoInput()
    {
        if (!auto.isActive) return;

        //condition
        Vector2 pos = unitRef.center;
        Vector2 target = (Vector2)auto.target.center;
        if (pos.x.isInRangeOffset(target.x, offset) && auto.target.canDestroy)
        {
            auto.deactivate();
        }
        else
        {
            float inputX = (pos.x < target.x) ? 1f : -1f;
            setInputDirection(inputX, 0f);
        }
    }
    private float offsetZperSec = 180;
    
    private List<Platform> currentPlatforms = new List<Platform>();
    private void checkGrounded()
    {
        FallSpeed = unitRef.rigid.velocity.y;

        //tạm thời là dùng rigid.velocity -> cần fix lại system detect grounded
        Grounded = (unitRef.rObjectCollider.contacts.down.isContacted(out currentPlatforms));

        //balance unit
        //nếu unit rigid không có hoặc rigid dạng lock Z thì k cần làm
        if (unitRef.rigid != null && 
            unitRef.rigid.constraints != RigidbodyConstraints2D.FreezeRotation &&
            unitRef.rigid.constraints != RigidbodyConstraints2D.FreezeAll 
            )
        {
            //nếu đang arbone mà có Z khác 0f
            if (Grounded)
            {
                if (Grounded)
                    unitRef.rigid.angularVelocity = 0f;
                if (currentPlatforms.Count == 1)
                {
                    balance(currentPlatforms[0].angle - 90);
                }
            }
            else
            {
                balance(0f);

            }
        }
    }

    private void balance(float targetAngle)
    {
        float currentZ = unitRef.zAngle;
        if (currentZ.isInRangeOffset(targetAngle,1f))
            return;

        //chuyển từ từ về target;
        float zOffset = currentZ.to180() - targetAngle.to180(); //zOffset = độ lệch
        float zPlus = (offsetZperSec * Time.deltaTime);
        zPlus = Mathf.Clamp(zPlus, 0, Mathf.Abs(zOffset));
        zPlus = zPlus * -Mathf.Sign(zOffset);

        //chuyển thẳng trong khoảng min-max
        float min = targetAngle - GlobalConfig.MovementConfig.WalkAngleScale;
        float max = targetAngle + GlobalConfig.MovementConfig.WalkAngleScale;
        float result = (unitRef.transform.eulerAngles.z + zPlus).to180();
        unitRef.zAngle = Mathf.Clamp(result, min, max);
    }
}
