﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Unit))]
public class Movement_Ground_Player : Movement_Ground
{
    #region Animator

    float InputY
    {
        set
        {
            unitRef.model.animation.SetFloat("InputY", value);
        }
    }
    float InputX
    {
        set
        {
            unitRef.model.animation.SetFloat("InputX", value);
        }
    }
    bool LookUp
    {
        set
        {
            unitRef.model.animation.SetBool("LookUp", value);
        }
    }
    bool LookDown
    {
        set
        {
            unitRef.model.animation.SetBool("LookDown", value);
        }
    }
    private string crouchCode = "CrouchCode";
    private bool crouchTrigger = false;
    bool Crouch
    {
        set
        {
            if (value == true && crouchTrigger == false)
            {
                movementSpeed.addMultis(0f, crouchCode);
                crouchTrigger = true;
            }
            else if (value == false && crouchTrigger == true)
            {
                movementSpeed.removeMultis(crouchCode);
                crouchTrigger = false;
            }
            unitRef.model.animation.SetBool("Crouch", value);
        }
    }
    #endregion

    [Header("===========HEROIC============")]
    public bool allowYInput = true;
    public bool canSprint = true;
    private bool sprintActive = false;
    public float SprintBonus = 1.3f;
    public bool canCrouch = true;
    public Jump jump;
    public WallJump wallJump;
    public WallSlice wallSlice;

    private AbilityButton jumpHotKey = new AbilityButton(RButton.Jump, RButtonType.down);
    private AbilityButton sprintHotKey = new AbilityButton(RButton.Item, RButtonType.press);
    #region NestedClass
    [System.Serializable]
    public class Jump
    {
        [ReadOnly]
        public bool canJump = true;
        public float jumpForce = 1500f;
        public float horizontalAdditionalForce = 0f;
    }
    [System.Serializable]
    public class WallJump
    {
        public float horizontalScale = 0.5f;
        public string animatorTriggerCall = "WallJump";
    }
    [System.Serializable]
    public class WallSlice
    {
        public bool enable = false;
        public string animatorBoolCall = "WallRide";
        public float wallSlideDrag = 15f;
    }
#endregion
    
    public override void FinishSetup()
    {
        base.FinishSetup();
        jumpHotKey.setup(actionJump);
        unitRef.button.Add(jumpHotKey);
        //sprintHotKey.setup(actionSprint);
        //unitRef.button.Add(sprintHotKey);
    }

    protected override void Update()
    {
        base.Update();

        lookingUpDown(Input.y);
        wallSliceUpdate();
    }

    private void lookingUpDown(float y_input)
    {
        if (!enable) return;

        InputY = y_input;
        //trường hợp slice
        if (_wallSliceIsActivated)
        {
            LookDown = false;
            LookUp = false;
            Crouch = false;
            return;
        }
        if (y_input > 0f)
        {
            LookUp = true;
        }
        else
        {
            LookUp = false;
        }
        if (y_input < 0f)
        {
            //if (unitRef.rObjectCollider.contacts.down.isContacted())
            if (Grounded && canCrouch)
            {
                Crouch = true; LookDown = false;
            }
            else
            {
                LookDown = true; Crouch = false;
            }
        }
        else
        {
            LookDown = false;
            Crouch = false;
        }
    }

    #region WallSlice
    private bool _wallSliceIsActivated = false;
    public void wallSliceStart()
    {
        _wallSliceIsActivated = true;
        unitRef.model.animation.SetBool(wallSlice.animatorBoolCall, true);
        Rigidbody2D rigid = unitRef.rigid;
        rigid.drag = wallSlice.wallSlideDrag;
    }
    public void wallSliceStop()
    {
        if (!enable) return;
        _wallSliceIsActivated = false;

        unitRef.model.animation.SetBool(wallSlice.animatorBoolCall, false);
        Rigidbody2D rigid = unitRef.rigid;
        rigid.drag = 0f;

        Reactivate();
    }
    
    public void wallSliceUpdate()
    {
        if (enable == false)
            return;
        if (wallSliceCondition())
        {
            if (!_wallSliceIsActivated)
                wallSliceStart();
        }
        else if (_wallSliceIsActivated)
        {
            wallSliceStop();
        }
    }
    private bool wallSliceCondition()
    {
        if (unitRef.rigid.velocity.y >= 0)
            return false;

        float InputX = unitRef.movement.Input.x;
        RObjectCollider unitCollider = unitRef.rObjectCollider;
        if (unitCollider.contacts.down.isContacted())
            return false;
        return ((InputX < 0f && unitCollider.contacts.left.isContacted())
         || (InputX > 0f && unitCollider.contacts.right.isContacted()));
    }
    #endregion

    #region Jump, wallJump
    bool JumpCondition()
    {
        //return (unitRef.rObjectCollider.contacts.down.isContacted());
        return Grounded;
    }
    bool wallJumpCondition()
    {
        if (unitRef.rObjectCollider.contacts.down.isContacted())
            return false;
        return (unitRef.rObjectCollider.contacts.left.isContacted() || unitRef.rObjectCollider.contacts.right.isContacted());
    }
    void stopWallSlice()
    {
        unitRef.model.animation.SetTrigger(wallJump.animatorTriggerCall);
        wallSliceStop();
    }
    public void Reactivate() //wallSlice cooldown
    {
        wallSlice.enable = false;
        StartCoroutine(WallSliceReactive());
    }
    IEnumerator WallSliceReactive()
    {
        yield return new WaitForSeconds(0.2f);
        wallSlice.enable = true;
    }
    #endregion

    #region Drop Platform
    bool dropPlatformCondition()
    {
        if (unitRef.movement.Input.y >= 0)
            return false;

        List<Platform> _contactedPlatforms;
        if (!unitRef.rObjectCollider.contacts.down.isContacted(out _contactedPlatforms))
            return false;
        foreach (Platform plat in _contactedPlatforms)
        {
            if (!plat.dropable)
                return false;
        }
        return true;
    }
    IEnumerator DropPlatformRestoreLayer()
    {
        yield return new WaitForSeconds(0.3f);
        unitRef.rObjectCollider.SemiLayer = false;
    }
    #endregion

    void actionJump()
    {
        StartCoroutine(actionJumpEndFrame());
    }
    void startJump()
    {
        if (!jump.canJump || !enable) return;

        if (JumpCondition())
        {
            if (dropPlatformCondition())
            {
                //drop platform
                unitRef.rObjectCollider.SemiLayer = true;
                StartCoroutine(DropPlatformRestoreLayer());

            }
            else
            {
                //normal jump
                float jumpX = jump.horizontalAdditionalForce;
                if (!unitRef.model.sprites.isRightFacing)
                    jumpX *= -1f;
                unitRef.rObjectCollider.AddForce(new Vector2(jumpX, jump.jumpForce));
            }
            return;
        }
        if (wallJumpCondition())
        {
            unitRef.rigid.velocity = new Vector2(unitRef.rigid.velocity.x, 0f);
            float x = wallJump.horizontalScale;
            Vector2 vec = ((unitRef.rObjectCollider.contacts.left.isContacted()) ? new Vector2(x, 1) : new Vector2(-x, 1));
            vec *= jump.jumpForce;

            wallSliceStop();
            unitRef.movement.AddKnockBack(vec, 0.2f);
        }
    }
    
    IEnumerator actionJumpEndFrame()
    {
        yield return new WaitForEndOfFrame();

        startJump();
        
    }
}
