﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RObjectAddOn : MonoBehaviour
{
    protected bool finishSetup = false;

    protected RObject _rObjectRef;
    public RObject rObjectRef
    {
        get
        {
            return _rObjectRef;
        }
    }

    protected virtual void Awake()
    {
        _rObjectRef = GetComponentInParent<RObject>();
    }
    protected virtual void Start()
    {
        _rObjectRef.RegNewAddOn(this);
    }
    protected virtual void OnDestroy()
    {
        _rObjectRef.RemoveAddOn(this);
    }
    
    public virtual void FinishSetup()
    {
        finishSetup = true;
    }
}
public abstract class UnitAddOn : RObjectAddOn
{
    public Unit unitRef
    {
        get
        {
            return ((Unit)_rObjectRef);
        }
    }
    protected override void Awake()
    {
        Unit unit = GetComponentInParent<Unit>();
        if (unit)
        {
            _rObjectRef = unit;
            return;
        }
        Debug.Log("Không thể tìm thấy Processor Unit cho " + this.name, gameObject);
    }
    public virtual void Unit_Die()
    {

    }
}