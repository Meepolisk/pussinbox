﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class RAI : UnitAddOn
#if UNITY_EDITOR
    , hideTransform, lockTransform
#endif
{
    #region nested class
    public enum Stage { none, free, action}
    [System.Serializable]
    public class RSightSeeProfile
    {
        public float top = 4, bottom = 1, left = 2, right = 5;
        public bool canSeeThroughWall = false;
        public float maxRangeCanSee = 20f;
        public string eyeAttachment = "";
    }
    [System.Serializable]
    public class Info
    {
        [ReadOnly]
        public Stage stage = Stage.none;
        [ReadOnly]
        public Unit focusUnit;
        [ReadOnly]
        [HideInInspector]
        public Vector2? focus_lastPos;

        public bool isCanSeeFocus = false;
        [ReadOnlyWhenPlaying]
        [Range(0.1f, 6f)]
        public float searchTime = 3f;
        [ReadOnlyWhenPlaying]
        public bool onlyInBoxSight = false;
    }
    #endregion

    #region local use / getter, setter
    private Vector2 getScale()
    {
        return new Vector2((sightInfo.left + sightInfo.right) * transform.lossyScale.x, (sightInfo.top + sightInfo.bottom) * transform.lossyScale.y);
    }
    private Vector2 getCenter(Vector2 scale)
    {
        return new Vector2(transform.position.x - (scale.x / 2f - sightInfo.right * transform.lossyScale.x),
                   (transform.position.y - (scale.y / 2f - sightInfo.top * transform.lossyScale.x)));
    }
    public Vector2 startingRaycastPoint
    {
        get
        {
            return unitRef.model.sprites.GetAttachmentCenter(sightInfo.eyeAttachment);
        }
    }
#if UNITY_EDITOR
    public void DrawBox()
    {
        Vector2 scale = getScale();
        Vector2 center = getCenter(scale);
        Handles.color = Color.red;
        Handles.DrawWireCube(center, scale);
    }
#endif
#endregion

    public RSightSeeProfile sightInfo;
    public Info info;

    [HideInInspector]
    public List<AI_CustomStage> customStages = new List<AI_CustomStage>();

    protected Vector2 firstPosition;

    private RSight rSight;
    private bool coroutineActive = false;

    protected override void Awake()
    {
        createRigid();
        customStages.AddRange(GetComponents<AI_CustomStage>());

        base.Awake();

        rEvent.Unit_Die += rEventAUnitDie;
        createRSight();
    }
    protected virtual void OnEnable()
    {
        firstPosition = unitRef.center;
    }
    protected virtual void OnDisable()
    {
        StopAllCoroutines();
        rEvent.Unit_Die -= rEventAUnitDie;
    }
    void createRSight()
    {
        GameObject go = Instantiate(RPlatformer.LoadRSight());
        go.transform.parent = gameObject.transform;
        Vector2 scale = getScale();
        go.transform.position = getCenter(scale);
        go.transform.localScale = new Vector2 (scale.x / transform.lossyScale.x, scale.y / transform.lossyScale.y);
        rSight = go.AddComponent<RSight>();
        rSight.setup(this);
        go.SetActive(true);

        //set EyeAttachment;
    }
    void createRigid()
    {
        //setup rigid đứng im
        Rigidbody2D rigid = gameObject.AddComponent<Rigidbody2D>();
        rigid.isKinematic = true;
        rigid.simulated = true;
        rigid.constraints = RigidbodyConstraints2D.FreezeAll;
    }
    //void StopAllAI()
    //{
    //    unitRef.order.Clear();
    //    gameObject.SetActive(false);
    //}
    protected virtual void Update()
    {
        if (info.stage == Stage.action)
        {
            baseStage_Action();
        }
    }
    private void rEventAUnitDie(Unit _unitA, Unit _unitB)
    {
        if (_unitA == info.focusUnit)
        {
            searchNewFocusUnit();
        }
    }
    #region onStage
    AI_CustomStage ContainCustomStage (Stage _stageCheck)
    {
        foreach (var CS in customStages)
        {
            if (CS.stage == _stageCheck && CS.Condition())
            {
                return CS;
            }
        }
        return null;
    }
    private void baseStageChange(Stage _newStage)
    {
        Stage _oldStage = info.stage;
        AI_CustomStage containCustomStageA = ContainCustomStage(_oldStage);
        if (containCustomStageA != null)
            containCustomStageA.StageExit();
        else
            onStageExit(_oldStage, _newStage);


        info.stage = _newStage;
        AI_CustomStage containCustomStageB = ContainCustomStage(_newStage);
        if (containCustomStageB != null)
            containCustomStageB.StageEnter();
        else
            onStageEnter(_newStage, _oldStage);
    }
    protected virtual void onStageExit(Stage _stage, Stage _nextStage) { }
    protected virtual void onStageEnter(Stage _stage, Stage _previousStage) { }
    /// <summary>
    /// chạy mỗi hàm update khi state l
    /// </summary>
    private void baseStage_Action()
    {
        stage_Action();
        RayCheckFocusUnit();
    }
    private void RayCheckFocusUnit()
    {
        if (RSight.canSee(startingRaycastPoint, info.focusUnit, sightInfo.maxRangeCanSee, sightInfo.canSeeThroughWall))
        {
            info.focus_lastPos = info.focusUnit.center;
            info.isCanSeeFocus = true;
            if (coroutineActive == true)
                StopCoroutine(MissingTimerExpired());
        }
        else // nếu không thấy
        {
            searchNewFocusUnit();
        }
    }
    private void searchNewFocusUnit()
    {
        //check xem còn thằng khác TRONG SIGHT không
        if (rSight.units.Count > 0)
        {
            int randomIndex = UnityEngine.Random.Range(0, rSight.units.Count);
            info.focusUnit = rSight.units[randomIndex];
            info.focus_lastPos = info.focusUnit.center;
        }
        else
        {
            info.isCanSeeFocus = false;
            if (coroutineActive == false && this.enabled)
                StartCoroutine(MissingTimerExpired());
        }
    }
    IEnumerator MissingTimerExpired()
    {
        coroutineActive = true;
        yield return new WaitForSeconds(info.searchTime);
        coroutineActive = false;
        
        info.focusUnit = null;
        info.focus_lastPos = null;
        info.isCanSeeFocus = false;

        //về lại stage cũ
        baseStageChange(Stage.free);
    }
    protected virtual void stage_Action()
    {

    }
    #endregion
    public void GetNewFocusedUnit(Unit _unit) //hàm này chỉ gọi từ sight, nếu lần đầu thấy kẻ thù
    {
        //Debug.Log("Xác định focus unit mới");
        info.focusUnit = _unit;
        info.focus_lastPos = (Vector2)_unit.transform.position;
        baseStageChange(Stage.action);
    }
    public void UnitLeaveBoxSight(Unit _unit)
    {
        if (rSight.units.Count > 0 && info.onlyInBoxSight)
        {
            searchNewFocusUnit();
        }
    }
    public override void Unit_Die()
    {
        base.Unit_Die();
        gameObject.SetActive(false);
    }
    public virtual void FinishAllOrder(ROrder _lastOrder)
    {
        //nếu stage là none, tức là các order đc thêm lúc khởi tạo, thì chuyển từ none sang free
        if (info.stage == Stage.none)
        {
            baseStageChange(Stage.free);
        }
        foreach (var CS in customStages)
        {
            if (CS.isActive) //phải check có active hay không
            {
                CS.FinishAllOrder(_lastOrder);
            }
        }
    }
    public override void FinishSetup()
    {
        base.FinishSetup();
        //chỗ này định đoạt xem stage đầu tiên là gì
        if (info.focusUnit != null)
        {
            //info.stage = Stage.action;
            baseStageChange(Stage.action);
        }
        else if(unitRef.orders.isEmpty)
        {
            baseStageChange(Stage.free);
        }
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(RAI), true)]
public class RAIEditor : Editor
{
    void OnSceneGUI()
    {
        RAI t = target as RAI;

        #region See Box
        t.DrawBox();
        #endregion
    }
}
#endif