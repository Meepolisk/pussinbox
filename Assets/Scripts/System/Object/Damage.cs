﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Damage
{
    public float amount = 20f;
    [Range(0f, 1f)]
    public float enemyScale = 1f;
    [Range(0f, 1f)]
    public float allyScale = 0f;
    
}