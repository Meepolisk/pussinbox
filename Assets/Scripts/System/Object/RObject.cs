﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class RObject : MonoBehaviour
#if UNITY_EDITOR
    , editorUpdate
#endif 
{
    //    #region rObjectEditorTransform
    //#if UNITY_EDITOR
    //    [SerializeField]
    //    private Vector2 _position;
    //    public Vector2 posititon
    //    {
    //        set
    //        {
    //            _position = value;
    //            transform.position = value;
    //        }
    //    }
    //    [SerializeField]
    //    private float _zAngle;
    //    [SerializeField]
    //    private float _size = 1f;
    //#endif
    //#endregion

    public float size
    {
        set
        {
            //#if UNITY_EDITOR
            //            _size = value;
            //#endif
            transform.localScale = new Vector3(value, value, 1f);
        }
        get
        {
            return transform.localScale.x;
        }
    }
    public float zAngle
    {
        set
        {
            //#if UNITY_EDITOR
            //            _zAngle = value.to360();
            //#endif
            transform.eulerAngles = new Vector3(0f, 0f, value);
        }
        get
        {
            return transform.eulerAngles.z;
        }
    }

    [System.Serializable]
    public class ModelControler
    {
        public bool flipOnStart = false;
        public int spriteDepth = 0;

        RObject rObjectRef;

        public void setup(RObject _robject)
        {
            rObjectRef = _robject;
        }

        public void start()
        {
            if (rObjectRef.model != null)
            {
                if (flipOnStart)
                    rObjectRef.model.Flip();
                if (rObjectRef.model.sprites != null)
                {
                    rObjectRef.model.sprites.customDepth = spriteDepth;
                }
            }
        }
    }

    public ModelControler modelControler;

    [ReadOnly]
    public ModelHandler model;
    [HideInInspector]
    public Rigidbody2D rigid;
    [HideInInspector]
    public RObjectCollider rObjectCollider;


    #region AddOn Manager
    protected List<RObjectAddOn> AddOnS = new List<RObjectAddOn>();
    public void RegNewAddOn(RObjectAddOn _addOn)
    {
        if (!AddOnS.Contains(_addOn))
        {
            AddOnS.Add(_addOn);
            if (setuped)
            {
                _addOn.FinishSetup();
            }
        }
    }
    public void RemoveAddOn(RObjectAddOn _addOn)
    {
        if (AddOnS.Contains(_addOn))
        {
            AddOnS.Remove(_addOn);
        }
    }

    private bool setuped = false;
    protected virtual IEnumerator setup()
    {
        yield return new WaitForEndOfFrame();
        FinishSetup();
    }
    #endregion

    public Vector2 center
    {
        get
        {
            if (rObjectCollider != null)
                return rObjectCollider.center;
            return transform.position;
        }
        set
        {
            if (rObjectCollider.collider == null)
            {
                gameObject.transform.position = value;
            }
            else
            {
                gameObject.transform.position = value - rObjectCollider.collider.offset;
            }
        }
    }
    protected virtual void Awake()
    {
        //if (modelControler != null) //rMissile tao = script nen khong co modelControler;
        //    modelControler.setup(this);
    }
    protected virtual void Start()
    {
        StartCoroutine(setup());
    }
    protected virtual void FinishSetup()
    {
        foreach (var addOn in AddOnS)
        {
            addOn.FinishSetup();
        }
        if (modelControler != null) //rMissile tao = script nen khong co modelControler;
        {
            modelControler.setup(this);
            modelControler.start();
        }
        setuped = true;
    }

    #region animation mess caller
    public Dictionary<string, List<Action>> animationMess = new Dictionary<string, List<Action>>();
    public void AnimationMess(string _mess)
    {
        if (animationMess.ContainsKey(_mess))
            foreach (var _action in animationMess[_mess])
            {
                _action();
            }
        else
            Debug.LogWarning("Cant access key " + _mess);
    }
    
    public void RegNewAction(string _mess, Action _action)
    {
        if (animationMess.ContainsKey(_mess))
        {
            animationMess[_mess].Add(_action);
        }
        else
        {
            List<Action> newList = new List<Action>();
            newList.Add(_action);
            animationMess.Add(_mess, newList);
        }
    }

    #endregion
    
    #region Editor
#if UNITY_EDITOR
    public void EditorUpdate()
    {
        //_position = transform.position;
        //_zAngle = transform.eulerAngles.z;
    }
    public void editorFlip()
    {
        //try
        //{
            if (model != null)
            {
                if (model.sprites != null)
                {
                    model.Flip();
                }
            }
        //}
        //catch (Exception ex)
        //{
        //    Debug.LogError("Không thể FlipSprite vì chưa đủ điều kiện: có ModelHandler và SpritesHandler", gameObject);
        //}
    }

    public void RefreshInspector()
    {
        if (Application.isPlaying) return;

        //posititon = _position;
        //zAngle = _zAngle;
        //size = _size;
    }
#endif
    #endregion

    #region ModelUtils
    public void GenerateModel(ModelHandler _model)
    {
        ModelProfile newMP = new ModelProfile();
        newMP.model = _model;
        newMP.size = 1f;

        GenerateModel(newMP);
    }

    public void GenerateModel(ModelProfile _modelProfile)
    {
        if (model != null)
        {
            Destroy(model.gameObject);
        }
        model = _modelProfile.InstanceOnRObject(this);
    }
    #endregion

    #region PhysicEvent
    public virtual void OnContactPlatform(Platform _platform, Vector2[] _contactPoint)
    {

    }
#endregion
}

#if UNITY_EDITOR
[CustomEditor(typeof(RObject),true)]
public class CI_RObject : Editor
{
    void OnSceneGUI()
    {
        RObject t = target as RObject;


        //Draw button
        Handles.BeginGUI();
        GUILayout.BeginArea(new Rect(5, 5, 90, 50));
        if (GUILayout.Button("FlipSprite"))
        {
            t.editorFlip();
        }
        GUILayout.EndArea();
        Handles.EndGUI();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        RObject t = (RObject)target;

        t.RefreshInspector();
    }
}
#endif