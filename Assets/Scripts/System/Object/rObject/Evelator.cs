﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Evelator : RObject
{
    public enum Stage { stand, increase, topspeed, decrease}

    [SerializeField]
    [ReadOnlyWhenPlaying]
    private bool _sideActive = false;
    public bool sideActive
    {
        set
        {
            _sideActive = value;
            model.animation.SetBool("sideOpen", _sideActive);
        }
    }

    [ReadOnlyWhenPlaying]
    public int currentLevelIndex = 0;
    public List<Vector2> destinations;
    public float maxSpeed = 20, timeReachMaxSpeed = 2f;

    [SerializeField]
    [ReadOnly]
    private Stage stage = Stage.stand;
    
    private Action action;
    [SerializeField]
    private Vector2 direction;
    private int nextStopIndex = 0;
    private float calRange = 1f;
    private float _timeTick = 0f;
    protected override void Awake()
    {
        base.Awake();
        rigid = gameObject.AddComponent<Rigidbody2D>();
        rigid.isKinematic = true;
        currentLevelIndex = 0;
    }
    /// <summary>
    /// Di chuyển thang máy đến vị trí destination[index], kèm theo action nếu có
    /// </summary>
    /// <param name="index"></param>
    /// <param name="_action"></param>
    public void MovingToIndex(int _index, Action _action = null)
    {
        if (_index <= destinations.Count && _index != currentLevelIndex)
        {
            //StartCoroutine(StartMoving(_index, _action));
            startMoving(_index, _action);
        }
    }

    private void startMoving(int _index, Action _action = null)
    {
        nextStopIndex = _index;
        action = _action;
        Vector2 pos = transform.position;
        Vector2 target = destinations[nextStopIndex];
        calRange = Vector2.Distance(pos, target);
        direction = (target - pos).normalized;

        stage = Stage.increase;
        _timeTick = 0f;
    }
    private void endMoving()
    {
        currentLevelIndex = nextStopIndex;
        rigid.velocity = Vector2.zero;
        transform.position = destinations[currentLevelIndex];
        stage = Stage.stand;

        if (action != null)
            action.Invoke();
        action = null;
    }
    // chia quang duong ra lam 4 phan;
    // ham so se dung: 
    // f(x1) = (-cos(40x/pi) + 1) / 2    ; voi x < 1 || x > 3
    // f(x2) = 1                         ; voi x >= 1 && x <= 3 (else)
    // x = thoi gian, f(x) = van toc
    private float breakRange = 0;
    private void Update()
    {
        if (stage == Stage.stand) return;


        Vector2 pos = transform.position;
        Vector2 target = destinations[nextStopIndex];
        float remainingDistance = Vector2.Distance(target, pos);

        if (stage == Stage.topspeed)
        {
            changeVelocity(1);
            if (remainingDistance <= breakRange)
            {
                stage = Stage.decrease;
                _timeTick = timeReachMaxSpeed;
            }
        }
        else
        {
            if (stage == Stage.increase)
            {
                //neu nhu dang tang toc do, ma di duoc hon nua duong thi chuyen sang giam toc luon
                if (remainingDistance <= (calRange / 2f))
                {
                    stage = Stage.decrease;
                }
                else
                {
                    _timeTick += Time.deltaTime;
                    if (_timeTick >= timeReachMaxSpeed)
                    {
                        //chuyen sang topSpeed
                        stage = Stage.topspeed;
                        float movedDistance = Vector2.Distance(destinations[currentLevelIndex], pos);
                        breakRange = movedDistance;
                    }
                }
            }
            else //stage = decrease
            {
                //neu dang giam toc do, ma quang duong con ngan thi tat luon
                if (_timeTick <= 0.2f)
                {
                    endMoving();
                    return;
                }
                else
                {
                    _timeTick -= Time.deltaTime;
                }
            }
            float x = _timeTick / timeReachMaxSpeed;
            float y = (-Mathf.Cos((10 * x) / Mathf.PI) + 1) / 2;
            changeVelocity(y);
        }
    }

    void changeVelocity(float _scale)
    {
        rigid.velocity = direction * maxSpeed * _scale;
        model.animation.SetFloat("movingSpeed", _scale);
    }
}