﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [System.Serializable]
    public class UnitGroup
    {
        public List<Unit> units = new List<Unit>();
        private Action action; 

        public void setup(Action _action)
        {
            action = _action;
            rEvent.Unit_Die += REvent_Unit_Die;
        }

        private void REvent_Unit_Die(Unit _unitA, Unit _unitB)
        {
            if (units.Contains(_unitA))
            {
                units.Remove(_unitA);
                if (units.Count <= 0)
                    Trigger();
            }
        }
        private void Trigger()
        {
            action();
            rEvent.Unit_Die -= REvent_Unit_Die;
        }
    }
    
    public GameObject blocker;
    public UnitGroup groupA;
    public UnitGroup groupB;
    public Unit spawn1;
    public Unit spawn2;
    public UnitGroup groupC;
    public List<Vector2> destination;
    public float speed;
    //public RegionCaller region;

    private RFlag flag;
    private Rigidbody2D rigid;

    private void Awake()
    {
        rigid = gameObject.GetComponent<Rigidbody2D>();
        rigid.isKinematic = true;
        groupA.setup(groupA_Dies);
        flag = GetComponentInChildren<RFlag>();
    }
    private void groupA_Dies()
    {
        //region.action = move0to1;
        //region.gameObject.SetActive(true);
    }
    private void move0to1()
    {
        //region.gameObject.SetActive(false);
        blocker.gameObject.SetActive(true);
        CStartMoving = StartCoroutine(StartMoving(1, prepareGroupB));
    }
    private void prepareGroupB()
    {
        blocker.gameObject.SetActive(false);
        foreach (var unit in groupB.units)
        {
            unit.gameObject.SetActive(true);
            RAI rai = unit.GetComponentInChildren<RAI>();
            rai.unitRef.orders.Order(new OrderMoveToPoint(flag.center));
        }
        groupB.setup(groupB_Dies);
    }
    private void groupB_Dies()
    {
        //region.action = move1to2;
        //region.gameObject.SetActive(true);
    }
    private void move1to2()
    {
        //region.gameObject.SetActive(false);
        //blocker.gameObject.SetActive(true);
        CspawnMob = StartCoroutine(spawnMob());
        CStartMoving = StartCoroutine(StartMoving(2, prepareGroupC));
    }
    private Coroutine CspawnMob;
    IEnumerator spawnMob()
    {
        yield return new WaitForSeconds(5f);
        spawn1.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        spawn2.gameObject.SetActive(true);
        StopCoroutine(CspawnMob);
    }
    private void prepareGroupC()
    {
        blocker.gameObject.SetActive(false);
        foreach (var unit in groupC.units)
        {
            unit.gameObject.SetActive(true);
            RAI rai = unit.GetComponentInChildren<RAI>();
            rai.unitRef.orders.Order(new OrderMoveToPoint(flag.center));
        }
        groupC.setup(groupC_Dies);
    }
    private void groupC_Dies()
    {
        //region.action = move2to3;
        //region.gameObject.SetActive(true);
    }
    private void move2to3()
    {
        //region.gameObject.SetActive(false);
        blocker.gameObject.SetActive(true);
        CStartMoving = StartCoroutine(StartMoving(3, finish));
    }
    private void finish()
    {
        blocker.gameObject.SetActive(false);
    }

    private Coroutine CStartMoving;
    IEnumerator StartMoving(int destinationIndex, Action _action)
    {
        yield return new WaitForSeconds(2);
        //if (destinationIndex <= 0 || destinationIndex >= destination.Count)
        //    return;

        Vector2 pos = transform.position;
        Vector2 des = destination[destinationIndex];
        float range = Vector2.Distance(pos, des);
        Vector2 velocity = (des - pos).normalized* speed;
        float time = range / speed;

        rigid.velocity = velocity;
        CStopMoving = StartCoroutine(StopMoving(time, des, _action));
        StopCoroutine(CStartMoving);
    }
    private Coroutine CStopMoving;
    IEnumerator StopMoving(float _time, Vector2 targetPos, Action _action)
    {
        yield return new WaitForSeconds(_time);
        rigid.velocity = Vector2.zero;
        transform.position = targetPos;
        if (_action != null)
            _action();
        StopCoroutine(CStopMoving);
    }
    
}