﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : RObject
{
    [SerializeField]
    [ReadOnlyWhenPlaying]
    private bool _isOpen = false;
    public bool isOpen
    {
        set
        {
            _isOpen = value;
            model.animation.SetBool("isOpen", _isOpen);
        }
    }
}