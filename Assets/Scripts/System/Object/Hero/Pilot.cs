﻿using UnityEngine;
using System.Collections;

public class Pilot : Hero
{
    protected override void OnEnable()
    {
        base.OnEnable();
        Player.number[player].hero = this;
    }
    protected override void Die(Unit _killer)
    {
        Player.number[player].hero = null;
        base.Die(_killer);
    }
    
    protected override void Awake()
    {
        base.Awake();
        RegNewAction("callMech", CallMechStartEffect);
    }

    protected override void FinishSetup()
    {
        base.FinishSetup();
        if (autoEquipAtStart)
            StartCoroutine(firstReplace());
        functionKey.setup(actionCallMech);
        button.Add(functionKey);
    }

    #region CallMech
    [Header("====Transform Mech====")]
    public HeroBox boxPrefab;
    public bool autoEquipAtStart = true;
    
    private AbilityButton functionKey = new AbilityButton(RButton.Ability, RButtonType.down);

    IEnumerator firstReplace()
    {
        yield return new WaitForEndOfFrame();
        CreateBox(false);
    }

    private void actionCallMech()
    {
        if (mechCondition())
        {
            model.animation.SetTrigger("callMech");
        }
    }
    private void CallMechStartEffect()
    {
        CreateBox(true);
    }

    private bool mechCondition()
    {
        if (energy.current < energy.max)
            return false;
        if (!rObjectCollider.contacts.down.isContacted())
            return false;
        return true;
    }
    private void CreateBox(bool _birthAnimation)
    {
        energy.current = 0f;
        boxPrefab.Create(this, _birthAnimation);
        StartCoroutine(StopAnimation(0.15f));
    }

    IEnumerator StopAnimation(float time)
    {
        yield return new WaitForSeconds(time);
        disableCharracter();
    }
    void disableCharracter()
    {
        this.Hide = true;
    }
    #endregion
}
