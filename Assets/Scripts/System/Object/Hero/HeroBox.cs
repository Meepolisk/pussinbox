﻿using UnityEngine;
using System.Collections;

public class HeroBox : Hero
{
    [Header("====BOX Properties====")]
    [ReadOnly]
    public Pilot _pilot;
    public ModelProfile callMechSpecialEffect;
    public Vector2 ejectForce = new Vector2(-250f, 1000f);

    protected override void Awake()
    {
        base.Awake();
        RegNewAction("eject", ActualDie);
    }
    protected override void Die(Unit _killer)
    {
        base.Die(_killer);
        hitPoint.invulnerable = true;
    }
    public void ActualDie()
    {
        _pilot.transform.position = center;
        //pilot.gameObject.SetActive(true);
        _pilot.Hide = false;

        //phần của Dva knockback ra
        if (ejectForce != Vector2.zero)
        {
            Vector2 KnockbackForce = ejectForce;
            if (!model.sprites.isRightFacing)
                KnockbackForce.x = -KnockbackForce.x;
            _pilot.movement.AddKnockBack(KnockbackForce);
        }

        _pilot.gameObject.GetComponent<PlayerController>().enabled = true;
        gameObject.GetComponent<PlayerController>().enabled = false;
    }


    public void Create(Pilot _pilot, bool isWait = true)
    {
        this._pilot = _pilot;
        Vector2 pos = this._pilot.center;   
        this._pilot.GetComponent<PlayerController>().enabled = false;

        GameObject newGO = Instantiate(this.gameObject, pos, Quaternion.Euler(Vector2.zero));
        Unit morpthUnit = newGO.GetComponent<Unit>();
        morpthUnit.center = pos;
        morpthUnit.copyPlayerStat(this._pilot);

        //animation
        if (isWait)
        {
            morpthUnit.model.animation.SetTrigger("Summon");
            SpecialEffect.Create(callMechSpecialEffect, newGO.transform.position);
            SpecialEffect.DestroyLastCreatedSpecialEffect();
        }
    }


    
}
