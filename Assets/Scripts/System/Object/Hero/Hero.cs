﻿using UnityEngine;
using System.Collections;

public abstract class Hero : Unit
{
    #region public thing
    public void heal(float heal)
    {
        if (isDead) return;
        hitPoint.healed(heal);
    }
    public void armorBonus(float armor)
    {
        if (isDead) return;
        if (armor <= hitPoint.bonusArmor) return;
        hitPoint.bonusArmor = armor;
    }
    public void restoreEnergy(float energyRes)
    {
        if (isDead) return;
        energy.current += energyRes;
    }
    #endregion
}
