﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Sprite))]
public class UnitDeadFrap : MonoBehaviour
{
    public Sprite deadSprite_0;
    public Sprite deadSprite_1;
}
