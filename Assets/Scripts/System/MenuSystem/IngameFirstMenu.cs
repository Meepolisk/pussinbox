﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameFirstMenu : IngameMenu {

    public IngameMenu SettingMenu;

    public void btn_resume_clicked()
    {
        Deactivate();
    }
    public void btn_restart_clicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void btn_option_clicked()
    {
        SettingMenu.Activate();
    }
    public void btn_exit_clicked()
    {
        Application.Quit();
    }
}
