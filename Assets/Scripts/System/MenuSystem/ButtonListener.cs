﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ButtonListener : MonoBehaviour {

    private static Action actionInvoke = null;
    public static RKeyCode? _lastRecordedButton = null;
    public static GameObject thisGameObject = null;

    public static bool isRecording
    {
        get
        {
            if (thisGameObject == null) return false;
            if (thisGameObject.activeInHierarchy)
                return true;
            return false;
        }
    }

    private Animator anim;

    private void Start()
    {
        if (thisGameObject == null)
        {
            thisGameObject = this.gameObject;
        }
        else
        {
            Destroy(gameObject);
        }
        thisGameObject.SetActive(false);
        anim = GetComponent<Animator>();
    }
    public static void startRecording(Action _action, string _name)
    {
        _lastRecordedButton = null;
        actionInvoke = _action;
        setText(_name);
        thisGameObject.SetActive(true);
        thisGameObject.GetComponent<ButtonListener>().anim.SetTrigger("open");
    }
    static void setText(string _name)
    {
        thisGameObject.findChild("text1").GetComponent<UnityEngine.UI.Text>().text = "Bind \'" + _name + "\'";
        string txt2 = "Press anykey now or " + RInputHandler.rKeys[(int)RButton.Cancel].key1 + " to cancel";
        thisGameObject.findChild("text2").GetComponent<UnityEngine.UI.Text>().text = txt2;
    }
    static void endRecording()
    {
        actionInvoke.Invoke();
        thisGameObject.GetComponent<ButtonListener>().anim.SetTrigger("close");
    }
    
    public void hide()
    {
        thisGameObject.SetActive(false);
    }
    public static void detectPressedKeyOrButton()
    {
        if (Input.anyKeyDown)
        {
            foreach (RKeyCode kcode in System.Enum.GetValues(typeof(RKeyCode)))
            {
                //không record nút đang bind vào cancel
                if (kcode == RInputHandler.rKeys[(int)RButton.Cancel].key1 || kcode == RInputHandler.rKeys[(int)RButton.Cancel].key2) continue;
                if (Input.GetKeyDown(kcode.toKeyCode()))
                {
                    _lastRecordedButton = kcode;
                    break;
                }
            }
            if (_lastRecordedButton != null)
            {
                endRecording();
            }
        }
    }

    private void Update()
    {
        detectPressedKeyOrButton();
    }
}
