﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ControlMenu : IngameMenu {
    
    private RectTransform planeRectTrans;
    private float requiHeight;
    private List<ControlButtonField> allButtonInstances = new List<ControlButtonField>();

    public GameObject buttonConfigPrefab;
    public RInputProfile rInputProfile;
    //for file IO
    public static string path;
    
    private void Awake()
    {
        rootMenu = GetComponentInParent<IngameMenuManager>();
    }

    void Start () {
        path = Application.persistentDataPath + "/controlData.dat";
        planeRectTrans = gameObject.findChild("Page").GetComponent<RectTransform>();
        requiHeight = gameObject.findChild("Mask").GetComponent<RectTransform>().rect.height;

        rInputProfile = new RInputProfile();
        LoadExternal();
        InstanceButtonConfig();
        this.gameObject.SetActive(false);
    }

    void InstanceButtonConfig()
    {
        allButtonInstances.Clear();
        float height = buttonConfigPrefab.GetComponent<RectTransform>().rect.height;
        float totalheight = 30f;
        GameObject page = gameObject.findChild("Page");
        for (int i = 0; i < RKey.RKeyCodeLength; i++)
        {
            GameObject newOne = page.createChild(buttonConfigPrefab);
            newOne.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -30f - (height * (float)i));
            newOne.GetComponent<Transform>().localScale = new Vector3(1f, 1f, 1f);
            ControlButtonField control = newOne.GetComponent<ControlButtonField>();
            allButtonInstances.Add(control);
        }
        totalheight += (height * RKey.RKeyCodeLength);
        page.GetComponent<RectTransform>().sizeDelta = new Vector2(page.GetComponent<RectTransform>().sizeDelta.x, totalheight);

        ApllyToButton();
    }
    private void ApllyToButton()
    {
        for (int i = 0; i < RKey.RKeyCodeLength; i++)
        {
            allButtonInstances[i].rkeyRef = rInputProfile.rKeyProfiles[i];
            allButtonInstances[i].ApplyRef();
        }
    }
    private void Canceling()
    {
        //if (allButtonInstances == null) return;
        rInputProfile.LoadCurrent();
        ApllyToButton();
    }
    void SaveToExternal()
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file;
            if (File.Exists(path))
                file = File.Open(path, FileMode.Open);
            else
                file = File.Create(path);


            bf.Serialize(file, rInputProfile);
            file.Close();
            Debug.Log("Control data saved at " + path);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
        }
        finally
        {
            RInputHandler.Apply(rInputProfile);
        }
    }

    void LoadExternal()
    {
        try
        {
            if (File.Exists(path))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(path, FileMode.Open);
                rInputProfile = (RInputProfile)bf.Deserialize(file);
                file.Close();
                //Debug.Log("Control data loaded at " + path);
            }
            else
            {
                rInputProfile.LoadDefault();
                //Debug.Log("Cant load control data. Loading default...");
            }
            RInputHandler.Apply(rInputProfile);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
            rInputProfile.LoadDefault();
            //Debug.Log("Cant load control data. Loading default..."); Debug.Log(path);
        }
    }
    void LoadDefault()
    {
        rInputProfile.LoadDefault();
        ApllyToButton();
    }

#region button, slider
    public void changePlane (float newValue)
    {
        float height = planeRectTrans.rect.height;
        float newY = ((height-requiHeight) *newValue);
        planeRectTrans.anchoredPosition = new Vector2(0f, newY);
    }
    public void btn_cancel_clicked()
    {
        Canceling();
        Deactivate();
    }
    public void btn_default_clicked()
    {
        LoadDefault();
    }
    public void btn_ok_clicked()
    {
        RInputHandler.Apply(rInputProfile);
        try
        {
            SaveToExternal();
        }
        catch
        {
            Debug.Log("Lỗi");
        }
        gameObject.SetActive(false);
    }
    #endregion

}
