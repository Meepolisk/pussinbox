﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class rEvent
{
    public delegate void Handler_Unit(Unit _unit);
    public delegate void Handler_Unit_Unit(Unit _unitA, Unit _unitB);
    public delegate void Handler_Unit_Ability(Unit _unit, Ability _ability);
    public delegate void Handler_Unit_Region(Unit _unit, RegionEvent _region);

    public static event Handler_Unit Unit_Enabled;
    public static event Handler_Unit Unit_Disabled;
    public static event Handler_Unit Unit_Controlable;
    public static event Handler_Unit Unit_Uncontrolable;
    public static event Handler_Unit_Unit Unit_Die;
    public static event Handler_Unit_Ability Ability_BeingCast;
    public static event Handler_Unit_Ability Ability_FinishCasting;
    public static event Handler_Unit_Region Unit_EnterRegion;
    public static event Handler_Unit_Region Unit_LeaveRegion;
    
    

    public static class Start
    {
        #region ability
        public static void Ability_BeingCast(Unit _caster, Ability _abi)
        {
            if (rEvent.Ability_BeingCast != null)
                rEvent.Ability_BeingCast(_caster, _abi);
        }
        public static void Ability_FinishCasting(Unit _caster, Ability _abi)
        {
            if (rEvent.Ability_FinishCasting != null)
                rEvent.Ability_FinishCasting(_caster, _abi);
        }
#endregion
        #region region
        public static void Unit_EnterRegion(RegionEvent _region, Unit _unit)
        {
            if (rEvent.Unit_EnterRegion != null)
                rEvent.Unit_EnterRegion(_unit,_region);
        }
        public static void Unit_LeaveRegion(RegionEvent _region, Unit _unit)
        {
            if (rEvent.Unit_LeaveRegion != null)
                rEvent.Unit_LeaveRegion(_unit,_region);
        }
#endregion
        #region Unit
        public static void Unit_Die(Unit _unitKilled, Unit _unitKiller)
        {
            if (rEvent.Unit_Die != null)
                rEvent.Unit_Die(_unitKilled, _unitKiller);
        }
        public static void Unit_Enabled(Unit _unit)
        {
            if (rEvent.Unit_Enabled != null)
                rEvent.Unit_Enabled(_unit);
        }
        public static void Unit_Disabled(Unit _unit)
        {
            if (rEvent.Unit_Disabled != null)
                rEvent.Unit_Disabled(_unit);
        }
        public static void Unit_Controlable(Unit _unit)
        {
            if (rEvent.Unit_Controlable != null)
                rEvent.Unit_Controlable(_unit);
        }
        public static void Unit_Uncontrolable(Unit _unit)
        {
            if (rEvent.Unit_Uncontrolable != null)
                rEvent.Unit_Uncontrolable(_unit);
        }

        #endregion
    }
}


