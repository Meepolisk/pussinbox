﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlButtonField : MonoBehaviour {
    
    public RKey rkeyRef;

    private Text label;
    private Button btn1;
    private Text btnText1;
    private Button btn2;
    private Text btnText2;

    private sbyte _slot = 0;
    // Use this for initialization
    void Awake ()
    {
        btn1 = gameObject.findChild("BTN_HOTKEY1").GetComponent<Button>();
        btn1.onClick.AddListener(btn1OnClick);
        btn2 = gameObject.findChild("BTN_HOTKEY2").GetComponent<Button>();
        btn2.onClick.AddListener(btn2OnClick);
        btnText1 = gameObject.findChild("Text1").GetComponent<Text>();
        btnText2 = gameObject.findChild("Text2").GetComponent<Text>();
        label = gameObject.findChild("LABEL").GetComponent<Text>();
    }

    public void ApplyRef()
    {
        label.text = rkeyRef.name;
        btnText1.text = rkeyRef.key1.ToString();
        btnText2.text = rkeyRef.key2.ToString();
    }

    void btn1OnClick()
    {
        _slot = 1;
        ButtonListener.startRecording(replace, rkeyRef.name);
    }
    void btn2OnClick()
    {
        _slot = 2;
        ButtonListener.startRecording(replace, rkeyRef.name);
    }
    // Update is called once per frame
    public void replace()
    {
        if (_slot == 1)
            rkeyRef.key1 = (RKeyCode)ButtonListener._lastRecordedButton;
        if (_slot == 2)
            rkeyRef.key2 = (RKeyCode)ButtonListener._lastRecordedButton;
        _slot = 0; //reset lại
        ApplyRef();
    }
}
