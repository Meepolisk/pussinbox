﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class IngameMenu : MonoBehaviour {

    protected IngameMenuManager rootMenu;
    
    public void Activate ()
    {
        if (gameObject.activeSelf) return;

        gameObject.SetActive(true);
        rootMenu.ActivateMenu(this);
    }
    public void Deactivate()
    {
        if (!gameObject.activeSelf) return;

        gameObject.SetActive(false);
        rootMenu.OpenPreviousMenu();
    }

    private void Awake()
    {
        rootMenu = GetComponentInParent<IngameMenuManager>();
    }

}
