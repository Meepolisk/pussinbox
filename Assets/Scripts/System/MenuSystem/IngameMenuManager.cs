﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMenuManager : MonoBehaviour
#if UNITY_EDITOR
    //,hideTransform, lockTransform
#endif  
{
    public IngameMenu firstMenu;
    private bool _active = false;
    public bool active
    {
        get
        {
            return _active;
        }
        set
        {
            _active = value;
            Background.SetActive(_active);
            if (_active == true)
                Time.timeScale = 0f;
            else
                Time.timeScale = 1f;
        }
    }

    private GameObject Background;
    private void Awake()
    {
        Refresh();
    }
    private void OnDestroy()
    {
        Time.timeScale = 1f;
    }

    private void Refresh()
    {
        Background = gameObject.findChild("Background");
        firstMenu.gameObject.SetActive(false);
        Background.SetActive(false);
    }

    private void Update()
    {
        if (RInputHandler.getKeyDown(RButton.Start) || RInputHandler.getKeyDown(RButton.Cancel))
        {
            activate();
        }
    }

    private void activate()
    {
        if (active == false)
        {
            active = true;
            ActivateDefaultMenu();
        }
        else if (currentMenu == firstMenu) //nếu đang ở firstMenu
        {
            currentMenu.Deactivate();
        }
    }
    private void ActivateDefaultMenu()
    {
        firstMenu.Activate();
    }
    
    public IngameMenu currentMenu = null;
    public List<IngameMenu> menuList = new List<IngameMenu>();

    public void ActivateMenu(IngameMenu newMenu)
    {
        if (currentMenu != null) //nếu không phải mở menu đầu tiên
            menuList.Add(currentMenu);
        currentMenu = newMenu;
        //currentMenu.Activate();
    }
    public void OpenPreviousMenu()
    {
        if (menuList.Count == 0) //là menu cuối cùng
        {
            currentMenu = null;
            active = false;
            return;
        }

        currentMenu = menuList[menuList.Count - 1];
        //currentMenu.Activate();
        menuList.RemoveAt(menuList.Count - 1);
    }
}
