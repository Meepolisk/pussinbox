﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ability))]
public class AbilityToggle : AbiAddOn
{
    public float time = 0f;
    public float minimumToggleTime = 0f;

    [ReadOnly]
    public float _timeTick = 0f;
    [ReadOnly]
    public bool isActive = false;
    
    private bool buttonStopTriggered = false;

    protected override void OnEnable()
    {
        base.OnEnable();
        abiRef.toggle = this;
    }

    public float remainingScale
    {
        get
        {
            if (_timeTick == 0f) return 0f;
            return (_timeTick / time);
        }
    }

    internal void start()
    {
        _timeTick = 0f;

        abiRef.unitRef.model.animation.SetBool(abiRef.animMess.atToggle_Bool, true);
        //abiRef.caster.playSoundRandom(toggleOnSounds);
        StartCoroutine(setActive());
    }
    IEnumerator setActive()
    {
        yield return new WaitForEndOfFrame();
        isActive = true;
    }

    public void forceStop(bool forceStop)
    {
        isActive = false;
        buttonStopTriggered = false;

        abiRef.unitRef.model.animation.SetBool(abiRef.animMess.atToggle_Bool, false);
        abiRef.baseAbilityToggleOff(forceStop);
    }
    public void stopButtonTriggered()
    {
        if (!abiRef.isActive) return;
        
        buttonStopTriggered = true;
    }

    private void Update()
    {
        if (!isActive) return;
        _timeTick += Time.deltaTime;
    }
    public override bool abiRefUpdateCondition()
    {
        if (!isActive)
            return true;

        if (time > 0 && _timeTick >= time) //hết tg nếu có tg giới hạn
            return false;
        //if ((minimumToggleTime > 0 && _timeTick > minimumToggleTime) && buttonStopTriggered)
        if (_timeTick > minimumToggleTime && buttonStopTriggered)
            return false;
        return true;
    }
}

