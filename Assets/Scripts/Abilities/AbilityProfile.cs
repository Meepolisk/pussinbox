﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ability))]
public class AbilityProfile : MonoBehaviour, iRegHotkey
{
    public AbilityButton mainHotKey;
    public AbilityButton toggleOffHotKey;

    public Texture2D icon = null;
    public short slot = 0;

    [HideInInspector]
    public UI_AbilityProfile profileRef = null;
    private Ability abiRef = null;

    private void Awake()
    {
        abiRef = GetComponent<Ability>();
        mainHotKey.setup(abiRef.UseAbi);
        AbilityToggle _toggle = GetComponent<AbilityToggle>();
        if (_toggle && _toggle.enabled)
            toggleOffHotKey.setup(_toggle.stopButtonTriggered);
    }
    private void OnEnable()
    {
        abiRef.profile = this;
        registHotKey(mainHotKey);
        AbilityToggle _toggle = GetComponent<AbilityToggle>();
        if (_toggle && _toggle.enabled)
            registHotKey(toggleOffHotKey);
    }
    public void registHotKey(AbilityButton _hotKey)
    {
        abiRef.unitRef.button.Add(_hotKey);
    }
    private void OnDisable()
    {
        unregistHotKey(mainHotKey);
        AbilityToggle _toggle = GetComponent<AbilityToggle>();
        if (_toggle && _toggle.enabled)
            unregistHotKey(toggleOffHotKey);
    }
    public void unregistHotKey(AbilityButton _hotKey)
    {
        abiRef.unitRef.button.Remove(_hotKey);
    }
    
    private bool condition
    {
        get
        {
            if (profileRef == null)
                return false;
            return true;
        }
    }

    public void UI_Casted()
    {
        if (condition)
            profileRef.abilityCasted();
    }
    public void UI_cooldownFinish()
    {
        if (condition)
            profileRef.cooldownFinish();
    }
}
