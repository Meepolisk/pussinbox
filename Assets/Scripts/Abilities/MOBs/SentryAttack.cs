﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SentryAttack : Ability {
    [System.Serializable]
    public class Advanced
    {
        public bool stopRotateWhenAttack = true;
        public bool autoRotate = false;
        public float autoRotateRestTime = 0;
    }
    [Header("================================")]
    public Advanced advanced;
    public MissileProfile missile;
    public Damage damage;
    public float radius = 2f;
    public ModelProfile shootEffect;
    public ModelProfile exploEffect;
    public GunController gunController;

    private int _count = 0;
    float Zangle
    {
        get
        {
            float _float = gunController.currentValue;
            if (unitRef.model.sprites.isRightFacing == false)
                _float = _float.revertZangle();
            return _float;
        }
    }

    protected override void Start()
    {
        base.Start();
        rotating();
    }
    public override void FinishSetup()
    {
        base.FinishSetup();
        if (advanced.autoRotate == true)
        {
            SetupAutoRotating();
        }
    }
    #region AutoRotating
    private enum RotatingStage { Up, Down, Stand }

    Coroutine AutoRotateCoroutine;
    private RotatingStage rotateStage = RotatingStage.Stand;
    void SetupAutoRotating()
    {
        SetTargetValue();
    }
    void SetTargetValue()
    {
        if (gunController.currentValue >= gunController.max)
        {
            gunController.targetValue = gunController.min;
            rotateStage = RotatingStage.Down;
        }
        else
        {
            gunController.targetValue = gunController.max;
            rotateStage = RotatingStage.Up;
        }
        AutoRotateCoroutine = StartCoroutine(autoRotating());
    }
    bool autoRotatingBool()
    {
        if (rotateStage == RotatingStage.Up && gunController.currentValue >= gunController.max)
            return true;
        if (rotateStage == RotatingStage.Down && gunController.currentValue <= gunController.min)
            return true;
        return false;
    }
    IEnumerator autoRotating()
    {
        yield return new WaitUntil(autoRotatingBool);
        //kiểm tra pause
        if (advanced.autoRotateRestTime > 0f)
            yield return new WaitForSeconds(advanced.autoRotateRestTime);
        SetTargetValue();
    }
    #endregion
    protected override void abilityOrdered()
    {
        unitRef.model.flipWhenTurn = false;
        _count = 0;
    }
    protected override void abilityCasted()
    {
        _count++;
        Shoot(unitRef.model.sprites.attachments["weapon"].transform.position);
    }
    private void Shoot(Vector2 pos)
    {
        SpecialEffect.Create(shootEffect, pos);
        SpecialEffect._lastCreatedSpecialEffect.transform.eulerAngles = new Vector3(0f, 0f, Zangle);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(pos, Zangle, missile);
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        SpecialEffect.Create(exploEffect, pos);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();

        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }
    }
    

    protected override void Update()
    {
        base.Update();
        if (unitRef.isDead || !unitRef.canControl)
            return;

        //update rotate angle
        if (!(advanced.stopRotateWhenAttack && stage > 0))
            gunController.updateCal();
        rotating();
    }
    private void rotating()
    {
        unitRef.model.animation.SetFloat(gunController.animatorParameter, gunController.result);
    }
}
