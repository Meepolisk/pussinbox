﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Trooper_Attack : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public Damage damage;
    [Range(1, 10)]
    public short bulletCount = 3;
    public float _intervalTime = 0.1f;
    [Range(0f, 1f)]
    public float _movementSpeedScale = 0.01f;

    private float _nextTime;
    private int _bulletRemain;
    private Transform _weapon;
    private const string BonusCode = "Trooper_Attack";

    protected override void Start()
    {
        base.Start();
        _weapon = gameObject.transform.root.gameObject.findChild("weapon").transform;
    } 

    protected override void abilityCasted()
    {
        _bulletRemain = bulletCount;
        //bắn
        Shoot();

        //Add Status
        AddSlow();
    }
    protected override void Update()
    {
        base.Update();

        if (_bulletRemain > 0 && Time.time >= _nextTime)
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        unitRef.model.animation.SetTrigger("Attack");
        //create Missile
        float Zangle = getZangle();
        missile.setData(unitRef, exploAction);
        Vector2 pos = unitRef.gameObject.findChild("weapon").transform.position;
        MissileProfile.Create(pos, Zangle, missile);

        _bulletRemain--;
        if (_bulletRemain > 0)
        {
            _nextTime = Time.time + _intervalTime;
        }
        if (_bulletRemain == 0)
        {
            RemoveSlow();
        }
    }
    
    void AddSlow()
    {
        unitRef.movement.movementSpeed.addMultis(_movementSpeedScale, BonusCode);
    }
    void RemoveSlow()
    {
        unitRef.movement.movementSpeed.removeMultis(BonusCode);
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
            unitRef.damage(target, damage);
    }
    
    float getZangle()
    {
        //float _float = _weapon.rotation.z * Mathf.Rad2Deg * 2f;
        float _float = _weapon.rotation.eulerAngles.z;
        if (unitRef.model.sprites.isRightFacing == false)
            _float += 180f;
        //Debug.Log(_float);
        return _float;
    }
}
