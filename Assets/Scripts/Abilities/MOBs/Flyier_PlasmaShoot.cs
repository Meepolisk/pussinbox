﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Flyier_PlasmaShoot : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public float spreadAngle = 1f;
    public float radius = 2f;
    public Damage damage;
    public short attackVariation = 0;
    
    protected override void abilityOrdered()
    {
        unitRef.movement.ActionForceStop();
        unitRef.movement.enable = false;
        unitRef.model.animation.SetInt("AttackVariation", attackVariation);
    }
    protected override void abilityCasted()
    {
        Shoot();
    }
    protected override void abilityFinished()
    {
        unitRef.movement.enable = true;
    }

    private void Shoot()
    {
        //create Missile
        missile.setData(unitRef, exploAction);
        Vector2 pos = unitRef.model.sprites.attachments["weapon"].center;
        float Zangle = unitRef.model.sprites.attachments["weapon"].zAngle;
        MissileProfile.Create(pos, Zangle, missile);

        //knockback
        //thisUnit.movement.AddKnockBack(RPlatformer.Angle2vector2(-Zangle, 300f));
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
            //Vector2 knockbackVec = ((Vector2)target.transform.position - pos).normalized * knockbackForce;
            //if (target.transform.position.y > (pos.y - 1f))
            //    knockbackVec += Vector2.up * knockbackForce;
            //if (knockbackForce > 0f)
            //    target.movement.AddKnockBack(knockbackVec);
        }
    }
}
