﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDash : Ability
{
    [Header("================================")]
    public float velocity = 25f;
    
    float overrideMovementX = 0;
    
    protected override void abilityCasted()
    {
        turnOn();
    }
    protected override void abilityFinished()
    { 
        turnOff();
    }
    void turnOn()
    {
        overrideMovementX = velocity;
        if (!unitRef.model.sprites.isRightFacing)
            overrideMovementX *= -1f;
        unitRef.movement.enable = false;
        unitRef.model.flipWhenTurn = false;
        unitRef.rObjectCollider.velocity = Vector2.zero;
    }
    void turnOff()
    {
        overrideMovementX = 0f;
        unitRef.movement.enable = true;
        unitRef.model.flipWhenTurn = true;
    }
    protected override void Update()
    {
        base.Update();
        //check lúc không dash
        if (!isActive) return;

        //dưới đây check lúc dash
        if (conditionUpdate()) //check điều kiện dash
        {
            //caster.movement.setInputDirection(overrideMovementX, 0f, false, true);
        }
        else
        {
            baseAbilityFinished();
        }


    }
    private bool conditionUpdate()
    {
        if (unitRef.rObjectCollider.contacts.right.isContacted() && overrideMovementX > 0)
            return false;
        if (unitRef.rObjectCollider.contacts.left.isContacted() && overrideMovementX < 0)
            return false;
        return true;
    }
}
