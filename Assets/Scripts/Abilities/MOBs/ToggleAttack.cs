﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbilityToggle))]
public class ToggleAttack : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public Damage damage;
    public float spread = 0.3f;
    [Header("========ANIMATIOR HELPER============")]
    [ReadOnlyWhenPlaying]
    public string animatorAttackSpeedParametter = "AttackSpeed";
    public float animationAttackSpeed = 1;
    [ReadOnlyWhenPlaying]
    public string animatorAttackVariationName = "AttackVariation";
    public short animatorAttackVariationParametter = 0;

    protected override void abilityOrdered()
    {
        unitRef.model.animation.SetInt(animatorAttackVariationName, animatorAttackVariationParametter);
        unitRef.model.animation.SetFloat(animatorAttackSpeedParametter, animationAttackSpeed);
    }

    protected override void abilityCasted()
    {
        Shoot();
    }

    private void Shoot()
    {
        //create Missile
        Attachment weapon = unitRef.model.sprites.attachments["weapon"];
        float Zangle = weapon.zAngle;
        Vector2 pos = weapon.center;
        pos += new Vector2(Random.Range(-spread, spread), Random.Range(-spread, spread));
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(pos, Zangle, missile);
    }
       
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
            unitRef.damage(target, damage);
    }
}
