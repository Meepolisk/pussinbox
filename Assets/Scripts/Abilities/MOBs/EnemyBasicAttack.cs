﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBasicAttack : Ability
{
    [Header("================================")]
    public MissileProfile missile;
    public float radius = 2f;
    public Damage damage;
    [Header("================================")]
    public ModelProfile specialEffectExplotion;
    public string animationTriggerCall = "Attack";
    //public float zAngle = 0f;
    
    private Transform _weaponPos
    {
        get
        {
            return unitRef.gameObject.findChild("weapon").transform;
        }
    }
    protected override void abilityPrepared()
    {
        unitRef.model.animation.SetTrigger(animationTriggerCall);
    }
    protected override void abilityCasted()
    {
        Shoot();
    }
    private void Shoot()
    {
        if (unitRef.isDead) return;
        //create Missile
        float Zangle = getZangle();
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(_weaponPos.position, Zangle, missile);
    }


    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        //SE
        SpecialEffect.Create(specialEffectExplotion, pos);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();
        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }
    }

    float getZangle()
    {
        //float _float = _weapon.rotation.z * Mathf.Rad2Deg * 2f;
        float _float = _weaponPos.rotation.eulerAngles.z;
        if (unitRef.model.sprites.isRightFacing == false)
            _float += 180f;
        //Debug.Log(_float);
        return _float;
    }
}
