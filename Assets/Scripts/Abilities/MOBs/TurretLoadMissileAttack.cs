﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TurretLoadMissileAttack : Ability {
    
    [Header("================================")]
    public MissileProfile missile;
    public float radius = 2f;
    public Damage damage;
    public ModelProfile shootEffect;
    public string animationReload = "WeaponReload";
    public GunController gunController;

    private int _count = 0;
    float Zangle
    {
        get
        {
            float _float = gunController.currentValue;
            if (unitRef.model.sprites.isRightFacing == false)
                _float = _float.revertZangle();
            return _float;
        }
    }

    protected override void Start()
    {
        base.Start();
        rotating();
    }
    protected override void abilityOrdered()
    {
        unitRef.model.flipWhenTurn = false;
        _count = 0;
    }
    protected override void abilityCasted()
    {
        _count++;
        Shoot(unitRef.model.sprites.attachments["weapon" + _count].transform.position);
    }
    private void Shoot(Vector2 pos)
    {
        SpecialEffect.Create(shootEffect, pos);
        SpecialEffect._lastCreatedSpecialEffect.transform.eulerAngles = new Vector3(0f, 0f, Zangle);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(pos, Zangle, missile);
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }
    }

    public override void finishedCooldown()
    {
        base.finishedCooldown();
        unitRef.model.animation.SetTrigger(animationReload);
        unitRef.model.flipWhenTurn = true;
    }

    protected override void Update()
    {
        base.Update();
        gunController.updateCal();
        rotating();
    }
    private void rotating()
    {
        unitRef.model.animation.SetFloat(gunController.animatorParameter, gunController.result);
    }
}
