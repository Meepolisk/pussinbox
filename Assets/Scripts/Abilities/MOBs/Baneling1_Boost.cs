﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Baneling1_Boost : Ability {
    [Header("================================")]
    public float force = 200f;

    private float oldInputDelay = 2f;
    protected override void abilityCasted()
    {
        createForce();
        removeInputDelay();
    }
    void createForce()
    {
        //get Vector
        Vector2 vec = new Vector2(-1f, 0.6f);
        if (unitRef.model.sprites.isRightFacing)
            vec = new Vector2(1f, 0.6f);
        //thisUnit.gameObject.GetComponent<Rigidbody2D>().AddForce(vec * force);
        unitRef.movement.AddKnockBack(vec * force);
    }
    void removeInputDelay()
    {
        oldInputDelay = unitRef.movement.inputDelay.amount;
        unitRef.movement.inputDelay.amount = 0f;
        RTimer.Add1TimeTimer("remove Input Delay for " + unitRef.name, 0.1f, restoreInputDelay);
    }
    void restoreInputDelay()
    {
        unitRef.movement.inputDelay.amount = oldInputDelay;
    }
}
