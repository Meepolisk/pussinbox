﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Solider_Attack : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public Damage damage;
    [Header("=================================")]
    public int variation = 0;
    public string animatorTriggerCallCast = "Attack";

    private Transform _weapon;

    protected override void Start()
    {
        base.Start();
        _weapon = gameObject.transform.root.gameObject.findChild("weapon").transform;
    }

    protected override void abilityOrdered()
    {
        unitRef.model.animation.SetInt(animatorTriggerCallCast + "Variation", variation);
    }

    protected override void abilityPrepared()
    {
        unitRef.model.animation.SetTrigger(animatorTriggerCallCast);
    }
    protected override void abilityCasted()
    {
        Shoot();
    }

    private void Shoot()
    {
        float Zangle = getZangle();
        missile.setData(unitRef, exploAction);
        Vector2 pos = _weapon.position;
        MissileProfile.Create(pos, Zangle, missile);
    }
    
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
            unitRef.damage(target, damage);
    }
    
    float getZangle()
    {
        //float _float = _weapon.rotation.z * Mathf.Rad2Deg * 2f;
        float _float = _weapon.rotation.eulerAngles.z;
        if (unitRef.model.sprites.isRightFacing == false)
            _float += 180f;
        //Debug.Log(_float);
        return _float;
    }
}
