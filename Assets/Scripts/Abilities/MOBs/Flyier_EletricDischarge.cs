﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Flyier_EletricDischarge : Ability {
    [Header("================================")]
    public float duration = 2f;
    public ModelProfile effectAttach;
    public Damage damage;
    public float radius = 5f;

    private SpecialEffect fx;
    Coroutine Timer;
    protected override void abilityPrepared()
    {
        if (fx != null)
        {
            fx.Destroy();
        }
        fx = SpecialEffect.Create(effectAttach, this.transform);
        if (Timer != null)
        {
            StopCoroutine(Timer);
        }
        Timer = StartCoroutine(timerEnd());

        //pause unit
        unitRef.canControl = false;
    }
    IEnumerator timerEnd()
    {
        yield return new WaitForSeconds(duration);
        unitRef.model.animation.SetTrigger(animMess.atOrder_Trigger);
    }
    protected override void abilityCasted()
    {
        Vector2 pos = fx.center;

        fx.transform.parent = null;
        fx.Destroy();

        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }


        unitRef.canControl = true;
    }
    public override void Unit_Die()
    {
        if (fx != null)
        {
            Destroy(fx);
        }
    }
}
