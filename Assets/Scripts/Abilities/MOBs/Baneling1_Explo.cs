﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Baneling1_Explo : Ability {
    [Header("================================")]
    [Range(2f,5f)]
    public float time = 3f;
    public MissileTarget targets;
    public Damage damage;
    public float radius = 2f;
    public float knockbackForce = 200f;

    public ModelProfile explotionSpecialEffect;

    private RTimer rtimerExplo;

    protected override void Awake()
    {
        base.Awake();

        rtimerExplo = gameObject.AddComponent<RTimer>();
    }

    protected override void abilityCasted()
    {
        rtimerExplo.setData(time, false, ExploAction);
        rtimerExplo.start();
    }
    private void ExploAction()
    {
        Vector2 pos = unitRef.transform.position;

        //animation
        SpecialEffect.Create(explotionSpecialEffect, pos);
        SpecialEffect.DestroyLastCreatedSpecialEffect();

        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        if (group != null)
        {
            foreach (Unit target in group)
            {
                unitRef.damage(target, damage);
                Vector2 knockbackVec = ((Vector2)target.transform.position - pos).normalized * knockbackForce;
                if (target.transform.position.y > (pos.y - 1f))
                    knockbackVec += Vector2.up * knockbackForce;
                if (knockbackForce > 0f)
                    target.movement.AddKnockBack(knockbackVec);
            }
        }
        unitRef.damage(unitRef, unitRef.hitPoint.remaining * 2f);

    }
}
