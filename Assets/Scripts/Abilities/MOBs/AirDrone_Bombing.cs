﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AirDrone_Bombing : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public ModelProfile effect;
    public float radius = 2f;
    public Damage damage;
    

    protected override void abilityCasted()
    {
        float Zangle = 0f;
        Vector2 pos = unitRef.model.sprites.attachments["weapon"].center;
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(pos, Zangle, missile);
    }

    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        //effect
        SpecialEffect.Create(effect, pos);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();

        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }
    }
}
