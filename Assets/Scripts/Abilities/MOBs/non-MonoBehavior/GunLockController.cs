﻿using UnityEngine;

[RequireComponent(typeof(AbilityCooldown))]
[System.Serializable]
public class GunController
{
    public float targetValue = 0f;
    [ReadOnlyWhenPlaying]
    public float currentValue = 0f;
    [Tooltip("Leave 0 to insta-lock")]
    public float valuePerSec = 20f;
    public float min = -10f, max = 40f;
    public string animatorParameter = "GunRotation";

    public float result
    {
        get
        {
            return (currentValue - min) / (max - min);
        }
    }
    public void updateCal()
    {
        if (targetValue == currentValue) return;

        float _RealTargetValue = Mathf.Clamp(targetValue, min, max);
        if (currentValue > _RealTargetValue)
            currentValue = Mathf.Clamp(currentValue - (valuePerSec * Time.deltaTime), _RealTargetValue, currentValue);
        else
            currentValue = Mathf.Clamp(currentValue + (valuePerSec * Time.deltaTime), currentValue, _RealTargetValue);
    }
}
