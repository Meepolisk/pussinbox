﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeImpact : UnitAddOn
{
    public Damage damage;
    
    private new Collider2D collider;

    protected override void Awake()
    {
        base.Awake();
        collider = GetComponent<Collider2D>();
    }
    
    void OnTriggerStay2D(Collider2D collision)
    {
        var targetCol = collision.GetComponent<RObjectCollider>();
        if (targetCol && targetCol.model.rObject is Unit)
        {
            Unit _target = targetCol.model.rObject as Unit;
            IMPACT(_target);
        }

    }
    void IMPACT(Unit _target)
    {
        //tạm thời để DPS
        if (!PlayerGroup.isAlly(unitRef.player, _target.player))
            unitRef.damage(_target, damage.amount * Time.deltaTime);
    }
}
