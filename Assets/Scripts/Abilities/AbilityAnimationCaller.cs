﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class AbilityAnimationHelper
{

    [Header("--[RECIEVE]--")]
    [SerializeField]
    [ReadOnlyWhenPlaying]
    private string _onPrepare = "";
    public string onPrepare { get { return _onPrepare; } }

    [SerializeField]
    [ReadOnlyWhenPlaying]
    private string _onCast = "";
    public string onCast { get { return _onCast; } }
    
    [SerializeField]
    [ReadOnlyWhenPlaying]
    private string _onFinish = "";
    public string onFinish { get { return _onFinish; } }


    [Header("--[SEND]--")]
    public string atOrder_Trigger = "";
    //public string atPrepare_Trigger = "";
    public string atToggle_Bool = "";

    private Ability abiRef = null;
    public void prepare(Ability _abi)
    {
        abiRef = _abi;

        //instance animation getter!
        if (!string.IsNullOrEmpty(_onPrepare))
            abiRef.unitRef.RegNewAction(_onPrepare, abiRef.baseAbilityPrepared);
        if (!string.IsNullOrEmpty(_onCast))
            abiRef.unitRef.RegNewAction(_onCast, abiRef.baseAbilityCasted);
        if (!string.IsNullOrEmpty(_onFinish))
            abiRef.unitRef.RegNewAction(_onFinish, abiRef.baseAbilityFinished);
    }

}
//#region Editor
//#if UNITY_EDITOR
//[CustomPropertyDrawer(typeof(AbilityAnimationHelper))]
//public class AbilityAnimationHelperInspector : PropertyDrawer
//{
//    private AbilityAnimationHelper thisHelper;
//    private bool _Foldout;
//    private const float memberHeight = 17f;
//    private const float kButtonWidth = 18f;

//    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//    {
//        CheckInitialize(property, label);
//        if (_Foldout)
//            return (thisHelper.Count + 1) * memberHeight;
//        return 17f;
//    }

//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        CheckInitialize(property, label);

//        position.height = memberHeight;

//        var foldoutRect = position;
//        foldoutRect.width -= 2 * kButtonWidth;
//        EditorGUI.BeginChangeCheck();
//        _Foldout = EditorGUI.Foldout(foldoutRect, _Foldout, label, true);
//        if (EditorGUI.EndChangeCheck())
//            EditorPrefs.SetBool(label.text, _Foldout);

//        var buttonRect = position;
//        //buttonRect.x = position.width - kButtonWidth + position.x;
//        buttonRect.x = position.width - kButtonWidth + position.x;
//        buttonRect.width = kButtonWidth + 2;

//        if (GUI.Button(buttonRect, new GUIContent("+", "Add item"), EditorStyles.miniButton))
//        {
//            AddNewItem();
//        }

//        buttonRect.x -= kButtonWidth;

//        //if (GUI.Button(buttonRect, new GUIContent("X", "Clear dictionary"), EditorStyles.miniButtonRight))
//        //{
//        //    ClearDictionary();
//        //}

//        if (!_Foldout)
//            return;

//        foreach (var item in thisHelper)
//        {
//            var key = item.Key;
//            var value = item.Value;

//            position.y += 17f;

//            var keyRect = position;
//            keyRect.width /= 2;
//            keyRect.width -= 4;
//            EditorGUI.BeginChangeCheck();
//            var newKey = DoField(keyRect, typeof(TK), key);
//            if (EditorGUI.EndChangeCheck())
//            {
//                try
//                {
//                    thisHelper.Remove(key);
//                    thisHelper.Add(newKey, value);
//                }
//                catch (Exception e)
//                {
//                    Debug.Log(e.Message);
//                }
//                break;
//            }

//            var valueRect = position;
//            valueRect.x = position.width / 2 + 15;
//            valueRect.width = keyRect.width - kButtonWidth;
//            EditorGUI.BeginChangeCheck();
//            value = DoField(valueRect, typeof(TV), value);
//            if (EditorGUI.EndChangeCheck())
//            {
//                thisHelper[key] = value;
//                break;
//            }

//            var removeRect = valueRect;
//            removeRect.x = valueRect.xMax + 2;
//            removeRect.width = kButtonWidth;
//            if (GUI.Button(removeRect, new GUIContent("x", "Remove item"), EditorStyles.miniButtonRight))
//            {
//                RemoveItem(key);
//                break;
//            }
//        }
//    }

//    private void RemoveItem(TK key)
//    {
//        thisHelper.Remove(key);
//    }

//    private void CheckInitialize(SerializedProperty property, GUIContent label)
//    {
//        if (thisHelper == null)
//        {
//            var target = property.serializedObject.targetObject;
//            thisHelper = fieldInfo.GetValue(target) as SerializableDictionary<TK, TV>;
//            if (thisHelper == null)
//            {
//                thisHelper = new SerializableDictionary<TK, TV>();
//                fieldInfo.SetValue(target, thisHelper);
//            }

//            _Foldout = EditorPrefs.GetBool(label.text);
//        }
//    }

//    private static readonly Dictionary<Type, Func<Rect, object, object>> _Fields =
//        new Dictionary<Type, Func<Rect, object, object>>()
//        {
//            { typeof(int), (rect, value) => EditorGUI.IntField(rect, (int)value) },
//            { typeof(float), (rect, value) => EditorGUI.FloatField(rect, (float)value) },
//            { typeof(string), (rect, value) => EditorGUI.TextField(rect, (string)value) },
//            { typeof(bool), (rect, value) => EditorGUI.Toggle(rect, (bool)value) },
//            { typeof(Vector2), (rect, value) => EditorGUI.Vector2Field(rect, GUIContent.none, (Vector2)value) },
//            { typeof(Vector3), (rect, value) => EditorGUI.Vector3Field(rect, GUIContent.none, (Vector3)value) },
//            { typeof(Bounds), (rect, value) => EditorGUI.BoundsField(rect, (Bounds)value) },
//            { typeof(Rect), (rect, value) => EditorGUI.RectField(rect, (Rect)value) }
//        };

//    private static T DoField<T>(Rect rect, Type type, T value)
//    {
//        Func<Rect, object, object> field;
//        if (_Fields.TryGetValue(type, out field))
//            return (T)field(rect, value);

//        if (type.IsEnum)
//            return (T)(object)EditorGUI.EnumPopup(rect, (Enum)(object)value);

//        if (typeof(UnityObject).IsAssignableFrom(type))
//            return (T)(object)EditorGUI.ObjectField(rect, (UnityObject)(object)value, type, true);

//        Debug.Log("Type is not supported: " + type);
//        return value;
//    }

//    //private void ClearDictionary()
//    //{
//    //    _Dictionary.Clear();
//    //}

//    private void AddNewItem()
//    {
//        TK key;
//        if (typeof(TK) == typeof(string))
//            key = (TK)(object)"";
//        else key = default(TK);

//        var value = default(TV);
//        try
//        {
//            thisHelper.Add(key, value);
//        }
//        catch (Exception e)
//        {
//            Debug.Log(e.Message);
//        }
//    }
//}
//#endif
//#endregion


[System.Serializable]
public class AbilityGroup
{
    [ReadOnlyWhenAnimation]
    [ReadOnlyWhenPlaying]
    [SerializeField]
    [Tooltip("Tạo group cho ability. Các Ability cùng một group không thể cùng lúc sử dụng được, " +
        " để trống sẽ cho ability dạng default, không bị ảnh hưởng của các abi khác (ví dụ: nhảy, ngồi,v.v...)")]
    private string _name = "";
    public string name
    {
        get
        {
            return _name;
        }
    }

    [ReadOnlyWhenAnimation]
    [ReadOnlyWhenPlaying]
    [SerializeField]
    [Tooltip("Tạo priority cho ability này trong group. Ability có Priority thấp hơn không thể can thiệp vào ability cùng group có priority cao hơn hoặc bằng")]
    private short _priority = 0;
    public short priority
    {
        get
        {
            return _priority;
        }
    }

    private Ability abiRef = null;
    public void prepare(Ability _abi)
    {
        abiRef = _abi;

        //instance animation getter!

        if (!string.IsNullOrEmpty(name))
        {
            abiRef.unitRef.AddGroup(_name);
        }
    }
}

#region Editor
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(AbilityGroup))]
public class AbilityGroupInspector : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var rect1 = new Rect(position.x, position.y, (position.width -100), position.height);
        var rect2 = new Rect(position.x + (position.width) - 100, position.y, 50, position.height);
        var rect3 = new Rect(position.x + (position.width) - 50, position.y, 50, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
        EditorGUI.PropertyField(rect1, property.FindPropertyRelative("_name"), GUIContent.none);
        GUI.Label(rect2, "priority");
        EditorGUI.PropertyField(rect3, property.FindPropertyRelative("_priority"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
#endif
#endregion