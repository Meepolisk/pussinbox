﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ability))]
public class AbilityCooldown : AbiAddOn
{
    [System.Serializable]
    public class Stack
    {
        public bool enabled
        {
            get
            {
                if (maxStack > 1)
                    return true;
                return false;
            }
        }
        [ReadOnlyWhenPlaying]
        public int currentStack = 1;
        public int maxStack = 1;
        [ReadOnly]
        public float _timeTick;
        public float stackCooldown = 0f;
    }

    [ReadOnly]
    public float _timeTick = 0f;
    [Range(0.1f, 10f)]
    public float time = 0f;
    public Ability.CheckType stage = Ability.CheckType.afterStart;
    public Stack stack;
    [ReadOnly]
    public bool isActive = false;
    
    protected override void OnEnable()
    {
        base.OnEnable();
        _timeTick = time;
        abiRef.cooldown = this;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        abiRef.cooldown = null;
    }

    public float UIremaining
    {
        get
        {
            //if (stack.enabled && stack._timeTick < _timeTick)
            //    return stack._timeTick;
            //return _timeTick;
            if (stack.currentStack == 0)
                return (_timeTick > stack._timeTick)?_timeTick:stack._timeTick;
            return stack._timeTick;
        }
    }
    public float UIscale
    {
        get
        {
            try
            {
                if (stack.currentStack == 0)
                {
                    return (UIremaining / time);
                }
                //_timeTick < stack._timeTick && 
                return (stack._timeTick / stack.stackCooldown);
            }
            catch
            {
                return 0f;
            }
        }
    }
    public void CheckToStart()
    {
        if (stack.currentStack == stack.maxStack)
            start();

        stack.currentStack--;
        if (stack.enabled)
        {
            if (stack.stackCooldown > 0f)
                stack._timeTick = stack.stackCooldown;
        }
    }
    private void start()
    {
        isActive = true;
    }
    private void finishCooldown()
    {
        stack.currentStack++;
        _timeTick = time;
        if (stack.currentStack == stack.maxStack)
            finishAllCooldown();
    }
    private void finishAllCooldown()
    {
        isActive = false;
        abiRef.finishedCooldown();
    }
    private void Update()
    {
        if (!isActive) return;

        _timeTick -= Time.deltaTime;
        if (_timeTick <= 0f)
        {
            finishCooldown();
        }
        if (stack.enabled && stack._timeTick > 0f)
        {
            stack._timeTick -= Time.deltaTime;
            if (stack._timeTick < 0f)
                stack._timeTick = 0f;
        }
    }
    public void reset()
    {
        if (!isActive) return;
        finishCooldown();
    }

    public override bool abiRefCastCondition()
    {
        //if (isActive)
        //    return false;
        if (stack.currentStack <= 0)
            return false;
        //if (stack.enabled && stack._timeTick > 0f)
        if (UIremaining > 0f)
            return false;
        return true;
    }
}

