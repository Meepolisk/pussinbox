﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ability))]
public abstract class AbiAddOn : MonoBehaviour
{
    protected Ability abiRef;
    protected virtual void Awake()
    {
        abiRef = GetComponent<Ability>();
    }
    protected virtual void OnEnable()
    {
        abiRef.AAOs.Add(this);
    }
    protected virtual void OnDisable()
    {
        abiRef.AAOs.Remove(this);
    }

    public virtual bool abiRefCastCondition()
    {
        return true;
    }
    public virtual bool abiRefUpdateCondition()
    {
        return true;
    }
}

