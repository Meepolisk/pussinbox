﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CallMech : Ability
{
    [Header("====Transform====")]
    public HeroBox boxPrefab;
    public bool autoEquipAtStart = true;
    public string animatorReseter = "Reset";

    public override void FinishSetup()
    {
        base.FinishSetup();
        if (autoEquipAtStart)
            StartCoroutine(firstReplace());
    }
    IEnumerator firstReplace()
    {
        yield return new WaitForEndOfFrame();
        CreateBox(false);
    }
    protected override bool condition()
    {
        if (unitRef.rObjectCollider.contacts.down.isContacted() == true)
            return true;
        return false;
    }
    protected override void abilityCasted()
    {
        CreateBox(true);
    }
    private void CreateBox(bool _birthAnimation)
    {
        //boxPrefab.Create(unitRef, _birthAnimation);
        StartCoroutine(StopAnimation(0.15f));
    }

    IEnumerator StopAnimation(float time)
    {
        yield return new WaitForSeconds(time);
        disableCharracter();
    }
    void disableCharracter()
    {
        unitRef.Hide = true;
    }
}