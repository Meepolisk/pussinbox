﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MeleeAttack : Ability {
    [Header("================================")]
    public Damage damage;
    public float offset = 1f;
    public float radius = 2f;

    protected override bool condition()
    {
        return (unitRef.rObjectCollider.contacts.down.isContacted());
    }
    protected override void abilityPrepared()
    {
        unitRef.movement.enable = false;
        unitRef.rigid.velocity = Vector2.zero;
    }
    protected override void abilityCasted()
    {
        //sdads
        Vector2 pos = getPos();
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }
    }
    protected override void abilityFinished()
    {
        unitRef.movement.enable = true;
    }
    private Vector2 getPos()
    {
        Vector2 pos = unitRef.transform.position;
        if (unitRef.model.sprites.isRightFacing)
        {
            pos.x += offset;
        }
        else
        {
            pos.x -= offset;
        }
        return pos;
    }
}
