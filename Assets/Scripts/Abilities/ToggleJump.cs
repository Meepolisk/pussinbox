﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbilityToggle))]
public class ToggleJump : Ability {
    [Header("================================")]
    public float jumpForceMin = 750f;
    public float jumpForceMax = 1500f;

    private float aditionalJumpForcePerSec = 0f;
    private bool waitTillFixedUpdate = false;

    protected override bool condition()
    {
        return (unitRef.rObjectCollider.contacts.down.isContacted());
    }
    protected override void abilityPrepared()
    {
        aditionalJumpForcePerSec = (jumpForceMax - jumpForceMin);
        if (toggle.time > 0)
        {
            aditionalJumpForcePerSec /= toggle.time;
        }

        waitTillFixedUpdate = false;
        unitRef.rObjectCollider.AddForce(new Vector2(0f, jumpForceMin));
        waitTillFixedUpdate = true;
        StartCoroutine(wait());
    }
    protected override void Update()
    {
        base.Update();
        if (!toggle.isActive || waitTillFixedUpdate) return;

        float fixedUpdateForceY = (aditionalJumpForcePerSec * Time.fixedDeltaTime);
        unitRef.rObjectCollider.AddForce(new Vector2(0f, fixedUpdateForceY));
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(Time.fixedDeltaTime * 1.2f);
        waitTillFixedUpdate = false;
    }
}
