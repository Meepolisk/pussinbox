﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PharahConcusiveBlast : Ability
{
    [Header("================================")]
    public MissileProfile missile;
    public float knockbackForce = 500f;
    public float aoe = 2f;

    protected override void abilityCasted()
    {
        unitRef.model.animation.SetTrigger("AbilityCast");
        //caster.GetComponent<AudioSource>().Play();

        //float Zangle = -135f;
        //if (caster.model.sprites.isRightFacing)
        //    Zangle = -45f;
        missile.setData(unitRef, exploAction);
        //MissileProfile.Create(Zangle, missile);
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        Unit[] group = Unit.GetUnitsInRange(pos, aoe);
        if (group == null) return;
        //do something to enemy in group
        foreach (Unit target in group)
        {
            Vector2 vec = ((Vector2)target.gameObject.transform.position - pos).normalized * aoe * knockbackForce;
            //Debug.Log(vec.ToString());
            //target.gameObject.GetComponent<Rigidbody2D>().AddForce(vec);
            //target.gameObject.GetComponent<Rigidbody2D>().velocity = vec/50f;

            target.gameObject.GetComponent<Movement>().AddKnockBack(vec);
        }
        RDebug.DrawCircle(pos, aoe,Color.red, 1f);
    }
}
