﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbilityToggle))]
public class Nheo_Attack : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public Damage damage;
    public float angleSpreadMax = 5f;
    public float energyRestore = 1f;
    
    private Attachment weapon;
    
    public override void FinishSetup()
    {
        base.FinishSetup();
        weapon = unitRef.model.sprites.attachments["weapon"];
    }
    protected override void abilityCasted()
    {
        float spread = Random.Range(-angleSpreadMax, angleSpreadMax);
        Vector2 pos = weapon.center;
        float zAngle = weapon.zAngle + Random.Range(-spread, spread);
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(pos, zAngle, missile);
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
        {
            unitRef.damage(target, damage);

            //restore energy
            if (energyRestore > 0)
            {
                unitRef.energy.current += energyRestore;
            }
        }
    }
}
