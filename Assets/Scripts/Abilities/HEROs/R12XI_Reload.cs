﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class R12XI_Reload : Ability {
    [Header("====================")]
    public FrapProfile SodaFrap;
    public FrapProfile SodaFrap2;
    public AudioClip reloadSound;

    public AbilityButton functionKey_Down = new AbilityButton(RButton.Shift, RButtonType.down);
    public AbilityButton boostJumpKey = new AbilityButton(RButton.Jump, RButtonType.down);
    
    public override void FinishSetup()
    {
        boostJumpKey.setup(ActionBoostJump);
        unitRef.button.Add(boostJumpKey);

        functionKey_Down.setup(ActionReload);
        unitRef.button.Add(functionKey_Down);
        
        unitRef.RegNewAction("UnloadSoda", UnloadEnergy);
        unitRef.RegNewAction("ReloadJumpStart", StartRolling);
        unitRef.RegNewAction("ReloadJumpFinish", FinishRolling);
    }

    #region KeyReg
    void ActionReload()
    {
        UseAbi();
    }
    void ActionBoostJump ()
    {
        if (isActive && canRoll)
        {
            canRoll = false;
            SpecialJump();
        }
    }
    #endregion

    private bool canRoll = true;
    private bool _reloadRollActive = false;
    void SpecialJump ()
    {
        unitRef.rigid.velocity = Vector2.zero;
        Debug.Log("ROLL");
        unitRef.model.animation.SetTrigger("ReloadJump");
    }
    void StartRolling()
    {
        _reloadRollActive = true;
        unitRef.movement.enable = false;
    }
    void FinishRolling()
    {
        //Debug.Log("ASDASDASD FINISH");
        //unitRef.rigid.velocity = Vector2.zero;
        unitRef.movement.actionFlip();
        _reloadRollActive = false;
        //unitRef.movement.enable = true;
        //unitRef.canControl = true;

        unitRef.movement.enable = true;
    }
    protected override void abilityOrdered()
    {
        canRoll = true;
        unitRef.movement.enable =  false;
    }
    protected override void abilityPrepared()
    {
        canRoll = false;
    }

    protected override void abilityCasted()
    {
        unitRef.energy.current = unitRef.energy.max;
    }
    protected override void abilityFinished()
    {
        //if (unitRef.movement is Movement_Ground_Player)
        //{
        //    Movement_Ground_Player mp = unitRef.movement as Movement_Ground_Player;
        //    mp.canCrouch = true;
        //    mp.jump.canJump = true;
        //}
        //unitRef.movement.movementSpeed.removeMultis("Reload");
        unitRef.movement.enable = true;
    }

    private void UnloadEnergy()
    {
        GameMaster.PlaySound(reloadSound);
        if (isActive)
        {
            //tạo vỏ chai
            Vector2 pos = unitRef.model.sprites.attachments["backWeapon1"].center;
            float zAngle = unitRef.model.sprites.attachments["backWeapon1"].zAngle;
            float force = 500f;

            if (isActive)
                unitRef.energy.current = 0f;

            SodaFrap.Create(pos, zAngle);
            Frap._lastCreatedFrap.rigid.AddForce(RPlatformer.Angle2vector2(zAngle, force));
            SodaFrap2.Create(pos, zAngle);
            Vector2 Vec = RPlatformer.Angle2vector2(90f, force);
            Frap._lastCreatedFrap.rigid.AddForce(Vec);
        }
    }
    protected override bool condition()
    {
        if (unitRef.movement is Movement_Ground)
        {
            Movement_Ground mp = unitRef.movement as Movement_Ground;
            if (!mp.Grounded)
                return false;
        }
        return true;
    }

    protected override void Update()
    {
        base.Update();
        if (_reloadRollActive)
        {
            float force = 20f;
            if (!unitRef.model.sprites.isRightFacing)
                force *= -1;
            unitRef.movement.setInputDirection(force, 0f, false, true);
        }
        //try
        //{
        //    Movement_Ground_Player mg = unitRef.movement as Movement_Ground_Player;
        //    if (mg && canJump && functionKey_Press.isTrigger)
        //    {
        //        mg.jump.canJump = false;
        //    }
        //    else
        //    {
        //        mg.jump.canJump = true;
        //    }
        //}
        //catch { }
    }
}
