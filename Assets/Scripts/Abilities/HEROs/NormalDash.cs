﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbilityToggle))]
public class NormalDash : Ability
{
    [Header("================================")]
    public bool canAirDash = false;
    public float velocity = 25f;

    private bool haveAirDashAlready = false;
    float overrideMovementX = 0;
    private bool airPhase = false;
    
    protected override bool condition()
    {
        // không airdash được hoặc airDash rồi mà còn lơ lửng thì fail
        if ((!canAirDash || haveAirDashAlready) && !unitRef.rObjectCollider.contacts.down.isContacted())
            return false;
        return true;
    }
    protected override void abilityPrepared()
    {
        //Debug.Break();
    }
    protected override void abilityCasted()
    {
        turnOn();
    }
    protected override void abilityToggleOff()
    { 
        turnOff();
    }
    void turnOn()
    {
        overrideMovementX = velocity;
        if (!unitRef.model.sprites.isRightFacing)
            overrideMovementX *= -1f;
        unitRef.movement.enable = false;
        unitRef.model.flipWhenTurn = false;

        //nếu contact down = groundDash
        if (unitRef.rObjectCollider.contacts.down.isContacted())
        {
            airPhase = false;
        }
        else
        {
            unitRef.rObjectCollider.velocity = Vector2.zero;
            unitRef.rigid.gravityScale = 0f;
            airPhase = true;
            haveAirDashAlready = true;
        }
    }
    void turnOff()
    {
        //Debug.Log("[" + Time.time + "]" + gameObject.name + " DASH STOP");
        overrideMovementX = 0f;
        unitRef.movement.enable = true;
        unitRef.model.flipWhenTurn = true;
        unitRef.rObjectCollider.velocity = Vector2.zero;

        //nếu contact down = groundDash
        if (airPhase)
        {
            unitRef.rigid.gravityScale = 1f;
        }
    }
    protected override void Update()
    {
        base.Update();

        //check lúc không dash
        if (!toggle.isActive)
        {
            //check nếu airDash rồi, mà contact ground thì đc airDash tiếp
            if (haveAirDashAlready && unitRef.rObjectCollider.contacts.down.isContacted())
            {
                haveAirDashAlready = false;
            }
            return;
        }

        //dưới đây check lúc dash
        if (conditionUpdate()) //check điều kiện dash
        {
            //set input để có tốc độ lướt
            //caster.movement.setInputDirection(overrideMovementX, 0f, false, true);
        }
        else
        {
            toggle.forceStop(true);
        }


    }
    private bool conditionUpdate()
    {
        if (unitRef.rObjectCollider.contacts.right.isContacted() && overrideMovementX > 0)
            return false;
        if (unitRef.rObjectCollider.contacts.left.isContacted() && overrideMovementX < 0)
            return false;
        if (!airPhase && !unitRef.rObjectCollider.contacts.down.isContacted())
            return false;
        return true;
    }
}
