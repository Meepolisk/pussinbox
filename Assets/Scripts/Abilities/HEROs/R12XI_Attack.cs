﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbilityToggle))]
public class R12XI_Attack : Ability {
    [Header("================================")]
    public MissileProfile missileNormal;
    public Damage damageNormal;
    public MissileProfile missileLowArmor;
    public Damage damageLow;
    public string variation1getter = "varNerf1";
    public string variation2getter = "varNerf2";
    public float _energyRequirePerShoot = 3f;

    public Speed speed;
    [System.Serializable]
    public class Speed
    {
        public float runMS = 1.4f;
        public float rapidFireMS = 0f;
        public float rapidFireAS = 1.5f;
    }
    
    private Attachment weapon;
    private const string BonusCode = "R12XI_WeaponMS";

    private bool _alternateFunction = false;
    public AbilityButton functionKey_Press = new AbilityButton(RButton.Shift, RButtonType.press);
    public AbilityButton mainHotKey = new AbilityButton(RButton.Attack, RButtonType.press);

    public override void FinishSetup()
    {
        unitRef.movement.movementSpeed.addMultis(1f, BonusCode);
    }
    protected override void Update()
    {
        base.Update();

        //use
        if (mainHotKey.isTrigger && !isActive)
        {
            UseAbi();
        }
        else if (!mainHotKey.isTrigger && isActive)
        {
            toggle.stopButtonTriggered();
        }

        _alternateFunction = functionKey_Press.isTrigger;
        if (_alternateFunction)
        {
            if (isActive)
            {
                unitRef.movement.movementSpeed.addMultis(speed.rapidFireMS, BonusCode);
                unitRef.model.animation.SetFloat("AttackSpeed",speed.rapidFireAS);
            }
            else
            {
                unitRef.movement.movementSpeed.addMultis(speed.runMS, BonusCode);
                unitRef.model.animation.SetFloat("AttackSpeed", 1f);
            }
        }
        else
        {
            unitRef.model.animation.SetFloat("AttackSpeed", 1f);
            unitRef.movement.movementSpeed.addMultis(1f, BonusCode);
        }
    }
    protected override void Awake()
    {
        base.Awake();
        unitRef.RegNewAction(variation1getter, setVar1);
        unitRef.RegNewAction(variation2getter, setVar2);
    }
    void setVar1()
    {
        weapon = unitRef.model.sprites.attachments["weapon1"];
    }
    void setVar2()
    {
        weapon = unitRef.model.sprites.attachments["weapon2"];
    }

    protected override void abilityPrepared()
    {

    }
    protected override void abilityCasted()
    {
        //float cal = (toggle._timeTick / timeSpreadMax) * angleSpreadMax - timeStartSpreading;
        //float spread = Mathf.Clamp(cal, 0, angleSpreadMax);
        Vector2 pos = weapon.center;
        float zAngle = weapon.zAngle; // + Random.Range(-spread, spread);
        if (unitRef.energy.current >= _energyRequirePerShoot)
        {
            ShootNormal(pos,zAngle);
            unitRef.energy.current -= _energyRequirePerShoot;
        }
        else
        {
            ShootLow(pos, zAngle);
        }
    }
    void ShootNormal(Vector2 pos, float zAngle)
    {
        missileNormal.setData(unitRef, exploActionNormal);
        MissileProfile.Create(pos, zAngle, missileNormal);
    }
    void ShootLow(Vector2 pos, float zAngle)
    {
        missileLowArmor.setData(unitRef, exploActionLow);
        MissileProfile.Create(pos, zAngle, missileLowArmor);
    }
    
    public void exploActionNormal(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
            unitRef.damage(target, damageNormal);
    }
    public void exploActionLow(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
            unitRef.damage(target, damageLow);
    }
}
