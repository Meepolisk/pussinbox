﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PharahJumpJet : Ability
{
    [Header("================================")]
    public float flyForce = 1500f;

    protected override void abilityCasted()
    {
        unitRef.model.animation.SetTrigger("Fly");
        //int i = Random.Range(0, jumpClips.Length);
        //AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);
        unitRef.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, flyForce));
    }
    
}
