﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AirJump : Ability {
    [Header("================================")]
    public float jumpForce = 1000f;
    public string animatorTriggerCall = "AirJump";
    
    private bool canJump = true;

    protected override bool condition()
    {
        return (!unitRef.rObjectCollider.contacts.down.isContacted() && canJump);
    }
    protected override void abilityPrepared()
    {
        unitRef.model.animation.SetTrigger(animatorTriggerCall);
    }
    
    protected override void abilityCasted()
    {
        unitRef.rigid.velocity = new Vector2(unitRef.rigid.velocity.x, 0f);
        unitRef.rigid.AddForce(new Vector2(0f, jumpForce));
        canJump = false;
    }

    protected override void Update()
    {
        base.Update();
        if (canJump == false && unitRef.rObjectCollider.contacts.down.isContacted())
            canJump = true;
    }
}
