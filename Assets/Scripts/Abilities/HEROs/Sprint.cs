﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbilityToggle))]
public class Sprint : Ability
{
    [Header("================================")]
    public float movementSpeedMul = 1.5f;
    [ReadOnlyWhenPlaying]
    public string animatorFloatCall = "MovingBlend";
    public float blendTimeActive = 0.5f;
    public float blendTimeInactive = 0.2f;
    [ReadOnlyWhenPlaying]
    public string bonusMScode = "SprintMS";

    [SerializeField]
    private float blendTick = 0f;
      
    protected override void abilityCasted()
    {
        if (blendTimeActive <= 0f)
        {
            unitRef.movement.movementSpeed.addMultis(movementSpeedMul, bonusMScode);
            unitRef.model.animation.SetFloat(animatorFloatCall, 1);
        }
        else
        {
            blendTick = 0f;
        }
    }
    protected override void abilityFinished()
    {
        if (blendTimeInactive <= 0f)
        {
            stopBlending();
        }
        else
        {
            blendTick = blendTimeInactive;
        }
    }

    private bool onGroundCondition()
    {
        if (unitRef.movement is Movement_Ground)
        {
            Movement_Ground mg = unitRef.movement as Movement_Ground;
            if (!mg.Grounded)
                return false;
        }
        return true;
    }
    protected override void Update()
    {
        base.Update();


        
        if (toggle.isActive && blendTimeActive > 0 && onGroundCondition())
        {
            if (blendTick < blendTimeActive)
            {
                blendTick += Time.deltaTime;
                adjustMS(blendTimeActive);
            }
            else
            {
                blendTick = blendTimeActive;
            }
        }
        else if (!toggle.isActive && blendTick > 0)
        {
            blendTick -= Time.deltaTime;
            adjustMS(blendTimeInactive);
            if (blendTick <= 0)
            {
                stopBlending();
                return;
            }
        }
    }
    private void stopBlending()
    {
        blendTick = 0f;
        unitRef.movement.movementSpeed.removeMultis(bonusMScode);
        unitRef.model.animation.SetFloat(animatorFloatCall, 0);
    }
    private void adjustMS(float max)
    {
        float scale = blendTick / max;
        float currentBonus = 1f + (scale * (movementSpeedMul - 1f));
        unitRef.movement.movementSpeed.addMultis(currentBonus, bonusMScode);
        unitRef.model.animation.SetFloat(animatorFloatCall, blendTick / blendTimeActive);
    }
}
