﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AbilityToggle))]
public class AirFloat : Ability {
    [Header("================================")]
    public float floatTimeMax = 4f;
    [ReadOnlyWhenPlaying]
    public float movingScale = 2f;
    public float movingSpeed = 8f;
    public string animatorBoolEnable = "Floating";
    
    private Movement_Fly newMovement = null;
    private Movement baseMovement = null;
    private bool oldFlipWhenTurn = false;
    
    protected override bool condition()
    {
        return (!unitRef.rObjectCollider.contacts.down.isContacted());
    }
    protected override void abilityPrepared()
    {
        unitRef.model.animation.SetBool(animatorBoolEnable,true);
    }
    
    protected override void abilityCasted()
    {
        startFloating();
    }
    protected override void abilityFinished()
    {
        stopFloating();
    }

    protected override void Update()
    {
        base.Update();
        float x = unitRef.movement.currentMoveForce.x;
        if (x != 0 && toggle.isActive)
            toggle._timeTick += (movingScale - 1) * Time.deltaTime;
    }
    private void startFloating()
    {
        generateNewMovementMethod();
    }
    private void stopFloating()
    {
        restoreMovementMethod();
        unitRef.model.animation.SetBool(animatorBoolEnable, false);
    }
    
    //sẽ phải chỉnh  cái này lại, dùng clone component
    private void generateNewMovementMethod()
    {
        baseMovement = unitRef.movement;
        
        newMovement = unitRef.gameObject.AddComponent<Movement_Fly>();
        CopyVar<Movement>(baseMovement as Movement,newMovement as Movement);
        oldFlipWhenTurn = unitRef.model.flipWhenTurn;
        newMovement.movementSpeed.baseValue = movingSpeed;
        newMovement.inputDelay.yScale = 0f;
        unitRef.model.flipWhenTurn = false;

        baseMovement.enabled = false;
    }
    private void restoreMovementMethod()
    {
        Destroy(newMovement);
        baseMovement.enabled = true;
        unitRef.model.flipWhenTurn = oldFlipWhenTurn;
    }
    public static void CopyVar <T>(T original, T destination) where T : Component
    {
        System.Type type = original.GetType().BaseType;
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(destination, field.GetValue(original));
        }
    }
}
