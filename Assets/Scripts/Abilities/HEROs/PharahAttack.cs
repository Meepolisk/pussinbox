﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PharahAttack : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public float aoe = 1f;
    public Damage damage;

    //public delegate void Action<T>(T obj);

    protected override void abilityCasted()
    {
        unitRef.model.animation.SetTrigger("Shoot");
        //caster.GetComponent<AudioSource>().Play();

        //float Zangle = 180f;
        //if (caster.model.sprites.isRightFacing)
        //    Zangle = 0f;
        missile.setData(unitRef, exploAction);
        //MissileProfile.Create(Zangle, missile);
        
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        Unit[] group = Unit.GetUnitsInRange(pos, aoe);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, aoe, Color.red, 1.5f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }
    }
}
