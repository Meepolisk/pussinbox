﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJump : Ability
{
    [Header("================================")]
    public float horizontalScale = 0.5f;
    public string animatorTriggerCall = "AirJump";
    public WallSlice wallSlice;

    [System.Serializable]
    public class WallSlice
    {
        public bool enable = false;
        public string animatorBoolCall = "WallRide";
        public float wallSlideDrag = 100f;

        private bool _isActivated = false;
        private WallJump abiRef;

        public void instance (WallJump _abiRef)
        {
            abiRef = _abiRef;
        }
        public void start()
        {
            _isActivated = true;
            abiRef.unitRef.model.animation.SetBool(animatorBoolCall, true);
            Rigidbody2D rigid = abiRef.unitRef.rigid;
            rigid.drag = wallSlideDrag;
        }
        public void stop()
        {
            if (!enable) return;
            _isActivated = false;

            abiRef.unitRef.model.animation.SetBool(animatorBoolCall, false);
            Rigidbody2D rigid = abiRef.unitRef.rigid;
            rigid.drag = 0f;

            abiRef.Reactivate();
        }
        

        public void update()
        {
            if (enable == false)
                return;
            if (wallSliceCondition())
            {
                if (!_isActivated)
                    start();
            }
            else if (_isActivated)
            {
                stop();
            }

        }
        private bool wallSliceCondition()
        {
            float InputX = abiRef.unitRef.movement.Input.x;
            RObjectCollider unitCollider = abiRef.unitRef.rObjectCollider;
            if (unitCollider.contacts.down.isContacted())
                return false;
            return ((InputX < 0f && unitCollider.contacts.left.isContacted())
             || (InputX > 0f && unitCollider.contacts.right.isContacted()));
        }
        
    }

    private Jump baseJumpAbi;
    public override void FinishSetup()
    {
        base.FinishSetup();
        baseJumpAbi = unitRef.GetComponentInChildren<Jump>();
    }

    protected override void Awake()
    {
        base.Awake();
        wallSlice.instance(this);
    }
    protected override bool condition()
    {
        if (unitRef.rObjectCollider.contacts.down.isContacted())
            return false;
        return (unitRef.rObjectCollider.contacts.left.isContacted() || unitRef.rObjectCollider.contacts.right.isContacted());
    }
    protected override void abilityPrepared()
    {
        unitRef.model.animation.SetTrigger(animatorTriggerCall);
        wallSlice.stop();
    }

    protected override void abilityCasted()
    {
        unitRef.rigid.velocity = new Vector2(unitRef.rigid.velocity.x, 0f);
        float x = horizontalScale;
        Vector2 vec = ((unitRef.rObjectCollider.contacts.left.isContacted()) ? new Vector2(x,1) : new Vector2(-x,1));
        //vec *= baseJumpAbi.jumpForce;
        vec *= 1500f;

        wallSlice.stop();
        unitRef.movement.AddKnockBack(vec, 0.2f);
    }

    protected override void Update()
    {
        base.Update();
        wallSlice.update();
    }
    public void Reactivate()
    {
        wallSlice.enable = false;
        StartCoroutine(reactive());
    }
    IEnumerator reactive()
    {
        yield return new WaitForSeconds(0.2f);
        wallSlice.enable = true;
    }
}
