﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PharahHover : Ability {
    [Header("================================")]
    public float hoverAditionalForce = 10f;
    public float maxHoverSpeed = 4f;

    private bool boo = true; //can fly again
    private Rigidbody2D rigid;

    protected override void Awake()
    {
        base.Awake();
        rigid = unitRef.GetComponent<Rigidbody2D>();
    }

    protected override bool condition()
    {
        if (boo == false)
            return false;
        return true;
    }

    protected override void abilityCasted()
    {
        unitRef.model.animation.SetBool("Hover", true);
        Rigidbody2D rigid = unitRef.GetComponent<Rigidbody2D>();
        if (rigid.velocity.y < 0f)
        {
            rigid.velocity = new Vector2(rigid.velocity.x, 0f);
        }

        //Debug.Log("casted");
    }

    protected override void abilityFinished()
    {
        base.abilityFinished();
        unitRef.model.animation.SetBool("Hover", false);
        rigid.gravityScale = 1f;
        boo = false;
        //Debug.Log(boo.ToString());
    }
    
    private void FixedUpdate()
    {
        if (toggle.isActive)
        {
            float y = maxHoverSpeed;
            Rigidbody2D rigid = unitRef.GetComponent<Rigidbody2D>();
            Vector2 velo = rigid.velocity;

            if (y > velo.y)
            {
                //Debug.Log(rigid.velocity);
                rigid.velocity = new Vector2(rigid.velocity.x, y);
            }
            {
                rigid.AddForce(Vector2.up * hoverAditionalForce * rigid.mass);
            }
        }
        //Debug.Log("FIXEDUPDATE");
    }
}
