﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R12XI_Dash : Ability
{
    [Header("================================")]
    public float velocity = 25f;

    private AbilityButton DashKey = new AbilityButton(RButton.Jump, RButtonType.down);
    private AbilityButton functionalButton = new AbilityButton(RButton.Shift, RButtonType.press);
    
    public override void FinishSetup()
    {
        base.FinishSetup();
        DashKey.setup(UsingDash);
        unitRef.button.Add(DashKey);

        unitRef.RegNewAction("DashOn", turnOnDash);
        unitRef.RegNewAction("DashOff", turnOffDash);
        unitRef.RegNewAction("DashJump", dashJumpTrigger);
    }
    private void UsingDash()
    {
        if (!isActive) //nếu chưa active thì dash bth;
        {
            if (RInput.GetAxisY < 0 && functionalButton.isTrigger)
            {
                UseAbi();
            }
        }
        if (isDashing && functionalButton.isTrigger) //đang dash, thì cho Jump
        {
            dashJumpAction();
        }
    }

    float getYdashJump
    {
        get
        {
            float result = 1500f;
            try
            {
                Movement_Ground_Player mgp = unitRef.movement as Movement_Ground_Player;
                result = mgp.jump.jumpForce;
            }
            finally { }
            return result;
        }
    }
    void dashJumpAction()
    {
        unitRef.model.animation.SetTrigger("DashJump");
    }
    void dashJumpTrigger()
    {
        //unitRef.rObjectCollider.AddForce(new Vector2(1500f, 1500f));
        float x = overrideMovementX * 10;
        float y = getYdashJump;
        unitRef.movement.AddKnockBack(new Vector2(x, y), 0.5f);

        turnOffDash();
    }
    protected override void abilityOrdered()
    {
        unitRef.movement.enable = false;

        unitRef.model.animation.SetTrigger("Dash");
    }
    protected override void abilityPrepared()
    {
        //unitRef.movement.enable = false;
    }
    protected override void abilityFinished()
    {
        //unitRef.rObjectCollider.SemiLayer = false;
        overrideMovementX = 0f;

        unitRef.movement.enable = true;
        //if (unitRef.rObjectCollider.contacts.down.isContacted())
        //    unitRef.rObjectCollider.velocity = Vector2.zero;
        
    }
    float overrideMovementX;
    bool isDashing = false;
    void turnOnDash()
    {
        //unitRef.rObjectCollider.SemiLayer = true;

        isDashing = true;
        overrideMovementX = velocity;
        if (!unitRef.model.sprites.isRightFacing)
            overrideMovementX *= -1f;
        
        unitRef.rObjectCollider.velocity = Vector2.zero;
    }
    void turnOffDash()
    {
        isDashing = false;
        overrideMovementX *= 0.35f;
    }

    protected override void Update()
    {
        base.Update();

        if (overrideMovementX != 0)
        {
            unitRef.movement.setInputDirection(overrideMovementX, 0, false, true);
        }
    }

    private bool conditionUpdate()
    {
        if (unitRef.rObjectCollider.contacts.right.isContacted() && overrideMovementX > 0)
            return false;
        if (unitRef.rObjectCollider.contacts.left.isContacted() && overrideMovementX < 0)
            return false;
        return true;
    }

    protected override bool condition()
    {
        if (unitRef.rObjectCollider.contacts.down.isContacted())
            return true;
        return false;
    }
}
