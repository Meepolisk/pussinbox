﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class R12XI_SodaShotgun : Ability {
    [Header("================================")]
    public ModelProfile specialEffect;
    public Damage damage;
    public float offset = 1f;
    public float radius = 2f;
    public string animationReload = "ReplaceSoda";

    protected void OnEnable()
    {
        unitRef.RegNewAction(animationReload, Reload);
    }
    
    protected override void abilityCasted()
    {
        //sdads
        Vector2 pos = unitRef.model.sprites.attachments["weapon1"].center;
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }

        //special Effect
        SpecialEffect.Create(specialEffect,pos);
        if (!unitRef.model.sprites.isRightFacing)
        {
            SpecialEffect._lastCreatedSpecialEffect.model.sprites.Flip();
        }
        SpecialEffect._lastCreatedSpecialEffect.Destroy();
    }
    private void Reload()
    {
        unitRef.model.animation.SetTrigger(animationReload);
    }
}
