﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class R12XI_PepsiNade : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public ModelProfile explosionEffect;
    public Damage damage;
    public float chargeDamageMax = 2f;
    public float chargeSpeedMax = 1.5f;
    public float radius = 3f;
    public float knockbackForce = 200f;


    public AbilityButton mainKey = new AbilityButton(RButton.Special, RButtonType.press);
    public AbilityButton chargeKey = new AbilityButton(RButton.Shift, RButtonType.press);
    public AbilityButton mainKeyDown = new AbilityButton(RButton.Special, RButtonType.down);

    public override void FinishSetup()
    {
        base.FinishSetup();

        mainKeyDown.setup(CastAbi);
        unitRef.button.Add(mainKeyDown);

        unitRef.RegNewAction("NadeChargeLv1", chargeLv1);
        unitRef.RegNewAction("NadeChargeLv2", chargeLv2);
        unitRef.RegNewAction("NadeChargeLv3", chargeLv3);
    }

    private bool _onHold = false;
    private void CastAbi()
    {
        if (chargeKey.isTrigger)
        {
            _onHold = true;
        }
        UseAbi();
    }
    protected override void Update()
    {
        base.Update();
        if (isActive && _onHold)
        {
            if (!mainKey.isTrigger || !chargeKey.isTrigger)
                Release();
        }
    }
    private float dmgMul = 1f;
    private float spdMul = 1f;
    protected override void abilityOrdered()
    {
        if (unitRef.movement is Movement_Ground_Player)
        {
            Movement_Ground_Player mp = unitRef.movement as Movement_Ground_Player;
            mp.canCrouch = false;
            mp.jump.canJump = false;
        }
        unitRef.movement.movementSpeed.addMultis(0.0f, "Nade");
        unitRef.movement.holdDirection = true;

        dmgMul = 1f;
        spdMul = 1f;
    }
    void chargeLv1()
    {
        if (isActive && _onHold)
        {
            dmgMul = 1.25f;
            spdMul = 1.2f;
        }
    }
    void chargeLv2()
    {
        if (isActive && _onHold)
        {
            dmgMul = 1.5f;
            spdMul = 1.4f;
        }
    }
    void chargeLv3()
    {
        if (isActive && _onHold)
        {
            dmgMul = 2f;
            spdMul = 1.6f;
        }
    }

    protected override void abilityPrepared()
    {
        if (!_onHold)
            Release();
    }
    private void Release()
    {
        _onHold = false;
        unitRef.model.animation.SetTrigger("NadeRelease");
    }

    protected override void abilityCasted()
    {
        _onHold = false;
        Shoot();
    }
    protected override void abilityFinished()
    {
        Debug.Log("nade FINISH");
        if (unitRef.movement is Movement_Ground_Player)
        {
            Movement_Ground_Player mp = unitRef.movement as Movement_Ground_Player;
            mp.canCrouch = true;
            mp.jump.canJump = true;
        }
        unitRef.movement.movementSpeed.removeMultis("Nade");
        unitRef.movement.holdDirection = false;
    }

    private void Shoot()
    {
        //create Missile
        Attachment _attachment = unitRef.model.sprites.attachments["weapon2"];
        Vector2 pos = _attachment.center;
        float Zangle = _attachment.zAngle;

        float oldSpeed = missile.speed;
        missile.speed *= spdMul;
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(pos, Zangle, missile);
        missile.speed = oldSpeed;
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        //fx
        SpecialEffect.Create(explosionEffect, pos);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();

        //nổ
        Unit[] group = Unit.GetUnitsInRange(pos, radius);
        if (group == null) return;
        //do something to enemy in group
        RDebug.DrawCircle(pos, radius, Color.red, 1f);

        Damage tmpDmg = damage;
        tmpDmg.amount *= dmgMul;
        foreach (Unit target in group)
        {
            unitRef.damage(target, tmpDmg);
            Vector2 knockbackVec = ((Vector2)target.transform.position - pos).normalized * knockbackForce;
            if (target.transform.position.y > (pos.y - 1f))
                knockbackVec += Vector2.up * knockbackForce;
            if (knockbackForce > 0f)
            {
                target.movement.AddKnockBack(knockbackVec);
            }
        }
    }
}
