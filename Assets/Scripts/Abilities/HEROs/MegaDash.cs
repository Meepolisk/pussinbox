﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MegaDash : Ability
{
    [Header("================================")]
    public Damage damage;

    public float JumpForceY = 1250;
    public float velocity = 25f;
    public ModelProfile loopEffect;
    public float loopRate = 0.2f;
    
    private List<Unit> targets = new List<Unit>();
    private new Collider2D collider;
    float overrideMovementX = 0;
    bool isDashing = false;

    protected override void Awake()
    {
        base.Awake();
        collider = GetComponent<Collider2D>();
        collider.enabled = false;
    }

    protected override void abilityOrdered()
    {
        unitRef.rigid.velocity = Vector2.zero;
        Vector2 _jumpF = new Vector2(0f, JumpForceY);
        unitRef.rigid.AddRelativeForce(_jumpF);
    }
    protected override void abilityPrepared()
    {
        turnOnDash();
        StartCoroutine(createFX());
    }
    protected override void abilityToggleOff()
    { 
        turnOff();
    }
    void turnOnDash()
    {
        unitRef.rObjectCollider.SemiLayer = true;
        isDashing = true;
        targets.Clear();
        collider.enabled = true;
        unitRef.movement.enable = false;
        overrideMovementX = velocity;
        if (!unitRef.model.sprites.isRightFacing)
            overrideMovementX *= -1f;
        unitRef.model.flipWhenTurn = false;
        unitRef.rObjectCollider.velocity = Vector2.zero;
        unitRef.rigid.gravityScale = 0f;
    }
    void turnOff()
    {
        unitRef.rObjectCollider.SemiLayer = false;
        isDashing = false;
        collider.enabled = false;
        //Debug.Log("[" + Time.time + "]" + gameObject.name + " DASH STOP");
        overrideMovementX = 0f;
        unitRef.movement.enable = true;
        unitRef.model.flipWhenTurn = true;
        unitRef.rObjectCollider.velocity = Vector2.zero;
        unitRef.rigid.gravityScale = 1f;
    }
    IEnumerator createFX(bool first = true)
    {
        Vector2 pos = unitRef.model.sprites.attachments["boxHead"].center;
        if (unitRef.model.sprites.isRightFacing)
            pos += (Vector2.right * 1.5f);
        else
            pos += (Vector2.left * 1.5f);
        
        SpecialEffect.Create(loopEffect,pos);
        if (!unitRef.model.sprites.isRightFacing)
            SpecialEffect._lastCreatedSpecialEffect.model.Flip();
        if (first)
            SpecialEffect._lastCreatedSpecialEffect.size *= 1.5f;
        SpecialEffect.DestroyLastCreatedSpecialEffect();

        yield return new WaitForSeconds(loopRate);
        if (isDashing)
        {
            StartCoroutine(createFX(false));
        }
    }
    protected override void Update()
    {
        base.Update();
        if (!isActive) return;
        
        if (conditionUpdate()) //check điều kiện dash
        {
            unitRef.movement.setInputDirection(overrideMovementX, 0f, false, true);
        }
        else
        {
            toggle.forceStop(true);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        var targetCol = collision.GetComponent<RObjectCollider>();
        if (targetCol && targetCol.model.rObject is Unit)
        {
            Unit _target = targetCol.model.rObject as Unit;
            if (!targets.Contains(_target))
            {
                targets.Add(_target);
                unitRef.damage(_target, damage);
            }
        }
    }
    private bool conditionUpdate()
    {
        if (unitRef.rObjectCollider.contacts.right.isContacted() && overrideMovementX > 0)
            return false;
        if (unitRef.rObjectCollider.contacts.left.isContacted() && overrideMovementX < 0)
            return false;
        return true;
    }
}
