﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DewRocket : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public Damage damage;
    

    protected override void abilityPrepared()
    {
        unitRef.model.animation.SetTrigger("Special");
    }
    protected override void abilityCasted()
    {
        //caster.model.resetTrigger("Special");
        //create Missile
        float Zangle = getZangle();
        missile.setData(unitRef, exploAction);
        Vector2 pos = unitRef.gameObject.findChild("weapon").transform.position;
        MissileProfile.Create(pos, Zangle, missile);
    }

    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
            unitRef.damage(target, damage);
    }
    
    float getZangle()
    {
        float _float = unitRef.gameObject.findChild("weapon").transform.eulerAngles.z;
        if (unitRef.model.sprites.isRightFacing == false)
            _float += 180f;
        return _float;
    }
}
