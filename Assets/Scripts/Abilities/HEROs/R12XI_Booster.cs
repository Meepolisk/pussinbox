﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R12XI_Booster : Ability
{
    [Header("================================")]
    public float speed = 25f;
    public float inputDelay = 0.5f;
    public float duration = 2f;
    public float initForce = 750f;

    private Movement_Fly movementFly = null;
    private Movement_Ground baseMovement = null;
    //bool inEffect = false;
    float oldMSspeed;
    float oldInputDelay;

    //RTimer _timer;
        
    protected override void abilityCasted()
    {
        base.abilityCasted();

        //start Timer
        //_timer.setData("booster duration", duration, false, EndAbi);
        //_timer.start();
        enable();
    }

    void EndAbi()
    {
        //_timer.stop();
        disable();
    }

    private void enable()
    {
        unitRef.model.animation.SetBool("Fly", true);
        generateNewMovementMethod();

        //inEffect = true;
    }

    private void disable()
    {
        unitRef.model.animation.SetBool("Fly", false);
        restoreMovementMethod();

        //inEffect = false;
    }

    //sẽ phải chỉnh  cái này lại, dùng clone component
    private void generateNewMovementMethod ()
    {
        //Vector2 inputVector = caster.movement.Input;
        //Debug.Log(inputVector);

        baseMovement = unitRef.gameObject.GetComponent<Movement_Ground>();
        oldMSspeed = baseMovement.movementSpeed.baseValue;
        oldInputDelay = baseMovement.inputDelay.amount;
        movementFly = unitRef.gameObject.AddComponent<Movement_Fly>();
        movementFly.copyFrom(baseMovement);
        movementFly.movementSpeed.baseValue = speed;
        movementFly.inputDelay.amount = inputDelay;
        movementFly.forceMove = true;
        movementFly.setMaxSpeed();
        unitRef.movement = movementFly;

        baseMovement.enabled = false;
        

        //Add SuperForce
        //Debug.Log(frceVec);
        //caster.movement.AddKnockBack(frceVec, 0.25f);
    }
    private void restoreMovementMethod()
    {
        baseMovement.copyFrom(movementFly);
        baseMovement.movementSpeed.baseValue = oldMSspeed;
        baseMovement.inputDelay.amount = oldInputDelay;
        baseMovement.forceMove = false;
        unitRef.movement = baseMovement;
        Destroy(movementFly);

        baseMovement.enabled = true;
    }
}
