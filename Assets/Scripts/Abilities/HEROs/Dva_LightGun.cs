﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Dva_LightGun : Ability {
    [Header("================================")]
    public MissileProfile missile;
    public Damage damage;
    public float spreadAngle = 1f;


    protected override void abilityCasted()
    {
        float Zangle = unitRef.model.sprites.attachments["weapon"].zAngle + Random.Range(-spreadAngle, spreadAngle);
        Vector2 pos = unitRef.model.sprites.attachments["weapon"].center;

        //create Missile
        missile.setData(unitRef, exploAction);
        MissileProfile.Create(pos, Zangle, missile);
        
    }
    
    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        if (colGameObject == null) return;
        Unit target = colGameObject.GetComponent<Unit>();
        if (target != null)
            unitRef.damage(target, damage);
    }
}
