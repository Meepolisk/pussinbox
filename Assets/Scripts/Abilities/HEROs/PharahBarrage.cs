﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PharahBarrage : Ability
{
    [Header("================================")]
    public MissileProfile missile;
    public Damage damage;
    public float aoe = 1f;
    [Header("ultimate")]
    public float ultiInterval = 0.2f;

    private RTimer ultiIntervalTimer;

    protected override void Awake()
    {
        base.Awake();
        ultiIntervalTimer = gameObject.AddComponent<RTimer>();
    }

    protected override void abilityCasted()
    {
        unitRef.model.animation.SetBool("Ulti", true);
        unitRef.GetComponent<Rigidbody2D>().gravityScale = 0f;
        unitRef.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //AudioSource.PlayClipAtPoint(ulti.AudioUltimate, transform.position);

        ultiIntervalTimer.setData("ultimate Interval", ultiInterval, true, UltiShoot);
        ultiIntervalTimer.start();

        //Debug.Log("casted");
    }
    protected override void abilityFinished()
    {
        unitRef.model.animation.SetBool("Ulti", false);
        unitRef.GetComponent<Rigidbody2D>().gravityScale = 1f;
        ultiIntervalTimer.stop();

        //Debug.Log("stopped");
    }

    public void UltiShoot()
    {
        float Zangle = 180f;
        if (unitRef.model.sprites.isRightFacing)
            Zangle = 0f;
        for (int i = 0; i<2;i++)
        {
            Zangle += Random.Range(-10f, 10f);
            float randomRange = 0.5f;
            Vector2 randomVec = (Vector2)unitRef.transform.position + new Vector2(Random.Range(-randomRange, randomRange), Random.Range(-randomRange, randomRange));

            missile.setData(unitRef, exploAction);
            MissileProfile.Create(randomVec,Zangle, missile);
            MissileProfile.lastCreatedMissile.gameObject.GetComponent<Animator>().SetFloat("speed", Random.Range(0.6f, 1.5f));
        }
    }

    public void exploAction(Vector2 pos, GameObject colGameObject)
    {
        Unit[] group = Unit.GetUnitsInRange(pos, aoe);
        if (group == null) return;
        //do something to enemy in group
        foreach (Unit target in group)
        {
            unitRef.damage(target, damage);
        }
    }
}
