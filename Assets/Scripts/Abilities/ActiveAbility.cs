﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class ActiveAbility : Ability
//{

//    #region khai bao biến
//    [System.Serializable]
//    public class Toggle
//    {
//        public bool isToggle = false;
//        [Range(0f, 10f)]
//        public float time = 0f;
//        [Range(0f, 10f)]
//        public float recover = 1f;
//        [ReadOnly]
//        public float _timeTick = 0f;
//        public AudioClip[] toggleOnSounds;
//        public AudioClip[] toggleOffSounds;
//        internal bool isActive = false;

//        internal ActiveAbility thisAbility;
//        public void prepare(ActiveAbility _abi)
//        {
//            thisAbility = _abi;
//        }

//        public float remainingScale
//        {
//            get
//            {
//                if (_timeTick == 0f) return 0f;
//                return (_timeTick / time);
//            }
//        }

//        internal void start()
//        {
//            if (!isToggle) return;

//            isActive = true;
//            thisAbility.caster.playSoundRandom(toggleOnSounds);
//        }

//        internal void stop()
//        {
//            if (!isToggle) return;

//            isActive = false;

//            thisAbility.baseAbilityFinished();
//            thisAbility.caster.playSoundRandom(toggleOffSounds);
//        }

//        internal void update(float _deltaTime)
//        {
//            if (time <= 0f || !isToggle) return;

//            if (isActive)
//            {
//                _timeTick -= _deltaTime;
//                if (_timeTick <= 0f)
//                {
//                    _timeTick = 0f;
//                    stop();
//                }
//            }
//            else
//            {
//                if (_timeTick >= time)
//                    _timeTick = time;
//                else
//                    _timeTick += (_deltaTime * recover);
//            }
//        }
//    }
//    [System.Serializable]
//    public class Channeling
//    {
//        public bool isChanneling
//        {
//            get
//            {
//                if (time > 0f)
//                    return true;
//                return false;
//            }
//        }
//        [Range(0f, 5f)]
//        public float time = 0f;
//        public string animatorAnimation = "";
//        [ReadOnly]
//        public float _timeTick = 0f;
//        private bool _isActive = false;
//        internal bool isActive
//        {
//            get
//            {
//                return _isActive;
//            }
//            set
//            {
//                _isActive = value;
//                if (!string.IsNullOrEmpty(animatorAnimation))
//                    thisAbility.caster.model.animationBool(animatorAnimation, value);
//            }
//        }
//        public AudioClip[] channelingSoundOn;
//        public AudioClip[] channelingSoundOff;

//        internal ActiveAbility thisAbility;
//        public void prepare(ActiveAbility _abi)
//        {
//            thisAbility = _abi;
//        }

//        internal void start()
//        {
//            if (!isChanneling) return;

//            thisAbility.abilityStartChannel();
//            _timeTick = time;
//            isActive = true;

//            thisAbility.caster.playSoundRandom(channelingSoundOn);
//            thisAbility.caster.canControl = false;
//        }
//        private void stop()
//        {
//            if (!isChanneling) return;

//            _timeTick = 0f;
//            isActive = false;

//            thisAbility.caster.playSoundRandom(channelingSoundOff);
//            thisAbility.caster.canControl = true;

//            thisAbility.baseAbilityPrepared();
//        }

//        internal void update(float deltaTime)
//        {
//            if (!isActive || !isChanneling)
//                return;
//            _timeTick -= deltaTime;
//            if (_timeTick <= 0f)
//            {
//                stop();
//            }
//        }
//    }
    
//    public Toggle toggle;
//    public Channeling channeling;
   
//    [HideInInspector]
//    public HotkeyState _hotkeyState;
//    public HotkeyState hotkeyState
//    {
//        get
//        {
//            if (!caster.canControl)
//                return HotkeyState.disable;
//            return _hotkeyState;
//        }
//    }

//    public AbilityType type
//    {
//        get
//        {
//            if (toggle.isToggle)
//                return AbilityType.toggle;
//            return AbilityType.normal;
//        }
//    }

//    #endregion
    
//    // Use this for initialization
//    protected override void Awake()
//    {
//        base.Awake();
        
//        if (handler != null)
//        {
//            handler.abilities.Add(this);
//        }
//        caster = GetComponentInParent<Unit>();
//        toggle.prepare(this);
//        channeling.prepare(this);
//    }
//    private bool baseCondition()
//    {
//        if (cooldown.isActive)
//            return false;
//        if (caster.isDead)
//            return false;
//        if (!caster.canControl)
//            return false;
//        if (caster.energy.current < energyRequi)
//            return false;
//        if (toggle.isToggle && toggle.isActive)
//            return false;
//        return condition();
//    }
//    private void baseAbilityOrdered()
//    {
//        rEvent.Start.Ability_BeingCast(caster, this);
//        cooldown.checkAndStart(CooldownType.afterOrder);
//        abilityOrdered();
//        if (channeling.isChanneling) //nếu cần channeling
//        {
//            channeling.start();
//        }
//        else
//        {
//            baseAbilityPrepared();
//        }
//    }
//    private void baseAbilityPrepared()
//    {
//        cooldown.checkAndStart(CooldownType.afterPrepare);
//        abilityPrepared();
//        if (!string.IsNullOrEmpty(waitForAnimatorMess) && !freeAccessFromAnimator)
//        {
//            waitForAnimation = true;
//        }
//        else
//        {
//            baseAbilityCasted();
//        }
//    }
//    public virtual void AnimationMess (string _sms)
//    {
//        if (waitForAnimation || freeAccessFromAnimator)
//        {
//            if (_sms == waitForAnimatorMess)
//            {
//                baseAbilityCasted();
//            }
//            if (!freeAccessFromAnimator)
//            {
//                waitForAnimation = false;
//            }
//        }
//    }
//    //class Channeling gọi khi finish, và animation Mess gọi khi có mess: 
//    private void baseAbilityCasted()
//    {
//        caster.energy.current -= energyRequi;

//        cooldown.checkAndStart(CooldownType.afterStart);
//        if (profile!= null) profile.UI_Casted();

//        if (toggle.isToggle) //nếu là toggle
//        {
//            toggle.start();
//        }
//        else
//        {
//            baseAbilityFinished();
//        }
//        abilityCasted();
//    }

//    //class Toggle gọi khi toggleOff:
//    private void baseAbilityFinished()
//    {
//        rEvent.Start.Ability_FinishCasting(caster, this);

//        abilityFinished();

//        cooldown.checkAndStart(CooldownType.afterStart);
//    }

//    public void UseAbi()
//    {
//        if (baseCondition())
//        {
//            baseAbilityOrdered();
//        }
//    }
//    protected virtual void Update()
//    {
//        //chạy các biến update của các class con
//        float _deltaTime = Time.deltaTime;
//        channeling.update(_deltaTime);
//        toggle.update(_deltaTime);
//        cooldown.update(_deltaTime);
//    }
//    #region all Virtual

//    protected virtual void abilityOrdered() { }
//    protected virtual void abilityStartChannel() { }
//    protected virtual void abilityPrepared() { }
//    protected virtual void abilityCasted() { }
//    protected virtual void abilityFinished() { }
//    protected virtual void finishedCooldown() { }
//    protected virtual bool condition() { return true; }
//    #endregion
//    #region all public

//    public void forceUse()
//    {
//        UseAbi();
//    }
//    public bool available
//    {
//        get
//        {
//            return baseCondition();
//        }
//    }

//    public static void forceUnitUseAbi(Unit _unit, string _abilityName)
//    {
//        GameObject go = _unit.gameObject.findChild(_abilityName);
//        go.GetComponent<ActiveAbility>().forceUse();
//    }
//    #endregion
//}
