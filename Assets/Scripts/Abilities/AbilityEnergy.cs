﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ability))]
public class AbilityEnergy : AbiAddOn
{
    [Range(0f,100f)]
    public float amount = 0f;
    public Ability.CheckType stage = Ability.CheckType.afterStart;

    [Range(0f, 100f)]
    [Tooltip("Trừ theo thời gian")]
    public float onToggle = 0f;
    
    protected override void OnEnable()
    {
        base.OnEnable();
        abiRef.energy = this;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        abiRef.energy = null;
    }
    public void start()
    {
        abiRef.unitRef.energy.current -= amount;
    }
    public override bool abiRefCastCondition()
    {
        float requireEnerger = amount;
        if (abiRef.toggle != null && abiRef.toggle.minimumToggleTime > 0 && onToggle > 0)
            requireEnerger += onToggle * abiRef.toggle.minimumToggleTime;

        if (abiRef.unitRef.energy.current < requireEnerger)
            return false;
        return true;
    }
    private void Update()
    {
        if (abiRef.toggle != null && abiRef.toggle.isActive && onToggle > 0 )
        {
            abiRef.unitRef.energy.current -= Time.deltaTime * onToggle;
        }
    }
    public override bool abiRefUpdateCondition()
    {
        if (abiRef.toggle != null && onToggle > 0 && abiRef.unitRef.energy.current <= 0f)
        {
            return false;
        }
        return true;
    }
}

