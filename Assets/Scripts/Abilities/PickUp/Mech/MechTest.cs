﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechTest : MonoBehaviour {

    public AudioClip pickupClip;
    public Pilot prefabHeroReplace;

    private Pilot oldHero;

    void OnTriggerEnter2D(Collider2D other)
    {
        //if (other.transform.root.gameObject.layer == LayerMask.NameToLayer("Player") && other.gameObject.isHero())
        if (true)
        {
            Pilot unit = other.GetComponentInParent<Pilot>();
            if (condition(unit))
            {
                //Status.AddStatus(unit,RPlatformer.LoadStatus("Slow"));
                replaceHero(unit);
                Destroy(gameObject);
                if (pickupClip != null)
                    AudioSource.PlayClipAtPoint(pickupClip, transform.position);
            }
        }
    }
    bool condition(Pilot hero)
    {
        if (hero != null && hero.player != 0)
            return true;
        return false;
    }
    void replaceHero(Pilot _heroPickUp)
    {
        oldHero = _heroPickUp;
        oldHero.gameObject.SetActive(false);
        Vector2 pos = oldHero.gameObject.transform.position;
        prefabHeroReplace.copyPlayerStat(oldHero);
        //GameObject newGO = Instantiate(prefabHeroReplace.gameObject, pos, Quaternion.Euler(Vector2.zero));
        Instantiate(prefabHeroReplace.gameObject, pos, Quaternion.Euler(Vector2.zero));
        //prefabHeroReplace.gameObject
    }
    
}
