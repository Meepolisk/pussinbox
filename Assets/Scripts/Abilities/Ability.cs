﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum AbilityState { hide, free, incooldown, istoggling, channeling, notEnoughtEnergy }
public enum HotkeyState { press, unpress, disable }
public enum AbilityType { normal, toggle, channeling }
public enum HotkeyType { keyUp, keyDown, getKey};
public abstract class Ability : UnitAddOn
#if UNITY_EDITOR 
    ,hideTransform, lockTransform 
#endif 
{

    #region khai bao biến
    public enum CheckType { none, afterPrepare, afterStart, afterFinish }

    public AbilityAnimationHelper animMess;
    public AbilityGroup group;
    [HideInInspector]
    public AbilityProfile profile;
    [HideInInspector]
    public AbilityToggle toggle;
    [HideInInspector]
    public AbilityCooldown cooldown;
    [HideInInspector]
    public AbilityEnergy energy;
    [ReadOnly]
    [SerializeField]
    private short _stage = 0;
    protected short stage
    {
        private set
        {
            _stage = value;
        }
        get
        {
            return _stage;
        }
    }

    [HideInInspector]
    public List<AbiAddOn> AAOs = new List<AbiAddOn>();
    [HideInInspector]
    public HotkeyState _hotkeyState;
    public HotkeyState hotkeyState
    {
        get
        {
            if (!unitRef.canControl)
                return HotkeyState.disable;
            return _hotkeyState;
        }
    }

    #endregion
    public bool isActive
    {
        get
        {
            if (stage > 0)
                return true;
            return false;
        }
    }

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        unitRef.abilities.Add(this);
        animMess.prepare(this);
        group.prepare(this);
    }
    private bool baseCondition()
    {
        if (isActive)
            return false;
        if (unitRef.CheckCurrentGroupAbi(this))
            return false;
        if (unitRef.isDead)
            return false;
        if (!unitRef.canControl)
            return false;
        foreach (AbiAddOn addOn in AAOs)
        {
            if (!addOn.abiRefCastCondition())
                return false;
        }
        return condition();
    }
    //private bool groupActive
    //{
    //    set
    //    {
    //        if (string.IsNullOrEmpty(animMess.group)) return; 
    //        handler.abilityGroup[animMess.group] = value;
    //    }
    //    get
    //    {
    //        string key = group.name;
    //        if (isActive)
    //            return true;
    //        if (string.IsNullOrEmpty(key))
    //            return false;
    //        return handler.abilityGroup[key];
    //    }
    //}
    //public bool groupIsActive
    //{
    //    get
    //    {
    //        return groupActive;
    //    }
    //}
    public void baseAbilityOrdered()
    {
        unitRef.ReplaceCurrentGroupAbi(this);

        //Debug.Log("[" + Time.time + "]" + gameObject.name + " ORDERED START");
        rEvent.Start.Ability_BeingCast(unitRef, this);
        abilityOrdered();
        stage = 1;
        //groupActive = true;

        //animation
        if (!string.IsNullOrEmpty(animMess.atOrder_Trigger))
            unitRef.model.animation.SetTrigger(animMess.atOrder_Trigger);

        //nếu có reg animation Mess thì đợi
        if (string.IsNullOrEmpty(animMess.onPrepare))
            baseAbilityPrepared();
        
    }
    public void baseAbilityPrepared()
    {
        if (stage != 1) return;
        
        checkType(CheckType.afterPrepare);
        abilityPrepared();

        //animation
        //if (!string.IsNullOrEmpty(animMess.atPrepare_Trigger))
        //    caster.model.animation.SetTrigger(animMess.atPrepare_Trigger);

        if (toggle != null) //nếu là toggle
        {
            stage = 2;
            toggle.start();
        }
        else
        {
            stage = 3;
            if (string.IsNullOrEmpty(animMess.onCast))
                baseAbilityCasted();
        }
        
    } 
    public void baseAbilityCasted()
    {
 
        if (!(stage == 2 || stage == 3)) return;
        
        checkType(CheckType.afterStart);
        if (profile!= null) profile.UI_Casted();

        //Debug.Log("[" + Time.time + "]" + gameObject.name + " START EFFECT");
        abilityCasted();

        if (string.IsNullOrEmpty(animMess.onFinish) && (toggle == null))
        {
            baseAbilityFinished();
        }
    }
    public void baseAbilityToggleOff(bool forceFinish = false)
    {
        stage = 4;
        abilityToggleOff();
        if (string.IsNullOrEmpty(animMess.onFinish) || forceFinish)
            baseAbilityFinished();
    }
    public void baseAbilityFinished()
    {
        //không làm gì nếu abi không active
        if (stage == 0) return;

        //nếu toggle off từ button-duration chưa được trigger, nhưng nhận đc finish, thì forceStop toggle
        if (toggle != null && toggle.isActive)
        {
            //Debug.Log("[" + Time.time + "]" + gameObject.name + " FORCE FINISH", gameObject);
            toggle.forceStop(true);
            return;
        }
        rEvent.Start.Ability_FinishCasting(unitRef, this);
        stage = 0;
        //groupActive = false;

        abilityFinished();

        checkType(CheckType.afterFinish);
        unitRef.RemoveCurrentGroupAbi(this);

        //Debug.Log("[" + Time.time + "]" + gameObject.name + " FINISH END");
    }
    private void checkType(Ability.CheckType _cdt)
    {
        if (cooldown != null)
            if (_cdt == cooldown.stage)
                cooldown.CheckToStart();
        if (energy != null)
            if (_cdt == energy.stage)
                energy.start();
    }

    public void UseAbi()
    {
        if (baseCondition())
        {
            stage = 0;
            baseAbilityOrdered();
        }
    }
    protected virtual void Update()
    {
        if (!isActive) return;

        //check điều kiện update
        foreach (var AAO in AAOs)
        {
            if (!AAO.abiRefUpdateCondition())
            {
                baseAbilityFinished();
                return;
            }
        }
    }
    #region all Virtual

    protected virtual void abilityOrdered() { }
    protected virtual void abilityPrepared() { }
    protected virtual void abilityCasted() { }
    protected virtual void abilityToggleOff() { }
    protected virtual void abilityFinished() { }
    public virtual void finishedCooldown()
    {
        if (profile != null)
            profile.UI_cooldownFinish();
    }
    protected virtual bool condition() { return true; }
    #endregion
    #region all public
    
    public bool isAvailable
    {
        get
        {
            return baseCondition();
        }
    }

    public static void ForceUnitUseAbi(Unit _unit, string _abilityName)
    {
        GameObject go = _unit.gameObject.findChild(_abilityName);
        go.GetComponent<Ability>().UseAbi();
    }
    public void ResetCooldown()
    {
        if (cooldown != null)
        {
            cooldown.reset();
        }

    }

    #endregion
}
