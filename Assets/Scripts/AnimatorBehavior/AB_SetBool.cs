﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_SetBool : StateMachineBehaviour {
    public enum BoolValue { True, False, Null};
    public string BoolName;
    public BoolValue _valueOnEnter = BoolValue.True;
    public BoolValue _valueOnExit = BoolValue.False;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //debug 1 time
        if (string.IsNullOrEmpty(BoolName))
        {
            Destroy(this);
            return;
        }

        if (_valueOnEnter != BoolValue.Null)
            animator.SetBool(BoolName, _valueOnEnter==BoolValue.True? true : false);
    }
    
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_valueOnExit != BoolValue.Null)
            animator.SetBool(BoolName, _valueOnExit == BoolValue.True ? true : false);
    }
    
}
