﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AB_SetInt : StateMachineBehaviour {
    [System.Serializable]
    public class ABIntValue
    {
        public bool enable;
        public int valueMin = 0;
        public int valueMax = 1;

        public ABIntValue(bool _enable)
        {
            enable = _enable;
        }
    }
    #region EditorFor ABIntValue
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ABIntValue))]
    public class ABIntValueInspector : PropertyDrawer
    {

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // Calculate rects
            var rect0 = new Rect(position.x, position.y, 20, position.height);
            var rect1 = new Rect(position.x + 20, position.y, (position.width / 2) - 15, position.height);
            var rect2 = new Rect(position.x + 20 + (position.width / 2) - 15, position.y, 10, position.height);
            var rect3 = new Rect(position.x + 10 + (position.width / 2) + 5, position.y, (position.width / 2) - 15, position.height);

            // Draw fields - passs GUIContent.none to each so they are drawn without labels
            //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("_health.current"), GUIContent.none);
            EditorGUI.PropertyField(rect0, property.FindPropertyRelative("enable"), GUIContent.none);
            EditorGUI.PropertyField(rect1, property.FindPropertyRelative("valueMin"), GUIContent.none);
            GUI.Label(rect2, "/");
            EditorGUI.PropertyField(rect3, property.FindPropertyRelative("valueMax"), GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
#endif
    #endregion

    [Tooltip("Set Min < Max will ensure the result alway Min")]
    public string IntName;
    public ABIntValue valueOnEnter = new ABIntValue(true);
    public ABIntValue valueOnExit = new ABIntValue(false);

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ChangeValue(valueOnEnter, animator);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ChangeValue(valueOnExit, animator);
    }

    private void ChangeValue(ABIntValue _abIntValue, Animator animator)
    {
        if (!_abIntValue.enable) return;
        int valueChanged = 0;
        if (_abIntValue.valueMin >= _abIntValue.valueMax)
        {
            valueChanged = _abIntValue.valueMin;
        }
        else
        {
            valueChanged = Random.Range(_abIntValue.valueMin, _abIntValue.valueMax + 1);
        }
        animator.SetInteger(IntName, valueChanged);
    }

}
