﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFrapWithForce : StateMachineBehaviour {
    
    public FrapProfile frapProfile;
    public string ObjectPosition;
    public float zAngle = 0f;
    public float force = 0f;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!string.IsNullOrEmpty(ObjectPosition))
        {
            Transform trans = animator.gameObject.findChild(ObjectPosition, true).transform;
            Unit unit = trans.GetComponentInParent<Unit>();
            //frapProfile.Create(trans.position, zAngle);

            float f = zAngle;
            if (!unit.model.sprites.isRightFacing)
            {
                //Frap._lastCreatedFrap.model.sprites.Flip();
                f = 180f - zAngle;
            }
            frapProfile.Create(trans.position, f);
            //applyForce
            Frap._lastCreatedFrap.rigid.AddForce(RPlatformer.Angle2vector2(f, force));
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
