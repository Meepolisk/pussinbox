﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_AddSound : StateMachineBehaviour
{

    public AudioClip[] sound;
    public string customEventMess;
    
    Animator _animator;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _animator = animator;
        if (!string.IsNullOrEmpty(customEventMess))
        {
            animator.GetComponentInParent<RObject>().RegNewAction(customEventMess, CreateSound);
        }
        else
        {
            CreateSound();
        }
    }

    void CreateSound()
    {
        Vector2 pos = _animator.transform.position;
        GameMaster.PlayRandomSound(sound);
    }
}
