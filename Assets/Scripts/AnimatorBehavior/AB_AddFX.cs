﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_AddFX : StateMachineBehaviour {
    //public SpecialEffect prefabsSpecialEffect;
    public ModelProfile specialEffect;
    public string customEventMess;

    public bool applyRootRotation = false;
    
    Animator _animator;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _animator = animator;
        if (!string.IsNullOrEmpty(customEventMess))
        {
            animator.GetComponentInParent<RObject>().RegNewAction(customEventMess, CreateFX);
        }
        else
        {
            CreateFX();
        }
    }

    void CreateFX()
    {
        Vector2 pos = _animator.transform.position;
        SpecialEffect.Create(specialEffect, pos);
        SpecialEffect.DestroyLastCreatedSpecialEffect();
        if (applyRootRotation)
        {
            RObject _object = _animator.GetComponentInParent<RObject>();
            if (_object)
            {
                SpecialEffect._lastCreatedSpecialEffect.transform.rotation = _object.transform.rotation;
            }
        }
        
    }
}
