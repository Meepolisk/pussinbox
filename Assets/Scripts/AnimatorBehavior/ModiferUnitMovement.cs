﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModiferUnitMovement : StateMachineBehaviour {

    public bool enableMovement = true;
    public float SpeedModifer = 0.5f;
    public float GravityScale = 1f;
    public bool lockDirection = true;

    const string speedCode = "GroundDroneTurretSpeed";

    float oldGravityScale = 0f;
    Unit unit;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        unit = animator.GetComponentInParent<Unit>();

        if (unit.movement != null)
        {
            unit.movement.enable = enableMovement;
            if (SpeedModifer != 1f)
                unit.movement.movementSpeed.addMultis(SpeedModifer, speedCode);
            unit.movement.holdDirection = lockDirection;

            //phần rigid
            oldGravityScale = unit.rigid.gravityScale;
            unit.rigid.gravityScale = GravityScale;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (unit.movement != null)
        {
            unit.movement.enable = !enableMovement;
            if (SpeedModifer != 1f)
                unit.movement.movementSpeed.removeMultis(speedCode);
            unit.movement.holdDirection = false;

            //phần rigid
            unit.rigid.gravityScale = oldGravityScale;
        }
    }
}
