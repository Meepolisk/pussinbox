﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploFrap : StateMachineBehaviour {
    public List<FrapManager> aditionalFraps = new List<FrapManager>();
    public Vector2 offset = new Vector2(1f, 1f);
    public string ReplaceSpriteName = "explo";
    [Header("Explosion Effect")]
    public ModelProfile specialEffect;
    private ModelHandler modelHandler;
    [Header("ForceApply")]
    public Vector2 centerForceOffset;
    public REditorFloat forcePower;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        modelHandler = animator.GetComponent<ModelHandler>();
        ExploOnDead(animator);
    }

    public void ExploOnDead(Animator anim)
    {
        Vector2 unitPos = modelHandler.rObject.rObjectCollider.center;
        //tạo effect
        SpecialEffect.Create(specialEffect, unitPos);
        SpecialEffect._lastCreatedSpecialEffect.Destroy();

        List<GameObject> listFrapRigid = new List<GameObject>();
        //tạo Fraps:
        GameObject frapHandler = modelHandler.sprites.frapHandler;

        #region chuyển các part có Component SpriteReplacer
        Transform[] allparts = modelHandler.sprites.GetComponentsInChildren<Transform>();
        {
            foreach (var part in allparts)
            {
                SpriteReplacer replacer = part.GetComponent<SpriteReplacer>();
                if (replacer != null)
                {
                    Frap newFrap = Frap.CreateFromSprite(replacer.spritesAlternate[ReplaceSpriteName], Random.Range(4f, 6f), replacer.gameObject);
                    listFrapRigid.Add(newFrap.gameObject);
                }
                part.gameObject.SetActive(false);
            }
        }
        #endregion

        #region tạo sparepart mới
        if (aditionalFraps.Count > 0) //chỉ tạo nếu có sparePart mới
        {
            foreach (var frP in aditionalFraps)
            {
                for (int i = 0; i < frP.count.value; i++)
                {
                    Vector2 pos = unitPos + new Vector2(Random.Range(-offset.x, offset.x), Random.Range(-offset.y, offset.y));
                    Frap newFrap = Frap.Create(frP.frap, pos, Random.Range(0f, 360f));
                    listFrapRigid.Add(newFrap.gameObject);
                }
            }
        }
        #endregion
        #region Tạo lực
        if (listFrapRigid.Count > 0)
        {
            foreach (var frap in listFrapRigid)
            {
                //frap.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 1500f);

                Vector2 force = (Vector2)frap.transform.position - (centerForceOffset+ unitPos);
                frap.GetComponent<Rigidbody2D>().AddRelativeForce(force.normalized * Random.Range(1000f, 2000f));
            }
        }
        #endregion
        //anim.enabled = false;
    }
}

[System.Serializable]
public class FrapManager
{
    public FrapProfile frap;
    public REditorInt count;
}