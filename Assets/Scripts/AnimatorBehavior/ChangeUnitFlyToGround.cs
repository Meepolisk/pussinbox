﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeUnitFlyToGround : StateMachineBehaviour {
    public bool lockZ = false;
    public REditorFloat aditionalForce;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Unit _unit = animator.GetComponentInParent<Unit>();
        if (_unit.movement.GetType() == typeof(Movement_Fly))
        {
            _unit.movement.enabled = false;
            _unit.gameObject.AddComponent<Movement_Ground>();
            if (lockZ)
            {
                _unit.rigid.freezeRotation = false;
            }
            float forceValue = aditionalForce.value;
            if (forceValue > 0f)
            {
                _unit.rigid.AddRelativeForce(new Vector2(Random.Range(-0.2f, 0.2f), 1f) * forceValue);
                _unit.rigid.AddTorque(30f);
            }
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
