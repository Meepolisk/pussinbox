﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_FrameByFloat : StateMachineBehaviour {
    
    public string ParameterName = "";
    //bool active = false;

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float value = animator.GetFloat(ParameterName);
        animator.Play(stateInfo.fullPathHash, layerIndex, Mathf.Clamp(value,0f,0.999f));
    }
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    active = true;
    //}
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    active = false;
    //}
}
