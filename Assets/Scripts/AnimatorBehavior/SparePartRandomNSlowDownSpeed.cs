﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparePartRandomNSlowDownSpeed : StateMachineBehaviour {
    
    public string ParametersName = "Speed";
    private float speedDec;
    public float initMax = 1.5f, initMin = 0.8f, updateMax = 0.5f, updateMin = 0.2f;

    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (anim != null) return; //để init 1 lần

        animator.SetFloat(ParametersName, Random.Range(initMin, initMax));
        speedDec = Random.Range(updateMax, updateMin);
    }

    //OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float f = animator.GetFloat(ParametersName);

        if (f <= 0f) return;

        f -= speedDec * Time.deltaTime;
        animator.SetFloat(ParametersName, f);
    }
    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    //OnStateMachineEnter is called when entering a statemachine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    //{
    //}

    // OnStateMachineExit is called when exiting a statemachine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash) {
    //
    //}
}
