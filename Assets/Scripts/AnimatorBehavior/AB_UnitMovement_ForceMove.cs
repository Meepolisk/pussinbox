﻿using UnityEngine;

public class AB_UnitMovement_ForceMove : StateMachineBehaviour
{
    public Vector2 inputApply = Vector2.right;
    public float overrideForce = 500f;

    private Unit _unit;
    private float newX;
    //private bool willOverrideForce = false;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _unit = animator.GetComponentInParent<Unit>();
        newX = (_unit.model.sprites.isRightFacing) ? inputApply.x : -inputApply.x;
        if (overrideForce > 0f)
        {
            newX *= overrideForce;
            //willOverrideForce = true;
        }
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //_unit.movement.setInputDirection(newX, inputApply.y,false,willOverrideForce);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
