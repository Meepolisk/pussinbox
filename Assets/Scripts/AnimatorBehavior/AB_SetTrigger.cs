﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_SetTrigger : StateMachineBehaviour {
    public string TriggerName;
    public bool sendOnEnter = true, sendOnExit = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!string.IsNullOrEmpty(TriggerName))
        {
            if (sendOnEnter == true)
                animator.SetTrigger(TriggerName);
        }
    }
    
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!string.IsNullOrEmpty(TriggerName))
        {
            if (sendOnExit == true)
                animator.SetTrigger(TriggerName); 
        }
    }
    
}
