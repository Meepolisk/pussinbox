﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AbilityProfile : UI_Profile {
    public int slotRef;
    private Ability abiRef;
    protected override void awake()
    {
        base.awake();
        Ability[] abis = unitRef.GetComponentsInChildren<Ability>();
        foreach (var abi in abis)
        {
            if (abi.profile == null)
                continue;
            if (abi.profile.slot == slotRef)
            {
                refAbi(abi);
                return;
            }
        }
    }
    private void refAbi(Ability abi)
    {
        transform.Find("Sprites").gameObject.SetActive(true);
        abi.profile.profileRef = this;
        abiRef = abi;
        gameObject.findChild("Image").GetComponent<RawImage>().texture = applyAbiIcon(abi.profile.icon,
            (Texture2D)gameObject.findChild("Image").GetComponent<RawImage>().texture);
    }
    Texture2D applyAbiIcon(Texture2D sourceTexture, Texture2D destinationTexture)
    {
        Texture2D newtexture = new Texture2D(sourceTexture.width, sourceTexture.height, TextureFormat.ARGB32, false);

        Color[] sourcePixels = sourceTexture.GetPixels(0);
        Color[] destinationPixels = destinationTexture.GetPixels(0);
        
        int xEnd = newtexture.width;
        int yEnd = newtexture.height;
        for (int y = 0; y < yEnd; ++y)
        {
            for (int x = 0; x < xEnd; ++x)
            {
                int index = (y * xEnd + x);
                if (sourcePixels[index].a > 0.1f)
                    destinationPixels[index].a = (1f - sourcePixels[index].a);
            }
        }

        newtexture.SetPixels(destinationPixels, 0);
        newtexture.Apply();
        return newtexture;
    }

    private void LateUpdate () {
        if (abiRef != null)
        {
            //cooldown
            if (abiRef.cooldown != null)
            {
                anim.SetFloat("cooldownScale", abiRef.cooldown.UIscale);
                anim.SetBool("isActive", abiRef.cooldown.isActive);
            }
                
            //anim.SetBool("isChanneling", abiRef.channeling.isActive);
        }
    }

    //hàm từ class ability gọi qua
    public void cooldownFinish()
    {
        anim.SetTrigger("cooldownFinish");
    }
    public void abilityCasted()
    {
        anim.SetTrigger("casted");
    }
}
