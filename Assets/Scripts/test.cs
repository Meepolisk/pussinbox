﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityObject = UnityEngine.Object;
using Dirox.Localization;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

public class test : MonoBehaviour
{
    public Texture2D texture;
    public ColorAnalystConfig config;
    public List<ColorProfile> result;

    void Start()
    {
        double time = EditorApplication.timeSinceStartup;
        result = new ColorAnalyst(texture, config).Get();
        Debug.Log("Time: " + (EditorApplication.timeSinceStartup - time).ToString());
    }
    [System.Serializable]
    public class ColorAnalystConfig
    {
        [Range(0f, 0.5f)]
        public float blackThreehold = 0.4f;
        [Range(0f, 0.5f)]
        public float whiteThreehold = 0.2f;
        [Range(6, 36)]
        public int hSegsCount = 12;
        [Range(1, 10)]
        public int vSegsCount = 2;
        [Range(1, 10)]
        public int sSegsCount = 2;
    }
    public class ColorAnalyst
    {
        private struct HSVColor
        {
            public float h;
            public float s;
            public float v;

            public HSVColor(Color32 _color)
            {
                Color.RGBToHSV(_color, out h, out s, out v);
            }
        }
        ColorAnalystConfig config;
        Texture2D texture;

        float hSegmentValue  = 1f;
        float sSegmentValue = 1f;
        float vSegmentValue = 1f;
        uint whiteCounter = 0;
        uint blackCounter = 0;
        uint[,,] colorHueCounter;

        public ColorAnalyst (Texture2D _texture, ColorAnalystConfig _config)
        {
            texture = _texture;
            config = _config;
            hSegmentValue = 1f / config.hSegsCount;

            vSegmentValue = (1f - config.blackThreehold);
            if (config.vSegsCount > 1)
                vSegmentValue /= config.vSegsCount;

            sSegmentValue = (1f - config.whiteThreehold);
            if (config.sSegsCount > 1)
                sSegmentValue /= config.sSegsCount;
            colorHueCounter = new uint[config.hSegsCount, config.vSegsCount, config.sSegsCount];
        }

        public List<ColorProfile> Get()
        {
            Color[] colors = texture.GetPixels();
            foreach (var item in colors)
            {
                CompareColor(item);
            }

            List<ColorProfile> result = new List<ColorProfile>();
            if (blackCounter > 0)
                result.Add(new ColorProfile(Color.black, blackCounter));
            if (whiteCounter > 0)
                result.Add(new ColorProfile(Color.white, whiteCounter));
            for (int h = 0; h < config.hSegsCount; h++)
                for (int s = 0; s < config.sSegsCount;s++)
                    for (int v = 0; v< config.vSegsCount;v++)
                        if (colorHueCounter[h,s,v] > 0)
                        {
                            float hue = (hSegmentValue * h);
                            float sat = config.whiteThreehold + (sSegmentValue * (s + 1));
                            float val = config.blackThreehold + (vSegmentValue * (v + 1));
                            result.Add(new ColorProfile(hue, sat, val, colorHueCounter[h,s,v]));
                        }
            return result;
        }

        private void CompareColor(Color _inputColor)
        {
            HSVColor _C = new HSVColor(_inputColor);
            //HSVColor _C = new HSVColor();
            //Debug.Log("reading: H = " + _C.h + ", S = " + _C.s + ", V = " + _C.v);

            if (_C.v <= config.blackThreehold)
            {
                RecordBlack();
                return;
            }
            else if (_C.s <= config.whiteThreehold)
            {
                RecordWhite();
                return;
            }
            else
            {
                RecordColor(_C);
                return;
            }
        }

        private void RecordBlack()
        {
            blackCounter++;
            //Debug.Log("black++ : " + blackCounter);
        }

        private void RecordWhite()
        {
            whiteCounter++;
            //Debug.Log("white++ : " + whiteCounter);
        }

        private void RecordColor(HSVColor _C)
        {
            //Hue
            int hIndex = Mathf.RoundToInt(_C.h / hSegmentValue);
            if (hIndex >= config.hSegsCount - 1)
            {
                hIndex = 0;
            }
            //Sat
            int sIndex = 0;
            if (config.sSegsCount > 1)
            {
                sIndex = (int)((_C.s - config.whiteThreehold) / sSegmentValue);
                if (sIndex >= config.sSegsCount)
                    sIndex = config.sSegsCount - 1;
            }

            //value
            int vIndex = 0;
            if (config.vSegsCount > 1)
            {
                vIndex = (int)((_C.v - config.blackThreehold) / vSegmentValue);
                if (vIndex >= config.vSegsCount)
                    vIndex = config.vSegsCount - 1;
            }
            colorHueCounter[hIndex, sIndex, vIndex]++;
            //Debug.Log("Hue[" + hIndex + ", " + sIndex + ", " + vIndex + "]" + "++ : " + colorHueCounter[hIndex, sIndex, vIndex]);
        }
    }
    [System.Serializable]
    public struct ColorProfile
    {
        public Color color;
        public uint count;

        public ColorProfile(Color _color, uint _count)
        {
            color = _color;
            count = _count;
        }

        public ColorProfile(float _hue, float _sat, float _val, uint _count)
        {
            color = Color.HSVToRGB(_hue, _sat, _val);
            count = _count;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(test))]
    private class PlatformHandlerEditor : Editor
    {
        private test handler;

        private ReorderableList reorderList;

        private void OnEnable()
        {
            handler = target as test;
        }

        public override void OnInspectorGUI()
        {
            if (handler == null)
                return;

            DrawDefaultInspector();
            if (GUILayout.Button("TEST"))
            {
                handler.Start();
                return;
            }
        }
    }
#endif
}