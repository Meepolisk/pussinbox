﻿using UnityEngine;
using System.Collections;

public class HealthPack : MonoBehaviour
{
    public AudioClip pickupClip;
    [Range(0,300)]
	public float HitPointRestore = 0f;
    [Range(0, 300)]
    public float ArmorBonus = 0f;
    [Range(0, 300)]
    public float EnergyRestore = 0f;

    [ReadOnly]
    public HealthPackSpawner spawner;
    
    void OnTriggerEnter2D (Collider2D other)
	{
//		if(other.transform.root.gameObject.layer == LayerMask.NameToLayer("Player") && other.gameObject.isHero())
        if (true)
		{
            Pilot unit = other.gameObject.GetComponent<Pilot>();
            bool triggered = false;
            if (HitPointRestore > 0f && unit.hitPoint.remaining < unit.hitPoint.max)
            {
                unit.heal(HitPointRestore); triggered = true;
            }
            if (ArmorBonus > 0f && ArmorBonus > unit.hitPoint.bonusArmor)
            {
                unit.armorBonus(ArmorBonus); triggered = true;
            }
            if (EnergyRestore > 0f && unit.energy.current < unit.energy.max)
            {
                unit.restoreEnergy(EnergyRestore); triggered = true;
            }
            if (triggered)
            {
                Destroy(gameObject);
                if (pickupClip != null)
                    AudioSource.PlayClipAtPoint(pickupClip, transform.position);
                if (spawner != null)
                    spawner.ResetTimer();
            }
        }
	}
}
