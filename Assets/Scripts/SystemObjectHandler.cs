﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemObjectHandler : MonoBehaviour
#if UNITY_EDITOR
    ,hideTransform, lockTransform
#endif 
{

    private void Awake () {
        RTimerSystem();
        EnableMenu();
        Player.ReinstancePlayer();
    }
    private void Start()
    {
         StartCoroutine(EndOfFirstFrameAction());
    }
    IEnumerator EndOfFirstFrameAction()
    {
        yield return new WaitForEndOfFrame();
        //CameraZone.ChangeRegion(); //để null thì nhảy vào activeZone
        //todo
    }

void RTimerSystem()
    {
        RTimer.handler = gameObject.createEmptyChild("TimerHandler");
    }
    void EnableMenu()
    {
        IngameMenuManager menu = GetComponentInChildren<IngameMenuManager>(true);
        menu.gameObject.SetActive(true);
    }
}
