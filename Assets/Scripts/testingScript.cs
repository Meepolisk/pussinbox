﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testingScript : MonoBehaviour {
    
    public Unit unit;
    public Pilot hero;

    private void Start()
    {
        StartCoroutine(run());
    }
    IEnumerator run()
    {
        yield return new WaitForSeconds(3);
        unit.gameObject.SetActive(true);
        Vector2 newPoint = hero.center.withOffset(transform.position, 5f);
        unit.orders.Order(new OrderMoveToPoint(newPoint));
    }
}
