﻿using UnityEngine;
using System.Collections;

public class AI_FlyierPlasma : RAI
{
    public float xOffsetAttack0 = 3f;
    public float xOffsetAttack1 = 6f;
    public float yHeight = 9f;
    public float Spread = 0.5f;
    public Flyier_PlasmaShoot attackRef;
    public SimpleDash dashRef;

    private Vector2 offset
    {
        get
        {
            return new Vector2(0f, yHeight);
        }
    }

    protected override void onStageEnter(Stage _stage, Stage _oldStage)
    {
        if (info.stage == Stage.action)
        {
            unitRef.movement.auto.create(info.focusUnit, offset);
        }
        else
        {
            if (_oldStage == Stage.action)
                unitRef.orders.Order(new OrderMoveToPoint(firstPosition));
        }
    }

    protected override void stage_Action()
    {
        base.stage_Action();
        checkAndAttack();
        
    }
    
    private void checkAndAttack()
    {
        if (ConditionAttack())
        {
            attackRef.UseAbi();
            unitRef.orders.Order(new OrderFollowUnit(info.focusUnit, offset));
        }
        else if (ConditionDash())
        {
            dashRef.UseAbi();
            unitRef.orders.Order(new OrderFollowUnit(info.focusUnit, offset));
        }
    }
    private bool ConditionDash()
    {
        if (!dashRef.isAvailable) return false;
        return true;
    }

    private bool ConditionAttack()
    {
        if (!attackRef.isAvailable) return false;

        Vector2 pos = (Vector2)info.focus_lastPos;
        float xOffset1 = xOffsetAttack0, xOffset2 = xOffsetAttack1;
        if (pos.x > unitRef.center.x)
        {
            xOffset1 *= -1f;
            xOffset2 *= -1f;
        }
        if (unitRef.center.x.isInRangeOffset(pos.x + xOffset1, Spread))
        {
            attackRef.attackVariation = 0;
            return true;
        }
        if (unitRef.center.x.isInRangeOffset(pos.x + xOffset2, Spread))
        {
            attackRef.attackVariation = 1;
            return true;
        }
        return false;
    }
}

