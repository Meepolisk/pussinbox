﻿using UnityEngine;
using System.Collections;

public class AI_AirDrone2 : RAI
{
    [ReadOnly]
    public AirDrone_Bombing attack;
    float offsetFromGround = 7f;

    protected override void Start()
    {
        base.Start();
        attack = unitRef.GetComponentInChildren<AirDrone_Bombing>();
    }

    Vector2 offset
    {
        get
        {
            return new Vector2(0f, offsetFromGround);
        }
    }

    protected override void onStageEnter(Stage _stage,Stage _oldStage)
    {
        if (info.stage == Stage.action)
        {
            unitRef.movement.auto.create(info.focusUnit, offset);
        }
        else if(info.stage == Stage.free)
        {
            if (_oldStage == Stage.action)
                unitRef.orders.Order(new OrderMoveToPoint(firstPosition));
        }
    }
    protected override void stage_Action()
    {
        base.stage_Action();
        if (ConditionAttack())
        {
            attack.UseAbi();
        }
    }
    const float SaiSo = 1.5f;
    private bool ConditionAttack()
    {
        //if (attack.groupIsActive) return false;

        Vector2 target = (Vector2)info.focus_lastPos;
        float targetX = target.x;
        float posX = unitRef.center.x;
        if (posX.isInRangeOffset(targetX,SaiSo))
        {
            return true;
        }
        return false;
    }
}

