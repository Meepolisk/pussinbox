﻿using UnityEngine;
using System.Collections;

public class AI_FlyierDrill : RAI
{

    protected override void onStageEnter(Stage _stage, Stage _oldStage)
    {
        if (info.stage != Stage.action && _oldStage == Stage.action)
        {
            unitRef.orders.Order(new OrderMoveToPoint(firstPosition));
        }
    }

    protected override void stage_Action()
    {
        base.stage_Action();
        follow();
        
    }
    void follow()
    {
        //unitRef.orders.Order(new OrderFollowUnit(info.focusUnit));
        unitRef.movement.auto.create(info.focusUnit);
    }
}

