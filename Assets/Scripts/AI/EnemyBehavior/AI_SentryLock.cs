﻿using UnityEngine;
using System.Collections;

public class AI_SentryLock : RAI
{
    [ReadOnly]
    [SerializeField]
    private SentryAttack abiRef;

    public string activeAnimatorName = "isActive";
    [Range(0.2f,2f)]
    public float attackDelay = 1f;

    private bool canAttack = false;

    protected override void Start()
    {
        base.Start();
        abiRef = unitRef.GetComponentInChildren<SentryAttack>();
    }

    protected override void onStageEnter(Stage _stage, Stage _oldStage)
    {
        if (info.stage == Stage.action)
        {
            unitRef.model.animation.SetBool(activeAnimatorName, true);
            if (canAttack == false)
            {
                StartCoroutine("setAttackBool");
            }
        }
        else
        {
            if (canAttack == false)
            {
                StopCoroutine("setAttackBool");
            }
            else
            {
                canAttack = false;
            }
            //abiRef.gunController.targetValue = 0;
            //unit.model.animation.SetBool(activeAnimatorName, false);
        }
    }
    IEnumerator setAttackBool()
    {
        yield return new WaitForSeconds(attackDelay);
        if (info.stage == Stage.action)
        {
            canAttack = true;
        }
    }
    protected override void stage_Action()
    {
        base.stage_Action();

        Vector2 enemyPos = (Vector2)info.focus_lastPos;
        Vector2 pos = unitRef.center;
        float angle = RPlatformer.AngleOf2Point(pos, enemyPos);
        if (unitRef.model.sprites.isRightFacing == false)
            angle = angle.revertZangle();
        abiRef.gunController.targetValue = angle;
        if (ConditionAttack(pos, enemyPos))
        {
            abiRef.UseAbi();
        }
    }

    private bool ConditionAttack (Vector2 pos, Vector2 enemyPos)
    {
        //check cooldown;
        if (!abiRef.isAvailable) return false;
        if (!canAttack) return false;

        float range = Vector2.Distance(enemyPos, pos);
        if (range <= 20f)
        {
            return true;
        }
        return false;
    }
}

