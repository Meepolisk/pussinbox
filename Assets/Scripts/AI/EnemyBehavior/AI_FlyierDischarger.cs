﻿using UnityEngine;
using System.Collections;

public class AI_FlyierDischarger : RAI
{
    [ReadOnly]
    public Flyier_EletricDischarge attack;

    protected override void Start()
    {
        base.Start();
        attack = unitRef.GetComponentInChildren<Flyier_EletricDischarge>();
    }

    protected override void onStageEnter(Stage _stage, Stage _oldStage)
    {
        if (info.stage != Stage.action && _oldStage == Stage.action)
        {
            unitRef.orders.Order(new OrderMoveToPoint(firstPosition));
        }
    }

    protected override void stage_Action()
    {
        base.stage_Action();
        follow();
        checkAndCast();
    }
    void follow()
    {
        //unitRef.orders.Order(new OrderFollowUnit(info.focusUnit));
        unitRef.movement.auto.create(info.focusUnit);
    }
    void checkAndCast()
    {
        if (!attack.isAvailable) return;

        Vector2 pos = unitRef.center;
        Vector2 target = info.focusUnit.center;

        if (Vector2.Distance(pos, target) < 4f)
        {
            unitRef.movement.auto.deactivate();
            attack.UseAbi();
        }
    }
    
}

