﻿using UnityEngine;
using System.Collections;

public class AI_Baneling1 : RAI
{
    [ReadOnly]
    public Baneling1_Explo attack;
    [ReadOnly]
    public Baneling1_Boost boost;

    protected override void Start()
    {
        base.Start();
        attack = unitRef.GetComponentInChildren<Baneling1_Explo>();
        boost = unitRef.GetComponentInChildren<Baneling1_Boost>();
    }

    #region onStage
    protected override void onStageEnter(Stage _stage, Stage _oldStage)
    {
        if (info.stage == Stage.action)
        {
            boost.UseAbi();
            attack.UseAbi();
            unitRef.movement.auto.create(info.focusUnit);
        }
    }
    #endregion
}

