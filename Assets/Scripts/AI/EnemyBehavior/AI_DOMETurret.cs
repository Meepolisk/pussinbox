﻿using UnityEngine;
using System.Collections;

public class AI_DOMETurret : RAI
{
    [ReadOnly]
    [SerializeField]
    private SentryAttack abiRef;

    [Range(0.2f,2f)]
    public float attackDelay = 1f;

    protected override void Start()
    {
        base.Start();
        abiRef = unitRef.GetComponentInChildren<SentryAttack>();
    }
    
    protected override void stage_Action()
    {
        base.stage_Action();
        
        if (abiRef.isAvailable)
        {
            abiRef.UseAbi();
        }
    }
}

