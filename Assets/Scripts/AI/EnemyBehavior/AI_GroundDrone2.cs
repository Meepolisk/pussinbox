﻿using UnityEngine;
using System.Collections;

public class AI_GroundDrone2 : RAI
{
    [SerializeField]
    [ReadOnlyWhenPlaying]
    private TurretLoadMissileAttack abiRef;
    public float rangeToAttack = 15f;
    public float rangeToKeepMin = 8f;
    public float rangeToKeepMax = 12f;

    protected override void Start()
    {
        base.Start();
        abiRef = unitRef.GetComponentInChildren<TurretLoadMissileAttack>();
    }

    protected override void onStageEnter(Stage _stage, Stage _oldStage)
    {
        if (info.stage == Stage.action)
        {
            StartCoroutine(actionCoroutine());
        }
        else if (info.stage == Stage.free)
        {
            if (_oldStage == Stage.action)
            {
                unitRef.movement.auto.deactivate();
                unitRef.orders.Order(new OrderMoveToPoint(firstPosition));
            }
        }
        
    }
    IEnumerator actionCoroutine()
    {
        yield return new WaitForSeconds(Random.Range(0.5f, 1f));
        if (info.stage == Stage.action)
        {
            Vector2 enemyPos = (Vector2)info.focus_lastPos;
            Vector2 pos = unitRef.center;
            float angle = RPlatformer.AngleOf2Point(pos, enemyPos);
            if (unitRef.model.sprites.isRightFacing == false)
                angle = angle.revertZangle();
            abiRef.gunController.targetValue = angle;
            if (ConditionAttack(pos, enemyPos))
            {
                //gunController.targetValue = RPlatformer.
                unitRef.movement.actionFaceToUnit(info.focusUnit);
                abiRef.UseAbi();
            }
            Moving(pos, enemyPos);

            StartCoroutine(actionCoroutine());
        }
    }


    private void Moving(Vector2 pos, Vector2 enemyPos)
    {
        float range = Vector2.Distance(pos, enemyPos);
        float OffsetXgoFar = (pos.x > enemyPos.x) ? 5f : -5f;
        if (range < rangeToKeepMin)
        {
            unitRef.movement.auto.create(unitRef, new Vector2(OffsetXgoFar, 0f));
        }
        else if (range > rangeToKeepMax)
            unitRef.movement.auto.create(unitRef, new Vector2(-OffsetXgoFar, 0f));
        else
        {
            unitRef.movement.auto.deactivate();
        }
    }
    
    private bool ConditionAttack (Vector2 pos, Vector2 enemyPos)
    {
        //check cooldown;
        if (!abiRef.isAvailable) return false;

        float range = Vector2.Distance(enemyPos, pos);
        if (range <= rangeToAttack)
        {
            return true;
        }
        return false;
    }
}

