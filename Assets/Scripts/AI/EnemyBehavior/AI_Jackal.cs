﻿using UnityEngine;
using System.Collections;
using System;

public class AI_Jackal : RAI
{
    [Header("=====SPECIAL CASE AWAKE BEHAVIOR======")]
    public bool ropeIn = false;
    public bool canSurprise = true;
    
    [Header("=====ACTION BEHAVIOR======")]
    public float rangeToStopFollow = 5f;
    public Attack attack;
    public SprintAbi sprint;
    
    private bool engaged = false;

    [System.Serializable]
    public class Attack
    {
        [ReadOnly]
        public ToggleAttack abiRef;
        public float rangeToAttack = 15f;
    }
    [System.Serializable]
    public class SprintAbi
    {
        [ReadOnly]
        public Sprint abiRef;
        public float rangeToRun = 5f;
        public float heightToRun = 2f;
    }
    public override void FinishSetup()
    {
        if (info.focusUnit != null)
            engaged = true;

        base.FinishSetup();

        attack.abiRef = unitRef.GetComponentInChildren<ToggleAttack>();
        sprint.abiRef = unitRef.GetComponentInChildren<Sprint>();

        //awake
        if (ropeIn)
        {
            unitRef.model.animation.SetTrigger("Rope");
        }
    }

    #region onStage
    protected override void onStageEnter(Stage _stage, Stage _oldStage)
    {
        if (info.stage == Stage.action)
        {
            unitRef.orders.Clear();
            if (!engaged)
            {
                if (canSurprise)
                    unitRef.model.animation.SetTrigger("Detect");
                engaged = true;
                StartCoroutine(actionCoroutineCheckAttack());
            }
            else
            {
                StartCoroutine(actionCoroutineWait());
            }
        }
    }
    IEnumerator actionCoroutineWait()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f, 1f));
        if (info.stage == Stage.action)
        {
            unitRef.movement.auto.deactivate();

            StartCoroutine(actionCoroutineCheckAttack());
            //Debug.Log("Action, tắt di chuyển");
        }
    }

    IEnumerator actionCoroutineCheckAttack()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.3f));
        if (info.stage == Stage.action)
        {
            Vector2 target = (Vector2)info.focus_lastPos;
            Vector2 pos = unitRef.center;
            float range = Vector2.Distance(pos, target);
            float rangeX = Mathf.Abs(pos.x - target.x);
            float rangeY = Mathf.Abs(pos.y - target.y);
            float angle = RPlatformer.AngleOf2Point(pos, target);
            if (unitRef.model.sprites.isRightFacing == false)
                angle = angle.revertZangle();

            if (RunCondition(rangeX, rangeY))
            {
                if (target.x > pos.x)
                    unitRef.movement.auto.moveLeft();
                else
                    unitRef.movement.auto.moveRight();
                sprint.abiRef.UseAbi();
                //Debug.Log("Action: Bỏ chạy");
            }
            else
            {
                unitRef.movement.actionFaceToUnit(info.focusUnit);
                if (AttackCondition(range, rangeX, rangeY, angle))
                {
                    attack.abiRef.UseAbi();
                    //Debug.Log("Action: Bắn");
                }
                else if (unitRef.canControl && (rangeX > rangeToStopFollow || rangeY > 2f))
                {
                    unitRef.orders.Order(new OrderMoveToPoint((Vector2)info.focus_lastPos));
                    //Debug.Log("Action: đi theo");
                }
            }
            StartCoroutine(actionCoroutineWait());
        }   
    }
    private bool AttackCondition(float range, float rangeX, float rangeY, float angle)
    {
        if (attack.abiRef == null)
            return false;
        if (!attack.abiRef.isAvailable)
        {
            //Debug.Log("không attack vì chưa avai");
            return false;
        }
        if (range > attack.rangeToAttack)
        {
            //Debug.Log("không attack vì xa quá: " + range);
            return false;
        }
        if (angle.isInRange(-15,15) || (rangeX < 3f && rangeY <1f))
        {
            attack.abiRef.animatorAttackVariationParametter = 0;
            return true;
        }
        if (angle.isInRange(35, 55))
        {
            attack.abiRef.animatorAttackVariationParametter = 1;
            return true;
        }
        if (angle > 65)
        {
            attack.abiRef.animatorAttackVariationParametter = 2;
            return true;
        }
        return true;
    }
    private bool RunCondition(float rangeX, float rangeY)
    {
        if (sprint.abiRef == null)
            return false;
        if (!sprint.abiRef.isAvailable)
            return false;
        if (rangeX > sprint.rangeToRun)
            return false;
        if (rangeY > sprint.heightToRun)
            return false;
        return true;
    }

    #endregion
}

