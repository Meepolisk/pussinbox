﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;

public class ROrderHandler : UnitAddOn
{
    private RAI aiRef;
    public ROrder currentOrder = null;
    //public Queue<ROrder> rOrdersQueue = new Queue<ROrder>();
    [ReadOnly]
    [SerializeField]
    private List<ROrder> rOrdersQueue = new List<ROrder>();

    public bool active = false;
    
    protected override void Awake()
    {
        aiRef = GetComponentInChildren<RAI>();
        base.Awake();
    }
    
    void Update()
    {
        if (currentOrder != null)
        {
            currentOrder.Update();
        }
    }

    public void Clear()
    {
        //if (currentOrder != null)
        if (!currentOrder.isNull())
        {
            currentOrder.baseFinish();
            currentOrder = null;
        }
        rOrdersQueue.Clear();
    }
    private bool Condition()
    {
        if (unitRef.isDead)
            return false;
        return true;
    }
    public void QueueOrder(ROrder _order)
    {
        if (!Condition()) return;

        if (!currentOrder.isNull() )
        {
            rOrdersQueue.Add(_order);
        }
        else
        {
            Order(_order);
        }
    }
    public void Order(ROrder _order)
    {
        if (!Condition()) return;
        
        rOrdersQueue.Clear();
        if (!currentOrder.isNull())
        {
            currentOrder.baseFinish();
        }
        currentOrder = _order;
        StartCoroutine(PlayOrders());
    }
    public void NextOrder()
    {
        if (rOrdersQueue.Count > 0) //queue có tồn tại
        {
            //currentOrder = rOrdersQueue.Dequeue();
            currentOrder = rOrdersQueue[0];
            rOrdersQueue.RemoveAt(0);
            StartCoroutine(PlayOrders());
        }
        else
        {
            ROrder _lastOrder = currentOrder;
            currentOrder = null;
            FinishedAllOrder(_lastOrder);
        }
    }
    IEnumerator PlayOrders()
    {
        //yield return new WaitForEndOfFrame();
        yield return null;
        if (currentOrder != null)
        {
            currentOrder.prepare(this);
            if (currentOrder.baseCondition())
            {
                currentOrder.Start();
            }
        }
    }

    public void FinishAnOrder(ROrder _orderFinished)
    {
        if (_orderFinished == currentOrder)
        {
            NextOrder();
        }
    }
    public void FinishedAllOrder(ROrder _lastOrder)
    {
        if (aiRef != null)
            aiRef.FinishAllOrder(_lastOrder);
    }
    public bool isEmpty
    {
        get
        {
            return (currentOrder == null);
        }
    }

    #region UnitAddOn function
    public override void Unit_Die()
    {
        Clear();
    }
#endregion
}
