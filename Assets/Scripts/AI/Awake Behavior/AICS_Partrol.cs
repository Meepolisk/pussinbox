﻿using UnityEngine;
using System.Collections;

public class AICS_Partrol : AI_CustomStage
{
    public RFlag patrolFlagA;
    public RFlag patrolFlagB;
    public float offTime = 2f;

    private ROrder patrolOrderA, patrolOrderB;

    private void PatrolSetup()
    {
        if (patrolFlagA != null && patrolFlagB != null)
        {
            unitRef.orders.Clear();

            patrolOrderA = new OrderMoveToPoint(patrolFlagA.center);
            patrolOrderB = new OrderMoveToPoint(patrolFlagB.center);
            unitRef.orders.Order(patrolOrderA);
            //Debug.Log("Order di chuyển pointA");
        }
    }
    public override void FinishAllOrder(ROrder _lastOrder)
    {
        if (aiRef.info.stage == RAI.Stage.free)
        {
            if (_lastOrder == patrolOrderA)
                StartCoroutine(patrolOrderStart(patrolOrderB));
            else if (_lastOrder == patrolOrderB)
                StartCoroutine(patrolOrderStart(patrolOrderA));
        }
    }
    IEnumerator patrolOrderStart(ROrder _order)
    {
        yield return new WaitForSeconds(offTime);
        unitRef.orders.Order(_order);
        //Debug.Log("Order di chuyển tiếp");
    }
    public override void StageEnter()
    {
        base.StageEnter();
        PatrolSetup();
    }
    public override void StageExit()
    {

    }
}
