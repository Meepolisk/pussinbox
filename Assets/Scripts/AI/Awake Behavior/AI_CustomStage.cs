﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(RAI))]

public abstract class AI_CustomStage : UnitAddOn
{
    [ReadOnly]
    public bool isActive = false;
    [HideInInspector]
    public RAI aiRef;
    public RAI.Stage stage = RAI.Stage.free;

    protected override void Awake()
    {
        base.Awake();
        aiRef = GetComponent<RAI>();
    }

    public virtual bool Condition()
    {
        return true;
    }

    public virtual void StageEnter()
    {
        isActive = true;
    }
    public virtual void StageExit()
    {
        isActive = false;
    }

    public virtual void FinishAllOrder(ROrder _lastOrder)
    { 

    }
}
