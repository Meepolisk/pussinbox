﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class OrderFollowUnit  : ROrder
{
    public Unit TargetUnit;
    public Vector2 offset;
    public bool StopWhenGetClosed;
    public float Range = 0.2f;
    
    public OrderFollowUnit (Unit _unit, Vector2? _offset = null)
    {
        name = "follow unit " + _unit.name;
        if (_offset == null)
            offset = Vector2.zero;
        else
            offset = (Vector2)_offset;
        TargetUnit = _unit;
    }
    public override void Start()
    {
        base.Start();
        unit.movement.auto.create(TargetUnit, offset , this);

    }
    protected override void Finish()
    {
        base.Finish();
        if (unit != null)
            unit.movement.auto.deactivate();
    }
}
