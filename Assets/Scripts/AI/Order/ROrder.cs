﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ROrder
{
    public string name;
    public ROrderHandler handler;
    protected Unit unit;
    public bool isActive = false;

    public void prepare(ROrderHandler _p_handler)
    {
        handler = _p_handler;
        unit = handler.unitRef;
    }

    public bool baseCondition()
    {
        if (handler.unitRef.isDead)
            return false;
        return Condition();
    }

    public virtual void Start()
    {
        if (baseCondition())
        {
            isActive = true;
        }
        else
        {
            baseFinish();
        }
    }

    public virtual void Update()
    {

    }

    protected virtual void Finish()

    {
    }
    /// <summary>
    /// Khi override nhớ bỏ cái đầu
    /// </summary>
    protected virtual bool Condition()
    {
        return true;
    }
    public void baseFinish()
    {
        if (isActive)
        {
            Finish();
            handler.FinishAnOrder(this);
        }
    }
}
