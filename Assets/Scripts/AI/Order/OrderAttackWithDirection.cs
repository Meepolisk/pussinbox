﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class OrderAttackWithDirection : ROrder
{
    public Vector2 AI_input = Vector2.zero;

    public OrderAttackWithDirection(Vector2 _direction)
    {
        name = "attack with direction";
        AI_input = _direction;

        //Debug.Log("khởi tạo: " + Destination.ToString());
    }

    public override void Start()
    {
        base.Start();
        
        unit.movement.setInputDirection(AI_input.x, AI_input.y);

        Ability.ForceUnitUseAbi(unit, "Attack");
    }
    protected override void Finish()
    {
        AI_input = Vector2.zero;
        unit.movement.setInputDirection(AI_input.x, AI_input.y);
        base.Finish();
    }
}
