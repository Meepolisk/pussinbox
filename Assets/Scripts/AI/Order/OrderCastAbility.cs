﻿using UnityEngine;
using System.Collections;
 
[System.Serializable]
public class OrderCastAbility : ROrder
{
    private string abilityName = "";
    private Ability abi = null;
    /// <summary>
    /// cast ability cho Unit
    /// </summary>
    /// <param name="abilityName">tên của GameObject chứa code Ability đó</param>
    public OrderCastAbility(string _abilityName)
    {
        name = "cast ability: " + _abilityName;
        abilityName = _abilityName;
    }
    public OrderCastAbility(Ability _abi)
    {
        name = "cast ability: " + _abi.name;
        abi = _abi;
    }

    public override void Start()
    {
        base.Start();

        if (abi != null)
            abi.UseAbi();
        else
            Ability.ForceUnitUseAbi(unit, abilityName);
    }
}
