﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class OrderMoveToPoint : ROrder
{
    public Vector2 Destination;
    public Vector2 AI_input = Vector2.zero;

    public OrderMoveToPoint(Vector2 _destination)
    {
        name = "move to " + _destination.ToString();
        Destination = _destination;
    }

    public override void Start()
    {
        base.Start();

        unit.movement.auto.create(Destination, this);
        
    }

    protected override void Finish()
    {
        base.Finish();
        if (unit != null)
            unit.movement.auto.deactivate();
    }
}
