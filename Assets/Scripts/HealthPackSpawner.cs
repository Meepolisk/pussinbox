﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPackSpawner : MonoBehaviour {

    public HealthPack[] healthPacks;
    public float respawnTime;
    private RTimer timer;
    
	void Start () {
        timer = gameObject.AddComponent<RTimer>();
        timer.setData(respawnTime, false, CreateHealthPack);
        CreateHealthPack();
	}

    void CreateHealthPack()
    {
        GameObject hp = Instantiate(healthPacks[Random.Range(0, healthPacks.Length)].gameObject, gameObject.transform) ;
        //hp.transform.parent = gameObject.transform;
        hp.transform.localPosition = Vector2.zero;
        hp.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 500f);
        hp.GetComponent<HealthPack>().spawner = this;
    }

    public void ResetTimer()
    {
        timer.start();
    }
}
