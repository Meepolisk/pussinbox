﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Profile : MonoBehaviour {
    private Unit _unitRef;
    public Unit unitRef
    {
        get
        {
            return _unitRef;
        }
        set
        {
            _unitRef = value;
            awake();
        }
    }
    protected virtual void awake()
    {

    }
    protected Animator anim;
    private void Awake()
    {
        //tạm thời là thế
        anim = GetComponentInChildren<Animator>(true);
    }
}
