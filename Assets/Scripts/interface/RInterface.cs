﻿using System;
using UnityEngine;

public interface iUnitTakeDamage
{
    void UnitTakeDamage(Unit dmgSource, Vector2 dmgPos);
}

public interface iRegHotkey
{
    void registHotKey(AbilityButton abiButton);
    void unregistHotKey(AbilityButton abiButton);
}

#if UNITY_EDITOR
public interface hideTransform { }
public interface hideSprite { }
public interface lockTransform { }
public interface editorUpdate
{
    void EditorUpdate();
}
public interface editorHierachyChange
{
    void editorHierachyChange();
}
#endif