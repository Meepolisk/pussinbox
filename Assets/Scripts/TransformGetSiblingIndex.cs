﻿using UnityEngine;

public class TransformGetSiblingIndex : MonoBehaviour
{
    [ReadOnly]
    [SerializeField]
    private int _depthLevel = 0;
    public int DepthLevel
    {
        get
        {
            return _depthLevel;
        }
    }
    public void GetParentIndex()
    {
        _depthLevel = 0;
        Transform checkingTransform = transform.parent;
        while (checkingTransform != null)
        {
            _depthLevel++;
            checkingTransform = checkingTransform.parent;
        }
    }

    private void Start()
    {
        GetParentIndex();
    }
}