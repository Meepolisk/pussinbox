﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CameraSystem
{
	//[System.Serializable]
	//public class BackgroundLayerGroup
	//{
	//	[SerializeField]
	//	private Transform _transform;
	//	public Transform transform => _transform;

	//	public float offX = 0f, offY = 0f;
	//}

	public class BackgroundParallax : MonoBehaviour
	{
		[SerializeField]
		private Vector2 cameraAnchorPos = Vector2.zero;
		[SerializeField]
		private BGParallaxLayer[] layers = null;
		[SerializeField]
		private RCamera2D _camera;
		
		public void Paralax()
		{
			if (layers.Length == 0) return;

			Vector2 localPos = transform.localPosition;
			for (int i = 0; i < layers.Length; i++)
			{
				Vector2 bgPos = layers[i].transform.localPosition;
				bgPos.x = localPos.x * layers[i].offX;
				bgPos.y = localPos.y * layers[i].offY;

				layers[i].transform.localPosition = bgPos;
			}
		}

#if UNITY_EDITOR
		[CustomEditor(typeof(BackgroundParallax))]
		private class Inspector : Editor
        {

        }
#endif
    }
}
