﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RSystemPosLocker : MonoBehaviour
{
    void Awake()
    {
        reTransform();
    }

    // Update is called once per frame
    void Update()
    {
        reTransform();
    }

void reTransform()
    {
        gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
        gameObject.transform.eulerAngles = new Vector3(0f, 0f, 0f);
        Vector2 tr = gameObject.transform.localScale;
        gameObject.transform.localScale = new Vector3((int)tr.x, (int)tr.y, 1f);
        this.enabled = true;
    }
}
#endif
