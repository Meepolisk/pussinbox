﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

//public class RevertPrefabInstance : MonoBehaviour
//{
//    [MenuItem("RTools/Revert to Prefab")]
//    static void Revert()
//    {
//        var selection = Selection.gameObjects;

//        if (selection.Length > 0)
//        {
//            for (var i = 0; i < selection.Length; i++)
//            {
//                PrefabUtility.RevertPrefabInstance(selection[i]);
//            }
//        }
//        else
//        {
//            Debug.Log("Cannot revert to prefab - nothing selected");
//        }
//    }
//}
public class RecleanGameObjects : MonoBehaviour
{
    [MenuItem("RTools/Reclean GameObjects")]
    static void Reclean()
    {
        var selection = Selection.gameObjects;

        if (selection.Length > 0)
        {
            foreach (var s in selection)
            {
                SpriteHelper[] sHP = s.GetComponentsInChildren<SpriteHelper>();
                foreach (var hp in sHP)
                {
                    Debug.Log("DELETED <SpriteHelper> in " + s.name + "/" + hp.gameObject.name);
                    DestroyImmediate(hp);
                }

                SpriteRenderer[] srds = s.GetComponentsInChildren<SpriteRenderer>();
                foreach (var sr in srds)
                {
                    Debug.Log("Restore Default <SpriteHelper> in " + s.name + "/" + sr.gameObject.name);
                    sr.material = new Material(Shader.Find("Sprites/Default"));
                }
            }
        }
        else
        {
            Debug.Log("Cannot reclean - nothing selected");
        }
    }
}
public class ResetAllTransformHideFlag : MonoBehaviour
{

    [MenuItem("RTools/Reset HideFlag")]
    static void resetToDefault()
    {
        Component[] all = GameObject.FindObjectsOfType<Component>();
        foreach (var tran in all)
        {
            tran.hideFlags = HideFlags.None;
        }
    }
}
public class CreateRFlag : MonoBehaviour
{

    [MenuItem("GameObject/RObject/Empty MovementFlag", false, 0)]
    static void createFlag()
    {
        RFlag.Create();
    }
}
#endif